<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// DATATABLE
$route['datatable/monitoring/issue-actionplan'] = 'datatable/monitoring_issueap';
$route['datatable/monitoring/bast-project'] 	= 'datatable/monitoring_bastproject';
$route['datatable/master/customer-executive'] 	= 'datatable/customer_executive';
$route['datatable/dashboard-executive/project'] = 'datatable/projectDashboardExecutive';

// DASHBOARD
$route['dashboard-info'] 						= 'dashboard/dashboard_info';
$route['dashboard-executive'] 					= 'dashboard/dashboardExecutive';
$route['dashboard-executive-info'] 				= 'dashboard/dashboardExecutiveInfo';
$route['dashboard/chart-project-scale/(:any)']	= 'dashboard/chartProjectScale/$1';
$route['dashboard/status-project/(:any)']		= 'dashboard/status_project/$1';


// REPORT
$route['download-list-project/(:any)'] 	= 'report/listProject/$1';
$route['download-list-bast'] 			= 'report/listBast';

// BAST
$route['sample-bast'] 					= 'bast/sampleGenerated';
$route['export-bast/(:any)'] 			= 'bast/exportPdf/$1';

// PROJECT
$route['p/(:any)']						= 'project/viewProject/$1';
$route['project/d/(:any)']				= 'project/show/$1';
$route['project-data/(:any)']			= 'project/data/$1';
$route['project-data']					= 'project/data';
$route['project-add']['get']			= 'project/addProject';
$route['project-add']['post']			= 'project/addProjectProcess';
$route['project-delete']				= 'project/deleteProject';
$route['project-edit/(:any)']['get']	= 'project/editProject/$1';
$route['project-edit']['post']			= 'project/editProjectProcess';
$route['project-add-access']['post']	= 'project/addProjectAccess';
$route['project-delete-access']['post']	= 'project/deleteProjectAccess';
$route['project-description/(:any)']	= 'project/getProjectDescription/$1';

// MASTER
$route['master/customer-executive/(:any)']	= 'master/customerExecutive/$1';
$route['master/get-admin']					= 'master/getAdmin';
$route['master/get-user']					= 'master/get_user';
$route['configuration']						= 'master/configuration';
$route['configuration-email']['post']		= 'master/saveConfigurationEmail';
$route['log']								= 'master/indexLog';

// SETTINGS
$route['settings/role-access']["get"]		= 'user/RoleAccess';
$route['settings/role-access']['post']		= 'user/UpdateRoleAccess';

// VALIDATION
$route['v/check-user-id']					= 'landing/check_userId';
$route['v/check-user-email']				= 'landing/check_userEmail';

// PUBLIC PAGE
$route['user-activation/(:any)']			= 'landing/activationUser/$1';
$route['forget-password']					= 'landing/forgetPassword';
$route['forget-password-proccess'] 			= 'landing/forgetPasswordProcess';
$route['user-reset-password/(:any)']		= 'landing/resetPassword/$1';
$route['user-reset-password-proccess'] 		= 'landing/resetPasswordProcess';

// USER
$route['user/change-password'] 				= 'user/changePassword';
$route['user/change-password-process'] 		= 'user/changePasswordProcess';
$route['user/check-password'] 				= 'user/checkPassword';
$route['user/activate-proccess']			= 'landing/activationProcess';


// DATA
$route['data-account-manager']				= 'data/accountManager'; 
$route['data-account-manager-segmen']		= 'data/accountManagerSegmen'; 


// UTILITY
$route['utility/uploader']					= 'utility/uploader';
$route['utility/upload-project']			= 'utility/uploadScreening';
$route['utility/upload-project-process']	= 'utility/uploadProcess';
$route['utility/des']						= 'utility/getDesProject';
$route['utility/des-update']				= 'utility/updateDesProject';
$route['utility/des-update-candidate']		= 'utility/updateDesProjectCandidate';
$route['utility/des-deliverable']			= 'utility/updateDesDeliverable';
$route['utility/des-deliverable-update'] 	= 'utility/updateDesDeliverable2';
$route['utility/des-issue'] 				= 'utility/updateDesIssue';
$route['utility/des-action-plan']			= 'utility/updateDesActionPlan';
$route['utility/des-symptom']				= 'utility/updateSymptom';
$route['utility/update-progress']			= 'utility/update_progress';
$route['utility/des-master']				= 'utility/syncMasterDes';
$route['utility/des-document']				= 'utility/syncDocument';
$route['utility/des-partner']				= 'utility/getDesProjectPartner';