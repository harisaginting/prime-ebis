<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Bast extends MY_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('M_Bast','main_model');    
        if(!$this->isLoggedIn()){
            redirect(base_url()); 
        };  
    } 

    public function index(){
        if (!$this->check_access("BAST","R")) {
            redirect(base_url());
        }
        $this->adminView("bast/index");         
    }

    public function add($spk,$top = null){
        if (!$this->check_access("PROJECT","C")) {
            redirect(base_url());
        }
        $spk = urldecode($spk);
        $spk = str_replace('^', '/', $spk);

        if(!empty($top)){
            $top = urldecode($top);
            $top = str_replace('^', '/', $top);
        }

        // echo $spk.'<br>'.$top;die;

    	$data 					= $this->main_model->get_spk($spk,$top);
    	// echo json_encode($data);die;

        $data['type_project']   = $this->getConfig('TYPE_PROJECT');
        $data['regional']       = $this->getConfig('REGIONAL');
        // echo json_encode($data);die;
        $this->adminView("bast/add",$data);         
    }

    function add_bast_process(){
        // echo json_encode($this->input->post());die;
        $data['ID']                 = $this->getGUID();
        $data['ID_PROJECT']         = $this->input->post('id_project');
        $data['CUSTOMER']           = $this->input->post('bast-customer');
        $data['PARTNER']            = $this->input->post('bast-partner');
        $data['SPK']                = $this->input->post('bast-spk');
        $data['SPK_DATE']           = $this->input->post('bast-spk-date');
        $data['KL']                 = $this->input->post('bast-kl');
        $data['KL_DATE']            = $this->input->post('bast-kl-date');
        $data['VALUE']              = intval($this->input->post('bast-value'));
        $data['BAST_DATE']          = $this->input->post('bast-date');
        $data['TYPE']               = $this->input->post('bast-type');
        $data['TERMIN']             = $this->input->post('bast-termin');
        $data['PROGRESS']           = $this->input->post('bast-progress');
        $data['PIC']                = $this->input->post('bast-pic');
        $data['PIC_EMAIL']          = $this->input->post('bast-pic-email');
        $data['DIVISION']           = $this->input->post('bast-division');
        $data['BAUT']               = !empty($this->input->post('baut')) ? '1' : '0';
        $data['STATUS']             = 'RECEIVED';

        // echo json_encode($this->input->post());die;
            // add evidence
            $input_evidence = $this->input->post('bast-evidence');
            foreach ($input_evidence as $key => $value) {
                $evidence[$key]['ID']                 = $this->getGUID();
                $evidence[$key]['ID_BAST']            = $data['ID'];   
                $evidence[$key]['VALUE']              = $value;   
            }

            // add approver
            $input_signer  = $this->input->post('signer');
            foreach ($input_signer as $key => $value) {
                $signer[$key]['ID']                 = $this->getGUID();
                $signer[$key]['ID_BAST']            = $data['ID'];   
                $signer[$key]['ID_SIGNER']          = $value;      
                $signer[$key]['STEP']               = $key;      
            }

        $history['ID']      = $this->getGUID();
        $history['ID_BAST'] = $data['ID'];
        $history['STATUS']  = $data['STATUS'];
        $history['COMMEND'] = $this->input->post('bast-note');
        $history['USER_NAME']  = $this->session->userdata('name');
        $history['USER_ID']    = $this->session->userdata('userid');
        $this->loggerPrime('BAST', "ADD BAST" , $data);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan BAST'));
        $this->main_model->put_bast($data,$evidence,$signer,$history);
        redirect(base_url().'bast/view/'.$data['ID']);
    }


    // view 
    function view($id_bast){
        $data = $this->main_model->get_bast($id_bast);
        if(empty($data['bast']['ID'])){
            redirect(base_url().'bast');       
        }
        $this->adminView("bast/show",$data);  
    }

    function approve_bast($id_bast){
        $bastBefore= $this->main_model->get_bast($id_bast);
        $targetDir = "./assets/document/".$this->input->post('id_project');
        if(!is_dir($targetDir)){
            mkdir($targetDir,777);
        }
        // echo json_encode($_FILES['bast-file']);die;
        $filename = null;
        if(!empty($_FILES['bast-file']['name'])){
            $this->load->library('upload');
            $config['upload_path'] = $targetDir;
            $config['allowed_types'] = '*';
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['file_name'] = $this->input->post($id_bast);



            $this->upload->initialize($config);
                if ($this->upload->do_upload('bast-file'))
                {
                    $upload_data    = $this->upload->data();
                    $filename       = $upload_data['file_name'];
                    $result['data'] = 'success';
                }else{
                    $upload_error = array('error' => $this->upload->display_errors());
                    echo json_encode($upload_error);die;
                }

                // echo json_encode($result);die;
        }
        $no_bast = null;
        $get_bast   = $this->main_model->get_bast($id_bast);
        $bast       = $get_bast['bast'];
        if(empty($bast['NO_BAST'])){
            switch ($bast['TYPE']) {
                case 'OTC':
                    $set_type = 'O';
                    break;
                case 'TERMIN':
                    $set_type = 'T';
                    break;
                case 'PROGRESS': 
                    $set_type = 'P';
                    break;
                case 'RECURRING':
                    $set_type = 'R';
                    break;
                case 'OTC & RECURRING':
                    $set_type = 'OR';
                    break;
                default:
                    $set_type = "#ERROR";
            }

            $type = 'BAST';
            if($bast['BAUT']=='1'){
                $type = 'BAUT';
            }
            $squen      = $this->main_model->count_bast_spk($bast['SPK']);
            $bast_date  = explode('/', $bast['BAST_DATE2']);
            $no_bast = $type.'/'.$bast['SPK'].'.'.$squen.'/'.$bast['DIVISION'].'/'.$bast['PARTNER'].'/'.$bast_date[1].'/'.$bast_date[2];
        }     
    

        $bastAfter = $this->main_model->get_bast($id_bast);
        $this->main_model->approve_bast($id_bast,$this->input->post('bast-status'),$this->input->post('bast-note'),$this->getGUID(), $filename,$no_bast);
        $this->loggerPrime('BAST', "UPDATE BAST" , array("STATUS"=>$this->input->post('bast-status'), "NOTE" =>$this->input->post('bast-note'), "BEFORE"=> $bastBefore , "AFTER"=>$bastAfter ));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Approval BAST Berhasil'));
        echo 'success';
    }

    function revision_bast($id_bast){
        $id     = $this->getGUID();
        $note   = $this->input->post('bast-note');
        $status   = $this->input->post('approval-status');
        $this->main_model->revision_bast($id,$id_bast,$note,$status);

        $this->loggerPrime('BAST', "UPDATE BAST" , array("BEFORE"=>$status,"NOTE" =>$this->input->post('bast-note'), "AFTER"=>"REVISION" ));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Revisi BAST Berhasil'));

        echo 'success';
    }

    // Export BAST
    function exportPdf($id_bast){
        $data = $this->main_model->get_bast($id_bast);
        if(empty($data["bast"])){echo "no data found!";die;}

        $data["bast"]['PARTNER_NAME_FIX'] = $this->fixNamaPerusahaan($data["bast"]['PARTNER_NAME']);
        $arrSpkFix                       = explode("/", $data["bast"]['SPK_DATE2']);
        $data["bast"]['SPK_DATE_FIX']    = $arrSpkFix[0]." ".$this->ejaBulan($arrSpkFix[1])." ".$arrSpkFix[2];

        $arrBastFix                         = explode("/", $data["bast"]['BAST_DATE2']);
        $BastTimestamp = strtotime($arrSpkFix[2].'-'.$arrSpkFix[1].'-'.$arrSpkFix[0]);
        $data["bast"]['BAST_DATE_DAYNAME']  = $this->hari(date('l', $BastTimestamp));
        $data["bast"]['BAST_DATE_DAY']      = $this->ejaHari($arrBastFix[0]);
        $data["bast"]['BAST_DATE_MONTH']    = $this->ejaBulan($arrBastFix[1]);
        $data["bast"]['BAST_DATE_YEAR']     = $this->tahun($arrBastFix[2]);
        // $this->load->library('M_pdf');
        $this->load->view('report/bast',$data);
        // $html = $this->load->view('report/bast',$data,true);
        
       //  $this->m_pdf->debug = true;
       //  $this->m_pdf->pdf->WriteHTML($html);

       //  if (file_exists(BASEPATH.'../file
       //     / bast/'.$data["bast"]["ID"].'.pdf')) {
       //      unlink(BASEPATH.'../file/bast/'.$data["bast"]["ID"].'.pdf');
       //      } 
       
       // $this->m_pdf->pdf->Output(BASEPATH.'../file/bast/'.$data["bast"]["ID"].'.pdf', 'F'); 
       // ob_clean();
       // $this->m_pdf->pdf->Output();
    }

}