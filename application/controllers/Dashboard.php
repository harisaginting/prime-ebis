<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    private $division;
    public function __construct() 
    {
        parent::__construct();
        if(!$this->isLoggedIn()){
            redirect(base_url().'login');
        }    
        $this->load->model('M_Dashboard','main_model'); 
        $this->division = $this->session->userdata("division");
        $this->load->model('M_Master','master_model');
    } 

    // View Dashboard Page 
    public function index(){
        $data['type_project_value']   = $this->main_model->get_category_project_value();
        $data['bast']                 = $this->main_model->getChartBast();
        $data['regional']             = $this->main_model->getChartRegional();
        $this->adminView("dashboard/view",$data);         
    }

     public function dashboardExecutive(){
        if (!$this->check_access("DASHBOARD_EXECUTIVE","R")) {
            redirect(base_url());
        }
        $data['scale']             = $this->input->get('scale');
        $data['status']            = $this->input->get('status');
        $data['progress']          = $this->input->get('progress');
        $data['partner']           = $this->input->get('partner');
        $data['regional']          = $this->input->get('regional');
        $data['list_partner']      = $this->master_model->get_partner();
        $data['list_customer']     = $this->master_model->get_customer();
        $data['list_segmen']       = $this->master_model->get_segmen();
        $data['list_regional']     = $this->getConfig('REGIONAL');

        $this->adminView("dashboard/executive/view",$data);         
    }

    public function dashboard_info(){
        $info = $this->main_model->get_dashboard_info();
        echo json_encode($info);
    }

    public function dashboardExecutiveInfo(){
        $priority       = $this->input->get('priority');
        $year_start     = $this->input->get('year_start');
        $info = $this->main_model->dashboardExecutiveInfo($priority,$year_start);
        echo json_encode($info);
    }

    public function dashboard_bast(){
        $info = $this->main_model->getChartBast();
        echo json_encode($info);
    }



    function status_project($div = null){
        $priority       = $this->input->get('priority');
        $year_start     = $this->input->get('year_start');
        $data = $this->main_model->get_status_project($div,$priority,$year_start);
        $data['total_project']       = 0;
        $data['total_project_value'] = 0;
        foreach ($data['TOTAL'] as $key => $value) {
            $data['TOTAL'][$key]    = intval($value);
            $data['total_project']  = $data['total_project'] + intval($value);
        }
        foreach ($data['VALUE'] as $key => $value) {
            $data['VALUE'][$key]            = intval($value);
            $data['total_project_value']    = $data['total_project_value'] + intval($value);
        }

        echo json_encode($data);die;
    }

    function progress_project($type = null){
        $data = $this->main_model->get_progress_project();
        $data['total_project']       = 0;
        $data['total_project_value'] = 0;
        foreach ($data['TOTAL'] as $key => $value) {
            $data['TOTAL'][$key]    = intval($value);
            $data['total_project']  = $data['total_project'] + intval($value);
        }
        foreach ($data['VALUE'] as $key => $value) {
            $data['VALUE'][$key]            = intval($value);
            $data['total_project_value']    = $data['total_project_value'] + intval($value);
        }

        echo json_encode($data);die;
    }

    function chart_project_scale_simple(){
        $priority       = $this->input->get('priority');
        $year_start     = $this->input->get('year_start');
        $data = $this->main_model->get_chart_project_scale(null,$priority,$year_start);
        $result = array(
                        'REGULAR' => 0, 
                        'ORDINARY' => 0, 
                        'MEGA' => 0, 
                        'BIG' => 0 
                            );

        foreach ($data as $key => $value) {
            $result[$value["name"]] = intval($value["Y"]);
        }

        echo json_encode($result);
    }

    function chartProjectScale($div = null){
        $priority       = $this->input->get('priority');
        $year_start     = $this->input->get('year_start');
        $data = $this->main_model->get_chart_project_scale($div,$priority,$year_start);
        $result = array(
                        'REGULAR' => array('qty' => 0, 'value' => 0 ), 
                        'ORDINARY' => array('qty' => 0, 'value' => 0 ), 
                        'MEGA' => array('qty' => 0, 'value' => 0 ), 
                        'BIG' => array('qty' => 0, 'value' => 0 ) 
                            );

        foreach ($data as $key => $value) {
            $result[$value["name"]]['qty']      = intval($value["Y"]);
            $valueScale                         = intval($value["V"])/1000000000;
            $result[$value["name"]]['value']    = number_format((float)$valueScale, 2, '.', '');
        }

        echo json_encode($result);
    }

    function chart_project_scale(){
        $dataz =$this->main_model->get_chart_project_scale();
    

        foreach ($dataz as $key1 => $value1) {
           $total = array();
           
            $dataz[$key1]['Y'] = intval($dataz[$key1]['Y']); 
            $dataz[$key1]['drilldown'] = array();
            $dataz[$key1]['drilldown']['name']           = $value1['name'];
            $dataz[$key1]['drilldown']['data']           = $total;
            $dataz[$key1]['drilldown']['value']          = intval($dataz[$key1]['V']);
            switch ($value1['name']) {
                case "BIG":
                    $dataz[$key1]['color'] = '#03bb34';
                    break;
                case "MEGA":
                     $dataz[$key1]['color'] = '#00dc3a';
                    break;
                case "ORDINARY":
                     $dataz[$key1]['color'] = '#018c26';
                    break;
                default:
                      $dataz[$key1]['color'] = '#037521';
            }
        }
        $data['chartProjectScale']      = $dataz;
        // echo json_encode($data);die;
        return $this->load->view('dashboard/chart_project_scale',$data);
    }

    function chart_project_progress(){
        $result['colorTProj']     = array(
                                    "#ff8201","#ffc801",
                                    "#31f7c9","#0b8fc1",
                                    "#0a43c3","#890dd6",
                                    "#9fd03e","#03b732",
                                    "#d60db4","#ff0b65",
                                    "#ff8201","#ffc801"
                                    );
        $data           = $this->main_model->get_project_by_status();
        $type           = array();
        $type_project   = $this->getConfig('TYPE_PROJECT');

        foreach ($type_project as $key => $value) {
            array_push($type, $value['VALUE']);
        }

        foreach ($data as $key1 => $value1) {
           $total = array();
            foreach ($type as $key2 => $value2) {
               array_push($total, intval($this->main_model->get_project_by_status_type($value1['name'],$value2)));
                
            }
            $data[$key1]['Y'] = intval($data[$key1]['Y']); 
            $data[$key1]['drilldown'] = array();
            $data[$key1]['drilldown']['name']           = $value1['name'];
            $data[$key1]['drilldown']['categories']     = $type;
            $data[$key1]['drilldown']['data']           = $total;
            switch ($value1['name']) {
                case "LEAD":
                    $data[$key1]['color'] = '#0fc13e';
                    break;
                case "LAG":
                     $dataz[$key1]['color'] = '#ffc107';
                    break;
                case "DELAY":
                     $data[$key1]['color'] = '#F42021';
                    break;
                default:
                      $dataz[$key1]['color'] = '#b0b0b0';
            }
        }

        $progress = array();
        foreach ($data as $key => $value) {
            array_push($progress, $value['name']);
        }
        $result['progress']         = $progress;
        $result['chartProgress']    = $data;
        // echo json_encode($result);die;
        return $this->load->view('dashboard/chart_project_progress',$result);
    }

}

?>