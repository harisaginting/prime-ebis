<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class Data extends MY_Controller
{
   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Data','main_model'); 
    }

     public function index(){}


    function accountManager(){
        $data   = $this->main_model->accountManager($this->input->post('q')); 
        echo json_encode($data);
    }

    function accountManagerSegmen(){
    	$result = array("status"=>"success");
    	$data  	= array();
    	$email  = $this->input->post('email');
    	if (empty($email)) {
    		$result = array("status"=>"params email empty");
    		echo json_encode($result);die;
    	}
        $data   = $this->main_model->accountManagerSegmen($email); 
        if (!empty($data['SEGMEN']) && !empty($data['SEGMEN_NAME'])) {
        	$result["SEGMEN"] 		= $data['SEGMEN'];
        	$result["SEGMEN_NAME"] 	= $data['SEGMEN_NAME'];
        }
        echo json_encode($result);
    }
}