<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class Datatable extends MY_Controller
{
   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Datatable','main_model'); 
    }

    function projectDashboardExecutive(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $scale          = $this->input->post('scale');
        $progress       = $this->input->post('progress');
        $partner        = $this->input->post('partner');
        $division       = $this->input->post('division');
        $customer       = $this->input->post('customer');
        $segmen         = $this->input->post('segmen');
        $regional       = $this->input->post('regional');
        $priority       = $this->input->post('priority');
        $year_start     = $this->input->post('year_start');

        $model = $this->main_model->get_table_project_dashboard_executive($length, $start, $searchValue, $orderColumn, $orderDir, $order,$scale, $progress, $partner,$regional,$division,$segmen,$customer,$priority,$year_start);

     
        //echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_project_dashboard_executive($scale, $progress, $partner,$regional,$division,$segmen,$customer,$priority,$year_start),
            "recordsFiltered" => $this->main_model->count_filtered_table_project_dashboard_executive($searchValue, $orderColumn, $orderDir, $order,$scale,$progress, $partner,$regional,$division,$segmen,$customer,$priority,$year_start),
            "data" => $model,
        );
        echo json_encode($output);
    }

    function project_active(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $scale          = $this->input->post('scale');
        $progress       = $this->input->post('progress');
        $partner        = $this->input->post('partner');
        $regional       = $this->input->post('regional');
        $priority       = $this->input->post('priority');
        $customer       = $this->input->post('customer');
        $segmen         = $this->input->post('segmen');
        $div            = $this->input->post('division');
        $group            = $this->input->post('group');

        $model = $this->main_model->get_table_project_active($length, $start, $searchValue, $orderColumn, $orderDir, $order,$scale, $progress, $partner,$regional,$priority,$segmen, $customer, $div, $group);


        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_project_active($scale, $progress, $partner,$regional,$priority,$segmen, $customer, $div, $group),
            "recordsFiltered" => $this->main_model->count_filtered_table_project_active($searchValue, $orderColumn, $orderDir, $order,$scale,$progress, $partner,$regional,$priority, $segmen, $customer, $div, $group),
            "data" => $model,
        );
        echo json_encode($output);
    }


    function project_nonpm(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $status         = $this->input->post('status');
        $pm             = $this->input->post('pm');
        $customer       = $this->input->post('customer');
        $partner        = $this->input->post('mitra');
        $type           = $this->input->post('type');
        $regional       = $this->input->post('regional');
        $segmen         = $this->input->post('segmen');

        $model = $this->main_model->get_table_project_nonpm($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen);

     
        //echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_project_nonpm($status,$pm,$customer,$partner,$type,$regional,$segmen),
            "recordsFiltered" => $this->main_model->count_filtered_table_project_nonpm($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen),
            "data" => $model,
        );
        echo json_encode($output);
    }

    function project_candidate(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $status         = $this->input->post('status');
        $pm             = $this->input->post('pm');
        $customer       = $this->input->post('customer');
        $partner        = $this->input->post('mitra');
        $type           = $this->input->post('type');
        $regional       = $this->input->post('regional');
        $segmen         = $this->input->post('segmen');

        $model = $this->main_model->get_table_project_candidate($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen);

     
        //echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_project_candidate($status,$pm,$customer,$partner,$type,$regional,$segmen),
            "recordsFiltered" => $this->main_model->count_filtered_table_project_candidate($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen),
            "data" => $model,
        );
        echo json_encode($output);
    }

    function project_closed(){
        $length = !empty($this->input->post('length'))? $this->input->post('length') : null;
        $start = !empty($this->input->post('start'))?$this->input->post('start'): null;
        $searchValue = !empty(strtoupper($_POST['search']['value']) )? strtoupper($_POST['search']['value'])  : null;
        $orderColumn = !empty($_POST['order']['0']['column'])?$_POST['order']['0']['column']:null;
        $orderDir = !empty($_POST['order']['0']['dir'])? $_POST['order']['0']['dir']:null;
        $order = !empty($_POST['order'])? $_POST['order']: null;

        $status     = $this->input->post('status');
        $pm         = $this->input->post('pm');
        $customer   = $this->input->post('customer');
        $partner    = $this->input->post('mitra');
        $type       = $this->input->post('type');
        $regional   = $this->input->post('regional');
        $segmen     = $this->input->post('segmen');
        $escorded   = $this->input->post('escorded');

        $model = $this->main_model->get_table_project_closed($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_project_closed($status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded),
            "recordsFiltered" => $this->main_model->count_filtered_table_closed($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded),
            "data" => $model,
        );
        echo json_encode($output);
    }

    function bast(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $status         = $this->input->post('status');
        $pm             = $this->input->post('pm');
        $customer       = $this->input->post('customer');
        $partner        = $this->input->post('mitra');
        $type           = $this->input->post('type');
        $regional       = $this->input->post('regional');
        $segmen         = $this->input->post('segmen');

        $model = $this->main_model->get_table_bast($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen);

     
        // echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_bast($status,$pm,$customer,$partner,$type,$regional,$segmen),
            "recordsFiltered" => $this->main_model->count_filtered_table_bast($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen),
            "data" => $model,
        );

        echo json_encode($output);
    }


    ##DASHBOARD
    function dashboard_project_partner(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $status         = $this->input->post('status');
        $pm             = $this->input->post('pm');
        $customer       = $this->input->post('customer');
        $partner        = $this->input->post('mitra');
        $type           = $this->input->post('type');
        $regional       = $this->input->post('regional');
        $segmen         = $this->input->post('segmen');

        $model = $this->main_model->get_table_dashboard_project_partner($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen);

     
        //echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_dashboard_project_partner($status,$pm,$customer,$partner,$type,$regional,$segmen),
            "recordsFiltered" => $this->main_model->count_filtered_table_dashboard_project_partner($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen),
            "data" => $model,
        );
        echo json_encode($output);
    }

    function dashboard_project_pm(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $status         = $this->input->post('status');
        $pm             = $this->input->post('pm');
        $customer       = $this->input->post('customer');
        $partner        = $this->input->post('mitra');
        $type           = $this->input->post('type');
        $regional       = $this->input->post('regional');
        $segmen         = $this->input->post('segmen');

        $model = $this->main_model->get_table_dashboard_project_pm($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen);

     
        //echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_dashboard_project_pm($status,$pm,$customer,$partner,$type,$regional,$segmen),
            "recordsFiltered" => $this->main_model->count_filtered_table_dashboard_project_pm($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen),
            "data" => $model,
        );
        echo json_encode($output);
    }

    function dashboard_project_segmen(){

        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = strtoupper($_POST['search']['value']);
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $_POST['order'];

        $status         = $this->input->post('status');
        $pm             = $this->input->post('pm');
        $customer       = $this->input->post('customer');
        $partner        = $this->input->post('mitra');
        $type           = $this->input->post('type');
        $regional       = $this->input->post('regional');
        $segmen         = $this->input->post('segmen');

        $model = $this->main_model->get_table_dashboard_project_segmen($length, $start, $searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen);

     
        //echo $this->db->last_query();die;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_dashboard_project_segmen($status,$pm,$customer,$partner,$type,$regional,$segmen),
            "recordsFiltered" => $this->main_model->count_filtered_table_dashboard_project_segmen($searchValue, $orderColumn, $orderDir, $order,$status,$pm,$customer,$partner,$type,$regional,$segmen),
            "data" => $model,
        );
        echo json_encode($output);
    }
    ##END DASHBOARD


    ## MONITORING
        function monitoring_issueap(){

            $length         = $this->input->post('length');
            $start          = $this->input->post('start');
            $searchValue    = strtoupper($_POST['search']['value']);
            $orderColumn    = $_POST['order']['0']['column'];
            $orderDir       = $_POST['order']['0']['dir'];
            $order          = $_POST['order'];

            $model = $this->main_model->get_table_monitoringIssueAp($length, $start, $searchValue, $orderColumn, $orderDir, $order);

            $output = array(
                "draw" => $this->input->post('draw'),
                "recordsTotal" => $this->main_model->count_all_table_monitoringIssueAp(),
                "recordsFiltered" => $this->main_model->count_filtered_table_monitoringIssueAp($searchValue, $orderColumn, $orderDir, $order),
                "data" => $model,
            );
            echo json_encode($output);
        }

        function monitoring_bastproject(){

            $length         = $this->input->post('length');
            $start          = $this->input->post('start');
            $searchValue    = strtoupper($_POST['search']['value']);
            $orderColumn    = $_POST['order']['0']['column'];
            $orderDir       = $_POST['order']['0']['dir'];
            $order          = $_POST['order'];

            $model = $this->main_model->get_table_monitoringbastproject($length, $start, $searchValue, $orderColumn, $orderDir, $order);

            $output = array(
                "draw" => $this->input->post('draw'),
                "recordsTotal" => $this->main_model->count_all_table_monitoringbastproject(),
                "recordsFiltered" => $this->main_model->count_filtered_table_monitoringbastproject($searchValue, $orderColumn, $orderDir, $order),
                "data" => $model,
            );
            echo json_encode($output);
        }
    ##END MONITORIGN































    // MASTER
    function user(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_user($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_user(),
            "recordsFiltered" => $this->main_model->count_filtered_table_user($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }


    function partner(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_partner($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_partner(),
            "recordsFiltered" => $this->main_model->count_filtered_table_partner($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }

    function customer(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_customer($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_customer(),
            "recordsFiltered" => $this->main_model->count_filtered_table_customer($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }

    function customer_executive(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
        
        $model = $this->main_model->get_table_customerExecutive($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_customerExecutive(),
            "recordsFiltered" => $this->main_model->count_filtered_table_customerExecutive($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }  

    // Segmen
    function segmen(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_segmen($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_segmen(),
            "recordsFiltered" => $this->main_model->count_filtered_table_segmen($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }  


    function am(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_am($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_am(),
            "recordsFiltered" => $this->main_model->count_filtered_table_am($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }

    function pm(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_pm($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_pm(),
            "recordsFiltered" => $this->main_model->count_filtered_table_pm($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }  

     function log(){
        $length         = $this->input->post('length');
        $start          = $this->input->post('start');
        $searchValue    = trim(strtoupper($_POST['search']['value']));
        $orderColumn    = $_POST['order']['0']['column'];
        $orderDir       = $_POST['order']['0']['dir'];
        $order          = $this->input->post('order');
       
        $model = $this->main_model->get_table_log($length, $start, $searchValue, $orderColumn, $orderDir, $order);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->main_model->count_all_table_log(),
            "recordsFiltered" => $this->main_model->count_filtered_table_log($searchValue, $orderColumn, $orderDir, $order),
            "data" => $model,
        );
        echo json_encode($output);  
    }  


}
?> 