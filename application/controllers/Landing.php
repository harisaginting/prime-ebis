<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends MY_Controller{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('M_User','main_model');  
        $this->load->model('M_Master','master_model');  
    } 

    public function index($type = null)  
    {
        echo "Hi";         
    }

    public function activationUser($code){
        $data['user'] = $this->main_model->getActivateUser($code);
        if (!empty($data['user'])) {
            if (!empty($data['user']['ACTIVATION_CODE'])) {
                return $this->load->view('public/user-activation',$data);
            }
            
        }
        redirect(base_url().'dashboard');
    }

    public function activationProcess(){
        $data['ID']         = $this->input->post('userid');
        $data['PASSWORD']   = $this->hashPassword($this->input->post('password'));
        if ($this->main_model->activateUser($data)) {
           $this->session->set_flashdata('alError', $this->alert('alert-success','user id '.$data['ID'].' has been active'));
            return redirect(base_url().'login');
        }else{
            $this->session->set_flashdata('notification', $this->alert('alert-danger','failed activate user id '.$data['ID']));
            return $this->activationUser($this->input->post('activation_code'));
        }
    }

    public function forgetPassword(){
        return $this->load->view('public/user-forget-password',null);
    }

    public function forgetPasswordProcess(){
        $data   = $this->main_model->getUserByEmail($this->input->post('email')); 
        if (empty($data)) {
            $this->session->set_flashdata('notification', $this->alert('alert-danger','email : '.$this->input->post('email').' is not registered'));
        }else{
            $user['CHANGE_PASSWORD_TOKEN']  = $this->getGUID().'-'.$data['ID'];
            $id                             = $data['ID'];

            $this->main_model->updateUser($id,$user);
            $datauser   = $this->main_model->getUser($id); 
            if ($this->sendResetPasswordEmail($datauser)) {
                $this->session->set_flashdata('notification', $this->alert('alert-success','email for reset password has been send to '.$this->input->post('email')));
            }else{
                $this->session->set_flashdata('notification', $this->alert('alert-danger','email : '.$this->input->post('email').' is not registered'));
            }
        }
        return redirect(base_url().'forget-password');
        
    }

    private function sendResetPasswordEmail($user){
        $data['user'] = $user;
            
            $aliasName   = "PRIME";
            $aliasDomain = "admin@prime.telkom.co.id";
            $config = array();
            $configEmail = $this->getConfig('EMAIL');
            foreach ($configEmail as $key => $value) {
                if ($value['VALUE'] == 'smtp_port' || $value['VALUE'] == 'smtp_timeout') {
                    $config[$value['VALUE']] = intval($value['REMARKS']);
                }elseif ($value['VALUE'] == 'alias_domain' || $value['VALUE'] == 'alias_name') {
                    if ($value['VALUE'] == 'alias_name' ) {
                        $aliasName = $value['REMARKS'];
                    }
                    if ($value['VALUE'] == 'alias_domain' ) {
                        $aliasDomain = $value['REMARKS'];
                    }
                }else{
                    $config[$value['VALUE']] = $value['REMARKS'];
                }
            }
            
            // echo json_encode($config);die;
            $this->load->library('email', $config);

            $this->email->initialize($config);
            $this->email->from($aliasDomain,$aliasName);
            
            // $this->email->to('harisaginting@gmail.com');  
            $this->email->to($user['EMAIL']);   
            $this->email->bcc('darmawan.saputra@telkom.co.id ');
            $this->email->set_newline("\r\n");

            $this->email->subject('PRIME Reset Password for user ID '.$user['ID']);
            $this->email->message($this->load->view('email/user-reset-password', $data, true));           
            
            if(!$this->email->send()){
                echo "failed";
                echo $this->email->print_debugger();die;
                return false;
            }else{
                return true;
            }
            
            // $this->load->view('email/user-reset-password', $data); 
    }

    public function resetPassword($code){
        $data['user'] = $this->main_model->getTokenPasswordUser($code);
        if (!empty($data['user'])) {
            if (!empty($data['user']['CHANGE_PASSWORD_TOKEN'])) {
                return $this->load->view('public/user-reset-password',$data);
            }
            
        }
        redirect(base_url().'dashboard');
    }

    public function resetPasswordProcess(){
        $user = $this->main_model->getTokenPasswordUser($this->input->post('password_token'));
        $id                 = $user['ID'];
        $password           = $this->hashPassword($this->input->post('password'));
        if ($this->main_model->resetPassword($id,$password)) {
           $this->session->set_flashdata('alError', $this->alert('alert-success','password for user id '.$user['ID'].' has been reset'));
            return redirect(base_url().'login');
        }else{
            $this->session->set_flashdata('notification', $this->alert('alert-danger','failed reset password for '.$user['ID']));
            return $this->resetPassword($this->input->post('password_token'));
        }
    }




    function check_userId(){
        $data   = $this->master_model->check_userId($this->input->post('id')); 
        if (empty($data)) {
            echo 1;die;
        }
        echo 0;die;
    }

    function check_userEmail(){
        $data   = $this->master_model->check_useremail($this->input->post('email')); 
        if (empty($data)) {
            echo 1;die;
        }
        echo 0;die;
    }

}