<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Login','main_model');  
    }

    public function index()
        {            
            if($this->isLoggedIn()){
                redirect(base_url().'dashboard');
            }else{
                $this->load->view('login/index');  
            }  
        }

    
    function login_proccess(){
        $this->form_validation->set_rules('username', 'NIK', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == TRUE) {
            $username        = $this->security->xss_clean($this->input->post('username'));
            $password        = $this->security->xss_clean($this->input->post('password'));

            $check_login = $this->main_model->validate_login($username,$password); 
                if (!$check_login) {
                    $this->session->set_flashdata('alError', $this->alert('alert-danger','Maaf NIK atau Password Salah'));
                }else{
                    $this->loggerPrime('AUTH', "SIGN IN" ,$this->getUserData());
                    redirect(base_url().'login');
                }
        }else{
                $this->session->set_flashdata('alError', $this->alert('alert-danger','Mohon lengkapi inputan data'));  
        }
        redirect(base_url().'login');
    }


    function logout(){
        $this->loggerPrime('AUTH', "SIGN OUT" ,$this->getUserData());
        $this->session->sess_destroy();
        $this->auth->doLogout();
        echo json_encode($this->session->userdata());
        redirect(base_url());
    }


}

?>