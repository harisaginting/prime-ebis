<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class Master extends MY_Controller
{
   
    public function __construct()
    {
        parent::__construct();
        if(!$this->isLoggedIn()){
            redirect(base_url());
        };  
        $this->load->model('M_Master','main_model'); 
        $this->load->model('M_Project','p_model'); 
    } 

    function customer_partner(){
        if (!$this->check_access("ADMIN","R")) {
            redirect(base_url());
        }
        $this->adminView('master/customer_partner');
    }

    function configuration(){
        if (!$this->check_access("ADMIN","U")) {
            redirect(base_url());
        }

        $data['EMAIL']      = $this->getConfig('EMAIL');
        $data['REGIONAL']   = $this->getConfig('REGIONAL');
        $data['ROLE']       = $this->getConfig('ROLE');
        $data['SYMPTOM']    = $this->getConfig('SYMPTOM');
        $data['TYPE']       = $this->getConfig('TYPE_PROJECT');

        $this->adminView('master/configuration',$data,'CONFIGURATION');
    }

    function indexLog(){
        if (!$this->check_access("ADMIN","R")) {
            redirect(base_url());
        }
        $this->adminView('master/log',null,"LOG");
    }

    function saveConfigurationEmail(){
        $data                 = $this->input->post();
        $prevEmailConfig      = $this->getConfig('EMAIL');
        if ($this->main_model->saveConfigurationEmail($data)) {
            $this->session->set_flashdata('notification', $this->alert('alert-success','Konfigurasi Email PRIME berhasil diperbarui'));
            $this->loggerPrime('SETTINGS', "UPDATE CONFIGURATION EMAIL" , array("BEFORE"=> $prevEmailConfig,"AFTER" => $data));
            echo "success";die;
        }else{
            $this->session->set_flashdata('notification', $this->alert('alert-danger','Konfigurasi Email PRIME gagal diperbarui'));
            echo "failed";die;
        }   
    }

    function customerExecutive($method = null){
        switch ($method) {
            case 'add':
                $customer = $this->main_model->get_customer($this->input->post('customer'));
                if(!empty($customer)){
                    $data['ID']          = $this->getGUID();
                    $data['CODE']        = $customer['CODE'];
                    $data['DESCRIPTION'] = !empty($this->input->post('description')) ? trim(str_replace("\t", "", $this->input->post('description'))): null;
                    $data['START_DATE']  = !empty($this->input->post('start')) ? $this->input->post('start')  : null;
                    $data['END_DATE']    = !empty($this->input->post('end')) ? $this->input->post('end')  : null;
                    $this->main_model->add_customer_executive($data);
                    $this->loggerPrime('SETTINGS', "ADD CUSTOMER EXECUTIVE" , $data);
                    $this->session->set_flashdata('notification', $this->alert('alert-success','Data Customer VVIP telah berhasil ditambahkan'));
                }else{
                     $this->session->set_flashdata('notification', $this->alert('alert-danger','NIPNAS '.$this->input->post('CODE').' belum terdaftar'));
                }
                $this->p_model->uploadProjectStrategic();
                echo 'success';
                break;
            case 'delete' : 
                $customer = $this->main_model->get_customer(null,$this->input->post('ID'));
                if($this->main_model->delete_customer_executive($this->input->post('ID'))){
                    $this->loggerPrime('SETTINGS', "DELETE CUSTOMER EXECUTIVE" , $customer);
                    $this->p_model->uploadProjectStrategic();
                    echo 'success';
                }
                break;
            case 'edit' : 
                $id                  = $this->input->post('id');
                $customer            = $this->main_model->get_customer(null,$id);
                $data['CODE']        = !empty($this->input->post('customer')) ? $this->input->post('customer') : null;
                $data['DESCRIPTION'] = !empty($this->input->post('description')) ? trim(str_replace("\t", "", $this->input->post('description'))): null;
                $data['START_DATE']  = !empty($this->input->post('start')) ? $this->input->post('start')  : null;
                $data['END_DATE']    = !empty($this->input->post('end')) ? $this->input->post('end')  : null;

                $this->loggerPrime('SETTINGS', "UPDATE CUSTOMER EXECUTIVE" , array("BEFORE"=> $customer,"AFTER" => $data));
                if($this->main_model->update_customer_executive($id, $data)){
                    $this->session->set_flashdata('notification', $this->alert('alert-info','Data Customer VVIP telah berhasil diperbarui'));
                    $this->p_model->uploadProjectStrategic();
                    echo 'success';
                }
                break;
            case null : 
                $this->adminView('master/customer-executive');
                break;
            default:
                $this->adminView('master/customer-executive');
                break;
        }
    }

    // customer
    function customer($method = null){
        switch ($method) {
            case 'add':
                $exist = $this->main_model->cek_customer($this->input->post('CODE'));
                if($exist > 1){
                    $this->session->set_flashdata('notification', $this->alert('alert-danger','NIPNAS '.$this->input->post('CODE').' telah terdaftar'));
                }else{
                    $data = $this->input->post();
                    $data['ID'] = $this->getGUID();
                    $this->main_model->add_customer($data);
                    $this->loggerPrime('SETTINGS', "ADD CUSTOMER" , array($data));
                    $this->session->set_flashdata('notification', $this->alert('alert-success','Customer '.$data['NAME'].' telah berhasil ditambahkan'));
                }
                echo 'success';
                break;
            case 'delete' : 
                $customer = $this->main_model->get_customer($this->input->post('CODE'));
                if(!$this->main_model->delete_customer($this->input->post('CODE'))){
                    $this->loggerPrime('SETTINGS', "DELETE CUSTOMER EXECUTIVE" , $customer);
                     $this->session->set_flashdata('notification', $this->alert('alert-danger','Customer Yang ingin dihapus tidak boleh telah memiliki data Project'));
                }
                echo 'success';
                break;
            case 'edit' : 
                $data = $this->input->post();
                $customer = $this->main_model->get_customer(null,$data["ID"]);
                if($this->main_model->update_customer($data['ID'], $data)){
                    $this->loggerPrime('SETTINGS', "UPDATE CUSTOMER EXECUTIVE" , array("BEFORE"=> $customer,"AFTER" => $data));
                    $this->session->set_flashdata('notification', $this->alert('alert-info','Data Customer '.$data['NAME'].' telah berhasil diperbarui'));
                     echo 'success';
                }
                break;
            case null : 
                $this->adminView('master/customer');
                break;
            default:
                # code...
                break;
        }
        // echo $this->db->last_query();
    }


    // partner
    function partner($method = null){
        switch ($method) {
            case 'add':
                $exist = $this->main_model->cek_partner($this->input->post('CODE'));
                if($exist > 0){
                    echo 'PARTNER with code '.$this->input->post('CODE').' already exist';
                    $this->session->set_flashdata('notification', $this->alert('alert-danger','Partner dengan kode '.$this->input->post('CODE').' telah terdaftar'));
                }else{
                    $data = $this->input->post();
                    $data['ID'] = $this->getGUID();
                    $this->loggerPrime('SETTINGS', "ADD PARTNER" , $data);
                    if($this->main_model->add_partner($data)){
                        echo 'success';
                        $this->session->set_flashdata('notification', $this->alert('alert-success','Partner '.$data['NAME'].' telah berhasil ditambahkan'));
                    }
                }
                break;
            case 'edit' : 
                $data = $this->input->post();
                $partner = $this->main_model->get_partner(null,null,$data['ID']);
                if($this->main_model->update_partner($data['ID'], $data)){
                    $this->loggerPrime('SETTINGS', "UPDATE PARTNER" , array("BEFORE"=>$partner,"AFTER"=>$data));
                    $this->session->set_flashdata('notification', $this->alert('alert-info','Data Partner '.$data['NAME'].' telah berhasil diperbarui'));
                     echo 'success';
                }
                break;
            case 'delete' : 
                $partner = $this->main_model->get_partner($this->input->post('CODE'));
                if($this->main_model->delete_partner($this->input->post('CODE'))){
                    $this->loggerPrime('SETTINGS', "DELETE PARTNER" , $partner);
                    echo 'success';
                }
                break;
            case null : 
                $this->adminView('master/partner');
                break;
            default:
                # code...
                break;
        }

    }


    // Segmen
    function segmen($method = null){
        switch ($method) {
            case 'add':
                $exist = $this->main_model->cek_segmen(trim($this->input->post('CODE')));
                // ECHO $this->db->last_query();die;
                if($exist > 0){
                    $this->session->set_flashdata('notification', $this->alert('alert-danger','Segmen dengan kode '.$this->input->post('CODE').' telah terdaftar'));
                }else{
                    $data = $this->input->post();
                    $data['ID'] = $this->getGUID();
                    if($this->main_model->add_segmen($data)){
                        $this->loggerPrime('SETTINGS', "ADD SEGMEN" , $data);
                        echo 'success';
                    }
                }
                break;
            case 'delete' : 
                $segmen = $this->main_model->get_segmen($this->input->post('CODE')); 
                if($this->main_model->delete_segmen($this->input->post('CODE'))){
                    $this->loggerPrime('SETTINGS', "DELETE SEGMEN" , $segmen);
                    echo 'success';
                }
                break;
            case 'edit' : 
                $data = $this->input->post();
                $segmen = $this->main_model->get_segmen(null,$data['ID']); 
                if($this->main_model->update_segmen($data['ID'], $data)){
                    $this->session->set_flashdata('notification', $this->alert('alert-info','Data Segmen '.$data['NAME'].' telah berhasil diperbarui'));
                    $this->loggerPrime('SETTINGS', "UPDATE SEGMEN" , array("BEFORE"=>$segmen, "AFTER"=>$data));
                     echo 'success';
                }
                break;
            case null : 
                $this->adminView('master/segmen');
                break;
            default:
                # code...
                break;
        }

    }

    function accountManager(){
        $this->adminView('master/segmen_am',array(),'ACCOUNT MANAGER');
    }
    
    function am($method = null){
        switch ($method) {
            case 'add':
                $exist = $this->main_model->cek_am($this->input->post('EMAIL'));
                if($exist > 0){
                    $this->session->set_flashdata('notification', $this->alert('alert-danger','Email '.$this->input->post('EMAIL').' telah terdaftar'));
                    echo 'EMAIL '.$this->input->post('EMAIL').' already exist';
                }else{
                    $data = $this->input->post();
                    $data['ID'] = $this->getGUID();
                    if($this->main_model->add_am($data)){
                        $this->loggerPrime('SETTINGS', "ADD AM" , $data);
                        $this->session->set_flashdata('notification', $this->alert('alert-success','Account Manager telah berhasil ditambahkan'));
                        echo 'success';
                    }
                }
                break;
            case 'edit' : 
                $data = $this->input->post();
                $am = $this->main_model->get_am(null,$this->input->post('EMAIL'));
                if($this->main_model->update_am($data['ID'], $data)){
                    $this->loggerPrime('SETTINGS', "UPDATE AM" , array("BEFORE"=>$am, "AFTER"=>$data));
                    $this->session->set_flashdata('notification', $this->alert('alert-info','Data Account Manager '.$data['NAME'].' telah berhasil diperbarui'));
                    // echo $this->db->last_query();
                     echo 'success';
                }
                break;
            case 'delete' : 
                $am = $this->main_model->get_am(null,$this->input->post('EMAIL'));
                if($this->main_model->delete_am($this->input->post('EMAIL'))){
                    $this->loggerPrime('SETTINGS', "DELETE AM" , $am);
                    echo 'success';
                }
                break;
            case null : 
                $this->adminView('master/am');
                break;
            default:
                # code...
                break;
        }

    }

     // project manager
    function pm($method = null){
        switch ($method) {
            case 'add':
                $exist = $this->main_model->cek_pm($this->input->post('EMAIL'));
                if($exist > 0){
                    $this->session->set_flashdata('notification', $this->alert('alert-danger','Email '.$this->input->post('EMAIL').' telah terdaftar'));
                    echo 'EMAIL '.$this->input->post('EMAIL').' already exist';
                }else{
                    $data = $this->input->post();
                    $data['ID'] = $this->getGUID();
                    if($this->main_model->add_pm($data)){
                        $this->loggerPrime('SETTINGS', "ADD PM" , $data);
                        $this->session->set_flashdata('notification', $this->alert('alert-success','Project Manager telah berhasil ditambahkan'));
                        echo 'success';
                    }
                }
                break;
            case 'edit' : 
                $data = $this->input->post();
                $pm = $this->main_model->get_pm(null,null,$this->input->post('ID')); 
                if($this->main_model->update_pm($data['ID'], $data)){
                    $this->loggerPrime('SETTINGS', "UPDATE PM" , array("BEFORE"=>$pm, "AFTER"=>$data));
                    $this->session->set_flashdata('notification', $this->alert('alert-info','Data Project Manager '.$data['NAME'].' telah berhasil diperbarui'));
                    // echo $this->db->last_query();
                     echo 'success';
                }
                break;
            case 'delete' : 
                $pm = $this->main_model->get_pm(null,null,$this->input->post('ID')); 
                if($this->main_model->delete_pm($this->input->post('ID'))){
                    $this->loggerPrime('SETTINGS', "DELETE PM" , $pm);
                     $this->session->set_flashdata('notification', $this->alert('alert-danger','Project Manager telah dihapus'));
                     echo 'success';
                }
                break;
            case null : 
                $data['regional']       = $this->getConfig('REGIONAL');
                $this->adminView('master/pm',$data);
                break;
            default:
                # code...
                break;
        }
    }

    function get_spk_top_partner(){
        $spk    = $this->input->post('spk');
        $like   = $this->input->post('q');
        
        $data = $this->main_model->get_spk_top_partner($spk,$like);
        echo json_encode($data);
    }

    function get_deliverable($id_project){
        $like   = $this->input->post('q');
        $id     = $this->input->post('id');
        
        $data = $this->main_model->get_deliverable($id_project,$like,$id);
        echo json_encode($data);
    }

    function get_issue($id_project){
        $like   = $this->input->post('q');
        $id     = $this->input->post('id');
        
        $data   = $this->main_model->get_issue($id_project,$like,$id);
        echo json_encode($data);
    }

    function get_action($id_project){
        $like   = $this->input->post('q');
        $id     = $this->input->post('id');
        
        $data   = $this->main_model->get_action($id_project,$like,$id);
        echo json_encode($data);
    }

    function get_partner($code=null){
        $data   = $this->main_model->get_partner($code, $this->input->post('q')); 
        echo json_encode($data);
    }


    function get_user(){
        $data   = $this->main_model->get_user($this->input->post('q')); 
        // echo $this->db->last_query();exit;
        echo json_encode($data);
    }


     // P8
    function get_spk($spk=null){
        $data   = $this->main_model->get_spk($spk, $this->input->post('q')); 
        echo json_encode($data);
    }

    function get_spk_bast($spk=null){
        $data   = $this->main_model->get_spk($spk, $this->input->post('q')); 
        echo json_encode($data);
    }

    function get_am($segmen = null){
        $email  = $this->input->post('email'); 
        $data   = $this->main_model->get_am($segmen,$email, $this->input->post('q')); 
        echo json_encode($data);
    }

    function get_pm(){
        $email  = $this->input->post('email'); 
        $id  = $this->input->post('id'); 
        $data   = $this->main_model->get_pm($email,$this->input->post('q'),$id); 
        echo json_encode($data);
    }

    function getAdmin(){
        $email  = $this->input->post('email'); 
        $data   = $this->main_model->get_user($this->input->post('q'),$email,4); 
        echo json_encode($data);
    }

    function get_segmen($code = null , $id = null){ 
        $data = $this->main_model->get_segmen($code,$id, $this->input->post('q')); 
        echo json_encode($data);
    }

    function get_customer($code = null , $id = null){ 
        $like = $this->input->post('q');
        $data = $this->main_model->get_customer($code,$id,$like); 
        echo json_encode($data);
    }


    function segmen_am(){
        $this->adminView('master/segmen_am');
    }


    function update_id(){
        $project_id = $this->main_model->get_all_data();

        foreach ($project_id as $key => $value) {
                $new_id = $this->getGUID();
                $old_id = $value['ID'];

                $this->main_model->update_id_data($old_id,$new_id);
        }
    }

}