<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Monitoring extends MY_Controller
{
    public function __construct() 
    {
        parent::__construct();
        if(!$this->isLoggedIn()){
            redirect(base_url());
        };
        $this->load->model('M_Monitoring','main_model');    
    	
    }  

    public function index(){
        $this->adminView("monitoring/index");         
    }

    public function pm(){
    	$data['pm'] = $this->main_model->get_all_pm();
        $this->adminView("monitoring/project_manager",$data);         
    }

    public function get_project_pm(){
    	$id  	= $this->input->post('id');
    	$data 	= $this->main_model->get_pm_project($id);
        // echo json_encode($data['project']);die;
    	$this->load->view('monitoring/project_manager_project',$data);
    }

    public function issueap(){
        $this->adminView("monitoring/issue-actionplan");         
    }

    public function bastproject(){
        $this->adminView("monitoring/bast_project");         
    }

}