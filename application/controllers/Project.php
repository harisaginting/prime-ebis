<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class Project extends MY_Controller
{
   
    public function __construct()
    { 
        parent::__construct();
        $this->load->model('M_Project','main_model'); 
        $this->load->model('M_Master','master_model'); 
        if(!$this->isLoggedIn()){
            redirect(base_url());
        };  
    }

    
    public function index($type = null) { redirect(base_url().'project-data');}
    
    public function data($type = null){
        if (!$this->check_access("PROJECT","R")) {
            redirect(base_url());
        }
        $data['scale']             = $this->input->get('scale');
        $data['status']            = $this->input->get('status');
        $data['progress']          = $this->input->get('progress');
        $data['priority']          = $this->input->get('priority');
        $data['partner']           = $this->input->get('partner');
        $data['regional']          = $this->input->get('regional');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        if ( ! $foo = $this->cache->get('regional'))
        {
                $data['list_regional']     = $this->getConfig('REGIONAL');
                $this->cache->save('regional', $data['list_regional'], 72000);
        }else{
                $data['list_regional']     = $this->cache->get('regional');
        }


        if (strval($data['priority']) != "0" && empty($this->input->get('priority'))) {
            $data['priority'] = 'all';
        }
        $data['title']     = 'Projects'; 
        $data['type']      = !empty($type) ? $type : 'active';
        $this->adminView('project/index',$data,'Project');
    }

    public function viewProject($id_project=null){ 
        if(empty($id_project)){  redirect(base_url().'project'); }

        $p_access = $this->main_model->getProjectAccess($this->getUserData('email'), $id_project);
        // echo json_encode($p_access);die;
        if (!$this->check_access("PROJECT","R")) {
            return redirect(base_url().'project');
        }
        $data = $this->main_model->getProject($id_project);
            if(empty($data['project']['ID_PROJECT'])){redirect(base_url().'project');}
        
        
        $data["p_access"]           = $p_access;
        $data['list_symptom']       = $this->getConfig('SYMPTOM');
        $data['project']['PLAN']    = 0;
        $data['chart_progress']     = $this->main_model
                                        ->getChartProgress($id_project,$data['project']['START_DATE2'],$data['project']['VALUE']);
        $data['chart_month']        = $this->main_model
                                        ->getChartProgressMonth(
                                            $id_project,$data['project']['START_DATE2'],$data['project']['VALUE']);


        $chart_partner = array(); 
        foreach ($data['partner'] as $key => $value) {
            $chart_partner[$value['ID_PARTNER']] = $this->main_model->getChartProgressPartner($id_project,$value['ID_PARTNER'],$data['project']['START_DATE2']);
            
        }
        $data['chart_partner']      = $chart_partner;
        $this->adminView('project/view',$data,'Project');
    }

    public function editProject($id_project){
        $p_access = $this->main_model->getProjectAccess($this->getUserData('email'), $id_project);
        if (empty($p_access["U"])  || !$this->check_access("PROJECT","U")) {
            return redirect(base_url().'project');
        }
        $data                   = $this->main_model->getProject($id_project);
            if(empty($data['project']['ID_PROJECT'])){redirect(base_url().'project');}
        $data['p_access']       = $p_access;
        $data['type_project']   = $this->getConfig('TYPE_PROJECT');
        $data['regional']       = $this->getConfig('REGIONAL');
        $data['user_access']    = $this->main_model->getAllProjectAccess($id_project);
        return $this->adminView('project/edit',$data,'Edit Project');
    }

    public function addProject(){
        $data['type_project']   = $this->getConfig('TYPE_PROJECT');
        $data['regional']       = $this->getConfig('REGIONAL');
        $this->adminView('project/add',$data);   
    }

    public function addProjectProcess(){
        $input = $this->input->post();

        $data['ID_PROJECT'] = $this->getGUID();
        $data['ID_PROJECT_GLOBAL'] = !empty($input['id_global']) ? $input['id_global'] : null;
        $data['NAME']       = $input['name'];
        $data['DIVISION']   = $input['division'];
        $data['CUSTOMER']   = $input['customer'];
        $data['KB']         = $input['kb'];
        $data['VALUE']      = $input['value'];
        $data['SEGMEN']     = $input['segmen'];
        $data['START_DATE'] = $input['start'];
        $data['END_DATE']   = $input['end'];
        $data['CATEGORY']   = $input['category'];
        $data['REGIONAL']   = $input['regional'];
        $data['AM']         = $input['am'];
        $data['PM']         = !empty($input['pm']) ? $input['pm'] : null;
        $data['DESCRIPTION']= substr($input['description'], 0, 3999);
        $data['PM_ADMIN']   = $input['admin'];
        $data['STATUS']     = 'CANDIDATE';
        $data['IS_STRATEGIC'] = !empty($input['strategic']) ? '1' : '0';
        $data['SID']        = $input['sid'];  
        $data['NO_NCX']     = $input['no_ncx'];  
        $data['NO_PROJECT'] = $input['no_project'];  
        $data['GROUP_TYPE'] = !empty($input['group_type']) ? $input['group_type'] : "NON CONNECTIVITY";  
        $data['CREATED_BY'] = $this->getUserData('email');  
        
        $data['DATE_KICK_OFF']              = !empty($input['kickoff']) ? $input['kickoff'] : null;  
        $data['DATE_MONITORING_REQUEST']    = !empty($input['monitoring']) ? $input['monitoring'] : null;  
        $data['DATE_PM_APPOINTMENT']        = !empty($input['appointment']) ? $input['appointment'] : null; 

        foreach ($data as $key => $value) {
            if ( count($value) > 3999 ) {
                $this->session->set_flashdata('notification', $this->alert('alert-danger','field '.$key.' excess limit value'));
            }
        }

        if($this->main_model->put_project($data)){
            foreach ($input['top-customer-date'] as $key => $value) {
                 $top_customer['ID']         = $this->getGUID();
                 $top_customer['ID_PROJECT'] = $data['ID_PROJECT'];
                 $top_customer['DUE_DATE']   = $input['top-customer-date'][$key];
                 $top_customer['PROGRESS']   = $input['top-customer-progress'][$key];
                 $top_customer['VALUE']      = $input['top-customer-value'][$key];
                 $top_customer['NOTE']       = $input['top-customer-note'][$key];
                $this->main_model->put_top_customer($top_customer);
             } 
        }
        $this->loggerPrime('PROJECT', "ADD PROJECT" , $data);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan project'));
        $this->main_model->addProjectAccess($data['ID_PROJECT']);
        $this->main_model->uploadProjectStrategic();
        redirect(base_url().'project-edit/'.$data['ID_PROJECT']);
    }

    function deleteProject(){
        $id_project = $this->input->post('id');
        $projBefore         = $this->main_model->getProjectData($id_project);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menghapus project'));
        $this->loggerPrime('PROJECT', "DELETE PROJECT" , $projBefore);
        if ($this->main_model->deleteProject($id_project)) {
            $result['data'] = 'success';
            echo json_encode($result);
        }
    }

    public function saveDocument($id_project){
        $result['data'] = 'failed';
        $data['ID_PROJECT'] = $id_project;

        $targetDir = "./assets/document/".$id_project;
        if(!is_dir($targetDir)){
            mkdir($targetDir,0777);
        }

        if(!empty($_FILES['document'])){
            $this->load->library('upload');
            $config['upload_path'] = $targetDir;
            $config['allowed_types'] = '*';
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['file_name'] = $this->input->post('document-name');

            $this->upload->initialize($config);
                if ($this->upload->do_upload('document'))
                {
                    $upload_data    = $this->upload->data();
                    $data['NAME']   = $upload_data['file_name'];
                    $this->main_model->put_document($data);
                    $result['data'] = 'success';
                    $this->loggerPrime('PROJECT', "ADD DOCUMENT" , $data);
                }else{
                    $upload_error = array('error' => $this->upload->display_errors());
                    // echo json_encode($upload_error);die;
                }

                echo json_encode($result);die;
        }
    }

    public function activated_project($id_project){
        $result['data'] = 'failed';
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil merubah ke project aktif'));
        $projBefore     = $this->main_model->getProjectData($id_project);
        $this->loggerPrime('PROJECT', "ACTIVATED PROJECT" , $projBefore);
        if($this->main_model->set_project_active($id_project)){
            $result['data'] = 'success';
        }

        echo json_encode($result);
        
    }

    public function close_project($id_project){
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil close project'));
        if($this->main_model->set_project_close($id_project,$this->input->post('close-date'),$this->input->post('close-lesson'))){
            $this->loggerPrime('PROJECT', "CLOSE PROJECT" , array("CLOSE DATE" => $this->input->post('close-date'), "LESSON LEARNED"=>$this->input->post('close-lesson'), "ID_PROJECT" => $id_project ));

            echo 'success';
        }        
        // echo json_encode($this->input->post());
    }


    public function editProjectProcess(){
        $input = $this->input->post();
        $id_project         = $input['id_project'];
        $projBefore         = $this->main_model->getProjectData($id_project);
        $data['ID_PROJECT_GLOBAL'] = !empty($input['id_global']) ? $input['id_global'] : null;
        $data['NAME']       = $input['name'];
        $data['DIVISION']   = $input['division'];
        $data['CUSTOMER']   = $input['customer'];
        $data['KB']         = $input['kb'];
        $data['VALUE']      = $input['value'];
        $data['SEGMEN']     = $input['segmen'];
        $data['START_DATE'] = $input['start'];
        $data['END_DATE']   = $input['end'];
        $data['CATEGORY']   = $input['category'];
        $data['REGIONAL']   = $input['regional'];
        $data['AM']         = $input['am'];
        $data['PM']         = $input['pm'];
        $data['PM_ADMIN']   = $input['admin'];  
        $data['DESCRIPTION']= $input['description'];    
        $data['IS_STRATEGIC'] = !empty($input['strategic']) ? '1' : '0';     
        $data['SID']        = $input['sid'];  
        $data['NO_NCX']     = $input['no_ncx'];  
        $data['NO_PROJECT'] = $input['no_project'];  
        $data['GROUP_TYPE'] = !empty($input['group_type']) ? $input['group_type'] : "NON CONNECTIVITY";  

        $data['DATE_KICK_OFF']              = !empty($input['kickoff']) ? $input['kickoff'] : null;  
        $data['DATE_MONITORING_REQUEST']    = !empty($input['monitoring']) ? $input['monitoring'] : null;  
        $data['DATE_PM_APPOINTMENT']        = !empty($input['appointment']) ? $input['appointment'] : null;  
        
        if($this->main_model->update_project($id_project,$data)){
            $this->main_model->uploadProjectStrategic();
            $this->main_model->delete_top_customer($id_project);
            foreach ($input['top-customer-date'] as $key => $value) {
                 $top_customer['ID']         = $this->getGUID();
                 $top_customer['ID_PROJECT'] = $id_project;
                 $top_customer['DUE_DATE']   = $input['top-customer-date'][$key];
                 $top_customer['PROGRESS']   = $input['top-customer-progress'][$key];
                 $top_customer['VALUE']      = $input['top-customer-value'][$key];
                 $top_customer['NOTE']       = $input['top-customer-note'][$key];
                $this->main_model->put_top_customer($top_customer);
             } 
            $this->main_model->update_progress($id_project);
        }
        $this->main_model->addProjectAccess($id_project);
        $this->loggerPrime('PROJECT', "UPDATE PROJECT" , array("BEFORE"=> $projBefore, "AFTER" => $data ));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbaharui project'));

        redirect(base_url().'project-edit/'.$id_project);
    }

    function savePartner($id_project){
       
        $targetDir = "./assets/document/".$id_project;
        if(!is_dir($targetDir)){
            mkdir($targetDir,0777);
        }

        if(!empty($_FILES['file_spk'])){
            $this->load->library('upload');
            if (!empty($_FILES['file_spk']['name'])) {
                   $lname = strlen($_FILES['file_spk']['name']);
                   if ($lname > 99 ) {
                       $startCut = $lname - 99;
                       $fname    = substr($_FILES['file_spk']['name'], $startCut);
                       $config['file_name'] = $fname;
                   }
            }      
            $config['upload_path'] = $targetDir;
            $config['allowed_types'] = 'pdf|doc|docx|xlm|xlmx';
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->upload->initialize($config);
                if ($this->upload->do_upload('file_spk'))
                {
                    $upload_data = $this->upload->data();
                    $data['DOC_SPK'] = $upload_data['file_name'];
                }
        }

        // echo json_encode($this->input->post());die;
        $data['ID_PROJECT'] = $id_project;
        $data['ID_PARTNER'] = $this->input->post('partner');
        $data['SPK']        = $this->input->post('spk');
        $data['SPK_DATE']   = $this->input->post('spk_date');
        $data['KL']         = $this->input->post('kl');
        $data['KL_DATE']    = $this->input->post('kl_date');
        $data['VALUE']      = $this->input->post('value_partner');
        
        if($this->main_model->cek_project_partner($id_project,$data['SPK']) > 0){
             $this->main_model->update_project_partner($id_project, $data['SPK'], $data);
             $this->loggerPrime('PROJECT', "UPDATE PARTNER" , $data);
        }else{
            $this->loggerPrime('PROJECT', "ADD PARTNER" , $data);
            $this->main_model->put_project_partner($data);
        }


        $this->main_model->delete_top_partner($id_project,$data['SPK']);
        foreach ($this->input->post('top-partner-date') as $key => $value) {
            if(!empty($value)){
                $data_top['ID']         = $this->getGUID();
                $data_top['ID_PROJECT'] = $id_project;
                $data_top['ID_PARTNER'] = $this->input->post('partner');
                $data_top['SPK']        = $this->input->post('spk');
                $data_top['DUE_DATE']   = $this->input->post('top-partner-date')[$key];
                $data_top['PROGRESS']   = $this->input->post('top-partner-progress')[$key];
                $data_top['VALUE']      = $this->input->post('top-partner-value')[$key];
                $data_top['NOTE']       = $this->input->post('top-partner-note')[$key];
                $this->main_model->put_top_partner($data_top);  
            }
        }

        echo json_encode(array('data' => 'success'));      
    }

    function deletePartner(){
        $data['data'] = 'failed';

        if($this->main_model->delete_project_partner($this->input->post('spk'))){
            $this->loggerPrime('PROJECT', "DELETE PARTNER" , $this->input->post('spk'));
            $data['data'] = 'success';            
        }
        echo json_encode($data);
    }

    function deleteDeliverable(){
        $id_deliverable     = $this->input->post('id_deliverable');
        $id_project         = $this->input->post('id_project');
        
        $this->main_model->delete_deliverable($id_deliverable,$id_project);
        $this->main_model->update_progress($this->input->post('id_project'));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menghapus deliverable'));
        echo 'success';
    }
 
    function show($id_project=null){
        if(empty($id_project)){ 
             redirect(base_url().'project');
        }
        $data                       = $this->main_model->getProject($id_project);
            if(empty($data['project']['ID_PROJECT'])){redirect(base_url().'project');}
            if($this->session->userdata('role') == "PROJECT MANAGER" ){
                if ($data['project']['PM'] != $this->session->userdata('userid')) {
                    redirect(base_url().'project');
                }
            }

            if($this->session->userdata('role') == "PROJECT ADMIN" ){
                if ($data['project']['PM_ADMIN'] != $this->session->userdata('email')) {
                    redirect(base_url().'project');
                }
            }


        $data['list_symptom']       = $this->getConfig('SYMPTOM');
        $data['edit']               = 1;
        $data['project']['PLAN']    = 0;
        $data['chart_progress']     = $this->main_model->getChartProgress($id_project,$data['project']['START_DATE2'],$data['project']['VALUE']);
        $data['chart_month']        = $this->main_model->getChartProgressMonth($id_project,$data['project']['START_DATE2'],$data['project']['VALUE']);


        $chart_partner = array(); 
        foreach ($data['partner'] as $key => $value) {
            $chart_partner[$value['ID_PARTNER']] = $this->main_model->getChartProgressPartner($id_project,$value['ID_PARTNER'],$data['project']['START_DATE2']);
            
        }
        $data['chart_partner']  = $chart_partner;
        $this->adminView('project/show',$data,'Project');
    }

    function edit_Chart_deliverable($id_deliverable){
        $data  = $this->main_model->getSingleDeliverable($id_deliverable);

        $this->adminView('project/deliverable_plan',$data,'Project');
    }

    function get_deliverable($id_deliverable){
        $data = $this->main_model->get_deliverable($id_deliverable);
        echo json_encode($data);
    }
    
    function saveDeliverable($id_project){
        $data['ID']         = $this->getGUID();
        $data['ID_PROJECT'] = $id_project;
        $data['NAME']       = $this->input->post('deliverable-name');
        $data['PIC']        = $this->input->post('deliverable-pic');
        $data['DESCRIPTION']= $this->input->post('deliverable-description');
        $data['START_DATE'] = $this->input->post('deliverable-start');
        $data['END_DATE']   = $this->input->post('deliverable-end');
        $data['WEIGHT']     = $this->input->post('deliverable-weight');
        
        $is_partner = $this->getPartner($data['PIC']);
        if(empty($is_partner)){
            $data['PIC_NAME']  = $data['PIC']; 
        }else{
            $data['PIC_NAME'] = $is_partner['NAME']; 
        }

        $this->main_model->put_deliverable($data);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan deliverable'));
        $this->loggerPrime('PROJECT', "ADD DELIVERABLE" , $data);
        $this->main_model->update_progress($id_project);
        echo 'success';
    }

    function updateDeliverable($id_project){
        $data['ID']         = $this->input->post('edit-deliverable-id');
        $data['ID_PROJECT'] = $id_project;
        $data['NAME']       = $this->input->post('edit-deliverable-name');
        $data['PIC']        = $this->input->post('edit-deliverable-pic');
        $data['DESCRIPTION']= $this->input->post('edit-deliverable-description');
        // $data['START_DATE'] = $this->input->post('edit-deliverable-start');
        // $data['END_DATE']   = $this->input->post('edit-deliverable-end');
        $data['WEIGHT']     = $this->input->post('edit-deliverable-weight');
        
        $is_partner = $this->getPartner($data['PIC']);
        if(empty($is_partner)){
            $data['PIC_NAME']  = $data['PIC']; 
        }else{
            $data['PIC_NAME'] = $is_partner['NAME']; 
        }
        $deliverableBefore = $this->main_model->getDeliverable($data["ID"]);
        $this->main_model->update_deliverable($data['ID'], $data);
        $this->loggerPrime('PROJECT', "UPDATE DELIVERABLE" , array("BEFORE"=> $deliverableBefore, "AFTER"=>$data));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbaharui deliverable'));
        $this->main_model->update_progress($id_project);
        echo 'success';
    }

    function update_week_deliverable($id_deliverable,$id_project){
        $input = $this->input->post();

        $data = array();
        foreach ($input as $key => $value) {
            $key1 = explode('-', $key);
            $data[$key1[1]] = $value;
        }

        $this->main_model->update_deliverable_plan($id_deliverable,$data);
        $this->loggerPrime('PROJECT', "UPDATE DELIVERABLE WEEK" , array("ID_DELIVERABLE"=>$id_deliverable, "WEEK"=>$data));
        $this->main_model->update_progress($id_project);
        echo 'success';
    }

    function update_symptom($id_project){
        $symptom = $this->input->post('symptom');

        foreach ($symptom as $key => $value) {
            $this->main_model->update_symptom($this->getGUID(),$value,$id_project);
        }
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbaharui symptom'));
        $this->loggerPrime('PROJECT', "ADD SYMPTOM" , array("ID_PROJECT"=>$id_project, "SYMPTOM"=>$symptom));
        echo 'success';
    }

    function saveIssue($id_project){
        $data['ID_PROJECT']     = $id_project;
        $data['ID_DELIVERABLE'] = $this->input->post('issue-deliverable');
        $data['ISSUE_STATUS']   = 'OPEN';
        $data['NAME']           = $this->input->post('issue-name');
        $data['IMPACT']         = $this->input->post('issue-impact');
        $data['DESCRIPTION']    = $this->input->post('issue-description');

        if(empty($this->input->post('issue-id'))){
            $data['ID']     = $this->getGUID();
            $issueBefore    = $this->main_model->getIssue($data['ID']);
            $this->main_model->put_issue($data);      
            $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan issue'));
            $this->loggerPrime('PROJECT', "UPDATE ISSUE" , array("BEFORE"=> $issueBefore , "AFTER"=>$data));
        }else{
            $this->main_model->update_issue($this->input->post('issue-id'),$data);
            $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbaharui issue'));
            $this->loggerPrime('PROJECT', "ADD ISSUE" , $data);
        }
        
        echo 'success';
    }

    function saveAction($id_project){
        $data['ID_PROJECT']     = $id_project;
        $data['ACTION_STATUS']  = 'OPEN';
        $data['NAME']           = $this->input->post('action-name');
        $data['ID_ISSUE']       = $this->input->post('action-issue');
        $data['PIC']            = $this->input->post('action-pic');
        $data['PIC_NAME']       = $this->input->post('action-pic-name');
        $data['DESCRIPTION']    = $this->input->post('action-description');
        $data['START_DATE']     = $this->input->post('action-start');
        $data['END_DATE']       = $this->input->post('action-end');
        $data['ACTION_STATUS']  = 'OPEN';

        if(empty($this->input->post('action-id'))){
            $data['ID']     = $this->getGUID();
            $actionBefore   = $this->main_model->getAction($data['ID']);
            $this->main_model->put_action($data);    
            $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan action'));
            $this->loggerPrime('PROJECT', "UPDATE ACTION" , array("BEFORE"=> $actionBefore, "AFTER"=>$data));  
        }else{
            $this->main_model->update_action($this->input->post('action-id'),$data);
            $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbaharui action'));
            $this->loggerPrime('PROJECT', "ADD ACTION" , $data);
        }
        echo 'success';
    }


    function deleteIssue(){
        $id = $this->input->post('id');
        $issueBefore    = $this->main_model->getIssue($id);
        $this->main_model->delete_issue($id);
        $this->loggerPrime('PROJECT', "DELETE ISSUE" , $issueBefore);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menghapus issue'));
        echo 'success';
    }

    function deleteAction(){
        $id             = $this->input->post('id');
        $actionBefore   = $this->main_model->getAction($id);
        $this->main_model->delete_action($id);
        $this->loggerPrime('PROJECT', "DELETE ACTION" , $actionBefore);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menghapus action'));
        echo 'success';
    }

    function saveProgress(){
        $data['ID']             = $this->getGUID();
        $data['ID_DELIVERABLE'] = $this->input->post('progress-deliverable-id');
        $data['DATE_ACHIEVED']  = $this->input->post('progress-date');
        $data['ACHIEVEMENT']    = $this->input->post('progress-value');
        $data['WEEK']           = $this->diffWeek($this->input->post('progress-project-start'),$this->input->post('progress-date'));

        $this->main_model->put_progress($data);
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan progress'));
        $deliverable = $this->main_model->getDeliverable($data['ID_DELIVERABLE']);
        $this->main_model->update_sum_progress($data['ACHIEVEMENT'], $this->input->post('progress-project-id'),$data['WEEK'],$deliverable);
        $this->main_model->update_progress($this->input->post('progress-project-id'));
        $this->loggerPrime('PROJECT', "ADD PROGRESS" , array("ID_PROJECT"=>$this->input->post('progress-project-id') , "DATA" => $data));
        echo 'success';
    }

    function deleteProgress(){
        $week           = $this->diffWeek($this->input->post('project_start'),$this->input->post('progress_date'));
        $deliverable    = $this->main_model->getDeliverable($this->input->post('id_deliverable'));
        $this->main_model->delete_progress($this->input->post('achievement'), $this->input->post('project_id'),$week,$deliverable,$this->input->post('id'));
        $this->main_model->update_sum_progress($this->input->post('achievement'), $this->input->post('project_id'),$week,$deliverable,true);
        $this->main_model->update_progress($this->input->post('project_id'));
        $this->loggerPrime('PROJECT', "DELETE PROGRESS" , array("ID_PROJECT"=>$this->input->post('project_id') , "DELIVERABLE" => $deliverable, "ACHIEVEMENT"=>$this->input->post('achievement')));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menghapus progress'));
        echo 'success';
        
    }

    function addProjectAccess(){
        $id     = $this->input->post('id');
        $email  = $this->input->post('email');
        
        if (!empty($id) && !empty($email)) {
            if ($this->main_model->addProjectAccess($id,$email)) {
                $this->loggerPrime('PROJECT', "ADD PROJECT ACCESS" , array("ID_PROJECT"=>$id , "USER" => $email));
                $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan access '.$email));
                echo "success";die;
            }
        }
       
        echo "failed";die;
    }

     function deleteProjectAccess(){
        $id     = $this->input->post('id');
        $email  = $this->input->post('email');
        
        if (!empty($id) && !empty($email)) {
            if ($this->main_model->deleteProjectAccess($id,$email)) {
                $this->loggerPrime('PROJECT', "DELETE PROJECT ACCESS" , array("ID_PROJECT"=>$id , "USER" => $email));
                $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menghapus access '.$email));
                echo "success";die;
            }
        }
        echo "failed";die;
    }

    function getProjectDescription($id_project = null){
        if (empty($id_project)) {
            echo "-";die;
        }
        $desc = $this->main_model->getProjectDescription($id_project);
        echo $desc;die;
    }


    ##TOOLS
    public function uploadProjectStrategic(){
        $result = $this->main_model->uploadProjectStrategic();
        echo $result;die;
    }


}

?> 