<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller
{
    private $division;
    public function __construct() 
    {
        parent::__construct();
        if(!$this->isLoggedIn()){
            redirect(base_url().'login');
        }    
        $this->load->model('M_Report','main_model'); 
        $this->division = $this->session->userdata("division");
        $this->load->library('Hgn_spreadsheet');
    } 

    
    public function listProject($type){
        $type = strtoupper($type);

        $data = $this->main_model->excelListProject($type);
        // echo json_encode($data);die;
        $name = 'Projects '.strtolower($type)." ".date('Y-m-d');
        $this->hgn_spreadsheet->setHeader(
            array(
                'title' => $name
                ,'subject' => $name
                ,'description' => $name
                ,'sheet_name' => $name
            )
        );
        $data_title = array( 
             array('name' => 'ID',              'id' => 'ID_PROJECT', 'width' => 10)
            ,array('name' => 'DIVISION',        'id' => 'DIVISION', 'width' => 10)
            ,array('name' => 'NAME',            'id' => 'NAME', 'width' => 60)
            ,array('name' => 'CUSTOMER',        'id' => 'CUSTOMER_NAME', 'width' => 20)
            ,array('name' => 'SEGMEN',          'id' => 'SEGMEN_NAME', 'width' => 20)
            ,array('name' => 'NOMOR KB',        'id' => 'KB', 'width' => 20)
            ,array('name' => 'VALUE PROJECT',   'id' => 'VALUE', 'width' => 20)
            ,array('name' => 'START DATE',      'id' => 'START_DATE2', 'width' => 15)
            ,array('name' => 'END DATE',        'id'   => 'END_DATE2', 'width' => 15)
            ,array('name' => 'SCALE',           'id'   => 'SCALE', 'width' => 15)
            ,array('name' => 'CATEGORY',        'id'   => 'CATEGORY_NAME', 'width' => 15)
            ,array('name' => 'GROUP',           'id'   => 'GROUP_TYPE', 'width' => 15)
            ,array('name' => 'REGIONAL',        'id'   => 'REGIONAL_NAME', 'width' => 15)
            ,array('name' => 'ACCOUNT MANAGER', 'id'   => 'AM_NAME', 'width' => 15)
            ,array('name' => 'PROJECT MANAGER', 'id'   => 'PM_NAME', 'width' => 15)
            ,array('name' => 'DESCRIPTION',     'id'   => 'DESCRIPTION', 'width' => 15)
            ,array('name' => 'PARTNERS',        'id'   => 'PARTNER_NAME', 'width' => 15)
            ,array('name' => 'NO P8',           'id'   => 'NO_SPK', 'width' => 15)
            ,array('name' => 'NO KL',           'id'   => 'NO_KL', 'width' => 15)
            ,array('name' => 'VALUE KL',        'id'   => 'VALUE_KL', 'width' => 15)
            ,array('name' => 'STATUS PROJECT',  'id'   => 'STATUS', 'width' => 15)
            ,array('name' => 'PROGRES PROJECT', 'id'   => 'PROGRESS', 'width' => 15)
            ,array('name' => 'TOTAL WEIGHT',    'id'   => 'TOTAL_WEIGHT', 'width' => 15)
            ,array('name' => 'PLAN',            'id'   => 'TOTAL_PLAN', 'width' => 15)
            ,array('name' => 'ACHIEVEMENT',     'id'   => 'TOTAL_ACHIEVEMENT', 'width' => 15)
            ,array('name' => 'CLOSED DATE',     'id'   => 'DATE_CLOSED2', 'width' => 15)
            ,array('name' => 'DATE CREATED',    'id'   => 'DATE_CREATED2', 'width' => 15)
            ,array('name' => 'LAST UPDATE',     'id'   => 'DATE_UPDATED2', 'width' => 15)
            ,array('name' => 'SOURCE',          'id'   => 'SOURCE', 'width' => 15)
            );
        $this->hgn_spreadsheet->setDataTitle($data_title);
        $file = $this->hgn_spreadsheet->create($name, $data);
        $this->load->helper('download');
        force_download($file, NULL);      
    }

    public function listBast(){
        $data = $this->main_model->excelListBast();
        // echo json_encode($data);die;
        $name = 'BAST '.date('Y-m-d');
        $this->hgn_spreadsheet->setHeader(
            array(
                'title' => $name
                ,'subject' => $name
                ,'description' => $name
                ,'sheet_name' => $name
            )
        );
        $data_title = array( 
             array('name' => 'TYPE',            'id' => 'BASTBAUT', 'width' => 10)
            ,array('name' => 'NAME',            'id' => 'PROJECT_NAME', 'width' => 60)
            ,array('name' => 'CUSTOMER',        'id' => 'CUSTOMER_NAME', 'width' => 20)
            ,array('name' => 'PARTNER',         'id' => 'PARTNER_NAME', 'width' => 20)
            ,array('name' => 'NO P8/SPK',       'id' => 'SPK', 'width' => 20)
            ,array('name' => 'VALUE PROJECT',   'id' => 'SPK_DATE2', 'width' => 20)
            ,array('name' => 'START DATE',      'id' => 'KL', 'width' => 15)
            ,array('name' => 'END DATE',        'id'   => 'KL_DATE2', 'width' => 15)
            ,array('name' => 'NO BAST',         'id'   => 'NO_BAST', 'width' => 15)
            ,array('name' => 'BAST DATE',       'id'   => 'BAST_DATE2', 'width' => 15)
            ,array('name' => 'TERM OF PAYMENT', 'id'   => 'TOP_BAST', 'width' => 15)
            ,array('name' => 'VALUE',           'id'   => 'VALUE', 'width' => 15)
            ,array('name' => 'DIVISION',        'id'   => 'DIVISION', 'width' => 15)
            ,array('name' => 'STATUS',          'id'   => 'APPROVAL_STATUS', 'width' => 15)
            ,array('name' => 'PIC',             'id'   => 'PIC', 'width' => 15)
            ,array('name' => 'PIC EMAIL',       'id'   => 'PIC_EMAIL', 'width' => 15)
            );
        $this->hgn_spreadsheet->setDataTitle($data_title);
        $file = $this->hgn_spreadsheet->create($name, $data);
        $this->load->helper('download');
        force_download($file, NULL);      
    }
}

?>