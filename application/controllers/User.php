<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('M_User','main_model'); 
        $this->load->model('M_Login','login_model');   
        if(!$this->isLoggedIn()){
            redirect(base_url().'login');
        }    
    } 

    public function index(){
    	$this->adminView('user/index',null,'Users');
    }

    public function profile($id){
        $data  = $this->main_model->getUser($id);
        $this->adminView('user/profile',$data,'Profile');
    }

    public function add(){
        $data['regional']   = $this->getConfig('REGIONAL');
        $data['role']       = $this->getAllRole();
        $this->adminView('user/add',$data,'Add User');
    }

    public function roleAccess(){
        $data['role']           = $this->getAllRole();
        $data['role_access']    = $this->getRoleAccess();
        $this->adminView('settings/role/index',$data,'Role Access');
    }

    public function UpdateRoleAccess(){
        $id     = $this->input->post('id');
        $val    = $this->input->post('val');
        $sid    = $this->input->post('sid');
        $id = str_replace("switch-", "", $id);
        if ( in_array($sid, array("C","R","U","D")) && !empty($id) && in_array($val, array("1","0")) 
            && $this->main_model->UpdateRoleAccess($id,$val,$sid)) {
            $this->loggerPrime('SETTINGS', "UPDATE ROLE CONFIG" , array("ID_ROLE" => $id, "CONFIG"=>$sid, "VALUE"=>$val));
            echo "success";die;
        }else{
            echo "failed update data";die;
        }
    }

     public function changePassword(){
        $data['data']       = $this->main_model->getUser($this->session->userdata("userid"));
        $this->loggerPrime('USERS', "CHANGE PASSWORD");
        $this->adminView('user/change-password',$data,'Change Password');
    }

    public function edit($id = null){
        if(empty($id)){
            redirect(base_url().'user'); 
        }
        $data['regional']   = $this->getConfig('REGIONAL');
        $data['role']       = $this->getAllRole();
        $data['data']       = $this->main_model->getUser($id);

        
        $this->adminView('user/edit',$data,'Edit '-'..User');
    }
 
    public function add_proccess(){
        $data['ID']         = $this->input->post('userid');
        $data['ROLE']       = $this->input->post('role');
        $data['NAME']       = $this->input->post('name');
        $data['EMAIL']      = $this->input->post('email');
        $data['DIVISION']   = $this->input->post('division');
        $data['AVATAR']     = $this->input->post('avatar');
        $data['REGIONAL']   = $this->input->post('regional');
        $data['PHONE']      = $this->input->post('phone');
        $data['ACTIVATION_CODE']  = $this->getGUID().'-'.$data['ID'];
        // $data['PASSWORD']   = $this->hashPassword($this->input->post('password'));
        $userid             = $this->main_model->addUser($data);
        
        if($userid){
            $this->sendActivationEmail($userid);
            $this->loggerPrime('USERS', "ADD USER", $data);
            $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil menambahkan data user'));
            redirect(base_url().'user/add');
        }else{
            $this->session->set_flashdata('notification', $this->alert('alert-danger','Gagal menambahkan data user'));
            redirect(base_url().'user/add');
        }                  
    }

    private function sendActivationEmail($id){
        $data['user'] = $user  = $this->main_model->getUser($id);
        if (empty($user['ACTIVATION_CODE']) || $user['ACTIVATION_CODE'] == "" ) {
            echo "user already active";
        }   

            $aliasName   = "PRIME";
            $aliasDomain = "admin@prime.telkom.co.id";
            $config = array();
            $configEmail = $this->getConfig('EMAIL');
            foreach ($configEmail as $key => $value) {
                if ($value['VALUE'] == 'smtp_port' || $value['VALUE'] == 'smtp_timeout') {
                    $config[$value['VALUE']] = intval($value['REMARKS']);
                }elseif ($value['VALUE'] == 'alias_domain' || $value['VALUE'] == 'alias_name') {
                    if ($value['VALUE'] == 'alias_name' ) {
                        $aliasName = $value['REMARKS'];
                    }
                    if ($value['VALUE'] == 'alias_domain' ) {
                        $aliasDomain = $value['REMARKS'];
                    }
                }else{
                    $config[$value['VALUE']] = $value['REMARKS'];
                }
            }


            // $config['mailtype']      = "html";
            // $config['charset']      = "utf-8";
            // $config['smtp_crypto']  = "ssl";
            // $config['smtp_host'] = "smtp.telkom.co.id";
            // $config['smtp_user'] = "403387"; 
            // $config['smtp_pass'] = "ITppsda123";
            // $config['smtp_port']    = 465;
            // $config['smtp_timeout'] = 30;

            $this->load->library('email', $config);

            $this->email->initialize($config);
            $this->email->from($aliasDomain,$aliasName);
            
            // $this->email->to('harisaginting@gmail.com');  
            $this->email->to($user['EMAIL']);   
            $this->email->bcc('darmawan.saputra@telkom.co.id ');
            $this->email->set_newline("\r\n");

            $this->email->subject('New PRIME ID Activation '.$user['NAME']);
            $this->email->message($this->load->view('email/user-activation', $data, true));           
            
            if(!$this->email->send()){
                echo "failed";
                echo $this->email->print_debugger();
                return false;
            }else{
                return true;
            }
            
            // $this->load->view('email/user-activation', $data); 
    }

    public function update_proccess(){
        $id                 = $this->input->post('userid');
        $data['NAME']       = $this->input->post('name');
        $data['ROLE']       = $this->input->post('role');
        $data['EMAIL']      = $this->input->post('email');
        $data['PHONE']      = $this->input->post('phone');
        $data['DIVISION']   = $this->input->post('division');

        if(!empty($_FILES['avatar']['name'])){
            $config['upload_path']      = "./assets/avatar";
            $config['allowed_types']    = '*';
            $config['overwrite']        = TRUE;
            $config['remove_spaces']    = TRUE;
            $config['file_name']        = $id;

            $this->load->library('upload');
            $this->upload->initialize($config);
                if ($this->upload->do_upload('avatar'))
                {
                    $upload_data        = $this->upload->data();
                    $data['AVATAR']     = $upload_data['file_name'];
                }else{
                    $upload_error = array('error' => $this->upload->display_errors());
                    echo json_encode($upload_error);die;
                }

        }

        $userBefore = $this->main_model->getUser($id);
        $this->main_model->updateUser($id,$data);
        $this->loggerPrime('USERS', "ADD USER", array("BEFORE"=> $userBefore, "AFTER"=>$data));
        $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbarui data user'));
        redirect(base_url().'user/profile/'.$id);

    }

    function delete_user(){
        $result['data'] = 'failed';
        $id         = $this->input->post('id');
        $email      = $this->input->post('email');
        $userBefore = $this->main_model->getUser($id);
        $this->main_model->deleteUser($id,$email);
        $this->loggerPrime('USERS', "DELETE USER", $userBefore);
        $result['data'] = 'success';
        echo json_encode($result);
    }

    function checkPassword(){
        $id         = $this->input->post('id');
        $password   = $this->input->post('password');
        
        if (empty($id) || empty($password)) { echo 0;return;}

        $check = $this->login_model->validate_login($id,$password); 
        if (!$check) {
            echo 0;return;
        }else{
            echo 1;return;
        }
    }

    function changePasswordProcess(){
        $id         = $this->input->post('userid');
        $password   = $this->input->post('new_password');
        
        if (empty($id) || empty($password)) {
            $this->session->set_flashdata('notification', $this->alert('alert-danger','Gagal memperbarui password'));
            return $this->adminView('user/change-password',$userdata,'Change Password');
        }
        $data['PASSWORD'] = $this->hashPassword($password);

        if ($this->main_model->updateUser($id,$data)) {
            $this->session->set_flashdata('notification', $this->alert('alert-success','Berhasil memperbarui password'));
        }else{
            $this->session->set_flashdata('notification', $this->alert('alert-danger','Gagal memperbarui password'));
        }
        $userdata['data'] = $this->main_model->getUser($id);
        $this->loggerPrime('USERS', "UPDATE PASSWORD USER");
        $this->adminView('user/change-password',$userdata,'Change Password');
    }


}