<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Utility extends MY_Controller
{
    private $division;
    public function __construct() 
    {
        parent::__construct();
        // if(!$this->isLoggedIn()){
        //     redirect(base_url().'login');
        // }    
        $this->load->model('M_Utility','main_model'); 
        $this->load->model('M_Master','master_model'); 
        $this->division = $this->session->userdata("division");
        $this->load->library('Hgn_spreadsheet');
        $this->load->model('M_Project','p_model'); 
    } 

    public function uploader(){
        if (!$this->check_access("UTILITY","R")) {
            redirect(base_url());
        }
        $data = array();
        $this->adminView('utility/uploader',$data);
    }
    
    public function uploadScreening(){
        if (!$this->check_access("UTILITY","R")) {
            redirect(base_url());
        }
        $data = array(
            'import'        => array(),
            'import_v'      => array(),
            'regional'      => array(),
            'type_project'  => array(),
            'error'         => null
        );

        $data['type_project']       = $this->getConfig('TYPE_PROJECT');
        $data['regional']   = $this->getConfig('REGIONAL');
        if(!empty($_FILES['data-project'])){
            $targetDir = "./assets/uploaded-project/";
            if(!is_dir($targetDir)){
                mkdir($targetDir,0777);
            }
            $flname  = explode(".",$_FILES['data-project']["name"]);
            $ext     = $flname[1]; 

            $this->load->library('upload');
            $config['upload_path']      = $targetDir;
            $config['allowed_types']    = '*';
            $config['overwrite']        = TRUE;
            $config['remove_spaces']    = TRUE;
            $config['file_name']        = "uploaded-data-".date('Y-m-d-h-i-s').".".$ext;

            $this->upload->initialize($config);
                if ($this->upload->do_upload('data-project'))
                {
                    $upload_data        = $this->upload->data();
                    $result             = $this->hgn_spreadsheet->reader($targetDir.$upload_data["file_name"]);
                    // echo json_encode($result);die;
                    foreach ($result as $key => $value) {
                        // echo json_encode($value);die;
                        if ($key > 1) {
                            $data["import"][$key - 1]["NAME"]       = !empty($value["A"])  ? trim($value["A"]) : null; 
                            $data["import"][$key - 1]["VALUE"]      = !empty($value["B"])  ? trim($value["B"]) : null; 
                            $data["import"][$key - 1]["CUSTOMER_NAME"]   = !empty($value["C"])  ? trim($value["C"]) : null; 
                            $data["import"][$key - 1]["KB"]         = !empty($value["D"])  ? trim($value["D"]) : null; 
                            $data["import"][$key - 1]["SEGMEN"]     = !empty($value["E"])  ? trim($value["E"]) : null; 
                            $data["import"][$key - 1]["AM"]         = !empty($value["F"])  ? trim($value["F"]) : null; 
                            $data["import"][$key - 1]["PM"]         = !empty($value["G"])  ? trim($value["G"]) : null; 
                            $data["import"][$key - 1]["START_DATE"] = !empty($value["H"])  ? trim($value["H"]) : null; 
                            $data["import"][$key - 1]["END_DATE"]   = !empty($value["I"])  ? trim($value["I"]) : null; 
                            $data["import"][$key - 1]["REGIONAL"]   = !empty($value["J"])  ? trim($value["J"]) : null; 
                            $data["import"][$key - 1]["CATEGORY"]   = !empty($value["K"])  ? trim($value["K"]) : null;
                            $data["import"][$key - 1]["DIVISION"]   = !empty($value["L"])  ? trim($value["L"]) : null;
                            $data["import"][$key - 1]["IS_STRATEGIC"]   = !empty($value["M"])  ? trim($value["M"]) : null;
                            $data["import"][$key - 1]["DATE_CLOSED"]    = !empty($value["N"])  ? trim($value["N"]) : null;
                            $data["import"][$key - 1]["LESSON_LEARN"]   = !empty($value["O"])  ? trim($value["O"]) : null;

                            $data["import"][$key - 1]["CUSTOMER"]       = null; 
                            $data["import"][$key - 1]["SEGMEN_NAME"]    = null; 
                            $data["import"][$key - 1]["AM_NAME"]        = null; 
                            $data["import"][$key - 1]["PM_NAME"]        = null; 
                            $data["import"][$key - 1]["REGIONAL_NAME"]  = null; 
                            $data["import"][$key - 1]["CATEGORY_NAME"]  = null; 
                        }
                    }
                    $import_v =array();
                    foreach ($data["import"] as $key => $value) {
                        foreach ($value as $key1 => $value1) {
                            switch ($key1) {
                                case 'VALUE':
                                        $data["import"][$key][$key1] = strval(intval($value1));
                                    break;
                                case 'CUSTOMER_NAME':
                                         $customer = $this->master_model->get_customer(null, null, $value1);
                                         if (!empty($customer)) {
                                            $data['import'][$key]['CUSTOMER'] = $customer[0]['CODE'];
                                         }else{
                                            $data['import'][$key]['CUSTOMER_NAME'] = null;
                                         }
                                    break;
                                case 'SEGMEN':
                                        $segmen = $this->master_model->get_segmen($value1);
                                        if (!empty($segmen)) {
                                             $data['import'][$key]['SEGMEN_NAME'] = $segmen['NAME'];
                                         }else{
                                            $data['import'][$key]['SEGMEN'] = null; 
                                         } 
                                    break;
                                case 'AM':
                                        $am = $this->master_model->get_am(null,$value1);
                                        if (!empty($am)) {
                                             $data['import'][$key]['AM_NAME'] = !empty($am['NAME']) ? $am['NAME'] : "";
                                        }else{
                                            $data['import'][$key]['AM'] = null;
                                        }
                                    break;
                                case 'PM':
                                        $pm = $this->master_model->get_pm($value1);
                                        if (!empty($pm)) {
                                             $data['import'][$key]['PM']        = !empty($pm['ID']) ? $pm['ID'] :  "";
                                             $data['import'][$key]['PM_NAME']   = !empty($pm['NAME']) ? $pm['NAME'] : "";
                                        }else{
                                            $data['import'][$key]['PM'] = null;
                                        }
                                    break;
                                case 'START_DATE':
                                        $start = explode('/', $value1);
                                        if (empty($start[1]) || empty($start[2]) || intval($start[2]) < 2010 ) {
                                            $data['import'][$key]['START_DATE'] = null;
                                        }
                                    break;
                                case 'END_DATE':
                                        $end = explode('/', $value1);
                                        if (empty($end[1]) || empty($end[2]) || intval($end[2]) < 2010 ) {
                                            $data['import'][$key]['END_DATE'] = null;
                                        }
                                    break;
                                 case 'DATE_CLOSED':
                                        $cls = explode('/', $value1);
                                        if (empty($cls[1]) || empty($cls[2]) ) {
                                            $data['import'][$key]['DATE_CLOSED'] = null;
                                            $data['import'][$key]['LESSON_LEARN'] = null;
                                        }
                                    break;
                                case 'REGIONAL':
                                        $regional = $this->getConfig('REGIONAL',$value1);
                                        if (!empty($regional)) {
                                            $data['import'][$key]['REGIONAL']       = $regional[0]['ID'];
                                            $data['import'][$key]['REGIONAL_NAME']  = $regional[0]['VALUE'];
                                        }
                                    break;
                                case 'DIVISION':
                                        if ($value1 != 'DES' && $value1 != 'DGS' && $value1 != 'DBS') {
                                            $data['import'][$key]['DIVIION'] = null;
                                        }
                                    break;
                                case 'CATEGORY':
                                        $cat = $this->getConfig('TYPE_PROJECT',$value1);
                                        if (!empty($cat)) {
                                            $data['import'][$key]['CATEGORY']       = $cat[0]['ID'];
                                            $data['import'][$key]['CATEGORY_NAME']  = $cat[0]['VALUE'];
                                        }
                                    break;
                                case 'IS_STRATEGIC':
                                        if (trim(strtoupper($value1)) == 'Y') {
                                            $data['import'][$key]['IS_STRATEGIC']   = 1;
                                        }else{
                                             $data['import'][$key]['IS_STRATEGIC']  = 0;
                                        }
                                    break;
                                default:
                                    break;
                            }

                        }
                    }

                    $keyData = 0;
                    foreach ($data['import'] as $key => $value) {
                        $cekKb = $this->p_model->check_kb($value['KB']);
                        if (empty($cekKb) && !empty($value['KB'])) {
                            $data['import_v'][$keyData] = $value;
                            $keyData = $keyData + 1;
                        }                        
                    }
                }else{
                    $upload_error   = array('error' => $this->upload->display_errors());
                    $data["error"] = json_encode($upload_error);
                }
        }
        // echo json_encode($data);die;
        $this->adminView('utility/uploader-screening',$data);
    }

    public function uploadProcess(){
        $data = array();

        $p_regional = $this->input->post('project-regional');
        $p_category = $this->input->post('project-category');
        $p_name     = $this->input->post('project-name');
        $p_value    = $this->input->post('project-value');
        $p_segmen   = $this->input->post('project-segmen');
        $p_customer = $this->input->post('project-customer');
        $p_kb       = $this->input->post('project-kb');
        $p_am       = $this->input->post('project-am');
        $p_pm       = $this->input->post('project-pm');
        $p_start    = $this->input->post('project-start');
        $p_end      = $this->input->post('project-end');
        $p_div      = $this->input->post('project-division');
        $p_pro      = $this->input->post('project-priority');
        $p_cls      = $this->input->post('project-closed');
        $p_les      = $this->input->post('project-lesson');

        foreach ($p_name as $key => $value) {
            $data[$key]['ID_PROJECT']   = $this->getGUID();
            $data[$key]["NAME"]         = trim(str_replace("\t", "", $value));
            $data[$key]["REGIONAL"]     = !empty($p_regional[$key]) ? $p_regional[$key]  : null;
            $data[$key]["CATEGORY"]     = !empty($p_category[$key]) ? $p_category[$key] : null;
            $data[$key]["SEGMEN"]       = !empty($p_segmen[$key]) ? $p_segmen[$key] : null;
            $data[$key]["CUSTOMER"]     = !empty($p_customer[$key]) ? $p_customer[$key] : null;
            $data[$key]["KB"]           = !empty($p_kb[$key]) ? $p_kb[$key] : null;
            $data[$key]["AM"]           = !empty($p_am[$key]) ? $p_am[$key] : null;
            $data[$key]["PM"]           = !empty($p_pm[$key]) ? $p_pm[$key] : null;
            $data[$key]["DIVISION"]     = !empty($p_div[$key]) ? $p_div[$key] : null;
            $data[$key]["START_DATE"]   = !empty($p_start[$key]) ? $p_start[$key] : null;
            $data[$key]["END_DATE"]     = !empty($p_end[$key]) ? $p_end[$key] : null;
            $data[$key]["IS_STRATEGIC"] = !empty($p_pro[$key]) ? $p_pro[$key] : null;
            $data[$key]["DATE_CLOSED"]  = !empty($p_cls[$key]) ? $p_cls[$key] : null;
            $data[$key]["LESSON_LEARN"] = !empty($p_les[$key]) ? $p_les[$key] : null;

            $p_value2   = !empty($p_value[$key]) ? $p_value[$key] : "0";
            $p_value2   = intval(str_replace("Rp", "", str_replace(".", "", $p_value2)));
            $data[$key]["VALUE"]        = $p_value2;
            $data[$key]["SOURCE"]       = 'IMPORT';

            if (empty($data[$key]["DATE_CLOSED"])) {
                $data[$key]["STATUS"]       = "CANDIDATE";
            }else{
                $data[$key]["STATUS"]       = "CLOSED";
            }
        }
        $result['data'] = array();
        foreach ($data as $key => $value) {
            $cekKb = $this->p_model->check_kb($value['KB']);
            if (empty($cekKb) && !empty($value['KB'])) {
                $this->p_model->put_project($value);
                array_push($result['data'] , $value);
            }
        }
        $this->loggerPrime('UTILITY', "UPLOAD PROJECT" , $result['data']);
        $this->p_model->uploadProjectStrategic();
        $this->adminView('utility/upload-success',$result);

    }



    public function importDataAm(){
        $fileDir = "./assets/template/DATA_AM.xls";
        $result  = $this->hgn_spreadsheet->reader($fileDir);
        $data    = array();
        $data_v  = array();
        $keyData = 0;
        foreach ($result as $key => $value) {
            if ($key > 1) {
                $data[$keyData]["ID"]         = $this->getGUID(); 
                $data[$keyData]["NAME"]       = !empty($value["B"])  ? trim($value["B"]) : null; 
                $data[$keyData]["EMAIL"]      = !empty($value["E"])  ? trim($value["E"]) : null; 
                $data[$keyData]["PHONE"]      = !empty($value["F"])  ? trim($value["F"]) : null; 
                $segmen                       = !empty($value["G"])  ? trim($value["G"]) : null; 
                $c_segmen  = $this->master_model->get_segmen(trim($segmen));
                
                $arr_phone = explode('/', $data[$keyData]["PHONE"]);
                $data[$keyData]["PHONE"] = $arr_phone[0];

                if (!empty($c_segmen)) {
                    $data[$keyData]["SEGMEN"] = $segmen;
                }else{
                    $data[$keyData]["SEGMEN"] = null;
                }
                $keyData++;
            }
        }

        foreach ($data as $key => $value) {
            $c_am = $this->master_model->get_am(null,$value['EMAIL'],null);
            if (!empty($c_am)) {
                array_push($data_v, $value);
            }
        }
        // $this->main_model->importDataAm($data_v);
        echo json_encode($data_v);
    }


    public function fix(){
        $this->main_model->fixData();
        // $this->main_model->fixChart('22E16F4E-67A6-40F2-3DB4-273D309ADE0F');
    }

    public function update_progress($id = null){
        $data = $this->main_model->getActive($id);
        foreach ($data as $key => $value) {
            if (!empty($value['DIVISION']) && $value['DIVISION'] == "DES") {
                $this->main_model->fixChart($value['ID_PROJECT']);
            }
            $this->p_model->update_progress($value['ID_PROJECT']);
        }
        // $this->main_model->fixData();
        echo count($data)." has been updated";
    }

    public function fixData($id = null){
        $data = $this->main_model->getActive($id);
        foreach ($data as $key => $value) {
            $this->main_model->fixChart($value['ID_PROJECT']);
            $this->p_model->update_progress($value['ID_PROJECT']);
        }
        // $this->main_model->fixData();
        echo count($data)." has been updated";
    }

    public function fixProjectAccess(){
        $pro  = $this->main_model->getAllProject();
        foreach ($pro as $key => $value) {
            $data = $this->main_model->fixProjectAccess($value["ID_PROJECT"]);
            echo count($data)."<br>".$value["ID_PROJECT"]." :role has been updated<br><br>";
        }
    }

    ###SYNC
    public function getDesProject(){
        $data = array();
        $pro = $this->main_model->getDesProject();
        $pmExist = 0;
        foreach ($pro as $key => $value) {
            if ($value['STATUS'] == 'NON PM' && (!empty($value['PM']) || $value['PM'] == '@telkom.co.id')) {
                $pro[$key]['PM'] = $value['PM'] = null;
            }

            if (in_array($value['STATUS'], array('LEAD','LAG','DELAY'))) {
                $pro[$key]['STATUS']    = 'ACTIVE';
            }
            if (!in_array($value['PROGRESS'], array('LEAD','LAG','DELAY'))) {
                $pro[$key]['PROGRESS']    = NULL;
            }

            if ($value['CLOSE_BY_ID'] == '1') {
                $pro[$key]['CLOSE_BY_ID'] = null;
            }

            if (!empty($value['PM'])) {
                $pm = $this->master_model->get_pm($value['PM']);
                if (!empty($pm)) {
                    $pro[$key]['PM'] = $pm['ID'];
                }else{
                    $pro[$key]['PM'] = null;
                }
            }

            if ($value['PM'] == '@telkom.co.id') {
                 $pro[$key]['PM'] = null;
            }

            if (!intval($pro[$key]['IS_STRATEGIC'])) {
                $pro[$key]['IS_STRATEGIC'] = 0;
            }else{
                $pro[$key]['IS_STRATEGIC'] = intval($pro[$key]['IS_STRATEGIC']);
            }

            if ($pro[$key]['STATUS'] == 'ACTIVE' && !empty($pro[$key]['PM'])) {
                 $pmExist++;
            }


        }

        // echo $pmExist;die;
        foreach ($pro as $key => $value) {
            $this->p_model->put_project($value);
        }
        $this->p_model->uploadProjectStrategic();
        echo "NEW SYNC PROJECT :".$pmExist;die;
        $data['data'] = $pro;
        // $this->adminView('utility/sync-des',$data);
    }

    public function updateDesProject(){
        $data = array();
        $pro = $this->main_model->updateDesProject();
        $pmExist = 0;
        $updated = 0;
        foreach ($pro as $key => $value) {
            if ($value['STATUS'] == 'NON PM' && (!empty($value['PM']) || $value['PM'] == '@telkom.co.id')) {
                $pro[$key]['PM'] = $value['PM'] = null;
            }

            if (in_array($value['STATUS'], array('LEAD','LAG','DELAY'))) {
                $pro[$key]['STATUS']    = 'ACTIVE';
            }
            if (!in_array($value['PROGRESS'], array('LEAD','LAG','DELAY'))) {
                $pro[$key]['PROGRESS']    = NULL;
            }

            if ($value['CLOSE_BY_ID'] == '1') {
                $pro[$key]['CLOSE_BY_ID'] = null;
            }

            if (!empty($value['PM'])) {
                $pm = $this->master_model->get_pm($value['PM']);
                if (!empty($pm)) {
                    $pro[$key]['PM'] = $pm['ID'];
                }else{
                    $pro[$key]['PM'] = null;
                }
            }

            if ($value['PM'] == '@telkom.co.id') {
                 $pro[$key]['PM'] = null;
            }

            if (!intval($pro[$key]['IS_STRATEGIC'])) {
                $pro[$key]['IS_STRATEGIC'] = 0;
            }else{
                $pro[$key]['IS_STRATEGIC'] = intval($pro[$key]['IS_STRATEGIC']);
            }

            if (!empty($value['CATEGORY'])) {
                $cat = $this->getConfig('TYPE_PROJECT',$value['CATEGORY']);
                $pro[$key]['GROUP_TYPE']  = "NON CONNECTIVITY";
                if (!empty($cat)) {
                    $pro[$key]['CATEGORY']    = $cat[0]['ID'];
                    if (in_array($cat[0]['ID'], array('11','22','18','35','36','39'))) {
                        $pro[$key]['GROUP_TYPE']  = "CONNECTIVITY";
                    }
                }
                // echo $value['ID_PROJECT']." => ".$value["CATEGORY"]." => ".$pro[$key]['CATEGORY']."<br>";
            }

            if ($pro[$key]['STATUS'] == 'ACTIVE' && !empty($pro[$key]['PM'])) {
                 $pmExist++;
            }
        }

        // echo $pmExist;die;
        foreach ($pro as $key => $value) {
            $updated++;
            $this->p_model->update_project($value['ID_PROJECT'],$value);
            $this->p_model->update_progress($value['ID_PROJECT']);
        }
        $this->p_model->uploadProjectStrategic();
        // echo $pmExist;die;
        echo $updated.' has been updated';die;
        // $data['data'] = $pro;
        // $this->adminView('utility/sync-des',$data);
    }

    public function updateDesProjectCandidate(){
        $data = array();
        $pro = $this->main_model->updateDesProjectCandidate();
        $pmExist = 0;
        $updated = 0;
        foreach ($pro as $key => $value) {
            if ($value['STATUS'] == 'NON PM' && (!empty($value['PM']) || $value['PM'] == '@telkom.co.id')) {
                $pro[$key]['PM'] = $value['PM'] = null;
            }

            if (in_array($value['STATUS'], array('LEAD','LAG','DELAY'))) {
                $pro[$key]['STATUS']    = 'ACTIVE';
            }
            if (!in_array($value['PROGRESS'], array('LEAD','LAG','DELAY'))) {
                $pro[$key]['PROGRESS']    = NULL;
            }

            if ($value['CLOSE_BY_ID'] == '1') {
                $pro[$key]['CLOSE_BY_ID'] = null;
            }

            if (!empty($value['PM'])) {
                $pm = $this->master_model->get_pm($value['PM']);
                if (!empty($pm)) {
                    $pro[$key]['PM'] = $pm['ID'];
                }else{
                    $pro[$key]['PM'] = null;
                }
            }

            if ($value['PM'] == '@telkom.co.id') {
                 $pro[$key]['PM'] = null;
            }

            if (!intval($pro[$key]['IS_STRATEGIC'])) {
                $pro[$key]['IS_STRATEGIC'] = 0;
            }else{
                $pro[$key]['IS_STRATEGIC'] = intval($pro[$key]['IS_STRATEGIC']);
            }

            if (!empty($value['CATEGORY'])) {
                $cat = $this->getConfig('TYPE_PROJECT',$value['CATEGORY']);

                $pro[$key]['GROUP_TYPE']  = "NON CONNECTIVITY";
                if (!empty($cat)) {
                    $pro[$key]['CATEGORY']    = $cat[0]['ID'];
                    if (in_array($cat[0]['ID'], array('11','22','18','35','36','39'))) {
                        $pro[$key]['GROUP_TYPE']  = "CONNECTIVITY";
                    }
                }

            }

            if ($pro[$key]['STATUS'] == 'ACTIVE' && !empty($pro[$key]['PM'])) {
                 $pmExist++;
            }
        }

        // echo $pmExist;die;
        foreach ($pro as $key => $value) {
            $updated++;
            $this->p_model->update_project($value['ID_PROJECT'],$value);
            $this->p_model->update_progress($value['ID_PROJECT']);
        }
        $this->p_model->uploadProjectStrategic();
        // echo $pmExist;die;
        echo $updated.' has been updated';die;
        // $data['data'] = $pro;
        // $this->adminView('utility/sync-des',$data);
    }

    public function getDesProjectPartner(){
        $data = array();
        $pro  = $this->main_model->updateDesProjectPartner();
    }

    public function updateDesDeliverable(){
        $data = array();
        $data['data'] = $this->main_model->addDesDeliverable();
        
        
        foreach ($data['data'] as $key => $value) {
            $this->p_model->put_deliverable($value);
        }
    }

    public function updateDesDeliverable2(){
        // echo "DELIVERABLE ADD: ".count($data['data'])."<br>";
        $data['update'] = $this->main_model->updateDesDeliverable();

        foreach ($data['update'] as $key => $value) {
            $this->p_model->update_deliverable($value['ID'],$value);
        }
       


        foreach ($data['update'] as $key => $value) {
            $this->main_model->update_achievement($value);
            $this->p_model->update_progress($value['ID_PROJECT']);
        }
         echo "DELIVERABLE UPDATE: ".count($data['update'])."<br>";
    }

    function updateDesIssue(){
        $id = $this->main_model->getIdDesProjectSaved();
        $this->main_model->UpdateDesIssue($id);
    }

    function updateDesActionPlan(){
        $id = $this->main_model->getIdDesProjectSaved();
        $this->main_model->updateDesActionPlan($id);
    }

    function updateSymptom(){
        $id = $this->main_model->getIdDesProjectSaved();
        $this->main_model->updateSymptom($id);
    }

    public function syncMasterDes(){
        $data = array();
        $data['data'] = $this->main_model->syncMasterDes();
    }

    public function syncDocument(){
        $this->main_model->syncDocument();
    }
    ###END SYNC
}

?>