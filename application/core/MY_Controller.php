<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class MY_Controller extends CI_Controller {

	// Menampilkan Tema 
    function adminView($viewName = "", $data = null,$title = null){

    	$user = $this->session->userdata();
    	$header['title'] 			  = !empty($title) ? $title : $user['division'] ; 
		$CI =& get_instance();
		$sidebar = array();
		
		$division = "EBIS";
		if (!empty($user['division'])) {
			$division = $user['division'];
		}

		if (!empty($data['project'])) 
		{
			if (!empty($data['project']['DIVISION'])) {
				$division = $data['project']['DIVISION'];
			}
		}

		$access 	= array();
		$accessData = $this->db->from("PRIME_ROLE_ACCESS")->where("ID_ROLE",$user['role_id'])->get()->result_array();

		foreach ($accessData as $key => $value) {
			$access[$value['MODULE']]["C"] = $value["C"] == "1" ? true : false;
			$access[$value['MODULE']]["R"] = $value["R"] == "1" ? true : false;
			$access[$value['MODULE']]["U"] = $value["U"] == "1" ? true : false;
			$access[$value['MODULE']]["D"] = $value["D"] == "1" ? true : false;
		}


		$data['user'] 		= $header['user'] 		= $sidebar['user'] 	   = $user;
		$data['division'] 	= $header['division'] 	= $sidebar['division'] = $division;
		$data['access'] 	= $header['access'] 	= $sidebar['access']   = $access;
        $this->load->view('template/header',$header);
        $this->load->view('template/sidebar',$sidebar);
        $this->load->view($viewName, $data);
        $this->load->view('template/footer');
    }

    function getUserData($identifier = null){
    	$user 	= $this->session->userdata();
    	$result = "";
    	foreach ($user as $key => $value) {
    		if (strtoupper($key) == strtoupper($identifier) ) {
    			$result = $value;
    		}
    	}
    	if (empty($identifier)) {
    		return $user;
    	}
    	return $result;
    }

    #GET CONFIG
    function getConfig($type, $like = null){
    	$data = $this->db->select('*')->from('PRIME_CONFIG')->where('TYPE',$type)->order_by('VALUE','ASC');
    	
    	if (!empty($like)) {
    		if(!empty($like)){
                $data = $data->like('UPPER(VALUE)',strtoupper($like));
            }
    	}

    	return $data->get()->result_array();

    }
    #END GET CONFIG

    #GET CONFIG
    function getAllRole(){
    	return $this->db->select('*')->from('PRIME_ROLE')->where('ID != 0' )->order_by('ID','DESC')->get()->result_array();
    }

    function getRoleAccess(){
    	$q  = $this->db->select('A.NAME, MAIN.*')
    		  ->from('PRIME_ROLE_ACCESS MAIN')
    		  ->where('MAIN.ID_ROLE != 0' )
    		  ->where('MAIN.ID_ROLE != 1' )
    		  ->join("PRIME_ROLE A","MAIN.ID_ROLE = A.ID")
    		  ->order_by('A.NAME', 'ASC')
    		  ->order_by("MAIN.MODULE", "ASC")
    		  ->get()->result_array();
    
    	return $q;
    }


    #GET PARTNER
	    function getPartner($id_partner){
	    	if(empty($id_partner)){
	    		return $this->db->select('*')->from('PRIME_PARTNER')->get()->result_array();
	    	}else{
	    		return $this->db->select('*')->from('PRIME_PARTNER')->where('CODE',$id_partner)->get()->row_array();
	    	}
	    }
    #END GET PARTNER

    #END GET CONFIG

    #HASH PASSWORD 
	    function hashPassword($password){
	    	$salt = [
	                'salt' => 'inisaltpassworduntukprimeebis',
	                'cost' => 9
	            ];
	        return password_hash($password, PASSWORD_DEFAULT, $salt);
	    }
    #END HASH PASSWORD


	#START DIFF WEEK
	   function diffWeek($start, $end){
	   		$start 	= str_replace('/', '-', $start);
    		$end 	= str_replace('/', '-', $end);
		   	
		   	$day   	= 24 * 3600;
		    $from  = strtotime($start);
		    $to    = strtotime($end) + $day;
		    
		    $diff  = abs($to - $from);
		    $weeks = round($diff / $day / 7);
		    $checkMonday = date('D', $from);
		    if ($checkMonday=="Mon") {
				$weeks = $weeks+1;
			}

			if($weeks <= 0){
				$weeks = 1;
			}

			return $weeks;
	   }
	#END DIFF WEEK



	// Check if login?
	public function isLoggedIn(){
		$userid = $this->session->userdata("userid");
		if(!empty($userid)){
			return true;			
		}else{
			if($this->uri->segment(1)=="login"){
				return false;
			}else{
				redirect(base_url().'login');
			}
		}
	}


	// alert
	function alert($alert_tipe,$alert_text)
	{
		$alert = 	'<div class="alert '.$alert_tipe.'">
						'.$alert_text.'
					</div>';
		return $alert;
	}

	// generate GUID
	public function getGUID()
    {
        if (function_exists('getGUID')){
            return getGUID();
        }
        else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = //chr(123)// "{"
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
                //.chr(125);// "}"
            return $uuid;
        }
    } 

	
	// ROMANIZE
	function getMonthRomawi($bln){
                switch ($bln){
                    case '01': 
                        return "I";
                        break;
                    case '02':
                        return "II";
                        break;
                    case '03':
                        return "III";
                        break;
                    case '04':
                        return "IV";
                        break;
                    case '05':
                        return "V";
                        break;
                    case '06':
                        return "VI";
                        break;
                    case '07':
                        return "VII";
                        break;
                    case '08':
                        return "VIII";
                        break;
                    case '09':
                        return "IX";
                        break;
                    case '10':
                        return "X";
                        break;
                    case '11':
                        return "XI";
                        break;
                    case '12':
                        return "XII";
                        break;
                }
    }

 	// ENCODE URL
    function escapeChar($url) {
		   $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
		   $url = trim($url, "-");
		   $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
		   $url = strtolower($url);
		   $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
		   return $url;
		}

	function makeurl2($url) {
		   $new = str_replace(' ', '%20', $url);
		   return $new;
		}	


 ##UPLOAD FILE
	function upload_file($filename,$upload_path,$newName){
		$this->load->library('upload');
		$config['upload_path'] 	 = $upload_path;
		$config['allowed_types'] = '*';
		$config['max_size']      = 1000000;
		$config['file_name'] 	 = $newName;
		$config['remove_spaces'] = FALSE;
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($filename))
		{
		    // case - failure
		    echo $upload_path;
		    $upload_error = array('error' => $this->upload->display_errors());
		    echo json_encode($upload_error);die();
		}
		else
		{
		    // case - success
		    $upload_data = $this->upload->data();
		    //echo json_encode(value);die;
		    return $upload_data;
		}
    }

  ##CHECK ACCESS
    public function getUserTipe(){
		return $this->session->userdata('tipe_sess');		
	}

    public function check_access($module,$access)
	{
		$result = 0;
		$user 	= $this->session->userdata();
		$data 	= $this->db->from("PRIME_ROLE_ACCESS")->where("MODULE",$module)->where("ID_ROLE",$user['role_id'])->get()->row_array();

		if (!empty($data)) {
			$result = !empty($data[$access]) ? intval($data[$access]) : 0;
		}
		if ($result == 1) {
			return true;
		}else{
			return false;
		}
	}

  ##LOG
	public function addLog($id,$status,$type,$meta_data=null){
		$CI =& get_instance();
	    $CI->load->model('M_Project','m_project');
	    $CI->load->model('M_Master','master');
	    $ip = $this->input->ip_address();
	    $data = array(
            'ID_HISTORY'    => $this->getGUID(),
            'ID'            => $id,
            'STATUS'        => $status,
            'META'          => $meta_data,
            'ID_USER'       => $this->session->userdata('nik_sess'),
            'NAME_USER'     => $this->session->userdata('nama_sess'),
            'PHOTO_USER'    => !empty($this->session->userdata('photo')) ? $this->session->userdata('photo') : '',
            'TYPE'          => $type,
            'IP'			=> $ip
        ); 
	    $CI->m_project->updateProjectHistory($data);
	    return $CI->master->addLog($data);
	}


	function get_sequence($table) {
		$this->CI =& get_instance();	
		$query = $this->CI->db->query("	SELECT $table.nextVal AS ID
										FROM DUAL")->row_array();
		return $query;
	}


	public function add_credit_point($category,$title,$content,$point,$meta = null){
		$CI =& get_instance();
	    $CI->load->model('M_user','user');


	    $data_insert['ID']			= $this->getGUID();
        $data_insert['TITLE']       = $title;
        $data_insert['PIC']         = $this->session->userdata('nik_sess');
        $data_insert['CONTENT']     = $content;;
        $data_insert['POINT']       = $point;
        $data_insert['CATEGORY']    = $category;
        $data_insert['CREATED_BY']  = $this->session->userdata('nik_sess');
        $data_insert['META_DATA']   = $meta;

        if(!empty($meta)){
        	$check = $CI->user->checkCreditPoint($title,$content,$meta);
        	if($check < 1){
        		return $CI->user->addCreditPoint($data_insert);
        	}else{
        		return true;
        	}
        }
	    return $CI->user->addCreditPoint($data_insert);
	}

	function getNotification(){
		$CI =& get_instance();
	    $CI->load->model('User_model');
	    $accessArray = $CI->User_model->getNotification($this->session->userdata('nik_sess'));
	    return $accessArray[$configName];
	}

	function hari($day){
		switch ($day) {
			case 'Monday':
				return 'Senin';
				break;
			case 'Tuesday':
				return 'Selasa';
				break;
			case 'Wednesday':
				return 'Rabu';
				break;
			case 'Thursday':
				return 'Kamis';
				break;
			case 'Friday':
				return "Jum'at";
				break;
			case 'Saturday':
				return 'Sabtu';
				break;
			case 'Sunday':
				return 'Minggu';
				break;	
			default:
				return '';
				break;
		}

	}

	function ejaHari($day){
		switch ($day) {
			case '01':
				return "Satu";
				break;
			case '02':
				return "Dua";
				break;
			case '03':
				return "Tiga";
				break;
			case '04':
				return "Empat";
				break;
			case '05':
				return "Lima";
				break;
			case '06':
				return "Enam";
				break;
			case '07':
				return "Tujuh";
				break;
			case '08':
				return "Delapan";
				break;
			case '09':
				return "Sembilan";
				break;
			case '10':
				return "Sepuluh";
				break;
			case '11':
				return "Sebelas";
				break;
			case '12':
				return "Dua Belas";
				break;
			case '13':
				return "Tiga Belas";
				break;
			case '14':
				return "Empat Belas";
				break;
			case '15':
				return "Lima Belas";
				break;
			case '16':
				return "Enam Belas";
				break;
			case '17':
				return "Tujuh Belas";
			case '18':
				return "Delapan Belas";
				break;
			case '19':
				return "Sembilan Belas";
				break;
			case '20':
				return "Dua Puluh ";
				break;
			case '21':
				return "Dua Puluh Satu";
				break;
			case '22':
				return "Dua Puluh Dua";
				break;
			case '23':
				return "Dua Puluh Tiga";
				break;
			case '24':
				return "Dua Puluh Empat";
				break;
			case '25':
				return "Dua Puluh Lima";
				break;
			case '26':
				return "Dua Puluh Enam";
				break;
			case '27':
				return "Dua Puluh Tujuh";
				break;
			case '28':
				return "Dua Puluh Delapan";
				break;
			case '29':
				return "Dua Puluh Sembilan";
				break;
			case '30':
				return "Tiga Puluh";
				break;
			case '31':
				return "Tiga Puluh Satu";
				break;
			
			default:
				return "";
				break;
		}
	}

	function ejaBulan($month){
		switch ($month) {
			case '01':
				return 'Januari';
				break;
			case '02':
				return 'Februari';
				break;
			case '03':
				return 'Maret';
				break;
			case '04':
				return 'April';
				break;
			case '05':
				return 'Mei';
				break;
			case '06':
				return 'Juni';
				break;
			case '07':
				return 'Juli';
				break;
			case '08':
				return 'Agustus';
				break;
			case '09':
				return 'September';
				break;
			case '10':
				return 'Oktober';
				break;
			case '11':
				'return November';
				break;
			case '12':
				return 'Desember';
				break;
			default:
				return '';
				break;
		}
	}

	function tahun($year){
		switch ($year) {
			case '2018':
				return "Dua Ribu Delapan Belas";
				break;
			case '2019':
				return "Dua Ribu Sembilan Belas";
				break;
			case '2020':
				return "Dua Ribu Dua Puluh";
				break;
			case '2021':
				return "Dua Ribu Dua Puluh Satu";
				break;
			case '2022':
				return "Dua Ribu Dua Puluh Dua";
				break;
			
			default:
				# code...
				break;
		}
	}

	function fixNamaPerusahaan($value)
	{
		$value = strtolower($value);
		$value = str_replace("pt. ", "", $value);
		$value = str_replace("pt.", "", $value);
		$value = str_replace("pt ", "", $value);
		$value = str_replace("pt", "", $value);

		$arrValue = explode(" ", $value);

		$fixname  =  "PT. ";

		foreach ($arrValue as $key => $val) {
		 	$fixname = $fixname." ".ucfirst($val); 
		 } 

		 return $fixname;

	}

	// LOGGER
	public function loggerPrime($module,$action,$desc = null){
			$userid 	=  $this->getUserData('email');
			$division 	=  $this->getUserData('division');
			$ip  	= !empty($_SERVER['REMOTE_ADDR']) ? substr($_SERVER['REMOTE_ADDR'], 0, 51) : "";
			$ip_f  	= !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? substr($_SERVER['HTTP_X_FORWARDED_FOR'], 0, 51) : "";
			if ((is_array($desc) || is_object($desc)) && !empty($desc) ) {
				if (is_array($desc)) {
					if (!empty($desc["BEFORE"]) && !empty($desc["AFTER"])) {
						
						if (is_array($desc["BEFORE"]) && is_array($desc["AFTER"])) {
							$before = array();
							$after 	= array();
							foreach ($desc["BEFORE"] as $key => $value) {
								if (!empty($desc["AFTER"][$key])) {
									if ($value != $desc["AFTER"][$key]) {
										if (count($value) < 4000 && count($desc["AFTER"][$key]) < 4000) {
											$before[$key] = $value; 
											$after[$key]  = $desc["AFTER"][$key];
										}
									}
								}
							}
							$desc["BEFORE"] = $before;
							$desc["AFTER"]  = $after;
						}
					}
				}
				$desc = json_encode($desc);
			}

			if (empty($desc)) {
				$desc = "";
			}

			if (empty($userid)) {
				$userid = 'SYSTEM';
			}
			if (empty($division)) {
				$division = "SYSTEM";
			}

			$desc = substr($desc, 0, 3999);
            $this->db->set('ID_LOG',$this->getGUID());
            $this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
            $this->db->set('ID_USER',$userid);
            $this->db->set('MODULE', $module);
            $this->db->set('ACTION', $action);
            $this->db->set('DIVISION', $division);
            $this->db->set('DESCRIPTION', $desc);
            $this->db->set('CLIENT_IP', $ip);
            $this->db->set('CLIENT_IP_FORWARDER', $ip_f);
            $this->db->insert('PRIME_LOG');
    }

	// END LOGGER


	// AUTH
	public function doLogout(){

		// logout
		
		$this->ci->session->unset_userdata($this->SESS_USERID);
		$this->ci->session->unset_userdata($this->SESS_USERNAME);		
		$this->ci->session->unset_userdata($this->SESS_USERROLE);
		$this->ci->session->unset_userdata($this->SESS_SEGMEN);
		$this->ci->session->unset_userdata($this->SESS_DIVISION);
		$this->ci->session->unset_userdata($this->SESS_CATEGORY);
		$this->ci->session->unset_userdata($this->SESS_EMAIL);
		$this->ci->session->set_userdata(array('validated' => false));
		//session_destroy();
		return true;
	}

}