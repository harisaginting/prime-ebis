<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Bast extends CI_Model {

	function get_spk($spk, $top = null){
		if(empty($top)){
			$data = $this->db->select('A.*,B.NAME PARTNER_NAME')
					->from('PRIME_PROJECT_PARTNER A')
					->from('PRIME_PARTNER B','A.ID_PARTNER = B.CODE')
					->where('A.SPK',$spk)
					->get()->row_array();	
			$data['VALUE']		= 0;
			$data['NOTE']		= null;
			$data['KL'] 		= null;
			$data['KL_DATE'] 	= null;
			$data['KL_DATE2'] 	= null;
			$data['SPK_DATE2'] 	= null;
			$data['NOTE'] 		= null;
			$data['PROGRESS'] 	= null;
			$data['ID_TOP'] 	= null;
		}else{
			$data = $this->db->select("A.*, B.VALUE, B.NOTE, TO_CHAR(A.KL_DATE,'DD/MM/YYYY') KL_DATE2, TO_CHAR(A.SPK_DATE,'DD/MM/YYYY') SPK_DATE2, B.NOTE, B.PROGRESS, B.ID ID_TOP, B.VALUE, C.NAME PARTNER_NAME")
					->from('PRIME_PROJECT_PARTNER A')
					->join('PRIME_PROJECT_TOP_PARTNER B','A.SPK = B.SPK')
					->join('PRIME_PARTNER C','C.CODE = A.ID_PARTNER')
					->where('A.SPK',$spk)
					->where('B.ID',$top)
					->get()->row_array();
		}
		
		$data['type'] 		= null;
		$data['type_info'] 	= null;
		if(!empty($data['PROGRESS'])){
			$data['type'] 	   = 'PROGRESS';	
			$data['type_info'] = $data['PROGRESS'];	
		}

		if(!empty($data['ID_TOP']) && empty($data['PROGRESS'])){
				$termin = $this->db
								->select('A.*, ROWNUM',FALSE)
								->from('PRIME_PROJECT_TOP_PARTNER A')
								->where('A.SPK',$spk)
								->order_by('DUE_DATE')
								->get()
								->result_array();
				foreach ($termin as $key => $value) {
					if($value['ID'] == $data['ID_TOP']){
						$data['type'] 		= 'TERMIN';
						$data['type_info'] 	= $value['ROWNUM'];
					}
				}
		}
		
		$data['project'] = $this->db
    						->select("PROJECT.*, PROJECT.PROGRESS CURRENT_PROGRESS,  TO_CHAR(PROJECT.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(PROJECT.END_DATE,'DD/MM/YYYY') END_DATE2,TO_CHAR(PROJECT.DATE_UPDATED,'DD/MM/YYYY') DATE_UPDATED2, AM.NAME AM_NAME, CUSTOMER.NAME CUSTOMER_NAME, CUSTOMER.CODE CUSTOMER_CODE, CATEGORY.VALUE CATEGORY_NAME, REGIONAL.VALUE REGIONAL_NAME, SEGMEN.NAME SEGMEN_NAME, TYPE_PROJECT.VALUE TYPE_NAME, PM.NAME PM_NAME, NVL(WEIGHT.TOTAL, 0) TOTAL_WEIGHT ")
    						->from('PRIME_PROJECT PROJECT')
    						->join('PRIME_SEGMEN SEGMEN', "PROJECT.SEGMEN = SEGMEN.CODE","LEFT")
    						->join('PRIME_AM AM', "PROJECT.AM = AM.EMAIL","LEFT")
    						->join('PRIME_USER PM', "PROJECT.PM = PM.ID ", "LEFT")
    						->join('PRIME_CUSTOMER CUSTOMER', "PROJECT.CUSTOMER = CUSTOMER.CODE","LEFT")
    						->join('PRIME_CONFIG CATEGORY', "PROJECT.CATEGORY = CATEGORY.ID","LEFT")
    						->join('PRIME_CONFIG REGIONAL', "PROJECT.REGIONAL = REGIONAL.ID","LEFT")
    						->join('PRIME_CONFIG TYPE_PROJECT', "PROJECT.CATEGORY = TYPE_PROJECT.ID","LEFT")
    						->join('(SELECT SUM(WEIGHT) TOTAL , ID_PROJECT FROM PRIME_PROJECT_DELIVERABLE GROUP BY ID_PROJECT) WEIGHT', "WEIGHT.ID_PROJECT = PROJECT.ID_PROJECT","LEFT")
    						->where("PROJECT.ID_PROJECT",$data['ID_PROJECT'])
    						->get()
    						->row_array();
    	$data['project']['document'] = $this->db->get_where('PRIME_PROJECT_DOCUMENT',array('ID_PROJECT' => $data['ID_PROJECT']))->result_array();

		return $data;
	}

	function get_bast($id_bast){
		$data['bast']  = $this->db->select("MAIN.*, TO_CHAR(MAIN.BAST_DATE,'DD/MM/YYYY') BAST_DATE2, TO_CHAR(MAIN.SPK_DATE,'DD/MM/YYYY') SPK_DATE2, TO_CHAR(MAIN.KL_DATE,'DD/MM/YYYY') KL_DATE2, CUSTOMER.NAME CUSTOMER_NAME, PARTNER.NAME PARTNER_NAME")
								->from("PRIME_BAST MAIN")
								->join("PRIME_CUSTOMER CUSTOMER","MAIN.CUSTOMER = CUSTOMER.CODE","LEFT")
								->join("PRIME_PARTNER PARTNER","MAIN.PARTNER = PARTNER.CODE","LEFT")
								->where("MAIN.ID",$id_bast)
								->get()->row_array();


		if($data['bast']['STATUS'] == 'RECEIVED' || $data['bast']['STATUS'] == 'APPROVED' || $data['bast']['STATUS'] == 'DONE' || $data['bast']['STATUS'] == 'TAKE OUT'){
			$data['bast']['STATUS_NAME'] = $data['bast']['STATUS'];
		}else{
			$status_name  = $this->db
							->select('B.NAME NAME')
							->from('PRIME_BAST_APPROVAL A')
							->join('PRIME_USER B','A.ID_SIGNER = B.ID')
							->where('A.STEP',$data['bast']['STATUS'])
							->where('A.ID_BAST',$data['bast']['ID'])
							->get()->row_array();
			$data['bast']['STATUS_NAME'] = 'Check by '.$status_name['NAME'];

			$data['bast']['CURRENT_APPROVER'] = null;
			$q = $this->db
			 		->select('*')
			 		->from('PRIME_BAST_APPROVAL')
			 		->where('ID_BAST',$data['bast']['ID'])
			 		->where('STEP',intval($data['bast']['STATUS'])+1)
			 		->get()
			 		->row_array();
			 if(!empty($q['STEP'])){
			 	$data['bast']['CURRENT_APPROVER'] = $q['ID_SIGNER'];
			 }


		}
		$data['history'] = $this->db
									->select("MAIN.*, TO_CHAR(MAIN.DATE_CREATED,'DD/MM/YYYY') DATE_CREATED2, APPROVAL.NAME APPROVAL")
									->from("PRIME_BAST_HISTORY MAIN")
									->join("(SELECT * FROM PRIME_BAST_APPROVAL A JOIN PRIME_USER B ON A.ID_SIGNER = B.ID) APPROVAL","TO_CHAR(APPROVAL.STEP) = MAIN.STATUS AND APPROVAL.ID_BAST = MAIN.ID_BAST","LEFT")
									->where("MAIN.ID_BAST",$id_bast)
									->order_by("MAIN.DATE_CREATED","DESC")
									->get()
									->result_array();

									// echo json_encode($data['history']);die;

		$data['evidence'] = $this->db
									->select("MAIN.*")
									->from("PRIME_BAST_EVIDENCE MAIN")
									->where("MAIN.ID_BAST",$id_bast)
									->order_by("MAIN.VALUE","ASC")
									->get()
									->result_array();
		$data['approver'] = $this->db
									->select("MAIN.*, USER.NAME")
									->from("PRIME_BAST_APPROVAL MAIN")
									->join("PRIME_USER USER","USER.ID = MAIN.ID_SIGNER")
									->where("MAIN.ID_BAST",$id_bast)
									->order_by("MAIN.STEP","ASC")
									->get()
									->result_array();

		$data['project'] = $this->db
    						->select("PROJECT.*, PROJECT.PROGRESS CURRENT_PROGRESS,  TO_CHAR(PROJECT.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(PROJECT.END_DATE,'DD/MM/YYYY') END_DATE2,TO_CHAR(PROJECT.DATE_UPDATED,'DD/MM/YYYY') DATE_UPDATED2, AM.NAME AM_NAME, CUSTOMER.NAME CUSTOMER_NAME, CUSTOMER.CODE CUSTOMER_CODE, CATEGORY.VALUE CATEGORY_NAME, REGIONAL.VALUE REGIONAL_NAME, SEGMEN.NAME SEGMEN_NAME, TYPE_PROJECT.VALUE TYPE_NAME, PM.NAME PM_NAME, NVL(WEIGHT.TOTAL, 0) TOTAL_WEIGHT ")
    						->from('PRIME_PROJECT PROJECT')
    						->join('PRIME_SEGMEN SEGMEN', "PROJECT.SEGMEN = SEGMEN.CODE","LEFT")
    						->join('PRIME_AM AM', "PROJECT.AM = AM.EMAIL","LEFT")
    						->join('PRIME_USER PM', "PROJECT.PM = PM.ID ", "LEFT")
    						->join('PRIME_CUSTOMER CUSTOMER', "PROJECT.CUSTOMER = CUSTOMER.CODE","LEFT")
    						->join('PRIME_CONFIG CATEGORY', "PROJECT.CATEGORY = CATEGORY.ID","LEFT")
    						->join('PRIME_CONFIG REGIONAL', "PROJECT.REGIONAL = REGIONAL.ID","LEFT")
    						->join('PRIME_CONFIG TYPE_PROJECT', "PROJECT.CATEGORY = TYPE_PROJECT.ID","LEFT")
    						->join('(SELECT SUM(WEIGHT) TOTAL , ID_PROJECT FROM PRIME_PROJECT_DELIVERABLE GROUP BY ID_PROJECT) WEIGHT', "WEIGHT.ID_PROJECT = PROJECT.ID_PROJECT","LEFT")
    						->where("PROJECT.ID_PROJECT",$data['bast']['ID_PROJECT'])
    						->get()
    						->row_array();


		return $data;
	}


	function put_bast($data,$evidence,$signer,$history){
			$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
					 ->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);

			foreach ($data as $key => $value) {
					if($key=='SPK_DATE' || $key=='KL_DATE' || $key=='BAST_DATE' ){
						$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
					}else{
						if(!empty($value)){$this->db->set($key , $value);}	
					}
			}
	    	$this->db->insert('PRIME_BAST');

	    	$insert_signer 		= $this->db->insert_batch('PRIME_BAST_APPROVAL',$signer);
	    	$insert_evidence 	= $this->db->insert_batch('PRIME_BAST_EVIDENCE',$evidence);
			


			$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
					 ->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
			$this->db->insert('PRIME_BAST_HISTORY',$history);
	

	}

	function approve_bast($id_bast,$status,$note,$uuid,$filename = null,$no_bast = null){
		$update_status = null;
		if($status == 'RECEIVED' ){
			$update_status = '0';
		}elseif ($status == 'APPROVED') {
			if(!empty($filename)){
				$this->db->set('FILENAME',$filename);
			}
			$update_status = 'DONE';
		}elseif ($status == 'DONE') {
			$update_status = 'TAKE OUT';
		}else{
			
				 $q = $this->db
				 		->select('*')
				 		->from('PRIME_BAST_APPROVAL')
				 		->where('ID_BAST',$id_bast)
				 		->where('STEP',intval($status)+1)
				 		->get()
				 		->row_array();
				 if(!empty($q['STEP'])){
				 	$update_status = $q['STEP'];
				 }else{
				 	$update_status = 'APPROVED';
				 	if(!empty($no_bast)){
						$this->db->set("NO_BAST",$no_bast);
					}
				 }
		}

		$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
				 ->set('STATUS',$update_status)
				 ->set('REVISION',0);
				 
		$this->db->where('ID',$id_bast)->update('PRIME_BAST');

		$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
				 ->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
				 ->set('USER_NAME',$this->session->userdata('name'))
				 ->set('USER_ID',$this->session->userdata('userid'))
				 ->set('STATUS',$update_status)
				 ->set('COMMEND',$note)
				 ->set('ID_BAST',$id_bast)
				 ->set('ID',$uuid);
				 
	
		return $this->db->insert("PRIME_BAST_HISTORY");
	}

	function revision_bast($uuid,$id_bast,$note,$status){
		$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
				 ->set('REVISION',1)
				 ->where('ID',$id_bast)
				 ->update('PRIME_BAST');

		$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
				 ->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false)
				 ->set('USER_NAME',$this->session->userdata('name'))
				 ->set('USER_ID',$this->session->userdata('userid'))
				 ->set('STATUS',$status)
				 ->set('COMMEND',$note)
				 ->set('ID_BAST',$id_bast)
				 ->set('ID',$uuid)
				 ->set('REVISION',1);
	
	
		return $this->db->insert("PRIME_BAST_HISTORY");;
	}

	function count_bast_spk($spk){
		$data = $this->db
				->select("COUNT(1) TOTAL")
				->from('PRIME_BAST')
				->where("SPK",$spk)
				->get()->row_array();

		return $data['TOTAL']+1;		
	}

}