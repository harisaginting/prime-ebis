<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Dashboard extends CI_Model {
 
	function get_dashboard_info(){
		// Total Project
		$q 	= $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
						->where("STATUS","ACTIVE")
						->where('PM IS NOT NULL')
						->where('IS_EXIST',1)
						->from("PRIME_PROJECT");
			 	if($this->session->userdata("division") != 'EBIS'){
					$this->db->where('DIVISION',$this->session->userdata("division"));
				}

				// if($this->session->userdata('regional_id') != 1 ){
				// 	$this->db->where("REGIONAL",$this->session->userdata('regional_id'));
				// }

		$q 	= $q->get()->row_array();
		$data['total_project'] 			= intval($q['PROJECT_TOTAL']);
		$data['total_value_project'] 	= intval($q['PROJECT_VALUE']);

		$q 	= $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
						->from("PRIME_PROJECT");
			 	if($this->session->userdata("division") != 'EBIS'){
					$this->db->where('DIVISION',$this->session->userdata("division"));
				}
				// if($this->session->userdata('regional_id') != 1 ){
				// 	$this->db->where("REGIONAL",$this->session->userdata('regional_id'));
				// }
		$q 	= $q->get()->row_array();
		$data['total_all_project'] 			= intval($q['PROJECT_TOTAL']);
		$data['total_all_value_project'] 	= intval($q['PROJECT_VALUE']);


		$lastMonth =	 $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE, SUM(TOTAL_ACHIEVEMENT) ACH")
						->where("STATUS","ACTIVE")
						->where('IS_EXIST',1)
						->where('PM IS NOT NULL')
						->where("TO_CHAR(END_DATE,'MM')", date('m',strtotime('first day of -1 month')))
						->from("PRIME_PROJECT");
					 	if($this->session->userdata("division") != 'EBIS'){
							$this->db->where('DIVISION',$this->session->userdata("division"));
						}
						// if($this->session->userdata('regional_id') != 1 ){
						// 	$this->db->where("REGIONAL",$this->session->userdata('regional_id'));
						// }
		$lastMonth = $this->db->get()->row_array();
		$data['lastMonth']['total'] = intval($lastMonth['PROJECT_TOTAL']); 
		$data['lastMonth']['value'] = intval($lastMonth['PROJECT_VALUE']); 
		$data['lastMonth']['ach'] 	= 0 ;
		if(intval($lastMonth['ACH']) != 0){
			$data['lastMonth']['ach'] = round(intval($lastMonth['ACH']) / intval($lastMonth['PROJECT_TOTAL']),2);
		} 

		$currentMonth =	 $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE, SUM(TOTAL_ACHIEVEMENT) ACH")
						->where("STATUS","ACTIVE")
						->where('PM IS NOT NULL')
						->where('IS_EXIST',1)
						->where("TO_CHAR(END_DATE,'MM')", date('m'))
						->from("PRIME_PROJECT");
					 	if($this->session->userdata("division") != 'EBIS'){
							$this->db->where('DIVISION',$this->session->userdata("division"));
						}
						// if($this->session->userdata('regional_id') != 1 ){
						// 	$this->db->where("REGIONAL",$this->session->userdata('regional_id'));
						// }
		$currentMonth = $this->db->get()->row_array();
		$data['currentMonth']['total'] = intval($currentMonth['PROJECT_TOTAL']); 
		$data['currentMonth']['value'] = intval($currentMonth['PROJECT_VALUE']); 
			
		if(intval($currentMonth['ACH']) != 0){
			$data['currentMonth']['ach'] 	= round(intval($currentMonth['ACH']) / intval($currentMonth['PROJECT_TOTAL']),2);
		}


		$nextMonth =	 $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE, SUM(TOTAL_ACHIEVEMENT) ACH")
						->where("STATUS","ACTIVE")
						->where('PM IS NOT NULL')
						->where("TO_CHAR(END_DATE,'MM')", date('m',strtotime('first day of +1 month')))
						->from("PRIME_PROJECT");
					 	if($this->session->userdata("division") != 'EBIS'){
							$this->db->where('DIVISION',$this->session->userdata("division"));
						}
						// if($this->session->userdata('regional_id') != 1 ){
						// 	$this->db->where("REGIONAL",$this->session->userdata('regional_id'));
						// }
		$nextMonth = $this->db->get()->row_array();
		$data['nextMonth']['total'] = intval($nextMonth['PROJECT_TOTAL']); 
		$data['nextMonth']['value'] = intval($nextMonth['PROJECT_VALUE']);  
		$data['nextMonth']['ach'] 	= 0;
		if(intval($nextMonth['ACH']) != 0){
			$data['nextMonth']['ach'] 	= round(intval($nextMonth['ACH']) / intval($nextMonth['PROJECT_TOTAL']),2);
		}

		

		$data['group_achievement_lead'] = $data['group_achievement_lag'] = $data['group_achievement_delay'] = array();
		$progres = array("LEAD","LAG","DELAY");

		foreach ($progres as $key1 => $value1) {
				$achievement = $this->db
									->select("COUNT(1) TOTAL , TOTAL_ACHIEVEMENT ACHIEVEMENT")
									->from("PRIME_PROJECT")
									->where("STATUS","ACTIVE")
									->where('PM IS NOT NULL')
									->where('IS_EXIST')
									->where("PROGRESS",$value1)
									->group_by("TOTAL_ACHIEVEMENT")
									->get()
									->result_array();

				foreach ($achievement as $key => $value) {
					array_push($data['group_achievement_'.strtolower($value1)], array(intval($value["TOTAL"]), intval($value["ACHIEVEMENT"]) ));
				} 	
		} 




			$data['top_customer']['otc'] 	= $this->db->select("NVL(SUM(TOTAL),0) TOTALS, TRUNC(NVL(SUM(VALUEX) / 1000000000,0),2) VALUE")
											   ->from("(SELECT COUNT(1) TOTAL, SUM(VALUE) VALUEX, ID_PROJECT FROM PRIME_PROJECT_TOP_CUSTOMER 
											   			WHERE TO_CHAR(DUE_DATE,'MM') = ".date('m')."GROUP BY ID_PROJECT, VALUE) MAIN")
											   ->join("PRIME_PROJECT PROJECT", "MAIN.ID_PROJECT = PROJECT.ID_PROJECT")
											   ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$data['top_customer']['otc']->where("PROJECT.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata('regional_id') != 1 ){
			// 	$data['top_customer']['otc']->where("PROJECT.REGIONAL",$this->session->userdata('regional_id'));
			// }
			$data['top_customer']['otc'] 	= $data['top_customer']['otc']							
											   ->where("MAIN.TOTAL = 1")
											   ->get()
											   ->row_array();

		

			$data['top_customer']['termin'] = $this->db->select("NVL(SUM(TOTAL),0) TOTALS, TRUNC(NVL(SUM(VALUEX) / 1000000000,0),2) VALUE")
											   ->from("(SELECT COUNT(1) TOTAL, SUM(VALUE) VALUEX, ID_PROJECT FROM PRIME_PROJECT_TOP_CUSTOMER 
											   			WHERE TO_CHAR(DUE_DATE,'MM') = ".date('m')."GROUP BY ID_PROJECT, VALUE) MAIN")
											   ->join("PRIME_PROJECT PROJECT", "MAIN.ID_PROJECT = PROJECT.ID_PROJECT")
											   ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$data['top_customer']['termin']->where("PROJECT.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata('regional_id') != 1 ){
			// 	$data['top_customer']['termin']->where("PROJECT.REGIONAL",$this->session->userdata('regional_id'));
			// }
			$data['top_customer']['termin'] = $data['top_customer']['termin']								   
											   ->where("MAIN.TOTAL > 1")
											   ->get()
											   ->row_array();



			$data['top_partner']['otc']  	= $this->db->select("NVL(SUM(TOTAL),0) TOTALS, TRUNC(NVL(SUM(VALUEX) / 1000000000,0),2) VALUE")
											   ->from("(SELECT COUNT(1) TOTAL, SUM(VALUE) VALUEX, ID_PROJECT FROM PRIME_PROJECT_TOP_PARTNER 
											   			WHERE TO_CHAR(DUE_DATE,'MM') = ".date('m')."GROUP BY ID_PROJECT, VALUE) MAIN")
											   ->join("PRIME_PROJECT PROJECT", "MAIN.ID_PROJECT = PROJECT.ID_PROJECT")
											   ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$data['top_partner']['otc']->where("PROJECT.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata('regional_id') != 1 ){
			// 	$data['top_partner']['otc']->where("PROJECT.REGIONAL",$this->session->userdata('regional_id'));
			// }

			$data['top_partner']['otc']		= $data['top_partner']['otc']
											   ->where("MAIN.TOTAL = 1")
											   ->get()
											   ->row_array();



			$data['top_partner']['termin']  = $this->db->select("NVL(SUM(TOTAL),0) TOTALS, TRUNC(NVL(SUM(VALUEX) / 1000000000,0),2) VALUE")
											   ->from("(SELECT COUNT(1) TOTAL, SUM(VALUE) VALUEX, ID_PROJECT FROM PRIME_PROJECT_TOP_PARTNER 
											   			WHERE TO_CHAR(DUE_DATE,'MM') = ".date('m')."GROUP BY ID_PROJECT, VALUE) MAIN")
											   ->join("PRIME_PROJECT PROJECT", "MAIN.ID_PROJECT = PROJECT.ID_PROJECT");
			if($this->session->userdata("division") != 'EBIS'){
				$data['top_partner']['termin'] ->where("PROJECT.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata('regional_id') != 1 ){
			// 	$data['top_partner']['termin']->where("PROJECT.REGIONAL",$this->session->userdata('regional_id'));
			// }
			$data['top_partner']['termin'] 	= $data['top_partner']['termin'] 
											   ->where("MAIN.TOTAL > 1")
											   ->get()
											   ->row_array();



		if($data['top_customer']['otc']['VALUE'] < 1 && $data['top_customer']['otc']['VALUE'] > 0) {
			$data['top_customer']['otc']['VALUE'] = "0".$data['top_customer']['otc']['VALUE'];
		}
		if($data['top_customer']['termin']['VALUE'] < 1 && $data['top_customer']['termin']['VALUE'] > 0) {
			$data['top_customer']['termin']['VALUE'] = "0".$data['top_customer']['termin']['VALUE'];
		}

		if($data['top_partner']['otc']['VALUE'] < 1 && $data['top_customer']['termin']['VALUE'] > 0) {
			$data['top_partner']['otc']['VALUE'] = "0".$data['top_partner']['otc']['VALUE'];
		}
		if($data['top_partner']['termin']['VALUE'] < 1 && $data['top_partner']['termin']['VALUE'] > 0) {
			$data['top_partner']['termin']['VALUE'] = "0".$data['top_partner']['termin']['VALUE'];
		}

		$symptomCategory = array("TELKOM","MITRA","CUSTOMER");

		foreach ($symptomCategory as $key => $value) {
			$this->db->select("NVL(COUNT(1),0) TOTAL, CONFIG.VALUE",FALSE)
				 ->from("PRIME_PROJECT_SYMPTOM SYMPTOM")
				 ->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = SYMPTOM.ID_PROJECT")
				 ->join("PRIME_CONFIG CONFIG","CONFIG.ID = SYMPTOM.SYMPTOM")
				 ->where("PROJECT.STATUS","ACTIVE")
				 ->where('PROJECT.PM IS NOT NULL')
				 ->where('PROJECT.IS_EXIST',1)
				 ->where("CONFIG.REMARKS",$value);
			if($this->session->userdata("division") != 'EBIS'){
				$this->db->where("PROJECT.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata('regional_id') != 1 ){
			// 	$this->db->where("PROJECT.REGIONAL",$this->session->userdata('regional_id'));
			// }
			$data["top3symptom"][$value]  = $this->db->group_by(array("CONFIG.VALUE"))->get()->result_array();	
		}		
		return $data;
	}

	function dashboardExecutiveInfo($priority = null, $year_start){
		// Total Project
		$q 	= $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
						->where("STATUS","ACTIVE")
						->where('PM IS NOT NULL')
						->where('IS_EXIST',1)
						->from("PRIME_PROJECT");
			 // 	if($this->session->userdata("division") != 'EBIS'){
				// 	$this->db->where('DIVISION',$this->session->userdata("division"));
				// }

				// if($this->session->userdata('regional_id') != 1 ){
				// 	$this->db->where("REGIONAL",$this->session->userdata('regional_id'));
				// }
		if ($priority != "all") {
			$q->where("IS_STRATEGIC",$priority);
		}

		if (!empty($year_start)) {
			$q->where("START_DATE >=", "to_date('01/01/".$year_start."','DD/MM/YYYY')", false);
			$q->where("START_DATE <=", "to_date('31/12/".$year_start."','DD/MM/YYYY')", false);
		}

		$q 	= $q->get()->row_array();
		$data['t_active'] 			= intval($q['PROJECT_TOTAL']);
		$data['v_active'] 			= intval($q['PROJECT_VALUE']);

		$q 	= $this->db
						->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
						->where("STATUS","CLOSED")
						->where('IS_EXIST',1)
						->from("PRIME_PROJECT");
		if ($priority != "all") {
			$q->where("IS_STRATEGIC",$priority);
		}

		if (!empty($year_start)) {
			$q->where("START_DATE >=", "to_date('01/01/".$year_start."','DD/MM/YYYY')", false);
			$q->where("START_DATE <=", "to_date('31/12/".$year_start."','DD/MM/YYYY')", false);
		}

		$q 	= $q->get()->row_array();
		$data['t_close'] 			= intval($q['PROJECT_TOTAL']);
		$data['v_close'] 			= intval($q['PROJECT_VALUE']);


		$data['t_all']		= $data["t_active"] + $data["t_close"]; 
		$data['v_all']		= $data["v_active"] + $data["v_close"]; 


		$q 	= $this->db
					->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
					->from("PRIME_PROJECT")
					->where('IS_EXIST',1);
		$q 	= $q->get()->row_array();
		$data['total_all_project'] 			= intval($q['PROJECT_TOTAL']);
		$data['total_all_value_project'] 	= intval($q['PROJECT_VALUE']);


		$q 	= $this->db
					->select("COUNT(1) PROJECT_TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE, DIVISION")
					->from("PRIME_PROJECT")
					->where("STATUS","ACTIVE")
					->where('PM IS NOT NULL')
					->where('IS_EXIST',1)
					->group_by("DIVISION");
		if ($priority != "all") {
			$q->where("IS_STRATEGIC",$priority);
		}

		if (!empty($year_start)) {
			$q->where("START_DATE >=", "to_date('01/01/".$year_start."','DD/MM/YYYY')", false);
			$q->where("START_DATE <=", "to_date('31/12/".$year_start."','DD/MM/YYYY')", false);
		}

		$divData 	= $q->get()->result_array();
		
		$data['t_dbs'] = $data['v_dbs'] = $data['t_des'] = $data['t_des'] = $data['v_des'] = $data['t_dgs'] = $data['t_dgs'] = "0";
		foreach ($divData as $key => $value) {
			$data["t_".strtolower($value["DIVISION"])] = $value["PROJECT_TOTAL"];
			$data["v_".strtolower($value["DIVISION"])] = $value["PROJECT_VALUE"];
		}


		return $data;
	}


	function getChartBast(){
			$bast["RECEIVED"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")
									 ->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$bast["RECEIVED"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$bast["RECEIVED"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$bast["RECEIVED"] 		= $bast["RECEIVED"]						 
									 ->where("BAST.STATUS","RECEIVED")
									 ->where("REVISION","0")
									 ->where("BAUT","0")
									 ->get()
									 ->row_array()["TOTAL"];
			


			$bast["DONE"] 			= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$bast["DONE"]->where("BAST.DIVISION",$this->session->userdata("division"));									 
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$bast["DONE"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));									 
			// }
			$bast["DONE"] 			= $bast["DONE"]
									 ->where("BAST.STATUS","DONE")
									 ->where("REVISION","0")
									 ->where("BAUT","0")
									 ->get()
									 ->row_array()["TOTAL"];
			


			$bast["APPROVED"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("BAST.STATUS","APPROVED")
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){				
				$bast["APPROVED"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){				
			// 	$bast["APPROVED"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$bast["APPROVED"] 		= $bast["APPROVED"]
									 ->where("REVISION","0")
									 ->where("BAUT","0")
									 ->get()
									 ->row_array()["TOTAL"];


			$bast["TAKE_OUT"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("BAST.STATUS","TAKE_OUT")
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$bast["TAKE_OUT"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$bast["TAKE_OUT"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$bast["TAKE_OUT"] 		= $bast["TAKE_OUT"]						 
									 ->where("REVISION","0")
									 ->where("BAUT","0")
									 ->get()
									 ->row_array()["TOTAL"];


			$bast["CHECK"] 			= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where_not_in("BAST.STATUS",array("TAKE_OUT","REVISION","DONE","APPROVED","RECEIVED"))
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$bast["CHECK"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$bast["CHECK"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$bast["CHECK"] 			= $bast["CHECK"]
									 ->where("REVISION","0")
									 ->where("BAUT","0")
									 ->get()
									 ->row_array()["TOTAL"];


			$bast["REVISION"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("REVISION","1")
									 ->where('PROJECT.IS_EXIST',1)
									 ->where("BAUT","0");
			if($this->session->userdata("division") != 'EBIS'){
				$bast["REVISION"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$bast["REVISION"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$bast["REVISION"] 		= $bast["REVISION"]
									 ->get()
									 ->row_array()["TOTAL"];


			// BAUT
			$baut["RECEIVED"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("BAST.STATUS","RECEIVED")
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$baut["RECEIVED"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$baut["RECEIVED"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$baut["RECEIVED"] 		= $baut["RECEIVED"]
									 ->where("REVISION","0")
									 ->where("BAUT","1")
									 ->get()
									 ->row_array()["TOTAL"];


			$baut["DONE"] 			= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("BAST.STATUS","DONE")
									 ->where("REVISION","0")
									 ->where("BAUT","1");
			if($this->session->userdata("division") != 'EBIS'){
				$baut["DONE"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != '1'){
			// 	$baut["DONE"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$baut["DONE"] 			= $baut["DONE"]
									 ->get()
									 ->row_array()["TOTAL"];


			$baut["APPROVED"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("BAST.STATUS","APPROVED")
									 ->where('PROJECT.IS_EXIST',1)
									 ->where("REVISION","0");
			if($this->session->userdata("division") != 'EBIS'){
				$baut["APPROVED"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$baut["APPROVED"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$baut["APPROVED"] 		= $baut["APPROVED"]
									 ->where("BAUT","1")
									 ->get()
									 ->row_array()["TOTAL"];


			$baut["TAKE_OUT"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("BAST.STATUS","TAKE_OUT")
									 ->where('PROJECT.IS_EXIST',1)
									 ->where("REVISION","0");
			if($this->session->userdata("division") != 'EBIS'){
				$baut["TAKE_OUT"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			if($this->session->userdata("regional_id") != 1){
				$baut["TAKE_OUT"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			}
			$baut["TAKE_OUT"] 		= $baut["TAKE_OUT"]
									 ->where("BAUT","1")
									 ->get()
									 ->row_array()["TOTAL"];


			$baut["CHECK"] 			= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where_not_in("BAST.STATUS",array("TAKE_OUT","REVISION","DONE","APPROVED","RECEIVED"))
									 ->where("REVISION","0")
									 ->where('PROJECT.IS_EXIST',1);
			if($this->session->userdata("division") != 'EBIS'){
				$baut["CHECK"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$baut["CHECK"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$baut["CHECK"] 			= $baut["CHECK"]
									 ->where("BAST.DIVISION",$this->session->userdata("division"))
									 ->where("BAUT","1")
									 ->get()
									 ->row_array()["TOTAL"];


			$baut["REVISION"] 		= $this->db->select("COUNT(1) TOTAL")
									 ->from("PRIME_BAST BAST")->join("PRIME_PROJECT PROJECT","PROJECT.ID_PROJECT = BAST.ID_PROJECT")
									 ->where("REVISION","1")
									 ->where('PROJECT.IS_EXIST',1)
									 ->where("BAUT","1");
			if($this->session->userdata("division") != 'EBIS'){
				$baut["REVISION"]->where("BAST.DIVISION",$this->session->userdata("division"));
			}
			// if($this->session->userdata("regional_id") != 1){
			// 	$baut["REVISION"]->where("PROJECT.REGIONAL",$this->session->userdata("regional_id"));
			// }
			$baut["REVISION"] 		= $baut["REVISION"]
									 ->get()
									 ->row_array()["TOTAL"];

		$bastStatus = array("RECEIVED","REVISION","CHECK","APPROVED","DONE","TAKE_OUT");

		$data  = array();
		foreach ($bastStatus as $key => $value) {
			$data[$value] = array();
			array_push($data[$value], intval($bast[$value]));
			array_push($data[$value], intval($baut[$value]));
		}

		return $data;
	}

	function get_status_project($div = null, $priority = null, $year_start = null){
		// Status Project
		$q 	= $this->db
					->select("STATUS, COUNT(1) TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
					->from("PRIME_PROJECT")
					->where('IS_EXIST',1)
					->where('PM IS NOT NULL')
					->where("STATUS IS NOT NULL");
			   
		if (empty($div)) {
			if($this->session->userdata("division") != 'EBIS'){
				$this->db->where('DIVISION',$this->session->userdata("division"));
		   	}
		}else if ($div != "all") {
			$this->db->where('DIVISION',$div);
		}


		if ($priority != "all") {
			$this->db->where("IS_STRATEGIC",$priority);
		}

		if (!empty($year_start)) {
			$this->db->where("START_DATE >=", "to_date('01/01/".$year_start."','DD/MM/YYYY')", false);
			$this->db->where("START_DATE <=", "to_date('31/12/".$year_start."','DD/MM/YYYY')", false);
		}


		$q 	= $q->group_by("STATUS")->get()->result_array();
				
		$data = array(
			'STATUS' 	=> array_column($q, "STATUS"),
			'TOTAL' 	=> array_column($q, "TOTAL"),
			'VALUE' 	=> array_column($q, "PROJECT_VALUE")
		);
		return $data;
	}

	function get_progress_project(){
		// Progres Project
		$q 	= $this->db
					->select("PROGRESS, COUNT(1) TOTAL, TRUNC(NVL(SUM(VALUE) / 1000000000,0),2) PROJECT_VALUE")
					->from("PRIME_PROJECT")
					->where('PM IS NOT NULL')
					->where("STATUS","ACTIVE")
					->where('IS_EXIST',1)
					->where("PROGRESS IS NOT NULL");
			  if($this->session->userdata("division") != 'EBIS'){
					$this->db->where('DIVISION',$this->session->userdata("division"));
				}
			 //  if($this->session->userdata("regional_id") != 1){
				// 	$this->db->where('REGIONAL',$this->session->userdata("regional_id"));
				// }
		$q 	= $q->group_by("PROGRESS")->get()->result_array();

		$data = array(
			'PROGRESS' 	=> array_column($q, "PROGRESS"),
			'TOTAL' 	=> array_column($q, "TOTAL"),
			'VALUE' 	=> array_column($q, "PROJECT_VALUE")
		);
		return $data;
	}


	function get_category_project_value(){
	$this->db->select('CONFIGPRIME.VALUE CATEGORY_PROJECT, TRUNC(NVL(SUM(PROJECT.VALUE) / 1000000000,0),2) VALUE_PROJECT')
					->from('PRIME_CONFIG CONFIGPRIME')
					->join("PRIME_PROJECT PROJECT","PROJECT.CATEGORY = CONFIGPRIME.ID","LEFT")
					->where('PROJECT.IS_EXIST',1)
					->where("CONFIGPRIME.TYPE","TYPE_PROJECT");
					if($this->session->userdata("division") != 'EBIS'){
						$this->db->where('PROJECT.DIVISION',$this->session->userdata("division"));
					}
					// if($this->session->userdata("regional_id") != 1){
					// 	$this->db->where('PROJECT.REGIONAL',$this->session->userdata("regional_id"));
					// }
	return $this->db->group_by(array('CONFIGPRIME.VALUE'))
					->order_by("CONFIGPRIME.VALUE","ASC")
					->get()
					->result_array();
	}

	public function total_pm($division = null)
	{
		$data = $this->db
				->select('COUNT(1) TOTAL')
				->from('PRIME_USER')
				->where_in("ROLE",array(1,2));
		
		if($this->session->userdata("division") != 'EBIS'){
			$data->where('DIVISION',$this->session->userdata("division"));
		}
		// if($this->session->userdata("regional_id") != 1){
		// 	$data->where('REGIONAL',$this->session->userdata("regional_id"));
		// }

		$result = $data->get()->row_array();
		
		return $result['TOTAL'];
	}


	public function total_project($status, $division = null)
	{
		$data = $this->db
				->select('COUNT(1) TOTAL, SUM(VALUE) VALUE')
				->from('PRIME_PROJECT')
				->where('IS_EXIST',1)
				->where('PM IS NOT NULL')
				->where('STATUS',$status);
		
		if($this->session->userdata("division") != 'EBIS'){
			$data->where('DIVISION',$this->session->userdata("division"));
		}
		// if($this->session->userdata("regional_id") != 1){
		// 	$data->where('PROJECT.REGIONAL',$this->session->userdata("regional_id"));
		// }
		$result = $data->get()->row_array();
		
		return $result;
	}

	function get_chart_scale(){
		$this->db->select("SCALE name, count(*) as Y, SUM(VALUE) as V ")
					->from('PRIME_PROJECT A')
					->where('A.IS_EXIST',1)
					->where('PM IS NOT NULL')
					->where_in('A.STATUS',array('LEAD','LAG','DELAY'));
		

		if($this->session->userdata("division") != 'EBIS'){
			$data->where('DIVISION',$this->session->userdata("division"));
		}

		// if($this->session->userdata("regional_id") != 1){
		// 	$data->where('REGIONAL',$this->session->userdata("regional_id"));
		// }
						  
		return $this->db->where('EXIST',1)->group_by('SCALE')->get()->result_array();				  
	}


	function get_chart_project_scale($div = null, $priority = null, $year_start = null){
		$result = array(
					array('name' => 'ORDINARY','Y' => 0,'V' => 0),
					array('name' => 'REGULAR','Y' => 0,'V' => 0),
					array('name' => 'BIG','Y' => 0,'V' => 0),
					array('name' => 'MEGA','Y' => 0,'V' => 0)

					);
		$this->db->select("SCALE name, count(*) as Y, SUM(VALUE) as V ")
					->from('PRIME_PROJECT A')
					->where('PM IS NOT NULL')
					->where('A.IS_EXIST',1)
					->where('A.STATUS','ACTIVE');


		if (empty($div)) {
			if($this->session->userdata("division") != 'EBIS'){
				$this->db->where('A.DIVISION',$this->session->userdata("division"));
			}
		}else if ($div != 'all') {
			$this->db->where('A.DIVISION',$div);
		}
		// if($this->session->userdata("regional_id") != 1){
		// 	$this->db->where('REGIONAL',$this->session->userdata("regional_id"));
		// }
						  
		if ($priority != "all") {
			$this->db->where("IS_STRATEGIC",$priority);
		}

		if (!empty($year_start)) {
			$this->db->where("START_DATE >=", "to_date('01/01/".$year_start."','DD/MM/YYYY')", false);
			$this->db->where("START_DATE <=", "to_date('31/12/".$year_start."','DD/MM/YYYY')", false);
		}

		$data = $this->db->group_by('SCALE')->get()->result_array();				  
		
		if(!empty($data)){
			$result = $data;
		}

		return $result;
	}

	function get_chart_project_progress(){
		$result = array(
					array('name' => 'ORDINARY','Y' => 0,'V' => 0),
					array('name' => 'REGULAR','Y' => 0,'V' => 0),
					array('name' => 'BIG','Y' => 0,'V' => 0),
					array('name' => 'MEGA','Y' => 0,'V' => 0)

					);
		$this->db->select("SCALE name, count(*) as Y, SUM(VALUE) as V ")
					->from('PRIME_PROJECT A')
					->where('PM IS NOT NULL')
					->where('A.IS_EXIST',1)
					->where('A.STATUS','ACTIVE');
		if($this->session->userdata("division") != 'EBIS'){
			$data->where('A.DIVISION',$this->session->userdata("division"));
		}
		// if($this->session->userdata("regional_id") != 1){
		// 	$data->where('A.REGIONAL',$this->session->userdata("regional_id"));
		// }
						  
		$data = $this->db->group_by('SCALE')->get()->result_array();				  
		
		if(!empty($data)){
			$result = $data;
		}

		return $result;
	}

	function get_project_by_status(){
		$this->db->select("A.PROGRESS name, count(*) as Y");
		$this->db->from('PRIME_PROJECT A');
		$this->db->where_in('PROGRESS',array('LEAD','LAG','DELAY'));
		$this->db->where('STATUS','ACTIVE');
		$this->db->where('PM IS NOT NULL')->where('A.IS_EXIST',1);

		if($this->session->userdata("division") != 'EBIS'){
			$this->db->where('A.DIVISION',$this->session->userdata("division"));
		}
		// if($this->session->userdata("regional_id") != 1){
		// 	$data->where('A.REGIONAL',$this->session->userdata("regional_id"));
		// }
						  
		return $this->db->group_by('PROGRESS')->get()->result_array();				  
	}


	function get_project_by_status_type($status,$type){
		$this->db->select("count(*) T");
		$this->db->from('PRIME_PROJECT A');
		$this->db->where_in('A.PROGRESS',array('LEAD','LAG','DELAY'))->where('A.IS_EXIST',1);
		
		$this->db->where("A.PROGRESS",$status);				  
		$this->db->where("A.CATEGORY",$type);			

		if($this->session->userdata("division") != 'EBIS'){
			$data->where('A.DIVISION',$this->session->userdata("division"));
		}
		// if($this->session->userdata("regional_id") != 1){
		// 	$data->where('A.REGIONAL',$this->session->userdata("regional_id"));
		// }

		$res =  $this->db->get()->row();	
		return $res->T;			  
	}

	function getChartRegional(){
		$raw = $this->db
						->select("COUNT(1) QUANTITY, TRUNC(NVL(SUM(PROJECT.VALUE) / 1000000000,0),2) TOTAL_VALUE, CONFIG.VALUE REGIONAL_NAME")
						->from("PRIME_PROJECT PROJECT")
						->join("PRIME_CONFIG CONFIG","PROJECT.REGIONAL = CONFIG.ID")
						->where("PROJECT.STATUS","ACTIVE")
						->where('PROJECT.PM IS NOT NULL')
						->where('PROJECT.IS_EXIST',1)
						->group_by("CONFIG.VALUE")
						->order_by("CONFIG.VALUE","ASC")
						->get()
						->result_array();
		$reg = array();
		$treg = array();

		for ($i=1; $i <= 7 ; $i++) {
			$val = $qty = 0;

			foreach ($raw as $key => $value) {
			 	if($value["REGIONAL_NAME"] == "Regional ".$i){
			 		$val = floatval($value["TOTAL_VALUE"]);
			 		$qty = intval($value["QUANTITY"]);
			 	}
			 } 
			$reg["value"] 	= floatval($val) < 1 && floatval($val) > 0 ? floatval($val)."M" : floatval($val)."M";
			$reg["qty"] 	= $qty;
			array_push($treg, $reg);
		}

		$data = array(
			"row" => $raw,
			"det" => $treg
		);
		return $data;
	}


}