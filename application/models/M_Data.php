<?php if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class M_Data extends CI_Model
{
	public function __construct() {
        parent::__construct();       
    }

	function accountManager($like=null){
        $this->db->select('MAIN.*, SEGMEN.NAME SEGMEN_NAME')
        		->from("PRIME_AM MAIN")
        		->join("PRIME_SEGMEN SEGMEN","SEGMEN.CODE = MAIN.SEGMEN");

        if(!empty($like)){
                $this->db->like('UPPER(NAME)',strtoupper($like));
                $this->db->or_like('UPPER(EMAIL)',strtoupper($like));
            }
        $this->db->order_by('MAIN.NAME');
        return $this->db->get()->result_array();        
    }

    function accountManagerSegmen($email){
        $this->db->select('MAIN.SEGMEN SEGMEN, SEGMEN.NAME SEGMEN_NAME')
        		->from("PRIME_AM MAIN")
        		->join("PRIME_SEGMEN SEGMEN","SEGMEN.CODE = MAIN.SEGMEN")
        		->where("MAIN.EMAIL",$email);
        return $this->db->get()->row_array();        
    }
}