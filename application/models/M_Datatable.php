<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class M_Datatable extends CI_Model
{
    public function __construct() {
        parent::__construct();       
    }


    ## Datatable Project DASHBOARD EXECUTIVE
		var $column_orderDashboardExecutive 	= array('MAIN.NAME','MAIN.VALUE','M_DURATION','PARTNER_VALUE','PARTNER_NAME','MAIN.TOTAL_ACHIEVEMENT'); 
		var $column_searchDashboardExecutive 	= array('MAIN.ID_PROJECT','UPPER(MAIN.NAME)','UPPER(MAIN.DIVISION)','UPPER(CUSTOMER.NAME)','UPPER(PARTNER_NAME)'); 
		var $orderDashboardExecutive 			= array('MAIN.NAME', 'desc'); 

		public function _get_all_queryDashboardExecutive($scale, $progress,$partner,$regional,$division,$segmen,$customer,$priority,$year_start){

			$query = $this->db
							->select("MAIN.*, PM.NAME PM_NAME, AM.NAME AM_NAME, PARTNER_NAME, PARTNER_KL, ROUND(MONTHS_BETWEEN(MAIN.END_DATE,MAIN.START_DATE))  M_DURATION, REGIONAL.VALUE V_REGIONAL, EXTRACT(YEAR FROM MAIN.START_DATE) YEAR_START, PARTNER_VALUE, CUSTOMER.NAME AS CUSTOMER_NAME")
							->from("PRIME_PROJECT MAIN")
							->join('PRIME_PM PM','MAIN.PM = PM.ID',"LEFT")
							->join('PRIME_AM AM', "MAIN.AM = AM.EMAIL","LEFT")
							->join('PRIME_CUSTOMER CUSTOMER', "MAIN.CUSTOMER = CUSTOMER.CODE","LEFT")
							->join("(
								    SELECT ID_PROJECT, LISTAGG(NAME, ' # ') WITHIN GROUP (ORDER BY ID_PROJECT) AS PARTNER_NAME , LISTAGG(KL, '@ ') WITHIN GROUP (ORDER BY ID_PROJECT) AS PARTNER_KL , LISTAGG(NVL(VALUE,'0'), '@ ') WITHIN GROUP (ORDER BY ID_PROJECT) AS PARTNER_VALUE 
								    FROM (
								    SELECT PROJECT.ID_PROJECT, PARTNER.NAME, PROJECT.KL, PROJECT.VALUE FROM PRIME_PROJECT_PARTNER PROJECT JOIN PRIME_PARTNER PARTNER ON PROJECT.ID_PARTNER = PARTNER.CODE 
								    GROUP BY PROJECT.ID_PROJECT, PARTNER.NAME, PROJECT.KL , PROJECT.VALUE
								    ) GROUP BY ID_PROJECT
								) PARTNER", "PARTNER.ID_PROJECT = MAIN.ID_PROJECT","LEFT")
							->join("(SELECT ID, VALUE FROM PRIME_CONFIG) REGIONAL","REGIONAL.ID = MAIN.REGIONAL")
							->where("MAIN.PM IS NOT NULL")
							->where("MAIN.IS_EXIST",1)
							->where("MAIN.STATUS","ACTIVE");

			if (!empty($division) && $division != 'all') {
				$query->where("MAIN.DIVISION",$division);
			}

			if ($priority != "all") {
				$query->where("MAIN.IS_STRATEGIC",$priority);
			}

			if (!empty($year_start)) {
				$query->where("MAIN.START_DATE >=", "to_date('01/01/".$year_start."','DD/MM/YYYY')", false);
				$query->where("MAIN.START_DATE <=", "to_date('31/12/".$year_start."','DD/MM/YYYY')", false);
			}

			if(!empty($segmen)){
				$query->where("MAIN.SEGMEN",$segmen);
			}

			if(!empty($scale)){
				$query->where("MAIN.SCALE",$scale);
			}

			if(!empty($progress)){
				$query->where("MAIN.PROGRESS",$progress);
			}

			if(!empty($customer)){
				$query->wehre("MAIN.CUSTOMER",$customer);
			}

			if(!empty($partner)){
				$query->like("PARTNER_NAME",$partner);
			}
			return $query;

		}

		private function _get_datatables_queryDashboardExecutive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start){

		    $this->_get_all_queryDashboardExecutive($scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start);

		    $i = 0;

		    foreach ($this->column_searchDashboardExecutive as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchDashboardExecutive) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderDashboardExecutive[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderDashboardExecutive))
		    {	
		        $order = $this->orderDashboardExecutive;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_project_dashboard_executive($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start){
		    $this->_get_datatables_queryDashboardExecutive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_project_dashboard_executive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start){
		    $this->_get_datatables_queryDashboardExecutive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_project_dashboard_executive($scale,$progress,$partner,$regional,$division,$segmen,$customer,$priority,$year_start){
		    $this->_get_all_queryDashboardExecutive($scale, $progress, $partner, $regional,$division,$segmen,$customer,$priority,$year_start);
		    return $this->db->count_all_results();
		}
    ## End of Datatable Project DASHBOARD EXECUTIVE

    ## Datatable Project Active
		var $column_orderActive = array('MAIN.NAME','MAIN.VALUE','MAIN.TOTAL_ACHIEVEMENT'); 
		var $column_searchActive = array('MAIN.ID_PROJECT','UPPER(MAIN.NAME)','UPPER(MAIN.CUSTOMER_NAME)','UPPER(MAIN.DIVISION)'); 
		var $orderActive = array('MAIN.NAME', 'desc'); 

		public function _get_all_queryActive($scale, $progress,$partner,$regional,$priority,$segmen, $customer, $div, $group){
			// echo "_get_all_queryActive DIV : ".$div."<br>";
			$query = $this->db
							->select("MAIN.*, PARTNER_NAME")
							->distinct()
							->from("PROJECT MAIN")
							->join("(
								    SELECT ID_PROJECT, LISTAGG(NAME, ', ') WITHIN GROUP (ORDER BY ID_PROJECT) AS PARTNER_NAME, LISTAGG(CODE, ', ') WITHIN GROUP (ORDER BY ID_PROJECT) AS PARTNER_CODE 
								    FROM (
								    SELECT PROJECT.ID_PROJECT, PARTNER.NAME, PARTNER.CODE FROM PRIME_PROJECT_PARTNER PROJECT JOIN PRIME_PARTNER PARTNER ON PROJECT.ID_PARTNER = PARTNER.CODE 
								    GROUP BY PROJECT.ID_PROJECT, PARTNER.NAME, PARTNER.CODE
								    ) GROUP BY ID_PROJECT
								) PARTNER", "PARTNER.ID_PROJECT = MAIN.ID_PROJECT","LEFT")
							->where('MAIN.PM IS NOT NULL')
							->where('MAIN.STATUS','ACTIVE');

			if($this->session->userdata('division') == 'EBIS'){
				if (!empty($div)) {
					$query->where('MAIN.DIVISION', $div);
				}
			}else{
				$query->where('MAIN.DIVISION', $this->session->userdata('division'));
			}

			if(!empty($scale)){
				$query->where("SCALE",$scale);
			}

			if(!empty($progress)){
				$query->where("PROGRESS",$progress);
			}

			if(!empty($partner)){
				$query->like("UPPER(PARTNER_CODE)",trim(strtoupper($partner)));
			}

			if(!empty($segmen)){
				$query->where("SEGMEN",$segmen);
			}

			if(!empty($customer)){
				$query->where("CUSTOMER",$customer);
			}

			if(!empty($regional)){
				$query->like("MAIN.REGIONAL",$regional);
			}

			if ($priority != "all") {
				$query->where("MAIN.IS_STRATEGIC",$priority);
			}

			if (!empty($group)) {
				$query->where("MAIN.GROUP_TYPE",$group);
			}

			return $query;

		}

		private function _get_datatables_queryActive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$priority,$segmen, $customer, $div, $group){
			// echo "_get_datatables_queryActive DIV : ".$div."<br>";
		    $this->_get_all_queryActive($scale, $progress, $partner, $regional,$priority,$segmen, $customer, $div, $group);

		    $i = 0;

		    foreach ($this->column_searchActive as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchActive) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderActive[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderActive))
		    {	
		        $order = $this->orderActive;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_project_active($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$priority,$segmen, $customer, $div, $group){
		    $this->_get_datatables_queryActive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$priority,$segmen, $customer, $div, $group);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_project_active($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$priority, $segmen, $customer, $div, $group){
		    $this->_get_datatables_queryActive($searchValue, $orderColumn, $orderDir, $getOrder,$scale, $progress, $partner, $regional,$priority,$segmen, $customer, $div, $group);
		    $query = $this->db->get();
		    // echo $this->db->last_query();die;
		    return $query->num_rows();
		}

		public function count_all_table_project_active($scale,$progress,$partner,$regional,$priority,$segmen, $customer, $div,$group)
		{
		    $this->_get_all_queryActive($scale, $progress, $partner, $regional,$priority,$segmen, $customer, $div, $group);
		    return $this->db->count_all_results();
		}
    ## End of Datatable Project Active

	## Datatable Project NON PM
		var $column_orderNonpm = array('MAIN.NAME','MAIN.VALUE',null); 
		var $column_searchNonpm = array('MAIN.ID_PROJECT','UPPER(MAIN.NAME)','UPPER(CUSTOMER.NAME)'); 
		var $orderNonpm = array('MAIN.NAME', 'desc'); 

		public function _get_all_queryNonpm($status,$pm,$customer,$partner,$type,$regional,$segmen){

			$query = $this->db
							->select("MAIN.*, CUSTOMER.NAME CUSTOMER_NAME")
							->from("PRIME_PROJECT MAIN")
							->join('PRIME_CUSTOMER CUSTOMER', "MAIN.CUSTOMER = CUSTOMER.CODE","LEFT")
							->where("MAIN.IS_EXIST",1)
							->where("((MAIN.STATUS = 'ACTIVE' AND MAIN.PM IS NULL) OR STATUS = 'NON PM')");
			if($this->session->userdata('division') != 'EBIS'){
				$query->where('MAIN.DIVISION', $this->session->userdata('division'));
			}
			return $query;

		}

		private function _get_datatables_queryNonpm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){

		    $this->_get_all_queryNonpm($status,$pm,$customer,$partner,$type,$regional,$segmen);

		    $i = 0;

		    foreach ($this->column_searchNonpm as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }
		            if (count($this->column_searchNonpm) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderNonpm[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderNonpm))
		    {	
		        $order = $this->orderNonpm;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_project_nonpm($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder, $status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryNonpm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_project_nonpm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryNonpm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_project_nonpm($status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_all_queryNonpm($status,$pm,$customer,$partner,$type,$regional,$segmen);
		    return $this->db->count_all_results();
		}
    ## End of Datatable Project NON PM 

	 ## Datatable Project Candidate
		var $column_orderCandidate = array('PROJECT.NAME','PROJECT.VALUE',null); 
		var $column_searchCandidate = array('PROJECT.ID_PROJECT','UPPER(PROJECT.NAME)','UPPER(CUSTOMER.NAME)'); 
		var $orderCanidate = array('MAIN.NAME', 'desc'); 

		public function _get_all_queryCandidate($status,$pm,$customer,$partner,$type,$regional,$segmen){

			$query = $this->db
							/*->select('A.ID_PROJECT')*/
							->select("PROJECT.*, SEGMEN.NAME SEGMEN_NAME, CUSTOMER.NAME CUSTOMER_NAME, NVL(PM.NAME,'-') PM_NAME, NVL(AM.NAME,'-') AS AM_NAME, PARTNER_NAME")
							->from("PRIME_PROJECT PROJECT")
							->join("(SELECT A.ID_PROJECT, LISTAGG(B.NAME, ', ') WITHIN GROUP (ORDER BY A.ID_PROJECT) AS PARTNER_NAME FROM PRIME_PROJECT_PARTNER A JOIN PRIME_PARTNER B ON A.ID_PARTNER = B.CODE GROUP BY A.ID_PROJECT) PARTNER", "PARTNER.ID_PROJECT = PROJECT.ID_PROJECT","LEFT")
    						->join('PRIME_SEGMEN SEGMEN', "PROJECT.SEGMEN = SEGMEN.CODE","LEFT")
    						->join('PRIME_CUSTOMER CUSTOMER', "PROJECT.CUSTOMER = CUSTOMER.CODE","LEFT")
    						->join('PRIME_AM AM', "PROJECT.AM = AM.EMAIL","LEFT")
    						->join('PRIME_PM PM', "PROJECT.PM = PM.ID ", "LEFT")
							->where("PROJECT.IS_EXIST","1")
							->where('PROJECT.STATUS','CANDIDATE');

			if($this->session->userdata('division') != 'EBIS'){
				$query->where('PROJECT.DIVISION', $this->session->userdata('division'));
			}

			return $query;

		}

		private function _get_datatables_queryCandidate($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){

		    $this->_get_all_queryCandidate($status,$pm,$customer,$partner,$type,$regional,$segmen);

		    $i = 0;

		    foreach ($this->column_searchCandidate as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchCandidate) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderCandidate[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderCandidate))
		    {	
		        $order = $this->orderCanidate;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_project_candidate($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder, $status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryCandidate($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_project_Candidate($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryCandidate($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_project_Candidate($status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_all_queryCandidate($status,$pm,$customer,$partner,$type,$regional,$segmen);
		    return $this->db->count_all_results();
		}
    ## End of Datatable Project Candidate


	## Datatable Project Closed
		var $column_orderClosed = array('MAIN.NAME','MAIN.VALUE', 'MAIN.DATE_CLOSED'); 
		var $column_searchClosed = array('MAIN.ID_PROJECT','UPPER(MAIN.NAME)','UPPER(CUSTOMER.NAME)'); 
		var $orderClosed = array('MAIN.NAME', 'desc'); 
		
		public function _get_all_queryClosed($status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded){
				$query = $this->db
							->select("MAIN.*, PM.NAME PM_NAME, TO_CHAR(MAIN.DATE_CLOSED, 'DD-MM-YYYY') DATE_CLOSED2")
							->from("PRIME_PROJECT MAIN")
							->join('PRIME_USER PM','MAIN.PM = PM.ID',"LEFT")
							->join('PRIME_CUSTOMER CUSTOMER', "MAIN.CUSTOMER = CUSTOMER.CODE","LEFT")
							->where('MAIN.IS_EXIST',1)
							->where('MAIN.STATUS','CLOSED');
			if($this->session->userdata('division') != 'EBIS'){
				$query->where('MAIN.DIVISION', $this->session->userdata('division'));
			}

			if($this->session->userdata('role') == "PROJECT MANAGER" ){
				$query->where("MAIN.PM",$this->session->userdata('userid'));
			}

			if($this->session->userdata('role') == "PROJECT ADMIN" ){
				$query->where("MAIN.PM_ADMIN",$this->session->userdata('email'));
			}
	    }

	    private function _get_datatables_queryClosed($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded){

	        $this->_get_all_queryClosed($status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded);

	        $i = 0;

	        foreach ($this->column_searchClosed as $item) 
	        {
	            if ($searchValue) 
	            {

	                if ($i === 0) 
	                {
	                    $this->db->group_start(); 
	                    $this->db->like($item, $searchValue);
	                } else {
	                    $this->db->or_like($item, $searchValue);
	                }

	                if (count($this->column_searchClosed) - 1 == $i) 
	                    $this->db->group_end(); 
	            }
	            $i++;
	        }

	        if(isset($getOrder)&&$orderColumn!=null) 
	        {	
	        		
	            $this->db->order_by($this->column_orderClosed[$orderColumn], $orderDir);
	        }
	        else if(isset($this->orderClosed))
	        {
	            $order = $this->orderClosed;
	            $this->db->order_by(key($order), $orderDir);
	        }
	    }

	    function get_table_project_closed($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded){
	        $this->_get_datatables_queryClosed($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded);
	        if ($length != -1)
	            $this->db->limit($length, $start);
	        	$query = $this->db->get();
	        	// echo $this->db->last_query();exit;
	        return $query->result();
	    }

	    function count_filtered_table_closed($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded){
	        $this->_get_datatables_queryClosed($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded);
	        $query = $this->db->get();
	        return $query->num_rows();
	    }

	    public function count_all_table_project_closed($status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded){
	        $this->_get_all_queryClosed($status,$pm,$customer,$partner,$type,$regional,$segmen,$escorded);
	        return $this->db->count_all_results();
	    }
	## End of Datatable Project Closed
   

	## Datatable Bast
		var $column_orderBast 	= array('PARTNER.NAME','MAIN.STATUS','TERMIN','MAIN.VALUE'); 
		var $column_searchBast 	= array('MAIN.ID','UPPER(MAIN.SPK)'); 
		var $orderBast 			= array('MAIN.SPK', 'desc'); 

		public function _get_all_queryBast($status,$pm,$customer,$partner,$type,$regional,$segmen){

			$query = $this->db
							->select("MAIN.*,PROJECT.PROJECT_NAME, PROJECT.CUSTOMER_NAME, PARTNER.NAME PARTNER_NAME, APPROVAL.APPROVER")
							->from("PRIME_BAST MAIN")
							->join("(SELECT A.ID_PROJECT, A.SPK, B.NAME PROJECT_NAME, C.NAME CUSTOMER_NAME FROM PRIME_PROJECT_PARTNER A JOIN PRIME_PROJECT B ON A.ID_PROJECT = B.ID_PROJECT JOIN PRIME_CUSTOMER C ON C.CODE = B.CUSTOMER ) PROJECT","MAIN.SPK = PROJECT.SPK AND MAIN.ID_PROJECT = PROJECT.ID_PROJECT") 
							->join("(SELECT A.*,B.NAME APPROVER FROM PRIME_BAST_APPROVAL A JOIN PRIME_USER B ON A.ID_SIGNER = B.ID) APPROVAL","TO_CHAR(APPROVAL.STEP) = MAIN.STATUS AND APPROVAL.ID_BAST = MAIN.ID","LEFT")
							->join('PRIME_PARTNER PARTNER',"PARTNER.CODE = MAIN.PARTNER");

			if($this->session->userdata('division') != 'EBIS'){
				$query->where('MAIN.DIVISION', $this->session->userdata('division'));
			}
			return $query;

		}

		private function _get_datatables_queryBast($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){

		    $this->_get_all_queryBast($status,$pm,$customer,$partner,$type,$regional,$segmen);

		    $i = 0;

		    foreach ($this->column_searchBast as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchBast) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderBast[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderBast))
		    {	
		        $order = $this->orderBast;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_bast($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder, $status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryBast($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_bast($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryBast($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_bast($status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_all_queryBast($status,$pm,$customer,$partner,$type,$regional,$segmen);
		    return $this->db->count_all_results();
		}
    ## End of Datatable Bast

	## Datatable User
	    var $table_user = 'PRIME_USER';
	    var $column_order_user = array('MAIN.ID','MAIN.NAME','ROLE','MAIN.DIVISION','MAIN.EMAIL','MAIN.REGIONAL',null); 
	    var $column_search_user = array('UPPER(MAIN.NAME)', 'UPPER(MAIN.EMAIL)','UPPER(MAIN.DIVISION)','UPPER(MAIN.ID)'); 
	    var $order_user = array('NAME' => 'asc'); 
	    
	    public function _get_all_query_user(){
		        $data = $this->db
		        		->select("MAIN.ID ID, MAIN.NAME, EMAIL, PHONE, ROLE.NAME ROLE, DIVISION, REGIONAL.VALUE REGIONAL")
		        		->from("PRIME_USER MAIN")
		        		->join("(SELECT ID, NAME FROM PRIME_ROLE) ROLE","ROLE.ID = MAIN.ROLE")
		        		->join("(SELECT ID, VALUE FROM PRIME_CONFIG) REGIONAL","REGIONAL.ID = MAIN.REGIONAL");

		        if($this->session->userdata('division') != 'EBIS'){
					$data->where('MAIN.DIVISION', $this->session->userdata('division'));
				}
	            return $data;
	    }

	    private function _get_datatables_query_user($searchValue, $orderColumn, $orderDir, $getOrder){

	        $this->_get_all_query_user();

	        $i = 0;

	        foreach ($this->column_search_user as $item) 
	        {
	            if ($searchValue) 
	            {

	                if ($i === 0) 
	                {
	                    $this->db->group_start(); 
	                    $this->db->like($item, $searchValue);
	                } else {
	                    $this->db->or_like($item, $searchValue);
	                }

	                if (count($this->column_search_user) - 1 == $i) 
	                    $this->db->group_end(); 
	            }
	            $i++;
	        }

	        if(isset($getOrder)) 
	        {   
	                
	            $this->db->order_by($this->column_order_user[$orderColumn], $orderDir);
	        }
	        else if(isset($this->order_user))
	        {
	            $order = $this->order_user;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }

	    function get_table_user($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
	        $this->_get_datatables_query_user($searchValue, $orderColumn, $orderDir, $getOrder);
	        if ($length != -1)
	            $this->db->limit($length, $start);
	            $query = $this->db->get();
	            // echo $this->db->last_query();exit;
	        return $query->result();
	    }

	    function count_filtered_table_user($searchValue, $orderColumn, $orderDir, $getOrder){
	        $this->_get_datatables_query_user($searchValue, $orderColumn, $orderDir, $getOrder);
	        $query = $this->db->get();
	        return $query->num_rows();
	    }

	    public function count_all_table_user(){
	        $this->_get_all_query_user();
	        return $this->db->count_all_results();
	    }
	## End of User

	## Datatable customer
        var $table_customer = 'PARTNER_CUSTOMER';
        var $column_order_customer = array('CODE','NAME',null); 
        var $column_search_customer = array('UPPER(NAME)','UPPER(CODE)'); 
        var $order_customer = array('NAME' => 'asc'); 
        
        public function _get_all_query_customer(){
                $data = $this->db
                        ->select("*")
                        ->from("PRIME_CUSTOMER");
                return $data;
        }

        private function _get_datatables_query_customer($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_customer();

            $i = 0;

            foreach ($this->column_search_customer as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_customer) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_customer[$orderColumn], $orderDir);
            }
            else if(isset($this->order_customer))
            {
                $order = $this->order_customer;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_customer($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_customer($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_customer($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_customer($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_customer(){
            $this->_get_all_query_customer();
            return $this->db->count_all_results();
        }
    ## End of customer

    ## Datatable customer
        var $table_customerExecutive 			= 'PRIME_CUSTOMER_EXECUTIVE';
        var $column_order_customerExecutive 	= array('MAIN.ID','MAIN.CODE','MAIN.START_DATE','MAIN.DESCRIPTION',null); 
        var $column_search_customerExecutive 	= array('UPPER(A.NAME)','UPPER(MAIN.CODE)'); 
        var $order_customerExecutive 			= array('A.NAME' => 'asc'); 
        
        public function _get_all_query_customerExecutive(){
                $data = $this->db
                        ->select("MAIN.* , A.NAME AS NAME, TO_CHAR(MAIN.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(MAIN.END_DATE,'DD/MM/YYYY') END_DATE2")
                        ->from("PRIME_CUSTOMER_EXECUTIVE MAIN")
                		->join("PRIME_CUSTOMER A","MAIN.CODE = A.CODE");
                return $data;
        }

        private function _get_datatables_query_customerExecutive($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_customerExecutive();

            $i = 0;

            foreach ($this->column_search_customerExecutive as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_customerExecutive) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_customerExecutive[$orderColumn], $orderDir);
            }
            else if(isset($this->order_customerExecutive))
            {
                $order = $this->order_customerExecutive;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_customerExecutive($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_customerExecutive($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_customerExecutive($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_customerExecutive($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_customerExecutive(){
            $this->_get_all_query_customerExecutive();
            return $this->db->count_all_results();
        }
    ## End of customer Executive

    

    # Datatable partner
        var $table_partner= 'PRIME_PARTNER';
        var $column_order_partner= array('CODE','CODE','CFU',null,null); 
        var $column_search_partner= array('UPPER(NAME)','UPPER(CODE)'); 
        var $order_partner= array('NAME' => 'asc'); 
        
        public function _get_all_query_partner(){
                return $this->db->select("*")->from("PRIME_PARTNER");
        }

        private function _get_datatables_query_partner($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_partner();

            $i = 0;

            foreach ($this->column_search_partner as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_partner) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_partner[$orderColumn], $orderDir);
            }
            else if(isset($this->order_partner))
            {
                $order = $this->order_partner;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_partner($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_partner($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_partner($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_partner($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_partner(){
            $this->_get_all_query_partner();
            return $this->db->count_all_results();
        }
    ## End of partner

    ## Datatable segmen
        var $table_segmen = 'PRIME_SEGMEN';
        var $column_order_segmen = array('CODE','NAME',null); 
        var $column_search_segmen = array('UPPER(NAME)','UPPER(CODE)'); 
        var $order_segmen = array('NAME' => 'asc'); 
        
        public function _get_all_query_segmen(){
                $data = $this->db
                        ->select("*")
                        ->from("PRIME_SEGMEN");
                return $data;
        }

        private function _get_datatables_query_segmen($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_segmen();

            $i = 0;

            foreach ($this->column_search_segmen as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_segmen) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_segmen[$orderColumn], $orderDir);
            }
            else if(isset($this->order_segmen))
            {
                $order = $this->order_segmen;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_segmen($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_segmen($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_segmen($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_segmen($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_segmen(){
            $this->_get_all_query_segmen();
            return $this->db->count_all_results();
        }
    ## End of segmen


   	## Datatable AM
        var $table_am = 'PARTNER_AM';
        var $column_order_am = array('MAIN.NAME','MAIN.SEGMEN',null); 
        var $column_search_am = array('UPPER(MAIN.NAME)','UPPER(MAIN.EMAIL)'); 
        var $order_am = array('MAIN.NAME' => 'asc'); 
        
        public function _get_all_query_am(){
                $data = $this->db
                        ->select("MAIN.*,NVL(B.NAME,'-') SEGMEN_NAME")
                        ->from("PRIME_AM MAIN")
                        ->join("PRIME_SEGMEN B","B.CODE = MAIN.SEGMEN","LEFT");
                return $data;
        }

        private function _get_datatables_query_am($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_am();

            $i = 0;

            foreach ($this->column_search_am as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_am) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_am[$orderColumn], $orderDir);
            }
            else if(isset($this->order_am))
            {
                $order = $this->order_am;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_am($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_am($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_am($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_am($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_am(){
            $this->_get_all_query_am();
            return $this->db->count_all_results();
        }
    ## End of AM

    ## Datatable PM
        var $table_pm = 'PARTNER_AM';
        var $column_order_pm = array('MAIN.NAME','MAIN.EMAIL',null); 
        var $column_search_pm = array('UPPER(MAIN.NAME)','UPPER(MAIN.EMAIL)'); 
        var $order_pm = array('MAIN.NAME' => 'asc'); 
        
        public function _get_all_query_pm(){
                $data = $this->db
                        ->select("NVL(MAIN.EMAIL,'-') EMAIl, MAIN.NAME, MAIN.ID, MAIN.DIVISION")
                        ->from("PRIME_PM MAIN");
                return $data;
        }

        private function _get_datatables_query_pm($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_pm();

            $i = 0;

            foreach ($this->column_search_pm as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_pm) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_pm[$orderColumn], $orderDir);
            }
            else if(isset($this->order_pm))
            {
                $order = $this->order_pm;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_pm($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_pm($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_pm($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_pm($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_pm(){
            $this->_get_all_query_pm();
            return $this->db->count_all_results();
        }
    ## End of PM

    ## Datatable LOG
        var $table_log = 'PRIME_LOG';
        var $column_order_log = array('MAIN.DATE_CREATED','MAIN.ID_USER','MAIN.MODULE','MAIN.ACTION'); 
        var $column_search_log = array('UPPER(MAIN.ACTION)','UPPER(MAIN.ID_USER)','UPPER(MAIN.MODULE)'); 
        var $order_log = array('MAIN.DATE_CREATED' => 'desc'); 
        
        public function _get_all_query_log(){
                $data = $this->db
                        ->select("MAIN.*, A.NAME, TO_CHAR(MAIN.DATE_CREATED,'DD/MM/YYYY HH24:MI:SS') DATE_CREATED2")
                        ->join("PRIME_USER A", "MAIN.ID_USER = A.EMAIL")
                        ->from("PRIME_LOG MAIN");
                if($this->session->userdata('division') != 'EBIS'){
					$data->where('MAIN.DIVISION', $this->session->userdata('division'));
				}
                return $data;
        }

        private function _get_datatables_query_log($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_log();

            $i = 0;

            foreach ($this->column_search_log as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->column_search_log) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)) 
            {   
                    
                $this->db->order_by($this->column_order_log[$orderColumn], $orderDir);
            }
            else if(isset($this->order_log))
            {
                $order = $this->order_log;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        function get_table_log($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_log($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
                // echo $this->db->last_query();exit;
            return $query->result();
        }

        function count_filtered_table_log($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_log($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_log(){
            $this->_get_all_query_log();
            return $this->db->count_all_results();
        }
    ## End of LOG

    ##DASHBOARD PROJECT PARTNER
		var $column_orderDashboardProjectPartner = array('PARTNER_NAME','PROJECT_COUNT','PROJECT_VALUE',null); 
		var $column_searchDashboardProjectPartner = array('PARTNER_NAME','MAIN.ID_PARTNER'); 
		var $orderDashboardProjectPartner = array('MAIN.NAME', 'desc'); 

		public function _get_all_queryDashboardProjectPartner($status,$pm,$customer,$partner,$type,$regional,$segmen){
			$query = $this->db
							/*->select('A.ID_PROJECT')*/
							->select("MAIN.ID_PARTNER, PARTNER.NAME PARTNER_NAME, TRUNC(NVL((SUM(PROJECT.VALUE)/1000000000),0),2) PROJECT_VALUE, COUNT(ID_PARTNER) PROJECT_COUNT")
							->from("PRIME_PROJECT_PARTNER MAIN")
							->join("PRIME_PARTNER PARTNER", "PARTNER.CODE = MAIN.ID_PARTNER")
							->join("PRIME_PROJECT PROJECT", "MAIN.ID_PROJECT = PROJECT.ID_PROJECT")
							->group_by(array('MAIN.ID_PARTNER','PARTNER.NAME'))
							// ->where('PROJECT.STATUS','ACTIVE')
							->where("PROJECT.IS_EXIST","1");

			if($this->session->userdata('division') != 'EBIS'){
				$query->where('PROJECT.DIVISION', $this->session->userdata('division'));
			}				
			return $query;

		}

		private function _get_datatables_queryDashboardProjectPartner($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){

		    $this->_get_all_queryDashboardProjectPartner($status,$pm,$customer,$partner,$type,$regional,$segmen);

		    $i = 0;

		    foreach ($this->column_searchDashboardProjectPartner as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchDashboardProjectPartner) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderDashboardProjectPartner[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderDashboardProjectPartner))
		    {	
		        $order = $this->orderDashboardProjectPartner;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_dashboard_project_partner($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder, $status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryDashboardProjectPartner($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_dashboard_project_partner($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryDashboardProjectPartner($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_dashboard_project_partner($status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_all_queryDashboardProjectPartner($status,$pm,$customer,$partner,$type,$regional,$segmen);
		    return $this->db->count_all_results();
		}
    ##END DASHBOARD PROJECT PARTNER

	##DASHBOARD PROJECT PM
		var $column_orderDashboardProjectPm = array('CAST(PROJECT_VALUE as INTEGER)','PROJECT_COUNT','PROJECT_VALUE',null); 
		var $column_searchDashboardProjectPm = array('PM_NAME','MAIN.ID_PARTNER'); 
		var $orderDashboardProjectPm = array('PROJECT.VALUE', 'desc'); 

		public function _get_all_queryDashboardProjectPm($status,$pm,$customer,$partner,$type,$regional,$segmen){
			$query = $this->db
							/*->select('A.ID_PROJECT')*/
							->select("MAIN.ID, CASE WHEN LENGTH(MAIN.NAME) > 15 THEN SUBSTR(MAIN.NAME,0,18)||'...' ELSE MAIN.NAME END PM_NAME, TRUNC(NVL((SUM(PROJECT.VALUE)/1000000000),0),2) PROJECT_VALUE, COUNT(MAIN.ID) PROJECT_COUNT, USER.AVATAR")
							->from("PRIME_PM MAIN")
							->join("PRIME_PROJECT PROJECT", "MAIN.ID = PROJECT.PM")
							->join("PRIME_USER USER", "MAIN.EMAIL = USER.EMAIL",'LEFT')
							->group_by(array('MAIN.ID','MAIN.NAME','USER.AVATAR'))
							->where("PROJECT.IS_EXIST","1")
							->where("PROJECT.STATUS","ACTIVE")
							->where("PROJECT.PM IS NOT NULL");

			if($this->session->userdata('division') != 'EBIS'){
				$query->where('PROJECT.DIVISION', $this->session->userdata('division'));
			}			
			return $query;

		}

		private function _get_datatables_queryDashboardProjectPm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){

		    $this->_get_all_queryDashboardProjectPm($status,$pm,$customer,$partner,$type,$regional,$segmen);

		    $i = 0;

		    foreach ($this->column_searchDashboardProjectPm as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchDashboardProjectPm) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderDashboardProjectPm[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderDashboardProjectPm))
		    {	
		        $order = $this->orderDashboardProjectPm;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_dashboard_project_pm($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder, $status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryDashboardProjectPm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_dashboard_project_pm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryDashboardProjectPm($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_dashboard_project_pm($status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_all_queryDashboardProjectPm($status,$pm,$customer,$partner,$type,$regional,$segmen);
		    return $this->db->count_all_results();
		}
	##END DASHBOARD PROJECT PM

	##DASHBOARD PROJECT SEGMEN
		var $column_orderDashboardProjectSegmen = array('SEGMEN_NAME','PROJECT_COUNT','CAST(PROJECT_VALUE as INTEGER)',null); 
		var $column_searchDashboardProjectSegmen = array('SEGMEN_NAME','MAIN.ID_PARTNER'); 
		var $orderDashboardProjectSegmen = array('MAIN.NAME', 'desc'); 

		public function _get_all_queryDashboardProjectSegmen($status,$pm,$customer,$partner,$type,$regional,$segmen){
			$query = $this->db
							->select("MAIN.ID, MAIN.NAME SEGMEN_NAME, TRUNC(NVL((SUM(PROJECT.VALUE)/1000000000),0),2) PROJECT_VALUE, COUNT(MAIN.ID) PROJECT_COUNT")
							->from("PRIME_SEGMEN MAIN")
							->join("PRIME_PROJECT PROJECT", "MAIN.CODE = PROJECT.SEGMEN")
							->group_by(array('MAIN.ID','MAIN.NAME'))
							->where("PROJECT.IS_EXIST","1");

			if($this->session->userdata('division') != 'EBIS'){
				$query->where('PROJECT.DIVISION', $this->session->userdata('division'));
			}
			return $query;

		}

		private function _get_datatables_queryDashboardProjectSegmen($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){

		    $this->_get_all_queryDashboardProjectSegmen($status,$pm,$customer,$partner,$type,$regional,$segmen);

		    $i = 0;

		    foreach ($this->column_searchDashboardProjectSegmen as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->column_searchDashboardProjectSegmen) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->column_orderDashboardProjectSegmen[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderDashboardProjectSegmen))
		    {	
		        $order = $this->orderDashboardProjectSegmen;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_dashboard_project_segmen($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder, $status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryDashboardProjectSegmen($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_dashboard_project_segmen($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_datatables_queryDashboardProjectSegmen($searchValue, $orderColumn, $orderDir, $getOrder,$status,$pm,$customer,$partner,$type,$regional,$segmen);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_dashboard_project_segmen($status,$pm,$customer,$partner,$type,$regional,$segmen){
		    $this->_get_all_queryDashboardProjectSegmen($status,$pm,$customer,$partner,$type,$regional,$segmen);
		    return $this->db->count_all_results();
		}
	##END DASHBOARD PROJECT SEGMEN



	##MONITORING ISSUE ACTION PLAN
		var $orderMonitoringIssueAp 				= array('PROJECT.NAME','PROJECT.NAME','PROJECT.NAME');
		var $searchMonitoringIssueAp 				= array('PROJECT.NAME'); 
		var $initorderMonitoringIssueAp 			= array('PROJECT.NAME', 'desc');

		public function _get_all_query_monitoringIssueAp(){
			$query = $this->db
							->select("PROJECT.*, TO_CHAR(PROJECT.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(PROJECT.END_DATE,'DD/MM/YYYY') END_DATE2, CUSTOMER.NAME CUSTOMER_NAME, ISSUE.NAME ISSUE_NAME, ISSUE.IMPACT, TO_CHAR(ISSUE.DATE_CREATED,'DD/MM/YYYY') ISSUE_DATE, ACTION.NAME ACTION_NAME, ACTION.PIC_NAME, ACTION.PIC, TO_CHAR(ACTION.START_DATE,'DD/MM/YYYY') ACTION_START, TO_CHAR(ACTION.END_DATE,'DD/MM/YYYY') ACTION_END, PARTNER.NAME PIC2")
							->from("PRIME_PROJECT PROJECT")
							->join("(SELECT A.*, B.NAME DELIVERABLE FROM PRIME_PROJECT_ISSUE A JOIN PRIME_PROJECT_DELIVERABLE B  ON A.ID_DELIVERABLE = B.ID ) ISSUE", "ISSUE.ID_PROJECT = PROJECT.ID_PROJECT")
							->join('PRIME_PROJECT_ACTION ACTION', "ISSUE.ID = ACTION.ID_ISSUE","LEFT")
							->join('PRIME_CUSTOMER CUSTOMER', "PROJECT.CUSTOMER = CUSTOMER.CODE","LEFT")
							->join('PRIME_PARTNER PARTNER',"PARTNER.CODE = ACTION.PIC","LEFT")
							->where("PROJECT.IS_EXIST","1");

			if($this->session->userdata('division') != 'EBIS'){
				$query->where('PROJECT.DIVISION', $this->session->userdata('division'));
			}
			return $query;

		}

		private function _get_datatables_query_monitoringIssueAp($searchValue, $orderColumn, $orderDir, $getOrder){

		    $this->_get_all_query_monitoringIssueAp();

		    $i = 0;

		    foreach ($this->searchMonitoringIssueAp as $item) 
		    {
		        if ($searchValue) 
		        {

		            if ($i === 0) 
		            {
		                $this->db->group_start(); 
		                $this->db->like($item, $searchValue);
		            } else {
		                $this->db->or_like($item, $searchValue);
		            }

		            if (count($this->searchMonitoringIssueAp) - 1 == $i) 
		                $this->db->group_end(); 
		        }
		        $i++;
		    }

		    if(isset($getOrder)&&$orderColumn!=null) 
		    {	
		        $this->db->order_by($this->orderMonitoringIssueAp[$orderColumn], $orderDir);
		    }
		    else if(isset($this->orderMonitoringIssueAp))
		    {	
		        $order = $this->orderMonitoringIssueApn;
		        $this->db->order_by($order[0], $orderDir);
		    }
		}

		function get_table_monitoringIssueAp($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
		    $this->_get_datatables_query_monitoringIssueAp($searchValue, $orderColumn, $orderDir, $getOrder);
		    if ($length != -1)
		        $this->db->limit($length, $start);
		    	$query = $this->db->get();
		    
		    return $query->result();
		}

		function count_filtered_table_monitoringIssueAp($searchValue, $orderColumn, $orderDir, $getOrder){
		    $this->_get_datatables_query_monitoringIssueAp($searchValue, $orderColumn, $orderDir, $getOrder);
		    $query = $this->db->get();
		    return $query->num_rows();
		}

		public function count_all_table_monitoringIssueAp(){
		    $this->_get_all_query_monitoringIssueAp();
		    return $this->db->count_all_results();
		}
	##END MONITORING ISSUE ACTION PLAN

	##MONITORING BAST PROJECT
        var $orderMonitoringbastproject                 = array('PROJECT.NAME','PROJECT.VALUE','BAST.BAST_DATE','BAST.VALUE','BAST.STATUS');
        var $searchMonitoringbastproject                = array('PROJECT.NAME'); 
        var $initorderMonitoringbastproject             = array('PROJECT.NAME', 'desc');

        public function _get_all_query_monitoringbastproject(){
            $query = $this->db
                            ->select("PROJECT.*, TO_CHAR(PROJECT.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(PROJECT.END_DATE,'DD/MM/YYYY') END_DATE2, PARTNER.NAME PARTNER_NAME, P8.SPK P8, CUSTOMER.NAME CUSTOMER_NAME, BAST.NO_BAST, BAST.VALUE   BAST_VALUE,TO_CHAR(BAST.BAST_DATE,'DD/MM/YYYY') BAST_DATE2, APPROVAL.APPROVER, BAST.STATUS BAST_STATUS")
                            ->from("PRIME_PROJECT PROJECT")
                            ->join("PRIME_PROJECT_TOP_PARTNER P8","PROJECT.ID_PROJECT = P8.ID_PROJECT")
                            ->join('PRIME_PARTNER PARTNER',"PARTNER.CODE = P8.ID_PARTNER")
                            ->join('PRIME_BAST BAST',"BAST.SPK = P8.SPK")
                            ->join('PRIME_CUSTOMER CUSTOMER', "PROJECT.CUSTOMER = CUSTOMER.CODE")
                            ->join("(SELECT A.ID_BAST, A.STEP, B.NAME APPROVER FROM PRIME_BAST_APPROVAL A JOIN PRIME_USER B ON A.ID_SIGNER = B.ID) APPROVAL","TO_CHAR(APPROVAL.STEP) = BAST.STATUS AND APPROVAL.ID_BAST = BAST.ID ","LEFT")
                            ->where("PROJECT.IS_EXIST","1");

            if($this->session->userdata('division') != 'EBIS'){
                $query->where('PROJECT.DIVISION', $this->session->userdata('division'));
            }
            return $query;

        }

        private function _get_datatables_query_monitoringbastproject($searchValue, $orderColumn, $orderDir, $getOrder){

            $this->_get_all_query_monitoringbastproject();

            $i = 0;

            foreach ($this->searchMonitoringbastproject as $item) 
            {
                if ($searchValue) 
                {

                    if ($i === 0) 
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $searchValue);
                    } else {
                        $this->db->or_like($item, $searchValue);
                    }

                    if (count($this->searchMonitoringbastproject) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }

            if(isset($getOrder)&&$orderColumn!=null) 
            {   
                $this->db->order_by($this->orderMonitoringbastproject[$orderColumn], $orderDir);
            }
            else if(isset($this->orderMonitoringbastproject))
            {   
                $order = $this->orderMonitoringbastprojectn;
                $this->db->order_by($order[0], $orderDir);
            }
        }

        function get_table_monitoringbastproject($length, $start, $searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_monitoringbastproject($searchValue, $orderColumn, $orderDir, $getOrder);
            if ($length != -1)
                $this->db->limit($length, $start);
                $query = $this->db->get();
            
            return $query->result();
        }

        function count_filtered_table_monitoringbastproject($searchValue, $orderColumn, $orderDir, $getOrder){
            $this->_get_datatables_query_monitoringbastproject($searchValue, $orderColumn, $orderDir, $getOrder);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all_table_monitoringbastproject(){
            $this->_get_all_query_monitoringbastproject();
            return $this->db->count_all_results();
        }
    ##END MONITORING BAST PROJECT
}

  