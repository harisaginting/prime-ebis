<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class M_Login extends CI_Model
{
    
    private $password_salt;
    public function __construct()
    {
        parent::__construct();
        $this->password_salt = [
                'salt' => 'inisaltpassworduntukprimeebis',
                'cost' => 9
            ];
    }

    function validate_login($id,$pass)
    {   
        $data = $this->db
        ->select('A.*, B.NAME ROLE_NAME , C.VALUE REGIONAL_NAME')->from('PRIME_USER A')
        ->join("PRIME_ROLE B","A.ROLE = B.ID")
        ->join("PRIME_CONFIG C","A.REGIONAL = C.ID","LEFT")
        ->where('A.ID', $id)
        ->or_where('A.EMAIL', $id)
        ->get()->row_array();


        if (!empty($data)) {
            // buat array select data
            $login_password =  password_hash($pass, PASSWORD_DEFAULT, $this->password_salt);
            if ($data['PASSWORD'] === $login_password) {
                $dataSession = array(
                    'userid'        => $data['ID'],
                    'role_id'       => $data['ROLE'],
                    'role'          => $data['ROLE_NAME'],
                    'name'          => $data['NAME'],
                    'email'         => $data['EMAIL'],
                    'phone'         => $data['PHONE'],
                    'division'      => $data['DIVISION'],
                    'regional_id'   => $data['REGIONAL'], 
                    'regional'      => $data['REGIONAL_NAME'], 
                    'avatar'        => $data['AVATAR'],
                    'validated'     => true,
                );
                
                $this->session->set_userdata($dataSession);
                // echo json_encode($this->session->userdata());die; 
                return true;
            }
        }
        return false;
    }


    function registration_mitra($model){
        $this->db->insert('PRIME_USER_MITRA',$model);
    }

    function get_ex_ucs_am($nik)
    {
        $q = $this->db->query("SELECT DISTINCT EX_UCS
                               FROM ACCOUNT_TEAM_DES
                               WHERE NIK_AM='$nik'")->row_array();
        echo $q['EX_UCS'];
    }

    function checkId($value){
        $q = $this->db
                ->select('*')
                ->from('PRIME_USER_MITRA')
                ->where('ID',$value)
                ->get()->result_array();
        return count($q);
    }

    function set_session_mitra($id){
        $query = $this->db
                ->select("NAMA_PARTNER")
                ->from("PRIME_PARTNER_TATA")
                ->where("KODE_PARTNER",$id)
                ->get()
                ->row();

        if(!empty($query->NAMA_PARTNER)){
            $this->session->set_userdata(
                array('mitra_name' => $query->NAMA_PARTNER)
            );
        }
        return true;
    }

}

?>