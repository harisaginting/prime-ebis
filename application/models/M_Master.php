<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Master extends CI_Model {
    function get_all_role(){
        $query = $this->db
                    ->select("*")
                    ->from("PRIME_ROLE")
                    ->order_by("ROLE_NAME")
                    ->get()
                    ->result_array();
        return $query;
    }

    function cek_customer($code){
        $data =  $this->db
                        ->select('COUNT(1) TOTAL')
                        ->from('PRIME_CUSTOMER')
                        ->where('CODE',trim($code))
                        ->get()->row_array();
        return intval($data['TOTAL']);
    } 

    function get_customer($code=null,$id=null, $like = null, $limit = true){
        if(!empty($code)){
            return $this->db->select('*')->from('PRIME_CUSTOMER')->where('CODE',$code)->get()->row_array();
        }elseif (!empty($id)) {
             return $this->db->select('*')->from('PRIME_CUSTOMER')->where('ID',$id)->get()->row_array();
        }else{
            $this->db->select('*')->distinct()->from('PRIME_CUSTOMER');

            if(!empty($like)){
                $this->db->like('UPPER(NAME)',strtoupper($like));
            }
            $this->db->order_by('NAME');

            if ($limit) {
                $this->db->limit(10);
            }

            return $this->db->get()->result_array();
            // return $this->db->get()->result_array();
        }
    }

    function add_customer($data){
        // $data['DATE_CREATED'] = date('d/m/Y');
        $this->db->set("DATE_CREATED",'SYSDATE',FALSE);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE); 
        return $this->db->insert("PRIME_CUSTOMER",$data);
    }

    function add_customer_executive($data){
        foreach ($data as $key => $value) {
                if($key=='START_DATE' || $key=='END_DATE'){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    if(!empty($value)){$this->db->set($key , $value);}  
                }
        }
        return $this->db->insert("PRIME_CUSTOMER_EXECUTIVE");
    }

    function update_customer($id,$data){
        $this->db->where("ID",$id);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }
        return $this->db->update("PRIME_CUSTOMER",$data);
    }

    function update_customer_executive($id,$data){
        $this->db->where("ID",$id);
        foreach ($data as $key => $value) {
                if($key=='START_DATE' || $key=='END_DATE'){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    if(!empty($value)){$this->db->set($key , $value);}  
                }
        }
        return $this->db->update("PRIME_CUSTOMER_EXECUTIVE");
    }

    function delete_customer($code){
        $cek = $this->db
                ->select('*')
                ->from('PRIME_PROJECT')
                ->where('CUSTOMER',$code)
                ->get()
                ->result_array();
        if (!empty($cek)) {
            return false;
        }else{
            $this->db->where("CODE",$code)->delete("PRIME_CUSTOMER");
            $this->db->where("CODE",$code)->delete("PRIME_CUSTOMER_EXECUTIVE");
            return true;
        }
    }

    function delete_customer_executive($id){
        $this->db->where("ID",$id)->delete("PRIME_CUSTOMER_EXECUTIVE");
        return true;
    }

    function get_user($like = null,$email = null, $role =null){
        $this->db->select('*')->from('USERS');
            if (!empty($email)) {
                $this->db->where('UPPER(EMAIL)',strtoupper($email));
            }

            if (!empty($role)) {
                 $this->db->where('ROLE',$role);
            }

            if (!empty($like)) {
                $this->db->where("(UPPER(NAME) LIKE '%".strtoupper($like)."%' ESCAPE '!' OR UPPER(EMAIL) LIKE '%".strtoupper($like)."%' ESCAPE '!' )");
            }

            if($this->session->userdata('division') != 'EBIS'){
                $this->db->where("(DIVISION = 'EBIS' OR DIVISION = '".$this->session->userdata('division')."')");
            }

            return $this->db->get()->result_array();
    }

    function check_userId($id){
        $this->db->select('*')->from('PRIME_USER');
        $this->db->where('UPPER(ID)',strtoupper($id));
        return $this->db->get()->row_array();
    }

     function check_userEmail($email){
        $this->db->select('*')->from('PRIME_USER');
        $this->db->where('UPPER(EMAIL)',strtoupper($email));
        return $this->db->get()->row_array();
    }


    function get_partner($code=null,$like = null,$id = null){
        if(!empty($code)){
            return $this->db->select('*')->from('PRIME_PARTNER')->where('CODE',$code)->get()->row_array();
        }elseif (!empty($id)) {
            return $this->db->select('*')->from('PRIME_PARTNER')->where('ID',$id)->get()->row_array();
        }else{
            $this->db->select('*')->from('PRIME_PARTNER');
            if (!empty($like)) {
                $this->db->like('UPPER(NAME)',strtoupper($like));
                $this->db->or_like('UPPER(CODE)',strtoupper($like));
            }
            $this->db->order_by('NAME');
            return $this->db->get()->result_array();
        }
    }

    function get_spk($spk=null,$like = null){
        $this->db
                ->select('A.*, B.NAME PARTNER_NAME')
                ->from('PRIME_PROJECT_PARTNER A')
                ->join('PRIME_PARTNER B','A.ID_PARTNER = B.CODE')
                ->join('PRIME_PROJECT C','A.ID_PROJECT = C.ID_PROJECT');

        if(!empty($code)){
                $this->db->where('SPK',$code)->get()->row_array();
        }else{
            if (!empty($like)) {
                $this->db->like('UPPER(B.NAME)',strtoupper($like));
                $this->db->or_like('UPPER(A.SPK)',strtoupper($like));
            }
            return $this->db->get()->result_array();
        }
    }


    function get_spk_top_partner($spk,$like = null){
        $this->db
                ->select('A.*, B.NAME PROJECT_NAME')
                ->from('PRIME_PROJECT_TOP_PARTNER A')
                ->join('PRIME_PROJECT B','A.ID_PROJECT = B.ID_PROJECT')
                ->where('A.SPK',$spk);

        if (!empty($like)) {
            $this->db->like('UPPER(B.NAME)',strtoupper($like));
            $this->db->or_like('UPPER(A.SPK)',strtoupper($like));
        }
        return $this->db->get()->result_array();
        
    }

     function get_deliverable($id_project,$like = null, $id = null){
        $this->db
                ->select("A.*, TO_CHAR(START_DATE,'dd/mm/yyyy') START_DATE2 , TO_CHAR(END_DATE,'dd/mm/yyyy') END_DATE2")
                ->from('PRIME_PROJECT_DELIVERABLE A')
                ->where('A.ID_PROJECT',$id_project);

        if (!empty($like)) {
            $this->db->like('UPPER(A.NAME)',strtoupper($like));
            $this->db->or_like('UPPER(A.PIC_NAME)',strtoupper($like));
        }

        if(!empty($id)){
            return $this->db->where('ID',$id)->get()->row_array();
        }

        return $this->db->get()->result_array();
    }

    function get_issue($id_project,$like = null,$id=null){
        $this->db
                ->select("A.*, D.NAME DELIVERABLE_NAME, TO_CHAR(A.DATE_CREATED,'dd/mm/yyyy') DATE2")
                ->from('PRIME_PROJECT_ISSUE A')
                ->join('PRIME_PROJECT_DELIVERABLE D',"A.ID_DELIVERABLE = D.ID","LEFT")
                ->where('A.ID_PROJECT',$id_project);

        if (!empty($id)) {
            $this->db->where('A.ID', $id);
            return $this->db->distinct()->get()->row_array();
        }

        if (!empty($like)) {
            $this->db->like('UPPER(A.NAME)',strtoupper($like));
        }
        return $this->db->get()->result_array();
    }

    function get_action($id_project,$like = null,$id=null){
        $this->db
                ->select("A.*, TO_CHAR(A.START_DATE,'dd/mm/yyyy') START_DATE2, TO_CHAR(A.END_DATE,'dd/mm/yyyy') END_DATE2, B.NAME ISSUE_NAME")
                ->from('PRIME_PROJECT_ACTION A')
                ->join("PRIME_PROJECT_ISSUE B","A.ID_ISSUE = B.ID","LEFT")
                ->where('A.ID_PROJECT',$id_project);

        if (!empty($id)) {
            $this->db->where('A.ID', $id);
            return $this->db->distinct()->get()->row_array();
        }

        if (!empty($like)) {
            $this->db->like('UPPER(A.NAME)',strtoupper($like));
        }
        return $this->db->get()->result_array();
    }

    function cek_partner($code){
        $data =  $this->db
                        ->select('COUNT(1) TOTAL')
                        ->from('PRIME_PARTNER')
                        ->where('CODE',trim($code))
                        ->get()->row_array();
        return intval($data['TOTAL']);
    }

    function add_partner($data){
        // $data['DATE_CREATED'] = date('d/m/Y');
        $this->db->set("DATE_CREATED",'SYSDATE',FALSE);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        return $this->db->insert("PRIME_PARTNER",$data);
    }

    function update_partner($id,$data){
        $this->db->where("ID",$id);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }
        return $this->db->update("PRIME_PARTNER",$data);
    }

    function delete_partner($code){
        $this->db->where("CODE",$code);
        return $this->db->delete("PRIME_PARTNER");
    }

    function get_segmen($code=null,$id=null,$like=null){
        if(!empty($code)){
            return $this->db->select('*')->from('PRIME_SEGMEN')->where('CODE',$code)->get()->row_array();
        }elseif (!empty($id)) {
            return $this->db->select('*')->from('PRIME_SEGMEN')->where('ID',$id)->get()->row_array();
        }else{
            $this->db->select('*')->distinct()->from('PRIME_SEGMEN');

            if(!empty($like)){
                $this->db->like('UPPER(NAME)',strtoupper($like));
                $this->db->or_like('UPPER(CODE)',strtoupper($like));
            }
            $this->db->order_by('NAME');
            return $this->db->get()->result_array();
        }
    }

    function delete_segmen($code){
        $this->db->where("CODE",$code);
        return $this->db->delete("PRIME_SEGMEN");
    }

    function cek_pm($email){
        $data =  $this->db
                        ->select('COUNT(1) TOTAL')
                        ->from('PRIME_PM')
                        ->where("EMAIL",trim($email))
                        ->get()->row_array();
        return intval($data['TOTAL']);
    }

    function add_pm($data){
        $this->db->set("DATE_CREATED",'SYSDATE',FALSE);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        return $this->db->insert("PRIME_PM",$data);
    }

    function update_pm($id,$data){
        $this->db->where("ID",$id);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }
        return $this->db->update("PRIME_PM",$data);
    }


    function cek_am($email){
        $data =  $this->db
                        ->select('COUNT(1) TOTAL')
                        ->from('PRIME_AM')
                        ->where('EMAIL',trim($email))
                        ->get()->row_array();
        return intval($data['TOTAL']);
    }

    function get_am($segmen=null,$email=null,$like=null){
        $this->db->select('MAIN.*, SEGMEN.NAME SEGMEN_NAME')->from("PRIME_AM MAIN");
        $this->db->join("PRIME_SEGMEN SEGMEN","SEGMEN.CODE = MAIN.SEGMEN","LEFT");
        if(!empty($segmen)){
            $this->db->where('SEGMEN',$segmen);
        }

        if(!empty($email)){
            return $this->db->where('EMAIL',$email)->get()->row_array();
        }

        if(!empty($like)){
                $this->db->like('UPPER(NAME)',strtoupper($like));
                $this->db->or_like('UPPER(EMAIL)',strtoupper($like));
            }
        $this->db->order_by('MAIN.NAME');
        return $this->db->get()->result_array();

        
    }

    function delete_am($email){
        $this->db->where("EMAIL",$email);
        return $this->db->delete("PRIME_AM");
    }

    function get_pm($email=null, $like = null,$id=null){
        $this->db->select('*')->distinct()->from("PRIME_PM");

        if(!empty($email)){
            return $this->db->where('EMAIL',$email)->get()->row_array();
        }

        if(!empty($id)){
            return $this->db->where('ID',$id)->get()->row_array();
        }

        if(!empty($like)){
            $this->db->like('UPPER(NAME)',strtoupper($like));
            $this->db->or_like('UPPER(EMAIL)',strtoupper($like));
        }

        if($this->session->userdata('role_id') == "3"){
            $this->db->where('UPPER(EMAIL)',strtoupper($this->session->userdata('email')));
        }

        $this->db->order_by('NAME');
        return $this->db->get()->result_array();
        
    }

    function delete_pm($id){
        $this->db->where("ID",$id);
        return $this->db->delete("PRIME_PM");
    }


    function cek_segmen($code){
        $data =  $this->db
                        ->select('COUNT(1) TOTAL')
                        ->from('PRIME_SEGMEN')
                        ->where('CODE',trim($code))
                        ->get()->row_array();
        return intval($data['TOTAL']);
    }

    function add_segmen($data){
        $this->db->set("DATE_CREATED",'SYSDATE',FALSE);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        return $this->db->insert("PRIME_SEGMEN",$data);
    }

    function update_segmen($id,$data){
        $this->db->where("ID",$id);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }
        return $this->db->update("PRIME_SEGMEN",$data);
    }

    function add_am($data){
        $this->db->set("DATE_CREATED",'SYSDATE',FALSE);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        return $this->db->insert("PRIME_AM",$data);
    }

    function update_am($id,$data){
        $this->db->where("ID",$id);
        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }
        return $this->db->update("PRIME_AM",$data);
    }

    function get_all_data(){
        return $this->db->select('*')->from('PRIME_SEGMEN')->get()->result_array();
   }

   function update_id_data($old,$new){
    // $this->db->set('PM',$new)->where('PM',$old)->update('PRIME_PROJECT');
    return   $this->db->set('ID',$new)->where('ID',$old)->update('PRIME_SEGMEN');
   }

   function saveConfigurationEmail($data){
    foreach ($data as $key => $value) {
        if(!empty($value)){
            $this->db->set("REMARKS" , $value);
            $this->db->where("VALUE",$key);
            $this->db->where("TYPE","EMAIL");
            $this->db->update("PRIME_CONFIG");
        }
    }
    return true;

    } 
    

}