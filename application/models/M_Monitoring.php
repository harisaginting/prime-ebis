<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Monitoring extends CI_Model {

	function get_all_pm(){
		$this->db
					->select("MAIN.NAME, MAIN.EMAIL, MAIN.PHONE, MAIN.ID, USER.AVATAR, REGIONAL.VALUE REGIONAL_NAME")
					->from("PRIME_PM MAIN")
					->distinct()
					->join("PRIME_USER USER","USER.EMAIL  = MAIN.EMAIL","LEFT")
					->join("PRIME_CONFIG REGIONAL","MAIN.REGIONAL = REGIONAL.ID","LEFT")
					->join("PRIME_PROJECT PROJECT","PROJECT.PM = MAIN.ID")
					->where("PROJECT.STATUS","ACTIVE")
					->where("PROJECT.IS_EXIST","1")
					->where("PROJECT.PM IS NOT NULL");
					if($this->session->userdata('division') != 'EBIS'){
						$this->db->where('PROJECT.DIVISION', $this->session->userdata('division'));
					}
		return $this->db->order_by("MAIN.NAME","ASC")
					->get()->result_array();

	}

	function get_pm_project($pm){ 
							$this->db
    						->select("PROJECT.*, PROJECT.PROGRESS CURRENT_PROGRESS,  TO_CHAR(PROJECT.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(PROJECT.END_DATE,'DD/MM/YYYY') END_DATE2,TO_CHAR(PROJECT.DATE_UPDATED,'DD/MM/YYYY') DATE_UPDATED2, AM.NAME AM_NAME, CUSTOMER.NAME CUSTOMER_NAME, CUSTOMER.CODE CUSTOMER_CODE, CATEGORY.VALUE CATEGORY_NAME, REGIONAL.VALUE REGIONAL_NAME, SEGMEN.NAME SEGMEN_NAME, TYPE_PROJECT.VALUE TYPE_NAME, PM.NAME PM_NAME, NVL(WEIGHT.TOTAL, 0) TOTAL_WEIGHT ")
    						->from('PRIME_PROJECT PROJECT')
    						->join('PRIME_SEGMEN SEGMEN', "PROJECT.SEGMEN = SEGMEN.CODE","LEFT")
    						->join('PRIME_AM AM', "PROJECT.AM = AM.EMAIL","LEFT")
    						->join('PRIME_PM PM', "PROJECT.PM = PM.ID ", "LEFT")
    						->join('PRIME_CUSTOMER CUSTOMER', "PROJECT.CUSTOMER = CUSTOMER.CODE","LEFT")
    						->join('PRIME_CONFIG CATEGORY', "PROJECT.CATEGORY = CATEGORY.ID","LEFT")
    						->join('PRIME_CONFIG REGIONAL', "PROJECT.REGIONAL = REGIONAL.ID","LEFT")
    						->join('PRIME_CONFIG TYPE_PROJECT', "PROJECT.CATEGORY = TYPE_PROJECT.ID","LEFT")
    						->join('(SELECT SUM(WEIGHT) TOTAL , ID_PROJECT FROM PRIME_PROJECT_DELIVERABLE GROUP BY ID_PROJECT) WEIGHT', "WEIGHT.ID_PROJECT = PROJECT.ID_PROJECT","LEFT")
    						->where("PROJECT.PM IS NOT NULL")
    						->where("PROJECT.STATUS","ACTIVE");

    						if(!empty($pm)){
    							$this->db->where("PROJECT.PM",$pm);
    						};
    	
    	$data['project'] = $project = $this->db->get()->result_array();
    	foreach ($project as $key => $value) {
    		$data['project'][$key]['kurva'] = $this->getChartProgress($value['ID_PROJECT'],$value['START_DATE2'],$value['VALUE']);
    	}

    	return $data;
	}


	function getChartProgress($id_project,$start_date,$value_project) {

			// TOP
    		$top_customer = $this->db->select("A.*, TO_CHAR(A.DUE_DATE,'DD/MM/YYYY') DUE_DATE2")
    								->from("PRIME_PROJECT_TOP_CUSTOMER A")
    								->where("ID_PROJECT",$id_project)
    								->get()->result_array();

    		foreach ($top_customer as $key => $value) {
    			$top_customer[$key]['WEEK'] = $this->diffWeek($start_date,$value['DUE_DATE2']);
    		}

			$current_date 	= date('d-m-Y'); 
	    	$start 			= str_replace('/', '-', $start_date);

	    	$day   = 24 * 3600;
		    $from  = strtotime($start);
		    $to    = strtotime($current_date) + $day;
		    $diff  = abs($to - $from);
		    $current_week = round($diff / $day / 7);
		    $checkMonday = date('D', $from);
		    if ($checkMonday=="Mon") {
				$current_week = $current_week+1;
			}

			$query = $this->db->query("	SELECT 'WEEK #'||WEEK WEEKS, NVL(PLAN,0) PLAN, NVL(ACHIEVEMENT,0) ACHIEVEMENT
										FROM PRIME_PROJECT_PLAN
										WHERE ID_PROJECT = '$id_project' 
										ORDER BY WEEK ASC");
			
			$data = array(
					'WEEK' => array_column($query->result_array(), "WEEKS"),
					'PLAN' => array_column($query->result_array(), "PLAN"),
					'ACHIEVEMENT' => array_column($query->result_array(), "ACHIEVEMENT"),
				);
			$plan 	= 0;
			$ach 	= 0;
			$top 	= 0;
			$data['TOP'] = $data['DAYS'] = array();
			foreach ($data['WEEK'] as $key => $value) {
				$plan 	= $plan + $data['PLAN'][$key];
				$ach 	= $ach + $data['ACHIEVEMENT'][$key];
				$data['PLAN'][$key] = intval($plan);
				$data['ACHIEVEMENT'][$key] = intval($ach);
				
				foreach ($top_customer as $key1 => $value1) {
					if($key == ($value1['WEEK']-1)){
						$top = $top + (($value1['VALUE'] / $value_project) * 100);
					}
					
				}
				$data['TOP'][$key] = $top;

				$date = $this->db->query("SELECT TO_CHAR(TO_DATE('".$start_date."','DD/MM/YYYY') + (7*".$key."), 'DD/MM/YYYY') AS TANGGAL FROM DUAL ")->row_array();
				$data['DAYS'][$key] = $date['TANGGAL'];
				
			}
			return $data;
		}

	function diffWeek($start, $end){
	   		$start 	= str_replace('/', '-', $start);
    		$end 	= str_replace('/', '-', $end);
		   	
		   	$day   	= 24 * 3600;
		    $from  = strtotime($start);
		    $to    = strtotime($end) + $day;
		    
		    $diff  = abs($to - $from);
		    $weeks = round($diff / $day / 7);
		    $checkMonday = date('D', $from);
		   
			if($weeks <= 0){
				$weeks = 1;
			}

			return $weeks;
	   }
}