  <?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class M_Project extends CI_Model
{
    public function __construct() {
        parent::__construct();       
    }

    function getProjectData($id_project){
        return $this->db->from('PROJECT')->where("ID_PROJECT",$id_project)->get()->row_array();
    }

    function getProjectPartner($id_project){
        return $this->db
                        ->from('PROJECT_PARTNER')->where('ID_PROJECT',$id_project)->order_by('SPK_DATE','ASC')
                        ->get()->result_array();
    }

    function getProjectDelivrable($id_project){
        return $this->db
                        ->from('PROJECT_DELIVERABLE')->where('ID_PROJECT',$id_project)->order_by('START_DATE','ASC')
                        ->get()->result_array();
    }

    function getProjectTopCustomer($id_project){
        return $this->db
                        ->select("TOP.*,  TO_CHAR(TOP.DUE_DATE,'DD/MM/YYYY') DUE_DATE2")
                        ->from('PRIME_PROJECT_TOP_CUSTOMER TOP')
                        ->where('ID_PROJECT',$id_project)
                        ->order_by("TOP.DUE_DATE","ASC")
                        ->get()->result_array();
    }
    
    function getProject($id_project){
    	$data['project']             = $this->getProjectData($id_project);
        $data['top_customer']        = $this->getProjectTopCustomer($id_project);
        $data['deliverable']         = $this->getProjectDelivrable($id_project);
    	$data['project']['document'] = $this->db->get_where('PRIME_PROJECT_DOCUMENT',array('ID_PROJECT' => $id_project))->result_array();

    	$partner              = $this->getProjectPartner($id_project); 
    	$data['partner'] 	  = array();
    	foreach ($partner as $key => $value) {
    		$top_partner  = $this->db
    							->select("TOP.*,  TO_CHAR(TOP.DUE_DATE,'DD/MM/YYYY') DUE_DATE2 ")
    							->from('PRIME_PROJECT_TOP_PARTNER TOP')
    							->where('ID_PROJECT',$id_project)
                                ->where('SPK',$value['SPK'])
    							->order_by('DUE_DATE','ASC')
    							->get()
    							->result_array();
    		$partner[$key]['TOP'] = $top_partner;
            $bast_partner   = $this->db
                                ->select("BAST.*, TO_CHAR(BAST.BAST_DATE,'DD MONTH YYYY')  BAST_DATE2, APPROVAL.NAME APPROVAL_NAME")
                                ->from("PRIME_BAST BAST")
                                ->join("(SELECT * FROM PRIME_BAST_APPROVAL A JOIN PRIME_USER B ON A.ID_SIGNER = B.ID) APPROVAL","TO_CHAR(APPROVAL.STEP) = BAST.STATUS AND APPROVAL.ID_BAST = BAST.ID","LEFT")
                                ->where("BAST.SPK",$value["SPK"])
                                ->get()->result_array();
            $partner[$key]["BAST"] = $bast_partner;
    	}


    	$data['partner'] 	= $partner;
    	$data['pm'] 		= array();
        $data['am']         = array();
        $data['pm_admin']   = array();
    	if(!empty($data['project']['PM'])){
    		$data['pm'] = $this->db->select('MAIN.*, USERS.AVATAR')
                            ->from("PRIME_PM MAIN")
                            ->join("USERS","MAIN.EMAIL = USERS.EMAIL","LEFT")
                            ->where("MAIN.ID", $data['project']['PM'])
                            ->get()
                            ->row_array();
    	}

        if (!empty($data['project']['AM'])){
            $data['am'] = $this->db->select('MAIN.*, USER.AVATAR')
                            ->from("PRIME_AM MAIN")
                            ->join("USERS USER","MAIN.EMAIL = USER.EMAIL","LEFT")
                            ->where("MAIN.EMAIL", $data['project']['AM'])
                            ->get()
                            ->row_array();
            if (empty($data['am'])) {
                $data['am'] = $this->db->select('*')
                            ->from("USERS")
                            ->where("EMAIL", $data['project']['AM'])
                            ->get()
                            ->row_array();
            }
        }        

        if (!empty($data['project']['PM_ADMIN'])){
            $data['pm_admin'] = $this->db->select('*')
                            ->from("USERS")
                            ->where("EMAIL", $data['project']['PM_ADMIN'])
                            ->get()
                            ->row_array();     
        }

        $id_deliverable = array();
        foreach ($data['deliverable'] as $key => $value) {
            array_push($id_deliverable, $value['ID']);
            $data['achievement'][$value['ID']] = array();
        }

        if (!empty($id_deliverable)) {
            $achievement = $this->db->select("MAIN.*, TO_CHAR(MAIN.DATE_ACHIEVED,'DD MONTH YYYY')  DATE_ACHIEVED2, 
                            TO_CHAR(MAIN.DATE_ACHIEVED,'DD/MM/YYYY')  DATE_ACHIEVED3")
                            ->from("PRIME_PROJECT_ACHIEVEMENT MAIN")
                            ->where_in("MAIN.ID_DELIVERABLE", $id_deliverable)
                            ->get()->result_array();

            foreach ($achievement as $key => $value) {
                array_push($data['achievement'][$value['ID_DELIVERABLE']], $value);
            }
        }
        $data['issue']  = $this->db->from('PRIME_PROJECT_ISSUE')->where('ID_PROJECT',$id_project)
                                    ->order_by('NAME')->get()->result_array();
        $data['action'] = $this->db->get_where('PROJECT_ISSUE_ACTION',array('ID_PROJECT' => $id_project))->result_array();
    	
        foreach ($data['issue'] as $key => $value) {
            $data['issue'][$key]['action'] = array();
            foreach ($data['action'] as $key1 => $value1) {
                if($value1['ID_ISSUE'] == $value['ID']){
                    array_push($data['issue'][$key]['action'], $value1);
                }
            }
        }
        $data['symptom'] = $this->db->from("PROJECT_SYMPTOM")->where("ID_PROJECT",$id_project)
                                    ->order_by("DATE_CREATED","DESC")->get()->result_array();
    	return $data;
    }

     function deleteProject($id_project){
        $this->db->trans_start();
            $this->db->where('ID_PROJECT',$id_project)->set('IS_EXIST',0)->update('PRIME_PROJECT');
        return $this->db->trans_complete();
     }

    function update_progress($id_project){
        $this->db->trans_start();
        $this->db->where("ID_PROJECT",$id_project)->delete("PRIME_PROJECT_PLAN");
            $totalWeek      = $this->getProjectDate($id_project);
            $totalWeekPlan  = $this->db->select("MAX(WEEK) MAXWEEK")->from("PRIME_PROJECT_PLAN_DELIVERABLE")->where("ID_PROJECT",$id_project)->get()->row_array();

            $longweek = $totalWeek["TOTAL_WEEK"];
            if($longweek <  intval($totalWeekPlan['MAXWEEK'])){
                $longweek = $totalWeekPlan['MAXWEEK'];
            }
            for ($i=0; $i <= $longweek ; $i++) { 
                $this->db->set('ID_PROJECT',$id_project)
                ->set('WEEK',$i)
                ->set('PLAN',0)
                ->set('ACHIEVEMENT',0);
                if ($i != 0 ) {
                    $this->db->set('DATE_PLAN', "to_date((SELECT TO_CHAR(TO_DATE('".$totalWeek['START_DATE']."','DD-MM-YYYY') + (7*".$i."), 'DD-MM-YYYY') AS TANGGAL FROM DUAL),'DD-MM-YYYY HH24:MI:SS')",false);
                }else{
                    $this->db->set('DATE_PLAN', "to_date('".$totalWeek['START_DATE']."','DD-MM-YYYY HH24:MI:SS')",false);
                }
                $this->db->insert('PRIME_PROJECT_PLAN');
            }

            // $date = $this->db->query("SELECT TO_CHAR(TO_DATE('".$start_date."','DD/MM/YYYY') + (7*".$key."), 'DD/MM/YYYY') AS TANGGAL FROM DUAL ")->row_array();

    	$cek_deliverable = $this->db->select('COUNT(1) TOTAL')->from('PRIME_PROJECT_DELIVERABLE')->where('ID_PROJECT',$id_project)->get()->row_array();
        
    	if($cek_deliverable['TOTAL'] > 0){
    		$t_weight     = 0;
            $qt_weight    = $this->db->query("SELECT NVL(SUM(WEIGHT),0) AS TWEIGHT FROM PRIME_PROJECT_DELIVERABLE WHERE ID_PROJECT = '".$id_project."' GROUP BY ID_PROJECT")->row_array();

            if (!empty($qt_weight)) {
                if (!empty($qt_weight["TWEIGHT"])) {
                    $t_weight = number_format((float)$qt_weight["TWEIGHT"], 2, '.', '');
                }
            }

            $this->db->query("UPDATE PRIME_PROJECT SET TOTAL_WEIGHT = ".$t_weight." WHERE ID_PROJECT = '".$id_project."'");    

    		$plan_deliverable = $this->db
    							->select('ID_PROJECT, WEEK, SUM(PLAN) PLAN, SUM(ACHIEVEMENT) ACHIEVEMENT')
    							->from('PRIME_PROJECT_PLAN_DELIVERABLE')
    							->where('ID_PROJECT',$id_project)
    							->group_by('ID_PROJECT, WEEK')
    							->get()
    							->result_array();
    		foreach ($plan_deliverable as $key => $value) {
    			$this->db->set('PLAN',$value['PLAN'])->set('ACHIEVEMENT',$value['ACHIEVEMENT'])->where('WEEK',$value['WEEK'])->where('ID_PROJECT',$value['ID_PROJECT'])->update('PRIME_PROJECT_PLAN');
    		}
            $t_plan     = 0;
            $qt_plan    = $this->db->query("SELECT CASE WHEN NVL(SUM(PLAN),0) > 100 THEN 100 ELSE NVL(SUM(PLAN),0) END AS PLAN FROM PRIME_PROJECT_PLAN WHERE ID_PROJECT = '".$id_project."' GROUP BY ID_PROJECT")->row_array();

            $max_plan   = $this->db->select('ID_PROJECT, SUM(WEIGHT) TOTAL')->from('PRIME_PROJECT_DELIVERABLE')
                                            ->where('ID_PROJECT',$id_project)->group_by('ID_PROJECT')
                                            ->get()->row_array();
            // echo $this->db->last_query();die;
            if (!empty($qt_plan)) {
                if (!empty($qt_plan["PLAN"])) {
                    $t_plan = number_format((float)$qt_plan["PLAN"], 2, '.', '');
                }
            }
            if (!empty($max_plan)) {
                if (!empty($max_plan['TOTAL'])) {
                    if ($max_plan['TOTAL'] < $t_plan) {
                        $t_plan = number_format((float)$max_plan["TOTAL"], 2, '.', '');
                    }
                }
            }

            if ($t_plan >= 99.9) {
                $t_plan = 100;
            }

			$this->db->query("UPDATE PRIME_PROJECT SET TOTAL_PLAN = ".$t_plan." WHERE ID_PROJECT = '".$id_project."'"); 

			$t_ach   = 0;
            $qt_ach  = $this->db->query("SELECT CASE WHEN NVL(SUM(ACHIEVEMENT),0) > 100 THEN 100 ELSE NVL(SUM(ACHIEVEMENT),0) END AS ACH FROM PRIME_PROJECT_PLAN WHERE ID_PROJECT = '".$id_project."' GROUP BY ID_PROJECT")->row_array();
            if (!empty($qt_ach)) {
                if (!empty($qt_ach["ACH"])) {
                    $t_ach = number_format((float)$qt_ach["ACH"], 2, '.', '');
                }
            }       
            if ($t_ach >= 99.9) {
                $t_ach = 100;
            }   
            $this->db->query("UPDATE PRIME_PROJECT SET TOTAL_ACHIEVEMENT = ".$t_ach." WHERE ID_PROJECT = '".$id_project."'");
    	}else{
    		$this->db->query("UPDATE PRIME_PROJECT SET TOTAL_WEIGHT = 0 , TOTAL_PLAN = 0 , TOTAL_ACHIEVEMENT = 0 WHERE ID_PROJECT = '".$id_project."'");    
			$this->db->query("UPDATE PRIME_PROJECT_PLAN SET PLAN = 0 , ACHIEVEMENT = 0 WHERE ID_PROJECT = '".$id_project."'");
    	} 
		
        $this->db->query("UPDATE PRIME_PROJECT 
            SET SCALE = CASE 
                        WHEN VALUE < 1000000000 THEN 'REGULAR' 
                        WHEN VALUE >= 1000000000 AND VALUE < 20000000000 THEN 'ORDINARY' 
                        WHEN VALUE >= 2000000000 AND VALUE < 50000000000 THEN 'BIG' 
                        ELSE 'MEGA' END WHERE ID_PROJECT = '".$id_project."'");
        $this->db->query("UPDATE PRIME_PROJECT SET PROGRESS = CASE WHEN SYSDATE > END_DATE THEN 'DELAY' WHEN TOTAL_ACHIEVEMENT < TOTAL_PLAN THEN 'LAG' ELSE 'LEAD' END WHERE ID_PROJECT = '".$id_project."' AND STATUS = 'ACTIVE'");

    	return $this->db->trans_complete();
    }

    function put_project($data){
        $project_value = intval($data['VALUE']);
        $scale = null;
        if ($project_value < 1000000000) {
            $scale = 'REGULAR';
        }elseif ($project_value < 20000000000) {
            $scale = 'ORDINARY';
        }elseif ($project_value < 50000000000) {
            $scale = 'BIG';
        }else{
            $scale = 'MEGA';
        }

        $data['SCALE'] = $scale;

        if (empty($data['DATE_CREATED'])) {
            $this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        }
        if (empty($data['DATE_UPDATED'])) {
            $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        }

    	foreach ($data as $key => $value) {
            if (!empty($value)) {
                if($key=='DATE_CREATED' || $key=='DATE_UPDATED' || $key=='START_DATE' || $key=='END_DATE' || $key=='DATE_CLOSED' || $key == "DATE_KICK_OFF" || $key ==  "DATE_MONITORING_REQUEST" || $key == "DATE_PM_APPOINTMENT"){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    $this->db->set($key , $value);
                }   
            }
		}
        if (!empty($data["IS_STRATEGIC"])) {
            $this->db->set("IS_STRATEGIC", $data["IS_STRATEGIC"]);
        }else{
            $this->db->set("IS_STRATEGIC", "0");
        }
    	$this->db->insert('PRIME_PROJECT');
    
		$totalWeek = $this->getProjectDate($data['ID_PROJECT']);
    	
		for ($i=1; $i <= $totalWeek['TOTAL_WEEK'] ; $i++) { 
			$this->db->set('ID_PROJECT',$data['ID_PROJECT']);
			$this->db->set('WEEK',$i);
			$this->db->set('PLAN',0);
			$this->db->set('ACHIEVEMENT',0);
			$this->db->insert('PRIME_PROJECT_PLAN');
		}

		return true;
    }

    function update_project($id_project, $data){
        $project_value = intval($data['VALUE']);
        $scale = null;
        $scale = null;
        if ($project_value < 1000000000) {
            $scale = 'REGULAR';
        }elseif ($project_value < 20000000000) {
            $scale = 'ORDINARY';
        }elseif ($project_value < 50000000000) {
            $scale = 'BIG';
        }else{
            $scale = 'MEGA';
        }
        $data['SCALE'] = $scale;
    	$total_week1 = $this->getProjectDate($id_project);

        if (empty($data['DATE_UPDATED'])) {
            $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        }
    	
        foreach ($data as $key => $value) {
			if(!empty($value)){
                if($key=='DATE_UPDATED' || $key=='DATE_CREATED' || $key=='START_DATE' || $key=='END_DATE' || $key == "DATE_CLOSED" || $key == "DATE_KICK_OFF" || $key ==  "DATE_MONITORING_REQUEST" || $key == "DATE_PM_APPOINTMENT"){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    $this->db->set($key , $value);
                }
            }
		}

        if (!empty($data["IS_STRATEGIC"])) {
            $this->db->set("IS_STRATEGIC", $data["IS_STRATEGIC"]);
        }else{
            $this->db->set("IS_STRATEGIC", "0");
        }

		$this->db->where('ID_PROJECT',$id_project);
		$this->db->update('PRIME_PROJECT');

		$total_week2 = $this->getProjectDate($id_project);
		if($total_week2['TOTAL_WEEK'] > $total_week1['TOTAL_WEEK']){
			$extend = $total_week2['TOTAL_WEEK'] - $total_week1['TOTAL_WEEK'];

			for ($i=$total_week1['TOTAL_WEEK'] + 1; $i <= $total_week2['TOTAL_WEEK'] ; $i++) { 
				$this->db->set('ID_PROJECT',$id_project)->set('WEEK',$i)->set('PLAN',0)->set('ACHIEVEMENT',0)->insert('PRIME_PROJECT_PLAN');
			}
		}else if($total_week2['TOTAL_WEEK'] < $total_week1['TOTAL_WEEK']){
				for ($i=$total_week2['TOTAL_WEEK'] + 1; $i <= $total_week1['TOTAL_WEEK'] ; $i++) { 
					$this->db->where('ID_PROJECT', $data['ID_PROJECT'])->where('WEEK', $i)->delete('PRIME_PROJECT_PLAN');
				}
		}
        return true;
    }

    function set_project_active($id_project){
        $project = $this->getProjectData($id_project);
        $status  = 'ACTIVE';
        if ($project["PM"] == "" || empty($project["PM"])) {
            // $status = "NON PM";
        }

    	$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	$this->db->set('STATUS',$status);
    	$this->db->where('ID_PROJECT',$id_project);
    	return $this->db->update('PRIME_PROJECT');
    } 

    function set_project_close($id_project,$date_close,$lesson){
        $this->db
            ->set("STATUS","CLOSED")
            ->set("DATE_CLOSED", "TO_DATE('".$date_close."','DD/MM/YYYY')",false)
            ->set("LESSON_LEARN",$lesson)
            ->set('CLOSE_BY_ID',$this->session->userdata('userid'))
            ->set('CLOSE_BY_NAME',$this->session->userdata('name'))
            ->where("ID_PROJECT",$id_project);

        return $this->db->update("PRIME_PROJECT");
    }


    function put_project_partner($data){
    	$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	foreach ($data as $key => $value) {
				if($key=='SPK_DATE' || $key=='KL_DATE'){
					$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
				}else{
                    $value = str_replace(' ', '', $value);
					if(!empty($value)){$this->db->set($key , $value);}	
				}
		}
    	return $this->db->insert('PRIME_PROJECT_PARTNER');
    }

     function update_project_partner($id_project, $spk, $data){
    	$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	foreach ($data as $key => $value) {
				if($key=='SPK_DATE' || $key=='KL_DATE'){
					$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
				}else{
					if(!empty($value)){$this->db->set($key , $value);}	
				}
		}
		$this->db->where("SPK",$spk);
        $this->db->where("ID_PROJECT",$id_project);
    	return $this->db->update('PRIME_PROJECT_PARTNER');
    }

    function delete_project_partner($spk){
    	$this->db->where('SPK', $spk);
		return $this->db->delete('PRIME_PROJECT_PARTNER');	
    }

    function cek_project_partner($id_project,$spk){
    	$this->db->select('COUNT(1) TOTAL');
    	$this->db->from('PRIME_PROJECT_PARTNER');
    	$this->db->where('ID_PROJECT', $id_project);
    	$this->db->where('SPK', $spk);
    	$data = $this->db->get()->row_array();
		return 	intval($data['TOTAL']);
    }

    function delete_top_partner($id_project,$spk){
    	$this->db->where('ID_PROJECT', $id_project);
    	$this->db->where('SPK', $spk);
		return $this->db->delete('PRIME_PROJECT_TOP_PARTNER');	

    }

    function put_top_customer($data){
    	$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	foreach ($data as $key => $value) {
				if($key=='DUE_DATE'){
					$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
				}else{
					if(!empty($value)){$this->db->set($key , $value);}	
				}
		}
    	return $this->db->insert('PRIME_PROJECT_TOP_CUSTOMER');
    }

    function delete_top_customer($id_project){
    	$this->db->where('ID_PROJECT', $id_project);
		return $this->db->delete('PRIME_PROJECT_TOP_CUSTOMER');	

    }

    function put_top_partner($data){
    	$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	foreach ($data as $key => $value) {
				if($key=='DUE_DATE'){
					$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
				}else{
					if(!empty($value)){$this->db->set($key , $value);}	
				}
		}
    	return $this->db->insert('PRIME_PROJECT_TOP_PARTNER');
    }

    function getProjectPlanAchievement($id_project, $start_date){

    	$current_date 	= date('d-m-Y'); 
    	$start 			= str_replace('/', '-', $start_date);

    	$day   = 24 * 3600;
        $from  = strtotime('next monday',strtotime($start));
	    $to    = strtotime($current_date) + $day;
	    $diff  = abs($to - $from);
	    $weeks = round($diff / $day / 7);
	    $checkMonday = date('D', $from);
	    if ($checkMonday=="Mon") {
			$weeks = $weeks+1;
		}

		$deliverable = $this->db
						->select('NVL(SUM(PLAN),0) PLAN, NVL(SUM(ACHIEVEMENT),0) ACHIEVEMENT, ID_PROJECT')
						->from('PRIME_PROJECT_PLAN')
						->where('WEEK <= '.$weeks)
						->group_by('ID_PROJECT')
						->get()
						->row_array();
		$deliverable['CURRENT_WEEK'] = $weeks;
		return $deliverable;
    }

    function getDeliverable($id_deliverable){
    	return $this->db->select("A.*, TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE2")->from('PRIME_PROJECT_DELIVERABLE A')->where("ID",$id_deliverable)->get()->row_array();
    }
    function getSingleDeliverable($id_deliverable){
        $data['deliverable'] = $this->db
                ->select("A.*, TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE2")
                ->from('PRIME_PROJECT_DELIVERABLE A')
                ->where('ID',$id_deliverable)
                ->get()->row_array();
        $data['chart'] = $this->db
                    ->select('MAIN.*, ROUND(MAIN.PLAN,2) PLAN2,')
                    ->from('PRIME_PROJECT_PLAN_DELIVERABLE MAIN')
                    ->where('ID_DELIVERABLE',$id_deliverable)
                    ->order_by('WEEK','ASC')
                    ->get()->result_array();
        return $data;
    }


    function getProjectDate($id_project){
    	$project = $this->db
    				->select("A.*, TO_CHAR(A.START_DATE, 'DD-MM-YYYY') START_DATE2, TO_CHAR(A.END_DATE, 'DD-MM-YYYY') END_DATE2")
    				->from('PRIME_PROJECT A')
    				->where('ID_PROJECT',$id_project)
    				->get()
    				->row_array();
		

		$tomorrow = date('d-m-Y',strtotime($project['START_DATE2'] . "+1 days")); 

    	$day   = 24 * 3600;
	    $from  = strtotime($project['START_DATE2']);
	    $to    = strtotime($project['END_DATE2']) + $day;
	    $diff  = abs($to - $from);
	    $weeks = round($diff / $day / 7);
	    $checkMonday = date('D', $from);
	    if ($checkMonday=="Mon") { 
			$weeks = $weeks+1;
		}

		$date = array(
			'START' => $from,
            'START_DATE' => $project['START_DATE2'],
			'END' => $to,
            'END_DATE' => $project['END_DATE2'],
            'PROGRESS' => $project['PROGRESS'],
            'STATUS' => $project['STATUS'],
			'TOTAL_WEEK' => $weeks,
		);
	    return $date;
    }

    function put_document($data){
    	$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	foreach ($data as $key => $value) {
				if(!empty($value)){$this->db->set($key , $value);}	
		}
    	return $this->db->insert('PRIME_PROJECT_DOCUMENT');
    }

    function put_deliverable($data){
    	$this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	$this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
    	foreach ($data as $key => $value) {
				if($key=='START_DATE' || $key=='END_DATE'){
					$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
				}else{
					if(!empty($value)){$this->db->set($key , $value);}	
				}
		}
    	$this->db->insert('PRIME_PROJECT_DELIVERABLE');
    	
	    	$project_date = $this->getProjectDate($data['ID_PROJECT']);

	    	$start 	= str_replace('/', '-', $data['START_DATE']);
	    	$end 	= str_replace('/', '-', $data['END_DATE']);

	    	$day   = 24 * 3600;
		    $from  = strtotime($start);
		    $to    = strtotime($end);
		    $diff  = abs($to - $from);
		    $week_deliverable = round($diff / $day / 7);
		    $checkMonday = date('D', $from);

		$weight_week = 0;
		if($week_deliverable > 0){
            $weight_week = $data['WEIGHT'] / $week_deliverable;
        }else{
            $weight_week = $data['WEIGHT'] / 1;
        }
            $weight_week = number_format((float)$weight_week, 2, '.', '');

		$start_deliverable_project 	= round(abs($from - $project_date['START']) / $day / 7);
		$end_deliverable_project 	= $start_deliverable_project + $week_deliverable;

        $total_weight_week = 0;
        if ($end_deliverable_project - $start_deliverable_project <= 0) {
            $insert_plan_deliverable = $this->db->set('WEEK',1)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN',$weight_week)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
        }else{
            for ($i=$start_deliverable_project + 1; $i <= $end_deliverable_project; $i++) {
                $total_weight_week += $weight_week;

                if($total_weight_week < $data['WEIGHT'] && $i == $end_deliverable_project ){
                    $gap_close = ($data['WEIGHT'] - $total_weight_week) + $weight_week;
                    $gap_close = number_format((float)$gap_close, 2, '.', '');
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN', $gap_close)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
                }elseif($total_weight_week <= $data['WEIGHT']){
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN',$weight_week)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
                }else{
                      $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN', (($total_weight_week - $data['WEIGHT']) + $weight_week))
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');  
                } 
            }
        }
		return true;
    }


    function update_deliverable($id,$data){
        $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        foreach ($data as $key => $value) {
                if($key=='START_DATE' || $key=='END_DATE'){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    if(!empty($value)){$this->db->set($key , $value);}  
                }
        }
        $this->db->where('ID',$id);
        $this->db->update('PRIME_PROJECT_DELIVERABLE');

        $data = $this->db->get_where('PRIME_PROJECT_DELIVERABLE', array('ID' =>$id ))->row_array();
        
            $project_date = $this->getProjectDate($data['ID_PROJECT']);

            $start  = str_replace('/', '-', $data['START_DATE']);
            $end    = str_replace('/', '-', $data['END_DATE']);

            $day   = 24 * 3600;
            $from  = strtotime($start);
            $to    = strtotime($end) + $day;
            $diff  = abs($to - $from);
            $week_deliverable = round($diff / $day / 7);
            $checkMonday = date('D', $from);
            $weight_week = 0;
        if($week_deliverable > 0){
            $weight_week = $data['WEIGHT'] / $week_deliverable;
        }else{
            $weight_week = $data['WEIGHT'] / 1;
        }
            $weight_week = number_format((float)$weight_week, 2, '.', '');

        // echo $data['WEIGHT']."'<br>".$week_deliverable;

        $start_deliverable_project  = round(abs($from - $project_date['START']) / $day / 7);
        $end_deliverable_project    = $start_deliverable_project + $week_deliverable;

        $total_weight_week = 0;
        $this->db->where('ID_DELIVERABLE',$data['ID'])->delete('PRIME_PROJECT_PLAN_DELIVERABLE');
        if ($end_deliverable_project - $start_deliverable_project <= 0) {
            $insert_plan_deliverable = $this->db->set('WEEK',1)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN',$weight_week)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
        }else{
            for ($i=$start_deliverable_project + 1; $i <= $end_deliverable_project; $i++) { 
                $total_weight_week += $weight_week;
                if($total_weight_week < $data['WEIGHT'] && $i == $end_deliverable_project ){
                    $gap_close = ($data['WEIGHT'] - $total_weight_week) + $weight_week;
                    $gap_close = number_format((float)$gap_close, 2, '.', '');
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN', $gap_close)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');

                }elseif($total_weight_week <= $data['WEIGHT']){
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN',$weight_week)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');

                }else{
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN', (($total_weight_week - $data['WEIGHT']) + $weight_week))
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE'); 
                    // }
                }                   
            }
        }
        
        $achievement = $this->db
                        ->select("*")
                        ->from("PRIME_PROJECT_ACHIEVEMENT")
                        ->where("ID_DELIVERABLE",$data["ID"])
                        ->get()
                        ->result_array();

        foreach ($achievement as $key => $value) {
            $updt =  ($value["ACHIEVEMENT"] / 100) * number_format((float)$data['WEIGHT'], 2, '.', '') ;
            $this->db->set("ACHIEVEMENT",$updt)->where("ID_DELIVERABLE",$data["ID"])->where("WEEK",$value["WEEK"])->update("PRIME_PROJECT_PLAN_DELIVERABLE");
            
            echo $this->db->last_query()."<br>";
            }
        // $this->db->where('ID_DELIVERABLE',$data['ID'])->where('WEEK > '.$end_deliverable_project)->delete('PRIME_PROJECT_PLAN_DELIVERABLE');
        return true;
    }

    function update_deliverable_plan($id_deliverable,$data){
        foreach ($data as $key => $value) {
            $this->db->set('PLAN' , $value);
            $this->db->where('WEEK' , $key);
            $this->db->where('ID_DELIVERABLE',$id_deliverable);
            $this->db->update('PRIME_PROJECT_PLAN_DELIVERABLE');
        }

        return true;
        
    }

    function delete_deliverable($id_deliverable,$id_project){
		$this->db->where('ID',$id_deliverable)->where('ID_PROJECT',$id_project)->delete('PRIME_PROJECT_DELIVERABLE');
    	$this->db->where('ID_DELIVERABLE',$id_deliverable)->delete('PRIME_PROJECT_PLAN_DELIVERABLE');
    	$this->db->where('ID_DELIVERABLE',$id_deliverable)->delete('PRIME_PROJECT_ACHIEVEMENT');
		
        $issue_id = $this->db->select('ID')->from('PRIME_PROJECT_ISSUE')->where('ID_DELIVERABLE',$id_deliverable)->get()->result_array();
        $arr_issue_id = array_column($issue_id, "ID");

        if(!empty($arr_issue_id)){
            $this->db->where_in('ID',$arr_issue_id)->delete('PRIME_PROJECT_ISSUE');
            $this->db->where_in('ID_ISSUE',$arr_issue_id)->delete('PRIME_PROJECT_ACTION');
        }

        return true;
    }

    function delete_issue($id){
        $this->db->where('ID',$id)->delete('PRIME_PROJECT_ISSUE');
        $this->db->where('ID_ISSUE',$id)->delete('PRIME_PROJECT_ACTION');
        return true;
    }

    function getIssue($id){
        return $this->db->from('PRIME_PROJECT_ISSUE')->where('ID',$id)->get()->row_array();
    }

    function delete_action($id){
        $this->db->where('ID',$id)->delete('PRIME_PROJECT_ACTION');
        return true;   
    }



    function put_progress($data){
    	foreach ($data as $key => $value) {
				if($key=='DATE_ACHIEVED'){
					$this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
				}else{
					if(!empty($value)){$this->db->set($key , $value);}	
				}
		}

		$this->db->insert('PRIME_PROJECT_ACHIEVEMENT');
    }

    function update_sum_progress($achievement, $id_project,$week,$deliverable,$delete = false){
    	$current = $this->db
    					->select("NVL(ACHIEVEMENT,0) AS ACHIEVEMENT")
    					->from("PRIME_PROJECT_DELIVERABLE")
    					->where("ID",$deliverable['ID'])
    					->where("ID_PROJECT",$id_project)
    					->get()
    					->row_array();
        if($delete == true){
            $total_achievement = $current['ACHIEVEMENT'] - $achievement;
        }else{
            $total_achievement = $current['ACHIEVEMENT'] + $achievement;
        }
    	
        if ($total_achievement < 0) {
            $total_achievement = 0;
        }

        $this->db->set('ACHIEVEMENT', $total_achievement)->where('ID',$deliverable['ID'])->where('ID_PROJECT',$id_project)->update('PRIME_PROJECT_DELIVERABLE');	
    	
    	$thisachievement = ($achievement / 100) * number_format((float)$deliverable['WEIGHT'], 2, '.', ''); 
    	

    	$current_week = $this->db->select('ACHIEVEMENT')
    								->from('PRIME_PROJECT_PLAN_DELIVERABLE')
    								->where('ID_PROJECT',$id_project)
    								->where('ID_DELIVERABLE',$deliverable['ID'])
    								->where('WEEK',$week)
    								->get()
    								->row_array();
        if($delete == true){
            $current_total_achievement = $current_week['ACHIEVEMENT'] - $thisachievement;
        }else{
            $current_total_achievement = $current_week['ACHIEVEMENT'] + $thisachievement;
        }

        if ($current_total_achievement < 0) {
            $current_total_achievement = 0;
        }

        $cekWeek = $this->db->select("*")->from("PRIME_PROJECT_PLAN_DELIVERABLE")->where("ID_PROJECT",$id_project)->where("ID_DELIVERABLE",$deliverable['ID'])->where("WEEK",$week)->get()->row_array();

        if(!empty($cekWeek)){
             $this->db->set("ACHIEVEMENT",$current_total_achievement)->where('WEEK',$week)->where('ID_PROJECT',$id_project)->where('ID_DELIVERABLE',$deliverable['ID'])->update('PRIME_PROJECT_PLAN_DELIVERABLE');    
        }else{
             $this->db->set("ACHIEVEMENT",$current_total_achievement)->set("PLAN","0")->set('WEEK',$week)->set('ID_PROJECT',$id_project)->set('ID_DELIVERABLE',$deliverable['ID'])->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
        }
        return true;
    }


    function delete_progress($achievement, $id_project,$week,$deliverable,$id){
        $this->db->trans_start();
        $this->db->where('ID',$id)->delete('PRIME_PROJECT_ACHIEVEMENT');
        $this->db->trans_complete();

    }

    function getChartProgress($id_project,$start_date,$value_project) {
    		$top_customer = $this->db->select("A.*, TO_CHAR(A.DUE_DATE,'DD/MM/YYYY') DUE_DATE2")
    								->from("PRIME_PROJECT_TOP_CUSTOMER A")
    								->where("ID_PROJECT",$id_project)
    								->get()->result_array();

    		foreach ($top_customer as $key => $value) {
    			$top_customer[$key]['WEEK'] = $this->diffWeek($start_date,$value['DUE_DATE2']);
    		}

			$current_date 	= date('d-m-Y'); 
	    	$start 			= str_replace('/', '-', $start_date);

	    	$day   = 24 * 3600;
		    $from  = strtotime('next monday',strtotime($start));
		    $to    = strtotime($current_date) + $day;
		    $diff  = abs($to - $from);
		    $current_week = round($diff / $day / 7);
		    $checkMonday = date('D', $from);
		    if ($checkMonday=="Mon") {
				$current_week = $current_week+1;
			}

			$query = $this->db->query("	SELECT 'WEEK #'||WEEK WEEKS, NVL(PLAN,0) PLAN, NVL(ACHIEVEMENT,0) ACHIEVEMENT, DATE_PLAN , TO_CHAR(DATE_PLAN,'DD/MM/YYYY') DATE_PLAN2
										FROM PRIME_PROJECT_PLAN
										WHERE ID_PROJECT = '$id_project'
										ORDER BY WEEK ASC");
			$data = array(
					'WEEK' => array_column($query->result_array(), "WEEKS"),
					'PLAN' => array_column($query->result_array(), "PLAN"),
					'ACHIEVEMENT' => array_column($query->result_array(), "ACHIEVEMENT"),
                    'DAYS' => array_column($query->result_array(), "DATE_PLAN2")
				);

			$plan 	    = 0;
            $max_plan   = 0;
			$ach 	= 0;
			$top 	= 0;
            $week   = 1;
			$data['TOP'] = $data['DEVIASI'] =  array();

            $p          = $this->getProjectDate($id_project);
            $total_week = count($data['WEEK']);

            $qmax_plan   = $this->db->select('ID_PROJECT, SUM(WEIGHT) TOTAL')->from('PRIME_PROJECT_DELIVERABLE')
                                            ->where('ID_PROJECT',$id_project)->group_by('ID_PROJECT')
                                            ->get()->row_array();

            if (!empty($qmax_plan)) {
                if (!empty($qmax_plan['TOTAL'])) {
                    $max_plan = number_format((float)$qmax_plan["TOTAL"], 2, '.', '');
                }
            }

            for ($key=0; $key < $total_week ; $key++) { 
                if (!empty($data['PLAN'][$key])) {
                    $plan   = $plan + $data['PLAN'][$key];
                }
                if (!empty($data['ACHIEVEMENT'][$key])) {
                    $ach    = $ach + $data['ACHIEVEMENT'][$key];
                }

                 if ($plan > floatval($max_plan)) {
                    $plan = floatval($max_plan);
                }


                if ($plan >= 99.9) {
                    $plan = 100;
                }


                if ($ach >= 99.9) {
                    $ach = 100;
                }
                $data['PLAN'][$key]         = $plan;
                $data['ACHIEVEMENT'][$key]  = $ach;

                if ($ach < $plan) {
                    array_push($data['DEVIASI'], $plan - $ach);
                }else{
                    array_push($data['DEVIASI'], 0);
                }
                
                foreach ($top_customer as $key1 => $value1) {
                    if($key == ($value1['WEEK'])){
                        $top = $top + (($value1['VALUE'] / $value_project) * 100);
                    }
                    
                }

                $data['TOP'][$key] = $top;
            }

            if($ach < 100 && $current_week > $total_week){
                for ($i=$total_week; $i <= $current_week; $i++) {
                    array_push($data['PLAN'], $plan); 
                    array_push($data['ACHIEVEMENT'], $ach); 
                    array_push($data['WEEK'], 'WEEK #'.$i); 
                    foreach ($top_customer as $key1 => $value1) {
                        if($i == ($value1['WEEK']-1)){
                            $top = $top + (($value1['VALUE'] / $value_project) * 100);
                        }
                        
                    }
                    if ($ach < $plan) {
                        array_push($data['DEVIASI'], $plan - $ach);
                    }else{
                        array_push($data['DEVIASI'], 0);
                    }
                    array_push($data['TOP'], $top) ;
                    $date = $this->db->query("SELECT TO_CHAR(TO_DATE('".$start_date."','DD/MM/YYYY') + (7*".$i."), 'DD/MM/YYYY') AS TANGGAL FROM DUAL ")->row_array();
                    array_push($data['DAYS'], $date['TANGGAL']); 
                    $total_week++;
                }
            }
            // MARKING
            for ($key=0; $key <= $total_week ; $key++) { 
                if($key == ($current_week-1)){
                    $planx = $data['PLAN'][$key];
                    $data['PLAN'][$key] = array(
                        'y' => $planx,
                        'marker' => array(
                            'symbol' => 'url('.base_url().'assets/img/project_manager_icon.png)'
                        )
                    );
                }

                foreach ($top_customer as $key1 => $value1) {   
                        if($key == ($value1['WEEK'])){    
                           if ($data['TOP'][$key1]) {
                                $plan = $data['TOP'][$key1];
                                $data['TOP'][$key] = array(
                                    'y' => $plan,
                                    'marker' => array(
                                        'symbol' => 'url('.base_url().'assets/img/top.png)'
                                    )
                                );
                           }
                        }
                        
                    }
            }
            $result  = array(
                        'WEEK' => array(),
                        'PLAN' => array(),
                        'ACHIEVEMENT' => array(),
                        'DAYS' => array(),
                        'TOP' => array(),
                        'DEVIASI' => array(),
                        );
            foreach ($data["WEEK"] as $key => $value) {
                if ($key <= $total_week ) {
                    array_push($result["WEEK"], $data["WEEK"][$key]);
                    array_push($result["PLAN"], $data["PLAN"][$key]);
                    array_push($result["ACHIEVEMENT"], $data["ACHIEVEMENT"][$key]);
                    if (!empty($data["DEVIASI"][$key])) {
                        if ($p['PROGRESS'] == 'DELAY' && $p['STATUS'] == 'ACTIVE') {
                            array_push($result["DEVIASI"], $data["DEVIASI"][$key]);
                        }else{
                            array_push($result["DEVIASI"], 'null');
                        }
                    }
                    if (!empty($data["TOP"][$key])) {
                        array_push($result["TOP"], $data["TOP"][$key]);
                    }
                    if (!empty($data["DAYS"][$key])) {
                        if ($p['PROGRESS'] != 'LEAD' && $p['STATUS'] == 'ACTIVE' && $key == $total_week-1 && !empty($data["DEVIASI"][$key])) {
                             array_push($result["DAYS"], $data["DAYS"][$key]."<br>Deviasi ".$data["DEVIASI"][$key]."%");
                        }else{
                             array_push($result["DAYS"], $data["DAYS"][$key]);
                        }
                    }
                }
            }
			return $result;
		}

    function getChartProgressMonth($id_project,$start_date,$value_project) {
            $chartMonth = array();

            if (empty($start_date)) {
                return $chartMonth;
            }
            $top_customer = $this->db->select("to_char(DUE_DATE, 'YYYY-MM') DUE_DATE2, SUM(NVL(VALUE,0)) VALUE")
                                    ->from("PRIME_PROJECT_TOP_CUSTOMER A")
                                    ->where("ID_PROJECT",$id_project)
                                    ->group_by("to_char(DUE_DATE, 'YYYY-MM')")
                                    ->get()->result_array();

            foreach ($top_customer as $key => $value) {
                $top_customer[$key]['MONTH'] = $this->diffWeek($start_date,$value['DUE_DATE2']);
            }

            $current_date   = date('d-m-Y'); 
            $start          = str_replace('/', '-', $start_date);
            $query = $this->db->query(" SELECT to_char(DATE_PLAN, 'YYYY-MM') DP, 
                                        SUM(NVL(PLAN,0)) PLAN, SUM(NVL(ACHIEVEMENT,0)) ACHIEVEMENT
                                    FROM PRIME_PROJECT_PLAN 
                                    WHERE ID_PROJECT = '$id_project'  
                                    group by to_char(DATE_PLAN, 'YYYY-MM')
                                    ORDER BY DP ASC
                                    ");
            
            $data = array(
                    'MONTH' => array_column($query->result_array(), "DP"),
                    'PLAN' => array_column($query->result_array(), "PLAN"),
                    'ACH' => array_column($query->result_array(), "ACHIEVEMENT"),
                );
            $plan = $ach = $top = $week = 1;
            $data['TOP'] = $data['DAYS'] =  array();
            $c = null;
            foreach ($data['MONTH'] as $key => $value) {
                $plan   = $plan + $data['PLAN'][$key];
                $ach    = $ach + $data['ACH'][$key];
                if ($plan > 100) {
                    $plan = 100;
                }
                if ($ach > 100) {
                    $ach = 100;
                }
                $data['PLAN'][$key] = intval($plan);
                $data['ACH'][$key] = intval($ach);
                $data['DAYS'][$key]        = date('F-Y',strtotime($value));
                
                foreach ($top_customer as $key1 => $value1) {
                    if($value == $value1['MONTH']){
                        $top = $top + (($value1['VALUE'] / $value_project) * 100);
                    }
                }

                $data['TOP'][$key] = $top;
                $c = $value;
            }
            $current        = date('Y-m');
            $currentMonth   = date('Y-m');
            $currentYear    = date('Y-m');

            $cArr   = explode('-', $c);
            $cMonth = !empty($cArr[1]) ? $cArr[1] : "1";
            $cYear  = $cArr[0];

            $diffDate = (($currentYear - $cYear) * 12) + ($currentMonth - $cYear);

            if($ach < 100  && $diffDate > 0){
                for ($i=0; $i <= $diffDate; $i++) {
                    array_push($data['PLAN'], intval($plan)); 
                    array_push($data['ACH'], intval($ach)); 

                    if ($cMonth == 12) {
                        $cYear +=1; 
                        $cMonth =1; 
                    }else{
                        $cMonth++;
                    }

                    $dateUpdate = $cYear.'-'.$cMonth;
                    array_push($data['DAYS'], date('F-Y',strtotime($dateUpdate)));
                    foreach ($top_customer as $key1 => $value1) {
                        if($dateUpdate == $value1['MONTH']){
                            $top = $top + (($value1['VALUE'] / $value_project) * 100);
                        }
                    }

                    array_push($data['TOP'], $top);
                }
            }
            
            
            foreach ($data['MONTH'] as $key => $value) {
                if($value == $c){
                    $plan   = $data['PLAN'][$key];
                    $data['PLAN'][$key] = array(
                        'y' => $plan,
                        'marker' => array(
                            'symbol' => 'url('.base_url().'assets/img/project_manager_icon.png)'
                        )
                    );
                }

                foreach ($top_customer as $key1 => $value1) {   
                    if($value == $value1['MONTH']){    
                        $plan = $data['TOP'][$key];
                            $data['TOP'][$key] = array(
                                'y' => $plan,
                                'marker' => array(
                                    'symbol' => 'url('.base_url().'assets/img/top.png)'
                                )
                            );
                    }
                    
                }
                $tArr   = explode('-', $value);
                $tMonth = !empty($tArr[1]) ? $tArr[1] : 1;
                $tYear  = $tArr[0];
                $data["MONTH"][$key] =  date('F-Y',strtotime("01-".$tMonth.'-'.$tYear));
            }
            $chartMonth = $data;
            return $chartMonth;
        }

    function getChartProgressPartner($id_project,$id_partner, $start_date) {
            $current_date   = date('d-m-Y'); 
            $start          = str_replace('/', '-', $start_date);
            $result = array("WEEK"=>array(),"PLAN"=>array(),"ACHIEVEMENT"=>array());
            $plan  = 0;
            $ach   = 0;
            $day   = 24 * 3600;
            $from  = strtotime($start);
            $to    = strtotime($current_date) + $day;
            $diff  = abs($to - $from);
            $deliverable = $this->db->select('ID')->from('PRIME_PROJECT_DELIVERABLE')
                                    ->where('ID_PROJECT',$id_project)->where('PIC',$id_partner)
                                    ->get()->result_array();
            
            $deliverable_id = $data['WEEK'] = $data['PLAN'] = $data['ACHIEVEMENT'] = array();
            foreach ($deliverable as $key => $value) {
                array_push($deliverable_id, $value['ID']);
            }
            if(!empty($deliverable_id)){
                $query = $this->db->query(" SELECT 'WEEK #'||WEEK WEEKS, SUM(NVL(PLAN,0)) PLAN, SUM(NVL(ACHIEVEMENT,0)) ACHIEVEMENT
                                        FROM PRIME_PROJECT_PLAN_DELIVERABLE
                                        WHERE ID_PROJECT = '$id_project' 
                                        AND ID_DELIVERABLE IN ".str_replace(']', ')', str_replace('[', '(', str_replace('"', "'", json_encode($deliverable_id)) ))."
                                        GROUP BY WEEK 
                                        ORDER BY WEEK ASC");
            
                $data = array(
                        'WEEK' => array_column($query->result_array(), "WEEKS"),
                        'PLAN' => array_column($query->result_array(), "PLAN"),
                        'ACHIEVEMENT' => array_column($query->result_array(), "ACHIEVEMENT"),
                    );
                if ($id_partner == "905") {
                    // echo $this->db->last_query();die;
                }
                $partner_weight = $this->db->select('SUM(WEIGHT) TOTAL')->from('PRIME_PROJECT_DELIVERABLE')
                                            ->where('PIC',$id_partner)->where('ID_PROJECT',$id_project)->group_by('PIC')
                                            ->get()->row_array();

                $weight = 0;
                $tv     = 0;
                foreach ($data["PLAN"] as $key => $value) {
                    $weight = $weight + number_format((float)$value, 2, '.', '');         
                }

                // $weight = number_format((float)$partner_weight["TOTAL"], 2, '.', '');
                if ($weight == 0) {
                    $weight = 1;
                }

                foreach ($data['WEEK'] as $key => $value) {
                    if ($plan <= 100) {
                        $result["WEEK"] = $value;
                        $plan   = $plan + $data['PLAN'][$key];
                        $ach    = $ach  + $data['ACHIEVEMENT'][$key];
                        if ($plan >= 99) {
                            $result['PLAN'][$key]        = 100;
                        }else{
                            $result['PLAN'][$key]        = (number_format((float)$plan, 2, '.', '')/$weight) * 100;
                        }
                        if ($ach >= 99) {
                            $result['ACHIEVEMENT'][$key] = 100;
                        }else{
                            $result['ACHIEVEMENT'][$key] = (number_format((float)$ach, 2, '.', '')/$weight) * 100;
                        }
                    }                  
                }
            }

            return $result;
        }

    function put_issue($data){
        $this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }

        $this->db->insert('PRIME_PROJECT_ISSUE');
    }

    function update_issue($id,$data){
        $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key , $value);}
        }

        $this->db->where('ID',$id);
        $this->db->update('PRIME_PROJECT_ISSUE');
    }

    function put_action($data){
        $this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        foreach ($data as $key => $value) {
                if($key=='START_DATE' || $key=='END_DATE'){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    if(!empty($value)){$this->db->set($key , $value);}  
                }
        }
        $this->db->insert('PRIME_PROJECT_ACTION');
    }

    function update_action($id,$data){
        $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        foreach ($data as $key => $value) {
                if($key=='START_DATE' || $key=='END_DATE'){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    if(!empty($value)){$this->db->set($key , $value);}  
                }
        }
        $this->db->where('ID',$id);
        $this->db->update('PRIME_PROJECT_ACTION');
    }

    function getAction($id){
        return $this->db->from('PRIME_PROJECT_ACTION')->where('ID',$id)->get()->row_array();
    }

	function diffWeek($start, $end){
	   		$start 	= str_replace('/', '-', $start);
    		$end 	= str_replace('/', '-', $end);
		   	
		   	$day   	= 24 * 3600;
		    $from  = strtotime($start);
		    $to    = strtotime($end) + $day;
		    
		    $diff  = abs($to - $from);
		    $weeks = round($diff / $day / 7);
		    $checkMonday = date('D', $from);
		   
			if($weeks <= 0){
				$weeks = 1;
			}

			return $weeks;
	   }

    function update_symptom($id,$symptom, $id_project){
        $this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        $this->db->set('UPDATED_BY_ID',$this->session->userdata('userid'));
        $this->db->set('UPDATED_BY_NAME',$this->session->userdata('name'));
        $this->db->set('ID_SYMPTOM',$id);
        $this->db->set('ID_PROJECT',$id_project);
        $this->db->set('SYMPTOM',$symptom);

        return $this->db->insert('PRIME_PROJECT_SYMPTOM');
    }



    ##TOOLS
    public function uploadProjectStrategic(){
        $customer       = $this->db->select("CODE, TO_CHAR(START_DATE,'DD/MM/YYYY') START_DATE2, TO_CHAR(END_DATE,'DD/MM/YYYY') END_DATE2, IS_ACTIVE")->from('PRIME_CUSTOMER_EXECUTIVE')
                          ->distinct()->get()->result_array();
        // $this->db->set('IS_STRATEGIC','0')->update('PRIME_PROJECT');

        foreach ($customer as $key => $value) {
            if ($value["IS_ACTIVE"]== "1") {
                $this->db
                ->where('CUSTOMER',$value['CODE'])
                ->where('START_DATE >=', "to_date('".$value['START_DATE2']."','DD/MM/YYYY')", false)
                ->where('START_DATE <=', "to_date('".$value['END_DATE2']."','DD/MM/YYYY')", false)
                ->set('IS_STRATEGIC','1')
                ->update('PRIME_PROJECT');
            }
        }
        $this->db->where('VALUE >=','100000000000',true)
                ->set('IS_STRATEGIC','1')
                ->update('PRIME_PROJECT');
        return true;
    }

    function check_kb($kb){
        return $this->db->get_where('PRIME_PROJECT',array('UPPER(KB)' => strtoupper($kb)))->result_array();
    }

    function getAllProjectAccess($id_project){
        return $this->db->select("MAIN.*, U.NAME, U.ROLE_NAME, U.ROLE, U.REGIONAL, U.REGIONAL_NAME, U.PHONE, U.DIVISION")
                ->from("PRIME_PROJECT_ACCESS MAIN")
                ->join("USERS U","U.EMAIL = MAIN.EMAIL")
                ->where("MAIN.ID_PROJECT",$id_project)
                ->where("U.ROLE != 0")
                ->where("U.ROLE != 1")
                ->order_by('ROLE','ASC')
                ->order_by('NAME','ASC')
                ->get()
                ->result_array();
    }

    function getProjectAccess($email, $id_project){
        $data =  $this->db->select("MAIN.*, U.NAME, U.ROLE_NAME, U.ROLE, U.REGIONAL, U.REGIONAL_NAME, U.PHONE, U.DIVISION")
                ->from("PRIME_PROJECT_ACCESS MAIN")
                ->join("USERS U","U.EMAIL = MAIN.EMAIL")
                ->where("MAIN.ID_PROJECT",$id_project)
                ->where("MAIN.EMAIL",$email)
                ->order_by('ROLE','ASC')
                ->order_by('NAME','ASC')
                ->get()
                ->row_array();

        $result = array(
                        "C"=>NULL,
                        "R"=>NULL,
                        "U"=>NULL,
                        "D"=>NULL
                        );
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (in_array($key, array("C","R","U","D"))) {
                    $result[$key] = $value == "1" ? $value : NULL;
                }
            }
        }else{
            $countAccess =  $this->db->select("MAIN.*, U.NAME, U.ROLE_NAME, U.ROLE, U.REGIONAL, U.REGIONAL_NAME, U.PHONE, U.DIVISION")
                ->from("PRIME_PROJECT_ACCESS MAIN")
                ->join("USERS U","U.EMAIL = MAIN.EMAIL")
                ->where("MAIN.ID_PROJECT",$id_project)
                ->where("U.ROLE != 1")
                ->where("U.ROLE != 2")
                ->where("U.ROLE != 0")
                ->order_by('ROLE','ASC')
                ->order_by('NAME','ASC')
                ->get()
                ->result_array();
            if (empty($countAccess)) {
                $r = $this->session->userdata('role_id');
                if ($r == "0" || $r == "1" ) {
                    $result = array(
                        "C"=>"1",
                        "R"=>"1",
                        "U"=>"1",
                        "D"=>"1"
                        );   
                }
            }
        }        

        return $result;
    }


    function addProjectAccess($id_project, $email = null){
        $validUserAccessData = array();
        $project = $this->db
                    ->from('PROJECT')
                    ->where('ID_PROJECT',$id_project)
                    ->get()
                    ->row_array();

        $userAccess = array();



        if (!empty($project["PM_EMAIL"])) {
            array_push($userAccess, $project["PM_EMAIL"]);
        }
        if (!empty($project["PM_ADMIN_EMAIL"])) {
            array_push($userAccess, $project["PM_ADMIN_EMAIL"]);
        }
        if (!empty($project["AM_EMAIL"])) {
            array_push($userAccess, $project["AM_EMAIL"]);
        }
        if (!empty($project["CREATED_BY"])) {
            array_push($userAccess, $project["CREATED_BY"]);
        }

        $accessByRole = $this->db
                    ->from('USERS')
                    ->where_in('ROLE',array("0","1","2"))
                    ->where("(DIVISION = '".$project["DIVISION"]."' OR DIVISION = 'EBIS')");

        if (!empty($userAccess)) {
            $accessByRole = $accessByRole->where_not_in('EMAIL',$userAccess);
        }
        
        $accessByRole = $accessByRole->get()->result_array();

        foreach ($accessByRole as $key => $value) {
            if (!in_array($value["EMAIL"], $userAccess)) {
                array_push($userAccess, $value["EMAIL"]);
            }
        }

        if (!empty($email) && !in_array($email, $userAccess)) {
            array_push($userAccess, $email);
        }

        if (!empty($userAccess)) {
            $userAccessData = $this->db
                    ->from('USERS')
                    ->where_in('EMAIL',$userAccess)
                    ->get()
                    ->result_array();
            $userAccessDataEmail = array();
            foreach ($userAccessData as $key => $value) {
                array_push($userAccessDataEmail, $value["EMAIL"]);
            }

            $currentAccesData =   $this->db
                    ->from('PRIME_PROJECT_ACCESS')
                    ->where('ID_PROJECT',$id_project)
                    ->get()
                    ->result_array();
            $currentAccesDataEmail = array();
            if (!empty($currentAccesData)) {
                foreach ($currentAccesData as $key => $value) {
                    array_push($currentAccesDataEmail, $value["EMAIL"]);
                }
            }

            
            if (!empty($currentAccesDataEmail)) {
                foreach ($userAccessDataEmail as $key => $value) {
                    if (!in_array($value, $currentAccesDataEmail) ) {
                        array_push($validUserAccessData, $value);
                    }
                }
            }else{
                $validUserAccessData = $userAccessDataEmail;
            }
        }
        if (!empty($validUserAccessData)) {
            foreach ($validUserAccessData as $key => $value) {
                $this->db
                        ->set('ID',$this->getGUID())
                        ->set('ID_PROJECT',$id_project)
                        ->set('EMAIL',$value)
                        ->insert('PRIME_PROJECT_ACCESS');
            }
        }
        return $validUserAccessData;
    }

    function deleteProjectAccess($id_project, $email){
        return $this->db->where('ID_PROJECT',$id_project)->where('EMAIL',$email)->delete('PRIME_PROJECT_ACCESS');
    }

    function getProjectDescription($id_project){
        $desc = "-";
        $q  = $this->db->select("DESCRIPTION")->from("PRIME_PROJECT")->where("ID_PROJECT",$id_project)->get()->row_array();

        if (!empty($q["DESCRIPTION"])) {
            $desc = $q["DESCRIPTION"];
        }
        return $desc;
    }

    // generate GUID
    public function getGUID()
    {
        if (function_exists('getGUID')){
            return getGUID();
        }
        else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = //chr(123)// "{"
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
                //.chr(125);// "}"
            return $uuid;
        }
    } 


    function fixDesc($name,$desc){
            $this->db->where('UPPER(NAME)',trim(strtoupper($name)))->set('DESCRIPTION',trim($desc))->update('PRIME_PROJECT');
    }


}