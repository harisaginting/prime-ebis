<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Report extends CI_Model {
 
	// Excel
	function excelListProject($status){

		$data = $this->db
				->select("PROJECT.*, PARTNER.PARTNER_NAME, PARTNER.NO_SPK, PARTNER.NO_KL, PARTNER.VALUE_KL")
				->distinct()
				->from('PROJECT')
				->join("(
						    SELECT ID_PROJECT, LISTAGG(NAME, ', ') WITHIN GROUP (ORDER BY ID_PROJECT) AS PARTNER_NAME , 
						    LISTAGG(SPK, ', ')  WITHIN GROUP (ORDER BY ID_PROJECT) AS NO_SPK, 
						    LISTAGG(KL, ', ') 	WITHIN GROUP (ORDER BY ID_PROJECT) AS NO_KL,
						    LISTAGG(VALUE, ', ') WITHIN GROUP (ORDER BY ID_PROJECT) AS VALUE_KL
						    FROM (
						    SELECT PROJECT.ID_PROJECT, PARTNER.NAME, PROJECT.SPK, PROJECT.VALUE, PROJECT.KL FROM PRIME_PROJECT_PARTNER PROJECT 
						    	JOIN PRIME_PARTNER PARTNER ON PROJECT.ID_PARTNER = PARTNER.CODE 
						    	GROUP BY PROJECT.ID_PROJECT, PARTNER.NAME, PROJECT.SPK, PROJECT.KL, PROJECT.VALUE
						    ) GROUP BY ID_PROJECT
						) PARTNER", "PARTNER.ID_PROJECT = PROJECT.ID_PROJECT","LEFT")
				->where("PROJECT.IS_EXIST","1");

		// echo count($data);die;
		if($this->session->userdata("division") != "EBIS"){
			$data->where("PROJECT.DIVISION",$this->session->userdata("division"));
		}

		if($status == "ACTIVE"){
			$data->where('PROJECT.PM IS NOT NULL');
		}

		if($status == "NO_PM"){
			$data->where('PROJECT.PM IS NULL');
			$data->where("PROJECT.STATUS","ACTIVE");
		}else{
			$data->where("PROJECT.STATUS",$status);
		}
		$result = $data->get()->result_array();
		return $result;
	}

	function excelListBast(){

		$data = $this->db
							->select("MAIN.*,PROJECT.PROJECT_NAME, PROJECT.CUSTOMER_NAME, PARTNER.NAME PARTNER_NAME, CASE WHEN APPROVAL.APPROVER IS NULL THEN STATUS ELSE 'Check By '||APPROVAL.APPROVER END APPROVAL_STATUS, CASE WHEN MAIN.BAUT = 0 THEN 'BAST' ELSE 'BAUT' END BASTBAUT, TO_CHAR(MAIN.SPK_DATE,'DD/MM/YYYY') SPK_DATE2, TO_CHAR(MAIN.KL_DATE,'DD/MM/YYYY') KL_DATE2, TO_CHAR(MAIN.BAST_DATE,'DD/MM/YYYY') BAST_DATE2, CASE WHEN TYPE = 'TERMIN' THEN 'TERMIN '||TERMIN ELSE 'PROGRESS '||PROGRESS END TOP_BAST")
							->from("PRIME_BAST MAIN")
							->join("(SELECT A.ID_PROJECT, A.SPK, B.NAME PROJECT_NAME, C.NAME CUSTOMER_NAME FROM PRIME_PROJECT_PARTNER A JOIN PRIME_PROJECT B ON A.ID_PROJECT = B.ID_PROJECT JOIN PRIME_CUSTOMER C ON C.CODE = B.CUSTOMER ) PROJECT","MAIN.SPK = PROJECT.SPK AND MAIN.ID_PROJECT = PROJECT.ID_PROJECT") 
							->join("(SELECT A.*,B.NAME APPROVER FROM PRIME_BAST_APPROVAL A JOIN PRIME_USER B ON A.ID_SIGNER = B.ID) APPROVAL","TO_CHAR(APPROVAL.STEP) = MAIN.STATUS AND APPROVAL.ID_BAST = MAIN.ID","LEFT")
							->join('PRIME_PARTNER PARTNER',"PARTNER.CODE = MAIN.PARTNER");

		// echo count($data);die;
		if($this->session->userdata("division") != "EBIS"){
			$data->where("MAIN.DIVISION",$this->session->userdata("division"));
		}
		return $data->get()->result_array();
	}

}