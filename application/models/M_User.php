<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model {


    function getUser($id){
        return $this->db
                    ->select("MAIN.*, ROLE.NAME ROLE_NAME, REGIONAL.VALUE REGIONAL_NAME")
                    ->from("PRIME_USER MAIN")
                    ->join("PRIME_ROLE ROLE","MAIN.ROLE = ROLE.ID")
                    ->join("PRIME_CONFIG REGIONAL","MAIN.REGIONAL = REGIONAL.ID","LEFT")
                    ->where("MAIN.ID",$id)
                    ->get()
                    ->row_array();
    } 

    function getUserByEmail($email){
        return $this->db
                    ->select("MAIN.*, ROLE.NAME ROLE_NAME, REGIONAL.VALUE REGIONAL_NAME")
                    ->from("PRIME_USER MAIN")
                    ->join("PRIME_ROLE ROLE","MAIN.ROLE = ROLE.ID")
                    ->join("PRIME_CONFIG REGIONAL","MAIN.REGIONAL = REGIONAL.ID","LEFT")
                    ->where("UPPER(MAIN.EMAIL)",strtoupper($email))
                    ->get()
                    ->row_array();
    } 

    function addUser($data){
        $count = $this->db
                    ->select('COUNT(1) TOTAL')
                    ->from('PRIME_USER')
                    ->where('ID',$data['ID'])
                    ->or_where('EMAIL',$data['EMAIL'])
                    ->get()
                    ->row_array();
        if($count['TOTAL'] < 1){
            $this->db->insert('PRIME_USER', $data);
            if($data['ROLE'] == '2' || $data['ROLE'] == '3'){
                        $this->db->set("DATE_CREATED",'SYSDATE',FALSE);
                        $this->db->set("DATE_UPDATED",'SYSDATE',FALSE);
                        $this->db->set("ID",trim($data['ID']));
                        $this->db->set("NAME",trim($data['NAME']));
                        $this->db->set("EMAIL",trim($data['EMAIL']));
                        $this->db->set("PHONE",$data['PHONE']);
                        $this->db->set("DIVISION",$data['DIVISION']);
                        $this->db->set("REGIONAL",$data['REGIONAL']);
                        $this->db->set("USERID",$data['ID'].$data['EMAIL'].$data['PHONE']);
                        return $this->db->insert("PRIME_PM");
            }
            
        }
        return $data['ID'];
    }

    function updateUser($id,$data){
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key, trim($value));}
        }
        $this->db->where('ID',$id);
        return $this->db->update("PRIME_USER");
    }

    function deleteUser($id, $email){
        $this->db->where('ID',$id)->or_where('EMAIL',$email)->delete('PRIME_PM');
        return $this->db->where('ID',$id)->or_where('EMAIL',$email)->delete('PRIME_USER');
    }

    function getActivateUser($code){
        $user = $this->db
                    ->from("PRIME_USER MAIN")
                    ->where("MAIN.ACTIVATION_CODE",$code)
                    ->get()
                    ->row_array();
        // $this->db->where('ID',$user['ID'])->set('ACTIVATION_CODE','')->update('PRIME_USER');
        return $user;
    }

    function getTokenPasswordUser($code){
        $user = $this->db
                    ->from("PRIME_USER MAIN")
                    ->where("MAIN.CHANGE_PASSWORD_TOKEN",$code)
                    ->get()
                    ->row_array();
        // $this->db->where('ID',$user['ID'])->set('ACTIVATION_CODE','')->update('PRIME_USER');
        return $user;
    }

    function activateUser($data){
        foreach ($data as $key => $value) {
                if(!empty($value)){$this->db->set($key, trim($value));}
        }
        return $this->db->set('ACTIVATION_CODE','')->where('ID',$data['ID'])->update("PRIME_USER");
    }

    function resetPassword($id, $password){
        return $this->db->set('PASSWORD',$password)->set('CHANGE_PASSWORD_TOKEN','')->where('ID',$id)->update("PRIME_USER");
    }

    function UpdateRoleAccess($id,$val,$sid){
        if ($id != "0") {
            return $this->db->set($sid,$val)->where('ID',$id)->where('ID_ROLE != 0')->where('ID_ROLE != 1')->update("PRIME_ROLE_ACCESS");
        }
        return false;
    }

}