<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Utility extends CI_Model {

	function getDesProject(){
		$arrayIdp = $arrayId 	= $arrayKb = $data =  array();
		$id_global 	= $this->db->from("PRIME_PROJECT")->select("ID_PROJECT ,ID_PROJECT_GLOBAL")->distinct()->where("DIVISION","DES")->get()->result_array();
		foreach ($id_global as $key => $value) {
			array_push($arrayId, trim($value["ID_PROJECT_GLOBAL"]));
			array_push($arrayIdp, trim($value["ID_PROJECT"]));
		}
		$no_kb 		= $this->db->from("PRIME_PROJECT")->select("KB")->distinct()->where("KB != ","NULL", true)->get()->result_array();
		foreach ($no_kb as $key => $value) {
			array_push($arrayKb, trim($value['KB']));
		}

		// echo json_encode($arrayKb);die;
		$des = $this->load->database('des', TRUE);
        $raw 	= $des
					->select(
						"
						A.ID_PROJECT ID_PROJECT,
						A.ID_LOP_EPIC AS ID_PROJECT_GLOBAL, 
						A.NAME, 
						A.NIP_NAS AS CUSTOMER, 
						A.SEGMEN AS SEGMEN, 
						A.NO_KB AS KB, 
						A.VALUE, 
						TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,
						TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE,
						TO_CHAR(A.UPDATED_DATE,'DD/MM/YYYY') DATE_UPDATED,
						TO_CHAR(A.REQUEST_DATE,'DD/MM/YYYY') DATE_CREATED,
						A.TYPE AS CATEGORY, 
						A.AM_NIK||'@telkom.co.id' AS AM, 
						A.PM_NIK||'@telkom.co.id' AS PM,
						A.REGIONAL, 
						A.DESCRIPTION, 
						A.STATUS, 
						A.SCALE, 
						A.LESSON_LEARNED AS LESSON_LEARN, 
						A.STATUS AS PROGRESS, 
						A.PROJECT_STRATEGIS AS IS_STRATEGIC, 
						NVL(C.PLAN,0) AS TOTAL_WEIGHT,
						NVL(C.PLAN,0) AS TOTAL_PLAN,
						NVL(C.ACH,0)  AS TOTAL_ACHIEVEMENT,
						TO_CHAR(A.CLOSED_DATE,'DD/MM/YYYY') DATE_CLOSED, 
						A.CLOSED_BY_ID  AS CLOSE_BY_ID,
						'SYNC DES '||A.SOURCE_PROJECT AS SOURCE,
						'DES' AS DIVISION
						")
	                    ->from('PRIME_PROJECT A')
	                    ->join("PRIME_MONITORING_PROJECT C","C.ID_PROJECT = A.ID_PROJECT","LEFT")
	                    ->where_in('A.STATUS', array('LEAD','LAG','DELAY','CLOSED','NON PM')) 
		  				->where("A.EXIST","1")
		  				// ->where("A.PM_NIK IS NOT NULL")
		  				->order_by("TOTAL_PLAN","DESC");


		 // if (!empty($arrayKb)) {
		 // 	$raw = $raw->where_not_in('A.NO_KB', $arrayKb);
		 // }
		 // if (!empty($arrayId)) {
		 // 	$raw = $raw->where_not_in('A.ID_LOP_EPIC', $arrayId);
		 // }
		 $raw = $raw->get()->result_array();
		 foreach ($raw as $key => $value) {
		 	if (!in_array($value['ID_PROJECT'], $arrayIdp)) {
		 		
		 		// if (!empty($value['KB']) && !in_array($value['KB'], $arrayKb)) {
		 			array_push($data, $value);
		 		// }elseif (empty($value['KB'])) {
		 		// 	array_push($data, $value);
		 		// }
		 	}
		 }
		 return $data;

    } 

    function updateDesProject(){
		$arrayIdp 	= $arrayId 	= $arrayKb = $data = $data_n = $data_d = $idDes =  $data_r = array();
		$id_global 	= $this->db->from("PRIME_PROJECT")->select("ID_PROJECT ,ID_PROJECT_GLOBAL")->distinct()->where("DIVISION","DES")->where("STATUS","ACTIVE")->get()->result_array();
		foreach ($id_global as $key => $value) {
			array_push($arrayId, trim($value["ID_PROJECT_GLOBAL"]));
			array_push($arrayIdp, trim($value["ID_PROJECT"]));
		}

		// echo json_encode($arrayKb);die;
		$des = $this->load->database('des', TRUE);
        $raw 	= $des
					->select(
						"
						A.ID_PROJECT ID_PROJECT,
						A.ID_LOP_EPIC AS ID_PROJECT_GLOBAL, 
						A.NAME, 
						A.NIP_NAS AS CUSTOMER, 
						A.SEGMEN AS SEGMEN, 
						A.NO_KB AS KB, 
						A.VALUE, 
						TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,
						TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE,
						A.TYPE AS CATEGORY, 
						A.AM_NIK||'@telkom.co.id' AS AM, 
						A.PM_NIK||'@telkom.co.id' AS PM,
						A.REGIONAL, 
						A.DESCRIPTION, 
						A.STATUS, 
						A.SCALE, 
						A.LESSON_LEARNED AS LESSON_LEARN, 
						A.STATUS AS PROGRESS, 
						A.PROJECT_STRATEGIS AS IS_STRATEGIC, 
						NVL(C.PLAN,0) AS TOTAL_WEIGHT,
						NVL(C.PLAN,0) AS TOTAL_PLAN,
						NVL(C.ACH,0)  AS TOTAL_ACHIEVEMENT,
						TO_CHAR(A.CLOSED_DATE,'DD/MM/YYYY') DATE_CLOSED, 
						A.CLOSED_BY_ID  AS CLOSE_BY_ID,
						'SYNC DES '||A.SOURCE_PROJECT AS SOURCE,
						'DES' AS DIVISION
						")
	                    ->from('PRIME_PROJECT A')
	                    ->join("PRIME_MONITORING_PROJECT C","C.ID_PROJECT = A.ID_PROJECT","LEFT")
	                    ->where_in('A.STATUS', array('LEAD','LAG','DELAY','CLOSED','NON PM')) 
	                    // ->where_in('A.ID_PROJECT', array('PROJ10367','PROJ10046','PROJ10611','PROJ10620','PROJ10620')) 
		  				->where("A.EXIST","1")
		  				// ->where("A.PM_NIK IS NOT NULL")
		  				->order_by("TOTAL_PLAN","DESC");

		 // if (!empty($arrayKb)) {
		 // 	$raw = $raw->where_not_in('A.NO_KB', $arrayKb);
		 // }
		 // if (!empty($arrayId)) {
		 // 	$raw = $raw->where_not_in('A.ID_LOP_EPIC', $arrayId);
		 // }
		 $raw = $raw->get()->result_array();

		 // echo json_encode($arrayIdp);die;
		 foreach ($raw as $key => $value) {
		 	if (in_array(trim($value['ID_PROJECT']), $arrayIdp)) {
		 		array_push($data, $value);
		 		array_push($idDes, $value['ID_PROJECT']);
		 	}
		 }

		 foreach ($arrayIdp as $key => $value) {
		 	if (!in_array($value, $idDes)) {
		 		array_push($data_d, $value);
		 	}else{
		 		array_push($data_r, $value);
		 	}
		 }

		 $delete = 0;
		 foreach ($data_d as $key => $value) {
		 	$delete++;
		 	$this->db->query("UPDATE PRIME_PROJECT SET IS_EXIST = 0 WHERE ID_PROJECT = '".$value."' AND STATUS = 'ACTIVE'");
		 }

		 // foreach ($data_r as $key => $value) {
		 // 	$this->db->query("UPDATE PRIME_PROJECT SET IS_EXIST = 1 WHERE ID_PROJECT = '".$value."'");
		 // }	

		 // foreach ($data as $key => $value) {
		 // 	$fixDeliverable = $this->db->query("SELECT ROWNUM, TB.* FROM (SELECT COUNT(WEEK) TOTAL, WEEK,ID_PROJECT FROM PRIME_PROJECT_PLAN WHERE PLAN != 0 GROUP BY WEEK, ID_PROJECT ORDER BY TOTAL DESC) TB WHERE TOTAL > 1 AND ID_PROJECT = "."'".$value['ID_PROJECT']."'")->result_array();

		 // 	if (!empty($fixDeliverable) && count($fixDeliverable) > 1) {
		 // 		echo count($fixDeliverable);die;
		 // 	}

		 // }
		 return $data;

    }

    function updateDesProjectCandidate(){
		$arrayIdp 	= $arrayId 	= $arrayKb = $data = $data_n = $data_d = $idDes =  $data_r = array();
		$id_global 	= $this->db->from("PRIME_PROJECT")->select("ID_PROJECT ,ID_PROJECT_GLOBAL")
						->distinct()
						->where("DIVISION","DES")
						->where_in('STATUS', array('NON PM','CLOSED')) 
						->where('DATE_UPDATED < ', "to_date('".date('d/m/Y')."','DD/MM/YYYY')", false)
						->get()->result_array();
		foreach ($id_global as $key => $value) {
			array_push($arrayId, trim($value["ID_PROJECT_GLOBAL"]));
			array_push($arrayIdp, trim($value["ID_PROJECT"]));
		}
		// echo json_encode($id_global);die;
		// echo json_encode($arrayKb);die;
		$des = $this->load->database('des', TRUE);
        $raw 	= $des
					->select(
						"
						A.ID_PROJECT ID_PROJECT,
						A.ID_LOP_EPIC AS ID_PROJECT_GLOBAL, 
						A.NAME, 
						A.NIP_NAS AS CUSTOMER, 
						A.SEGMEN AS SEGMEN, 
						A.NO_KB AS KB, 
						A.VALUE, 
						TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,
						TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE,
						A.TYPE AS CATEGORY, 
						A.AM_NIK||'@telkom.co.id' AS AM, 
						A.PM_NIK||'@telkom.co.id' AS PM,
						A.REGIONAL, 
						A.DESCRIPTION, 
						A.STATUS, 
						A.SCALE, 
						A.LESSON_LEARNED AS LESSON_LEARN, 
						A.STATUS AS PROGRESS, 
						A.PROJECT_STRATEGIS AS IS_STRATEGIC, 
						NVL(C.PLAN,0) AS TOTAL_WEIGHT,
						NVL(C.PLAN,0) AS TOTAL_PLAN,
						NVL(C.ACH,0)  AS TOTAL_ACHIEVEMENT,
						TO_CHAR(A.CLOSED_DATE,'DD/MM/YYYY') DATE_CLOSED, 
						A.CLOSED_BY_ID  AS CLOSE_BY_ID,
						'SYNC DES '||A.SOURCE_PROJECT AS SOURCE,
						'DES' AS DIVISION
						")
	                    ->from('PRIME_PROJECT A')
	                    ->join("PRIME_MONITORING_PROJECT C","C.ID_PROJECT = A.ID_PROJECT","LEFT")
	                    ->where_in('A.STATUS', array('NON PM','CLOSED')) 
		  				->where("A.EXIST","1")
		  				// ->where("A.PM_NIK IS NOT NULL")
		  				->order_by("TOTAL_PLAN","DESC");
		 $raw = $raw->get()->result_array();

		 // echo json_encode($arrayIdp);die;
		 foreach ($raw as $key => $value) {
		 	if (in_array(trim($value['ID_PROJECT']), $arrayIdp)) {
		 		array_push($data, $value);
		 		array_push($idDes, $value['ID_PROJECT']);
		 	}
		 }

		 foreach ($arrayIdp as $key => $value) {
		 	if (!in_array($value, $idDes)) {
		 		array_push($data_d, $value);
		 	}else{
		 		array_push($data_r, $value);
		 	}
		 }

		 $delete = 0;
		 foreach ($data_d as $key => $value) {
		 	$this->db->query("UPDATE PRIME_PROJECT SET IS_EXIST = 0 WHERE ID_PROJECT = '".$value."' AND STATUS = 'NON PM' AND DATE_UPDATED < "."to_date('".date('d/m/Y')."','DD/MM/YYYY')");
		 	$delete++;
		 }

		 return $data;

    }


     function updateDesProjectPartner(){
		$arrayIdp 	= $arrayId 	= $arrayKb = $data =  array();;
		$count 		= 0;
		$id_global 	= $this->db->from("PRIME_PROJECT")->select("ID_PROJECT ,ID_PROJECT_GLOBAL")->distinct()->where("DIVISION","DES")->get()->result_array();

		foreach ($id_global as $key => $value) {
			array_push($arrayId, trim($value["ID_PROJECT"]));
		}

		$des = $this->load->database('des', TRUE);
		foreach ($arrayId as $key => $value) {
				$this->db->query("DELETE PRIME_PROJECT_PARTNER WHERE ID_PROJECT = '".$value."'"  );
			  	$raw 	= $des->select(
						"
						TO_CHAR(A.TGL_SIRKULIR,'DD/MM/YYYY') DATE_CREATED,
						TO_CHAR(A.DATE_UPDATED,'DD/MM/YYYY') DATE_UPDATED,
						A.ID_PROJECT,
						A.ID_PARTNER, 
						A.NO_P8 SPK, 
						TO_CHAR(A.TGL_P8,'DD/MM/YYYY') SPK_DATE,
						CASE WHEN B.NO_KL =  'null' THEN NULL ELSE B.NO_KL END AS KL
						")
	                    ->from('PRIME_PROJECT_PARTNERS A')
	                    ->join("PRIME_PROJECT B","A.ID_PROJECT = B.ID_PROJECT","LEFT")
		  				->where("A.ID_PROJECT",$value)
		  				->where("A.NO_P8 IS NOT NULL")
		  				->get()->result_array();

		  		foreach ($raw as $key1 => $value1) {
		  			if (!empty($value1['ID_PARTNER'])) {
		  				if (empty($value1['DATE_CREATED'])) {
				            $this->db->set('DATE_CREATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
				        }
				        if (empty($value1['DATE_UPDATED'])) {
				            $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
				        }
			  			foreach ($value1 as $key2 => $value2) {
			  				if($key2=='DATE_CREATED' || $key2=='DATE_UPDATED' || $key2=='SPK_DATE' || $key2=='KL_DATE'){
			  					if (!empty($value2) && $value2 != 'null') {
			  						 $this->db->set($key2, "TO_DATE('".$value2."','DD/MM/YYYY')",false);
			  						}
				                }else{
				                    $this->db->set($key2, $value2);
				                }
			  			}

			  			$this->db->insert('PRIME_PROJECT_PARTNER');
			  			$count++;
		  			}

		  		}
		}
		echo "PRIME_PROJECT_PARTNER HAS BEEN UPDATED : ".$count;

		// $des = $this->load->database('des', TRUE);
  //       $raw 	= $des
		// 			->select(
		// 				"
		// 				A.ID_PROJECT ID_PROJECT,
		// 				A.ID_LOP_EPIC AS ID_PROJECT_GLOBAL, 
		// 				A.NAME, 
		// 				A.NIP_NAS AS CUSTOMER, 
		// 				A.SEGMEN AS SEGMEN, 
		// 				A.NO_KB AS KB, 
		// 				A.VALUE, 
		// 				TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,
		// 				TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE,
		// 				A.TYPE AS CATEGORY, 
		// 				A.AM_NIK||'@telkom.co.id' AS AM, 
		// 				A.PM_NIK||'@telkom.co.id' AS PM,
		// 				A.REGIONAL, 
		// 				A.DESCRIPTION, 
		// 				A.STATUS, 
		// 				A.SCALE, 
		// 				A.LESSON_LEARNED AS LESSON_LEARN, 
		// 				A.STATUS AS PROGRESS, 
		// 				A.STRATEGIS AS IS_STRATEGIC, 
		// 				NVL(C.PLAN,0) AS TOTAL_WEIGHT,
		// 				NVL(C.PLAN,0) AS TOTAL_PLAN,
		// 				NVL(C.ACH,0)  AS TOTAL_ACHIEVEMENT,
		// 				TO_CHAR(A.CLOSED_DATE,'DD/MM/YYYY') DATE_CLOSED, 
		// 				A.CLOSED_BY_ID  AS CLOSE_BY_ID,
		// 				'SYNC DES '||A.SOURCE_PROJECT AS SOURCE,
		// 				'DES' AS DIVISION
		// 				")
	 //                    ->from('PRIME_PROJECT A')
	 //                    ->join("PRIME_MONITORING_PROJECT C","C.ID_PROJECT = A.ID_PROJECT","LEFT")
	 //                    ->where_in('A.STATUS', array('LEAD','LAG','DELAY','CLOSED','NON PM')) 
		//   				->where("A.EXIST","1")
		//   				// ->where("A.PM_NIK IS NOT NULL")
		//   				->order_by("TOTAL_PLAN","DESC");

		//  $raw = $raw->get()->result_array();
		//  foreach ($raw as $key => $value) {
		//  	if (in_array($value['ID_PROJECT'], $arrayIdp)) {
		//  		array_push($data, $value);
		//  	}
		//  }
		//  return $data;

    } 

    function addDesDeliverable(){
		$arrayIdp 	= $arrayId 	= $arrayDl = $data =  array();
		$id_global 	= $this->db->from("PRIME_PROJECT")->select("ID_PROJECT ,ID_PROJECT_GLOBAL")->distinct()->where("DIVISION","DES")->get()->result_array();
		$id_del_ebis = $this->db->from("PRIME_PROJECT_DELIVERABLE")->select("ID")->get()->result_array();

		foreach ($id_global as $key => $value) {
			array_push($arrayId, trim($value["ID_PROJECT_GLOBAL"]));
			array_push($arrayIdp, trim($value["ID_PROJECT"]));
		}

		foreach ($id_del_ebis as $key => $value) {
			array_push($arrayDl, trim($value["ID"]));
		}

		$des = $this->load->database('des', TRUE);
        $raw 	= $des
					->select(
						"
						A.ID_DELIVERABLE ID,
						A.ID_PROJECT ID_PROJECT,
						A.NAME, 
						A.WEIGHT, 
						'DES' AS PIC, 
						CASE WHEN A.WEIGHT =  0 THEN 0 ELSE NVL((A.PROGRESS_VALUE / A.WEIGHT) * 100, 0) END AS ACHIEVEMENT, 
						A.DESCRIPTION, 
						TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,
						TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE
						")
	                    ->from('PRIME_PROJECT_DELIVERABLES A');


		 $raw = $raw->get()->result_array();

		 // echo json_encode($arrayIdp);die;
		 foreach ($raw as $key => $value) {
		 	if (in_array($value['ID_PROJECT'], $arrayIdp) && !in_array($value['ID'], $arrayDl) && !empty($value['NAME']) && !empty($value['START_DATE']) && !empty($value['END_DATE']) && !empty($value['ID_PROJECT']) ) {
		 		array_push($data, $value);
		 	}
		 }

		 return $data;

    } 

    function updateDesDeliverable(){
		$arrayIdp 	= $arrayId 	= $arrayDl = $data =  array();
		$id_global 	= $this->db->from("PRIME_PROJECT")->select("ID_PROJECT ,ID_PROJECT_GLOBAL")->distinct()->where("DIVISION","DES")->get()->result_array();
		$id_del_ebis = $this->db->from("PRIME_PROJECT_DELIVERABLE")->select("ID")->get()->result_array();

		foreach ($id_global as $key => $value) {
			array_push($arrayId, trim($value["ID_PROJECT_GLOBAL"]));
			array_push($arrayIdp, trim($value["ID_PROJECT"]));
		}

		foreach ($id_del_ebis as $key => $value) {
			array_push($arrayDl, trim($value["ID"]));
		}

		$des = $this->load->database('des', TRUE);
        $raw 	= $des
					->select(
						"
						A.ID_DELIVERABLE ID,
						A.ID_PROJECT ID_PROJECT,
						A.NAME, 
						A.WEIGHT, 
						'DES' AS PIC, 
						CASE WHEN A.WEIGHT =  0 THEN 0 ELSE NVL((A.PROGRESS_VALUE / A.WEIGHT) * 100, 0) END AS ACHIEVEMENT, 
						A.DESCRIPTION, 
						TO_CHAR(A.START_DATE,'DD/MM/YYYY') START_DATE,
						TO_CHAR(A.END_DATE,'DD/MM/YYYY') END_DATE
						")
	                    ->from('PRIME_PROJECT_DELIVERABLES A');

		 $raw = $raw->get()->result_array();

		 // echo json_encode($arrayIdp);die;
		 foreach ($raw as $key => $value) {
		 	if (in_array($value['ID_PROJECT'], $arrayIdp) && in_array($value['ID'], $arrayDl) && !empty($value['NAME']) && !empty($value['START_DATE']) && !empty($value['END_DATE']) && !empty($value['ID_PROJECT']) ) {
		 		array_push($data, $value);
		 	}
		 }

		 return $data;

    } 

    function syncMasterDes(){
    	$des = $this->load->database('des', TRUE);


    	// SYNC SEGMEN
    	$querySegmenEbis  	= $this->db->from("PRIME_SEGMEN")->get()->result_array();
    	$segmenEbis  		= array_column($querySegmenEbis, "CODE");
    	$querySegmenDes = $des->select("SEGMEN AS CODE")
	                    ->from('PRIME_PROJECT')
	                    ->distinct()
	                    ->get() 
		  				->result_array();
		$segmenDes = array_column($querySegmenDes, "CODE");


		$resultSegmen 		= array();
		$resultSegmenUpdate = array();
		foreach ($segmenDes as $key => $value) {
			if (!empty($value)) {
				if (!in_array($value, $segmenEbis)) {
			 		$segmen['ID'] 	= $this->getGUID();
			 		$segmen['CODE'] = $value;
			 		$segmen['NAME'] = $value;
			 		$this->db->set("DATE_CREATED",'SYSDATE',FALSE)->set("DATE_UPDATED",'SYSDATE',FALSE)->insert("PRIME_SEGMEN",$segmen);
			 		array_push($resultSegmen, $segmen);
			 	}else{
			 		$segmenUpdate['CODE'] = $value;
			 		$segmenUpdate['NAME'] = $value;	
			 		$this->db->set("DATE_CREATED",'SYSDATE',FALSE)->set($segmenUpdate)->where("CODE",$segmenUpdate["CODE"])->update("PRIME_SEGMEN");
			 		array_push($resultSegmenUpdate, $segmenUpdate);
			 	}
			}
		 }
		 echo "SEGMEN SYNC ADD: ".count($resultSegmen)."<br>";
		 echo "SEGMEN SYNC UPDATE: ".count($resultSegmenUpdate)."<br>";
		 //END SYNC SEGMEN

		 //SYNC AM
    	$queryAmEbis  	= $this->db->from("PRIME_AM")->get()->result_array();
    	$amEbis  		= array_column($queryAmEbis, "EMAIL");
    	$queryAmDes 	= $des->select("AM_NAME AS NAME, AM_NIK||'@telkom.co.id' AS EMAIL")
	                    ->from('PRIME_PROJECT')
	                    ->distinct()
	                    ->get() 
		  				->result_array();
		$amDes  = array();
		foreach ($queryAmDes as $key => $value) {
			$amDes[$value['EMAIL']] = $value['NAME'];
		}
		$resultAm = array();
		$resultAmUpdate = array();
		foreach ($amDes as $key => $value) {
			if (!empty($value)) {
				if (!in_array(trim($key), $amEbis)) {
					$am['ID'] 		= $this->getGUID();
			 		$am['EMAIL'] 	= $key;
			 		$am['NAME'] 	= $value;
			 		array_push($resultAm, $am);
			 		$this->db->set("DATE_CREATED",'SYSDATE',FALSE)->set("DATE_UPDATED",'SYSDATE',FALSE)->insert("PRIME_AM",$am);
				}else{
			 		$amUpdate['EMAIL'] 	= $key;
			 		$amUpdate['NAME'] 	= $value;
			 		array_push($resultAmUpdate, $amUpdate);
			 		$this->db->set("DATE_UPDATED",'SYSDATE',FALSE)->set("NAME",$value)->where("EMAIL",$key)->update("PRIME_AM");
				}
			}
		}
		echo "AM SYNC ADD: ".count($resultAm)."<br>";
		echo "AM SYNC UPDATE: ".count($resultAmUpdate)."<br>";
    	//END SYNC AM


		// SYNC PM
		$resultPm = array();
		$resultPmUpdate = array();
    	$queryPmEbis  	= $this->db->from("PRIME_PM")->get()->result_array();
    	$pmEbis  		= array_column($queryPmEbis, "EMAIL");
    	$queryPmDes 	= $des->select("PM_NAME AS NAME, PM_NIK||'@telkom.co.id' AS EMAIL")
	                    ->from('PRIME_PROJECT')
	                    ->distinct()
	                    ->where('PM_NIK !=','NULL',true)
	                    ->get() 
		  				->result_array();

		foreach ($queryPmDes as $key => $value) {
			if (!empty($value["EMAIL"])) {
				if (!in_array(trim($value['EMAIL']), $pmEbis)) {
					$pm['ID'] 		= $this->getGUID();
			 		$pm['EMAIL'] 	= $value['EMAIL'];
			 		$pm['NAME'] 	= $value['NAME'];
			 		array_push($resultPm, $pm);
			 		$this->db->set("DATE_CREATED",'SYSDATE',FALSE)->set("DATE_UPDATED",'SYSDATE',FALSE)->insert("PRIME_PM",$pm);
				}else{
			 		$pmUpdate['EMAIL'] 	= $value['EMAIL'];
			 		$pmUpdate['NAME'] 	= $value['NAME'];
			 		array_push($resultPmUpdate, $pmUpdate);
			 		$this->db->set("DATE_UPDATED",'SYSDATE',FALSE)->set("NAME",$pmUpdate["NAME"])->where("EMAIL",$pmUpdate["EMAIL"])->update("PRIME_PM");
				}
			}
		}
		echo "PM SYNC ADD: ".count($resultPm)."<br>";
		echo "PM SYNC UPDATE: ".count($resultPmUpdate)."<br>";
		 // echo json_encode($queryPmDes);die;
		// END SYNC PM

		// SYNC CUSTOMER
		$resultCc 		= array();
		$resultCcUpdate = array();
    	$queryCcEbis  	= $this->db->from("PRIME_CUSTOMER")->get()->result_array();
    	$ccEbis  		= array_column($queryCcEbis, "CODE");
    	$queryCcDes 	= $des->select("NIP_NAS AS CODE, STANDARD_NAME AS NAME")
	                    ->from('PRIME_PROJECT')
	                    ->distinct()
	                    ->where('NIP_NAS !=','NULL',true)
	                    ->get() 
		  				->result_array();

		foreach ($queryCcDes as $key => $value) {
			if (!empty($value['CODE'])) {
				if (!in_array(trim($value['CODE']), $ccEbis)) {
					$cc['ID'] 		= $this->getGUID();
			 		$cc['CODE'] 	= $value['CODE'];
			 		$cc['NAME'] 	= $value['NAME'];
			 		array_push($resultCc, $cc);
			 		$this->db->set("DATE_CREATED",'SYSDATE',FALSE)->set("DATE_UPDATED",'SYSDATE',FALSE)->insert("PRIME_CUSTOMER",$cc);
				}else{
			 		$ccUpdate['CODE'] 	= $value['CODE'];
			 		$ccUpdate['NAME'] 	= $value['NAME'];
			 		array_push($resultCcUpdate, $ccUpdate);
			 		$this->db->set("NAME",$value["NAME"])->set("DATE_UPDATED",'SYSDATE',FALSE)->where("CODE",$value["CODE"])->update("PRIME_CUSTOMER");
				}
			}
		}
		echo "CC SYNC ADD: ".count($resultCc)."<br>";
		echo "CC SYNC UPDATE: ".count($resultCcUpdate)."<br>";
		// END SYNC CUSTOMER

		// SYNC PARTNER
		$resultPartner 		= array();
		$resultPartnerUpdate = array();
    	$queryPartnerEbis  	= $this->db->from("PRIME_PARTNER")->get()->result_array();
    	$partnerEbis  		= array_column($queryPartnerEbis, "CODE");
    	$queryPartnerDes 	= $des->select("KODE_PARTNERS AS CODE, NAMA_PARTNERS AS NAME")
			                    ->from('PRIME_PARTNER_DH')
			                    ->distinct()
			                    ->get() 
				  				->result_array();

		foreach ($queryPartnerDes as $key => $value) {
			if (!empty($value['CODE'])) {
				if (!in_array(trim($value['CODE']), $partnerEbis)) {
					$partner['ID'] 		= $this->getGUID();
			 		$partner['CODE'] 	= $value['CODE'];
			 		$partner['NAME'] 	= $value['NAME'];
			 		array_push($resultPartner, $partner);
			 		$this->db->set("DATE_CREATED",'SYSDATE',FALSE)->set("DATE_UPDATED",'SYSDATE',FALSE)->insert("PRIME_PARTNER",$partner);
				}else{
			 		$partnerUpdate['CODE'] 	= $value['CODE'];
			 		$partnerUpdate['NAME'] 	= $value['NAME'];
			 		array_push($resultPartnerUpdate, $partnerUpdate);
			 		$this->db->set("NAME",$value["NAME"])->set("DATE_UPDATED",'SYSDATE',FALSE)->where("CODE",$value["CODE"])->update("PRIME_PARTNER");
				}
			}
			
		}
		echo "PARTNER SYNC ADD: ".count($resultPartner)."<br>";
		echo "PARTNER SYNC UPDATE: ".count($resultPartnerUpdate)."<br>";
		// END SYNC PARTNER

		// SYNC CATEGORY PROJECT
		$resultCat 		= array();
    	$queryCatEbis  	= $this->db->from("PRIME_CONFIG")->where("TYPE","TYPE_PROJECT")->get()->result_array();
    	$CatEbis  		= array_column($queryCatEbis, "VALUE");
    	$queryCatDes 	= $des->select("TYPE AS VALUE")
			                    ->from('PRIME_PROJECT')
			                    ->where('TYPE !=',"NULL",TRUE)
			                    ->distinct()
			                    ->get() 
				  				->result_array();
		$maxID = $this->db->from("PRIME_CONFIG")->order_by("CAST(ID as INTEGER) ","DESC")->get()->row_array();
		$idDef = intval($maxID["ID"]) + 1;

		// echo json_encode($queryCatDes);die;
		foreach ($queryCatDes as $key => $value) {
			if (!in_array(trim($value['VALUE']), $CatEbis) && !empty($value['VALUE'])) {
				$cat['ID'] 		= $idDef;
		 		$cat['TYPE'] 	= 'TYPE_PROJECT';
		 		$cat['VALUE'] 	= $value['VALUE'];
		 		$cat['ACTIVE'] 	= 1;
		 		$cat['REMARKS'] = "IMPORT FROM PRIME DES";
		 		array_push($resultCat, $cat);
		 		$idDef++;
		 		// echo json_encode($cat);die;
		 		$this->db->insert("PRIME_CONFIG",$cat);
			}
		}
		echo "CATEGORY SYNC : ".count($resultCat)."<br>";
		// END SYNC CATEGORY
    }

    function checkGlobalProject($id){
    	return $this->db->get_where('PRIME_PROJECT',array('UPPER(ID_PROJECT_GLOBAL)' => trim(strtoupper($id)) ))->result_array();
    }
    function check_kb($kb){
        return $this->db->get_where('PRIME_PROJECT',array('UPPER(KB)' => strtoupper($kb)))->result_array();
    }


    function fixData(){
   		$data = $this->db
						->from('PRIME_PROJECT')
						->like('PM','@')
						->get()
						->result_array(); 	
		

		foreach ($data as $key => $value) {
			$pm = $this->db->select('*')->from("PRIME_PM")->where('EMAIL',$value['PM'])->get()->row_array();

			if (!empty($pm)) {
				if (!empty($pm['ID'])) {
					$this->db->where("PM",$pm['EMAIL'])->set("PM",$pm['ID'])->update("PRIME_PROJECT");
				}
			}       
		}
    }


    function getActive($id = null){
   		$data = $this->db
						->from('PRIME_PROJECT')
						->where('STATUS','ACTIVE');

		if (!empty($id)) {
			$data = $data->where('ID_PROJECT',$id);
		}

		$data = $data->get()->result_array();
		return $data; 	
    }

    function getAllProject($id = null){
   		$data = $this->db
						->from('PRIME_PROJECT');

		if (!empty($id)) {
			$data = $data->where('ID_PROJECT',$id);
		}

		$data = $data->get()->result_array();
		return $data; 	
    }

    function getIdDesProjectSaved(){
    	$idDes 	= array();
   		$data 	= $this->db
						->from('PROJECT')
						->where('DIVISION','DES')
						->where('STATUS','ACTIVE')
						->get()->result_array();
		
		foreach ($data as $key => $value) {
			array_push($idDes, $value['ID_PROJECT']);
		}
		return $idDes; 	
    }

    function updateSymptom($id){
    	$des 		= $this->load->database('des', TRUE);
        $rawIssue 	= $des->select("TO_CHAR(DATE_CREATED,'DD/MM/YYYY') DATE_CREATED,
        							ID_PROJECT,
        							ID ID_SYMPTOM,
        							'SYSTEM' UPDATED_BY_ID,
        							'SYSTEM' UPDATED_BY_NAME,
        							SYMPTOM")->from("PRIME_PROJECT_SYMPTOM")->where_in("ID_PROJECT",($id))->get()->result_array();

        $update = $insert = 0;
        foreach ($rawIssue as $key => $value) {
        	$symptom = null;
        	$cek = $this->db->from('PRIME_PROJECT_SYMPTOM')
	    				->where('ID_SYMPTOM',$value['ID_SYMPTOM'])
	    				->get()->row_array();

        	foreach ($value as $key1 => $value1) {
        		if ($key1 == 'DATE_CREATED') {
	        		$this->db->set($key1, "TO_DATE('".$value1."','DD/MM/YYYY')",FALSE);
	        	}else if($key1 == 'SYMPTOM'){
	        		switch ($value1) {
	        			case '1.Delivery barang/jasa (mitra)':
	        				$symptom = 24;
	        				break;
	        			case '10.Keterbatasan kapabilitas mitra (mitra)':
	        				$symptom = 44; 
	        				break;
	        			case '11.Komitmen mitra(termasuk deal harga)(mitra)':
	        				$symptom = 43; 
	        				break;
	        			case '12.Keterbatasan kapabilitas Telkom (Telkom)':
	        				$symptom = 42; 
	        				break;
	        			case '13.Kendala Finansial ( Customer )':
	        				$symptom = 24; 
	        				break;
	        			case '2.Kesiapan lokasi ( customer)':
	        				$symptom = 25; 
	        				break;
	        			case '3.Perubahan desain/spek pelanggan (customer)':
	        				$symptom = 26; 
	        				break;
	        			case '4.Keterlambatan BAST (customer)':
	        				$symptom = 27; 
	        				break;
	        			case '5.Keterlambatan SPK ke mitra (di awal) (Telkom)':
	        				$symptom = 28; 
	        				break;
	        			case '6.Masalah Administrasi & pembayaran mitra (Telkom)':
	        				$symptom = 29; 
	        				break;
	        			case '7.SoW belum sepakat (Telkom)':
	        				$symptom = 30; 
	        				break;
	        			case '8.CR (perubahan yang belum terkendali) & amandemen (customer)':
	        				$symptom = 41; 
	        				break;
	        			default:
	        				$symptom = null;
	        				break;
	        		}
	        		$this->db->set($key1, $symptom);
	        	}else{
	        		$this->db->set($key1, $value1);
	        	}
        	}

        	if (!empty($value['SYMPTOM']) && !empty($symptom)) {
        		if (empty($cek)) {
		    		$this->db->insert('PRIME_PROJECT_SYMPTOM');
		    		$insert++;
		    	}else{
		    		$this->db->where("ID_SYMPTOM",$value["ID_SYMPTOM"])->update('PRIME_PROJECT_SYMPTOM');
		    		$update++;
		    	}
        	}
        }

        echo "INSERTED : ".$insert."<br>";
        echo "UPDATED : ".$update."<br>";
    }

    function UpdateDesIssue($id){
    	$des 		= $this->load->database('des', TRUE);
        $rawIssue 	= $des->select("ISSUE_NAME NAME, STATUS_ISSUE ISSUE_STATUS, NVL(RISK_IMPACT,'Potential') IMPACT, ID_PROJECT, ID_DELIVERABLE, ID_ISSUE ID, TO_CHAR(INSERTED_DATE,'DD/MM/YYYY') DATE_CREATED, IMPACT DESCRIPTION")->from("PRIME_PROJECT_ISSUE")->where_in("ID_PROJECT",($id))->get()->result_array();

        $update = $insert = 0;
        foreach ($rawIssue as $key => $value) {
        	$cek = $this->db->from('PRIME_PROJECT_ISSUE')
	    				->where('ID',$value['ID'])
	    				->get()->row_array();

        	foreach ($value as $key1 => $value1) {
        		if ($key1 == 'DATE_CREATED') {
	        		$this->db->set($key1, "TO_DATE('".$value1."','DD/MM/YYYY')",FALSE);
	        	}else{
	        		$this->db->set($key1, $value1);
	        	}
        	}

        	if (empty($cek)) {
	    		$this->db->insert('PRIME_PROJECT_ISSUE');
	    		$insert++;
	    	}else{
	    		$this->db->where("ID",$value["ID"])->update('PRIME_PROJECT_ISSUE');
	    		$update++;
	    	}

        }

        echo "INSERTED : ".$insert."<br>";
        echo "UPDATED : ".$update."<br>";
    }

    function updateDesActionPlan($id){
    	$des 		= $this->load->database('des', TRUE);
        $rawAp 	= $des->select("ACTION_STATUS,  
        						TO_CHAR(ACTION_CLOSED_DATE,'DD/MM/YYYY') DATE_CLOSED, 
        						TO_CHAR(INSERTED_DATE,'DD/MM/YYYY') DATE_CREATED, 
        						TO_CHAR(LAST_UPDATE,'DD/MM/YYYY') DATE_UPDATED, 
        						ACTION_REMARKS DESCRIPTION, 
        						TO_CHAR(INSERTED_DATE,'DD/MM/YYYY') START_DATE, 
        						TO_CHAR(DUE_DATE,'DD/MM/YYYY') END_DATE, 
        						ID_ACTION_PLAN ID, 
        						ID_ISSUE, 
        						ID_PROJECT, 
        						ACTION_NAME NAME, 
        						CASE WHEN ASSIGN_TO !=  'SEGMEN' AND ASSIGN_TO !=  'SDV' AND ASSIGN_TO !=  'DES' THEN '' 
        							ELSE ASSIGN_TO END AS PIC")
        		->from("PRIME_PROJECT_ACTION_PLAN")
        		->where("ID_ISSUE IS NOT NULL")
        		->where_in("ID_PROJECT",($id))->get()->result_array();

        $update = $insert = 0;
        foreach ($rawAp as $key => $value) {
        	$cek = $this->db->from('PRIME_PROJECT_ACTION')
	    				->where('ID',$value['ID'])
	    				->get()->row_array();

        	foreach ($value as $key1 => $value1) {
        		if ($key1 == 'DATE_CREATED' ||  $key1 == 'DATE_UPDATED' || $key1 == 'DATE_CLOSED' || $key1 == 'START_DATE' || $key1 == 'END_DATE') {
	        		$this->db->set($key1, "TO_DATE('".$value1."','DD/MM/YYYY')",FALSE);
	        	}else if ($key1 == 'PIC') {
	        		if ($value1=='SEGMEN') {
	        			$sg = $this->db->select('*')->from('PRIME_PROJECT')->where('ID_PROJECT',$value['ID_PROJECT'])->get()->row_array();
	        			if (!empty($sg)) {
	        				$this->db->set($key1, $sg['SEGMEN']);
	        				$this->db->set('PIC_NAME', $sg['SEGMEN']);
	        			}
	        		}else{
	        			$this->db->set($key1, $value1);
	        		}
	        	}else{
	        		$this->db->set($key1, $value1);
	        	}
        	}

        	if (empty($cek)) {
	    		$this->db->insert('PRIME_PROJECT_ACTION');
	    		$insert++;
	    	}else{
	    		$this->db->where("ID",$value["ID"])->update('PRIME_PROJECT_ACTION');
	    		$update++;
	    	}

        }

        echo "INSERTED : ".$insert."<br>";
        echo "UPDATED : ".$update."<br>";
        echo json_encode($rawAp);
    }


    function fixChart($id_project){
    	$deliverable = $this->db->select("ID, ID_PROJECT, NAME, PIC, DESCRIPTION")->from("PRIME_PROJECT_DELIVERABLE")->where("ID_PROJECT",$id_project)->get()->result_array();

    	foreach ($deliverable as $keyDel => $del) {
    		$this->updateDeliverable($del["ID"],$del);
    	}

   		$fp = $this->db->query("SELECT MAIN.*, B.WEIGHT, A.ACH ACH FROM PRIME_PROJECT_PLAN_DELIVERABLE MAIN
                                    JOIN ( SELECT ID_DELIVERABLE, WEEK, SUM(ACHIEVEMENT) ACH FROM PRIME_PROJECT_ACHIEVEMENT  GROUP BY ID_DELIVERABLE, WEEK) A ON MAIN.ID_DELIVERABLE = A.ID_DELIVERABLE AND MAIN.WEEK = A.WEEK
                                    JOIN PRIME_PROJECT_DELIVERABLE B ON MAIN.ID_DELIVERABLE = B.ID
                                    WHERE MAIN.ID_DELIVERABLE IN  (SELECT ID FROM PRIME_PROJECT_DELIVERABLE WHERE ID_PROJECT = '".$id_project."') ORDER BY MAIN.ID_DELIVERABLE, MAIN.WEEK")->result_array();

            foreach ($fp as $key => $value) {
                 if ($value['ACH'] >= '100' && $value['ACHIEVEMENT'] != 0 && $value['WEIGHT'] - $value['ACHIEVEMENT'] < 1 && $value['WEIGHT'] - $value['ACHIEVEMENT'] > 0  ) {
                 	$up = $value['ACHIEVEMENT'] + ($value['WEIGHT'] - $value['ACHIEVEMENT']);
                 	$this->db->query("UPDATE PRIME_PROJECT_PLAN_DELIVERABLE SET ACHIEVEMENT = ".$up." WHERE ID_PROJECT = '".$id_project."' AND ID_DELIVERABLE = '".$value['ID_DELIVERABLE']."' AND WEEK = ".$value['WEEK']);
                 }
             } 
    }

    function updateDeliverable($id,$data){
        $this->db->set('DATE_UPDATED', "to_date('".date('d/m/Y H:i:s')."','DD/MM/YYYY HH24:MI:SS')",false);
        foreach ($data as $key => $value) {
                if($key=='START_DATE' || $key=='END_DATE'){
                    $this->db->set($key, "TO_DATE('".$value."','DD/MM/YYYY')",false);
                }else{
                    if(!empty($value)){$this->db->set($key , $value);}  
                }
        }
        $this->db->where('ID',$id);
        $this->db->update('PRIME_PROJECT_DELIVERABLE');

        $data = $this->db->get_where('PRIME_PROJECT_DELIVERABLE', array('ID' =>$id ))->row_array();
        
            $project_date = $this->getProjectDate($data['ID_PROJECT']);

            $start  = str_replace('/', '-', $data['START_DATE']);
            $end    = str_replace('/', '-', $data['END_DATE']);

            $day   = 24 * 3600;
            $from  = strtotime($start);
            $to    = strtotime($end) + $day;
            $diff  = abs($to - $from);
            $week_deliverable = round($diff / $day / 7);
            $checkMonday = date('D', $from);
            $weight_week = 0;
        if($week_deliverable > 0){
            $weight_week = $data['WEIGHT'] / $week_deliverable;
        }else{
            $weight_week = $data['WEIGHT'] / 1;
        }
            $weight_week = number_format((float)$weight_week, 2, '.', '');

        // echo $data['WEIGHT']."'<br>".$week_deliverable;

        $start_deliverable_project  = round(abs($from - $project_date['START']) / $day / 7);
        $end_deliverable_project    = $start_deliverable_project + $week_deliverable;

        $total_weight_week = 0;
        $this->db->where('ID_DELIVERABLE',$data['ID'])->delete('PRIME_PROJECT_PLAN_DELIVERABLE');
        if ($end_deliverable_project - $start_deliverable_project <= 0) {
            $insert_plan_deliverable = $this->db->set('WEEK',1)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN',$weight_week)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
        }else{
            for ($i=$start_deliverable_project + 1; $i <= $end_deliverable_project; $i++) { 
                $total_weight_week += $weight_week;
                if($total_weight_week < $data['WEIGHT'] && $i == $end_deliverable_project ){
                    $gap_close = ($data['WEIGHT'] - $total_weight_week) + $weight_week;
                    $gap_close = number_format((float)$gap_close, 2, '.', '');
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN', $gap_close)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');
                }elseif($total_weight_week <= $data['WEIGHT']){
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN',$weight_week)
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE');

                }else{
                    // if($data['WEIGHT'] - $total_weight_week > $weight_week){
                    $insert_plan_deliverable = $this->db->set('WEEK',$i)
                                                    ->set('ID_PROJECT',$data['ID_PROJECT'])
                                                    ->set('ID_DELIVERABLE',$data['ID'])
                                                    ->set('PLAN', (($total_weight_week - $data['WEIGHT']) + $weight_week))
                                                    ->insert('PRIME_PROJECT_PLAN_DELIVERABLE'); 
                    // }
                }                   
            }
        }
        // die;
        $achievement = $this->db
                        ->select("*")
                        ->from("PRIME_PROJECT_ACHIEVEMENT")
                        ->where("ID_DELIVERABLE",$data["ID"])
                        ->get()
                        ->result_array();

        foreach ($achievement as $key => $value) {
            $updt =  ($value["ACHIEVEMENT"] / 100) * number_format((float)$data['WEIGHT'], 2, '.', '') ;
            $this->db->set("ACHIEVEMENT",$updt)->where("ID_DELIVERABLE",$data["ID"])->where("WEEK",$value["WEEK"])->update("PRIME_PROJECT_PLAN_DELIVERABLE");
            
            echo $this->db->last_query()."<br>";
            }
        // $this->db->where('ID_DELIVERABLE',$data['ID'])->where('WEEK > '.$end_deliverable_project)->delete('PRIME_PROJECT_PLAN_DELIVERABLE');
        return true;
    }

    function update_achievement($value){   
		// if ($value['ID_PROJECT'] == 'PROJ7948') {
			$total = $total1 = 0;
    		$qtotal = $this->db->query("SELECT MAX(WEEK) T_WEEK FROM PRIME_PROJECT_PLAN_DELIVERABLE WHERE ID_PROJECT = '".$value["ID_PROJECT"]."'"." AND ID_DELIVERABLE = '".$value["ID"]."'"  )->row_array();

    		$qtotal1 = $this->db->query("SELECT MIN(WEEK) T_WEEK FROM PRIME_PROJECT_PLAN_DELIVERABLE WHERE ID_PROJECT = '".$value["ID_PROJECT"]."'"." AND ID_DELIVERABLE = '".$value["ID"]."'"  )->row_array();
	    	if (!empty($qtotal["T_WEEK"])) {
	    		$total = $qtotal["T_WEEK"];
	    	}
	    	if (!empty($qtotal1["T_WEEK"])) {
	    		$total1 = $qtotal1["T_WEEK"];
	    	}
	    	$avgAchievement = $value["ACHIEVEMENT"] / (($total - $total1) + 1);
	    	$this->db->query("DELETE PRIME_PROJECT_ACHIEVEMENT WHERE ID_DELIVERABLE = '".$value["ID"]."'"  );
	    	$ins2 = array();
	    	for ($i=$total1; $i <= $total ; $i++) { 
	    		$ins["ID"] 				= $this->getGUID();
	    		$ins["ID_DELIVERABLE"] 	= $value["ID"];
	    		$ins["ACHIEVEMENT"] 	= number_format((float)$avgAchievement, 2, '.', '');
	    		$ins["WEEK"] 			= $i;
	    		$daysWeek 				= $i*7;
	    		$this->db->set("DATE_ACHIEVED", "TO_DATE('".$value["START_DATE"]."','DD/MM/YYYY') + ".$daysWeek,false);
	    		foreach ($ins as $keyx => $valuex) {
	    			if(!empty($valuex)){$this->db->set($keyx , $valuex);}	
	    		}
	    		if (!empty($ins["WEEK"]) && $ins["WEEK"] != 0) {
	    			$this->db->insert('PRIME_PROJECT_ACHIEVEMENT');
	    		}
	    		$ins2[$i]["ID_PROJECT"] 	= $value["ID_PROJECT"];
    			$ins2[$i]["WEEK"] 			= $i;

    			if (empty($ins2[$i]["ACHIEVEMENT"])) {
    				$ins2[$i]["ACHIEVEMENT"] = $ins["ACHIEVEMENT"];
    			}else{
    				$ins2[$i]["ACHIEVEMENT"] = $ins2[$i]["ACHIEVEMENT"] + $ins["ACHIEVEMENT"];
    			}
	    	}
	    	foreach ($ins2 as $key => $value) {
	    		$cek =  $this->db->from('PRIME_PROJECT_PLAN A')
	                    ->where('A.ID_PROJECT', $value["ID_PROJECT"])
	                    ->where('A.WEEK', $value['WEEK'])
	                    ->get()
	                    ->row_array();
	           	if(!empty($cek)) {
	           		$this->db->where("ID_PROJECT",$value["ID_PROJECT"])->where("WEEK",$value['WEEK'])->set('ACHIEVEMENT' , $value["ACHIEVEMENT"])->update("PRIME_PROJECT_PLAN");
	           	}else{
	           		$this->db->insert("PRIME_PROJECT_PLAN",$value);
	           	}
	    	}
	    	// echo $this->db->last_query();die;

		// }
    }


    public function syncDocument(){
        $data = $this->db
						->from('PRIME_PROJECT')
						->where('STATUS','ACTIVE')
						->where('DIVISION','DES')
						->get()
						->result_array();

		$idDes 	= array();
		$docDes = array();
		foreach ($data as $key => $value) {
			array_push($idDes, $value['ID_PROJECT']);
			$docDes[$value['ID_PROJECT']] = array();
		}

		$des 		= $this->load->database('des', TRUE);
        $table_d 	= $des
					->select(
					"
					A.ID_PROJECT,
					A.NAME,
					A.ATTACHMENT
					")
                    ->from('PRIME_PROJECT_DOCUMENT A')
                    ->where_in('A.ID_PROJECT', $idDes)
                    ->get()
                    ->result_array();

        foreach ($table_d as $key => $value) {
        	$ins = array();
        	$ins['ID_PROJECT'] = $value['ID_PROJECT'];
        	$ins['NAME'] 	  = $value['NAME'];
        	$ins['PATH'] 	  = '../_files/'.$value['ATTACHMENT'];
        	array_push($docDes[$value['ID_PROJECT']], $ins);
        }

        $inserCount = 0;
        foreach ($docDes as $key => $value) {
        	if (!empty($value)) {
        		foreach ($value as $key1 => $value1) {
        			$cek =  $this->db->from('PRIME_PROJECT_DOCUMENT A')
	                    ->where('A.ID_PROJECT', $key)
	                    ->where('A.PATH', $value1['PATH'])
	                    ->get()
	                    ->row_array();
		            if (empty($cek)) {
		            	$this->db->insert("PRIME_PROJECT_DOCUMENT",$value1);
		            	$inserCount++;
		            }
        		}
        	}
        }

        echo "DOC SYNC : ".$inserCount;

        echo "<br>".json_encode($docDes);die;

     //    $table_p 	= $des
					// ->select(
					// "
					// A.ID_PROJECT,
					// A.DOC_AANWIZING,
					// A.DOC_BAKN_PB,
					// A.DOC_KB,
					// A.DOC_KL,
					// A.DOC_PROPOSAL,
					// A.DOC_RFP,
					// A.DOC_SPK
					// ")
     //                ->from('PRIME_PROJECT A')
     //                ->where_in('A.ID_PROJECT', $idDes)
     //                ->get()
     //                ->result_array();

     //    foreach ($table_p as $key => $value) {
     //    	if (!empty($value['DOC_AANWIZING'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_AANWIZING']);
     //    	}
     //    	if (!empty($value['DOC_BAKN_PB'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_BAKN_PB']);
     //    	}
     //    	if (!empty($value['DOC_KB'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_KB']);
     //    	}
     //    	if (!empty($value['DOC_KL'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_KL']);
     //    	}
     //    	if (!empty($value['DOC_PROPOSAL'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_PROPOSAL']);
     //    	}
     //    	if (!empty($value['DOC_RFP'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_RFP']);
     //    	}
     //    	if (!empty($value['DOC_SPK'])) {
     //    		array_push($docDes[$value['ID_PROJECT']], $value['DOC_SPK']);
     //    	}
     //    }

    }


    function importDataAm($data){
    	foreach ($data as $key => $value) {
    		$this->db->insert("PRIME_AM",$value);
    	}
    	return true;
    }


    private function getGUID()
    {
        if (function_exists('getGUID')){
            return getGUID();
        }
        else {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = //chr(123)// "{"
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
                //.chr(125);// "}"
            return $uuid;
        }
    } 

    function getProjectDate($id_project){
    	$project = $this->db
    				->select("A.*, TO_CHAR(A.START_DATE, 'DD-MM-YYYY') START_DATE2, TO_CHAR(A.END_DATE, 'DD-MM-YYYY') END_DATE2")
    				->from('PRIME_PROJECT A')
    				->where('ID_PROJECT',$id_project)
    				->get()
    				->row_array();
		

		$tomorrow = date('d-m-Y',strtotime($project['START_DATE2'] . "+1 days")); 

    	$day   = 24 * 3600;
	    $from  = strtotime($project['START_DATE2']);
	    $to    = strtotime($project['END_DATE2']) + $day;
	    $diff  = abs($to - $from);
	    $weeks = round($diff / $day / 7);
	    $checkMonday = date('D', $from);
	    if ($checkMonday=="Mon") { 
			$weeks = $weeks+1;
		}

		$date = array(
			'START' => $from,
            'START_DATE' => $project['START_DATE2'],
			'END' => $to,
            'END_DATE' => $project['END_DATE2'],
            'PROGRESS' => $project['PROGRESS'],
            'STATUS' => $project['STATUS'],
			'TOTAL_WEEK' => $weeks,
		);
	    return $date;
    }


    function fixProjectAccess($id_project){
    	$validUserAccessData = array();
    	$project = $this->db
					->from('PROJECT')
					->where('ID_PROJECT',$id_project)
					->get()
					->row_array();

		$userAccess = array();



		if (!empty($project["PM_EMAIL"])) {
			array_push($userAccess, $project["PM_EMAIL"]);
		}
		if (!empty($project["PM_ADMIN_EMAIL"])) {
			array_push($userAccess, $project["PM_ADMIN_EMAIL"]);
		}
		if (!empty($project["AM_EMAIL"])) {
			array_push($userAccess, $project["AM_EMAIL"]);
		}
		if (!empty($project["CREATED_BY"])) {
			array_push($userAccess, $project["CREATED_BY"]);
		}


		$accessByRole = $this->db
					->from('USERS')
					->where_in('ROLE',array("0","1","2"))
					->where("(DIVISION = '".$project["DIVISION"]."' OR DIVISION = 'EBIS')");

		if (!empty($userAccess)) {
			$accessByRole = $accessByRole->where_not_in('EMAIL',$userAccess);
		}
		
		$accessByRole = $accessByRole->get()->result_array();

		foreach ($accessByRole as $key => $value) {
            if (!in_array($value["EMAIL"], $userAccess)) {
                array_push($userAccess, $value["EMAIL"]);
            }
        }

		if (!empty($userAccess)) {
			$userAccessData = $this->db
					->from('USERS')
					->where_in('EMAIL',$userAccess)
					->get()
					->result_array();
			$userAccessDataEmail = array();
			foreach ($userAccessData as $key => $value) {
				array_push($userAccessDataEmail, $value["EMAIL"]);
			}

			$currentAccesData =   $this->db
					->from('PRIME_PROJECT_ACCESS')
					->where('ID_PROJECT',$id_project)
					->get()
					->result_array();
			$currentAccesDataEmail = array();
			if (!empty($currentAccesData)) {
				foreach ($currentAccesData as $key => $value) {
					array_push($currentAccesDataEmail, $value["EMAIL"]);
				}
			}

			
			if (!empty($currentAccesDataEmail)) {
				foreach ($userAccessDataEmail as $key => $value) {
					if (!in_array($value, $currentAccesDataEmail) ) {
						array_push($validUserAccessData, $value);
					}
				}
			}else{
				$validUserAccessData = $userAccessDataEmail;
			}
		}
		if (!empty($validUserAccessData)) {
			foreach ($validUserAccessData as $key => $value) {
				$this->db
						->set('ID',$this->getGUID())
						->set('ID_PROJECT',$id_project)
						->set('EMAIL',$value)
						->insert('PRIME_PROJECT_ACCESS');
			}
		}
		return $validUserAccessData;
    }
}