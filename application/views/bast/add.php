<style type="text/css">
/*Checkboxes styles*/
input[type="checkbox"] { display: none; }

input[type="checkbox"] + label {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 10px;
  font: 14px/20px;
  color: #000;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

input[type="checkbox"] + label:last-child { margin-bottom: 0; }

input[type="checkbox"] + label:before {
  content: ''; 
  display: block;
  width: 20px;
  height: 20px;
  border: 2px solid #f0f3f5;
  position: absolute;
  left: 0;
  top: 0;
  opacity: .6;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

input[type="checkbox"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}

.card-project  .project-info{
  border-bottom: 1px solid #888 !important;
}

.card-project  .form-control, .card-project  .project-info {
  background: #f0f3f5;
  border-color: #f0f3f5;
  color: #000 !important;
}

.card-project label {
  font-size: 0.7em !important;
}

#slider-baut{
  height: 20px;  
  padding-right: : 5px !important;
  margin-top: 2px !important;
}

#slider-baut:before {
  height: 12px !important;
  width: 12px !important;
  left: 10px !important;
}
</style>

<link href="<?= base_url(); ?>assets/css/project.css" rel="stylesheet">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <span class="judul">ADD BAST <a href="<?= base_url().'p/'.$project['ID_PROJECT'] ?>" target="_blank"><strong><?= $PARTNER_NAME ?></strong></a> </span>
      </div>
      <div class="card-body">
        <form id="form-bast" method="post" action="<?= base_url() ?>bast/add_bast_process"> 
          <div class="row">
            <div class="col-md-12  pl-4 pr-4">
              <div class="card card-project" style="border-radius: 10px;">
                <div class="card-body row" style="background:#f0f3f5;border-radius: 10px;">
                  <div class="form-group col-12"> 
                    <label>Name</label>
                    <input type="hidden" name="id_project" id="id_project" value="<?= $project['ID_PROJECT'] ?>">
                    <div class="w-100 project-info">
                      <strong><?= $project['NAME'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-4"> 
                    <label>Value <small class="text-primary"><strong>exclude PPN</strong></small></label>
                     <div class="w-95 project-info">
                        <strong class="rupiah"><?= $project['VALUE'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-4"> 
                    <label>Customer</label>
                    <div class="w-95 project-info">
                        <strong><?= $project['CUSTOMER_NAME'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-4"> 
                    <label>Kontrak Bersama</label>
                      <div class="w-95 project-info">
                        <strong><?= $project['KB'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-3">
                    <label>Division</label>
                    <div class="w-95 project-info">
                      <strong><?= $project['DIVISION'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-3"> 
                    <label>Segmen</label>
                    <div class="w-95 project-info">
                        <strong><?= $project['SEGMEN_NAME'] ?></strong>
                      </div>
                  </div>
                  
                    <div class="form-group col-3">
                      <label>Category</label>
                      <div class="w-95 project-info">
                        <strong><?= $project['CATEGORY_NAME'] ?></strong>
                      </div>
                    </div>

                    <?php if ($project['PM_NAME']) :  ?>
                      <div class="form-group col-3">
                        <label>Project Manager</label>
                        <div class="w-95 project-info">
                          <strong><?= $project['PM_NAME'] ?></strong>
                        </div>
                      </div>
                    <?php endif; ?>

                    <div class="form-group col-3">
                      <label>Regional</label>
                      <div class="w-95 project-info">
                        <strong><?= $project['REGIONAL_NAME'] ?></strong>
                      </div>
                    </div>

                    
                    
                  
                  <?php if (!empty($project['DESCRIPTION'])) : ?>
                    <div class="form-group col-12"> 
                      <label>Description</label>
                        <div class="w-100 ml-0 mr-0 project-info">
                            <strong><?= $project['DESCRIPTION'] ?></strong>
                          </div>
                    </div>
                  <?php endif; ?>
                  
                  <?php if (!empty($project['document'])) : ?>
                    <div class="w-100">
                      <label>Document</label>
                      <div class="w-100">
                          <ul class="list-group scroll-thin" style="max-height: 160px !important;overflow-y: scroll;">
                            <?php foreach ($project['document'] as $key => $value) : ?>
                                <a target="_blank" href="<?= base_url().'assets/document/'.$project['ID_PROJECT'].'/'.$value['NAME'] ?>" ><li class="list-group-item" style="padding: 0.2rem 1.25rem;"><?= $value['NAME'] ?></li></a>
                            <?php endforeach; ?>
                          </ul>
                      </div>
                    </div>
                  <?php endif; ?>

              </div>
            </div>
            </div>

            <div class="col-md-12">
              <div class="card" style="border-radius: 10px;">
                
                <div class="card-body row ml-0 mr-0" style="background: #4dbd74aa;border-radius: 10px;">
                  <div class="form-group col-6"> 
                    <label>SPK </label>
                    <input type="hidden" name="bast-customer" id="bast-customer" value="<?= $project['CUSTOMER'] ?>">
                    <input type="hidden" name="bast-partner" id="bast-partner" value="<?= $ID_PARTNER ?>">
                    <input type="hidden" name="bast-division" id="bast-division" value="<?= $project['DIVISION'] ?>">
                    <input type="text" name="bast-spk" id="bast-spk" placeholder="No. SPK (P8)" class="form-control" value="<?= $SPK ?>" readonly required>
                  </div>

                  <div class="form-group col-6"> 
                    <label>SPK Date </label>
                    <input type="text" name="bast-spk-date" id="bast-spk-date" placeholder="Tanggal SPK" class="form-control date-picker" value="<?= $SPK_DATE2 ?>" required readonly>
                  </div>

                  <div class="form-group col-6"> 
                    <label>KL </label>
                    <input type="text" name="bast-kl" id="bast-kl" placeholder="No. KL (Kontrak Layanan)" class="form-control" value="<?= $KL ?>" required>
                  </div>

                  <div class="form-group col-6"> 
                    <label>KL Date </label>
                    <input type="text" name="bast-kl-date" id="bast-kl-date" placeholder="Tanggal KL" class="form-control date-picker" value="<?= $KL_DATE2 ?>"  required readonly>
                  </div>

                  <div class="form-group col-6"> 
                    <label>BAST Value</label>
                    <input type="text" name="bast-value" id="bast-value" placeholder="Nilai BAST" class="form-control rupiah" value="<?= $VALUE ?>" required>
                  </div>

                  <div class="form-group col-6"> 
                    <label>BAST Date</label>
                    <input type="text" name="bast-date" id="bast-date" placeholder="Tanggal Bast" class="form-control date-picker" required>
                  </div>

                  <div class="form-group col-6"> 
                    <label>PIC <small>name</small></label>
                    <input type="text" name="bast-pic" id="bast-pic" placeholder="Person In Charge" class="form-control" required>
                  </div>

                  <div class="form-group col-6"> 
                    <label>PIC <small>email</small></label>
                    <input type="email" name="bast-pic-email" id="bast-pic-email" placeholder="Email Person In Charge" class="form-control" required>
                  </div>
                

                  <div id="evidence" class="form-group col-6" style="margin-bottom: 7px !important">
                       <label>Evidence</label>
                       <div class="row">
                         <div class="col-12">
                           <div class="boxes">
                              <input type="checkbox" id="bast-evidence-p71" value="P71" name="bast-evidence[]">
                              <label for="bast-evidence-p71">P7-1</label>

                              <input type="checkbox" id="bast-evidence-p8" value="P8" name="bast-evidence[]">
                              <label for="bast-evidence-p8">P8 (SPK)</label>

                              <input type="checkbox" id="bast-evidence-wo" value="Work Order" name="bast-evidence[]">
                              <label for="bast-evidence-wo">WO (Work Order)</label>

                              <input type="checkbox" id="bast-evidence-kl" value="Kontrak Layanan" name="bast-evidence[]">
                              <label for="bast-evidence-kl">KL</label>

                              <input type="checkbox" id="bast-evidence-baut" value="BAUT" name="bast-evidence[]">
                              <label for="bast-evidence-baut" >BA Uji Terima (BAUT) / BAPP Smart Building</label>

                              <input type="checkbox" id="bast-evidence-progress" value="Progress Attachment" name="bast-evidence[]">
                              <label for="bast-evidence-progress" >Lampiran Rincian Perhitungan Progress</label>

                              <input type="checkbox" id="bast-evidence-bacustomer" value="BA Customer" name="bast-evidence[]">
                              <label for="bast-evidence-bacustomer">BA Customer / BA Format Standar</label>

                              <input type="checkbox" id="bast-evidence-baperformansi" value="BA Performansi" name="bast-evidence[]">
                              <label for="bast-evidence-baperformansi">BA Performansi (Untuk layanan berbasis SLG)</label>

                              <input type="checkbox" id="bast-evidence-barekonsiliasi" value="BA Rekonsiliasi" name="bast-evidence[]">
                              <label for="bast-evidence-barekonsiliasi">BA Rekonsiliasi (Untuk layanan Transaksional berbasis rekon)</label>

                              <input type="checkbox" id="bast-evidence-keterlambatan" value="BA Keterlambatan" name="bast-evidence[]" >
                              <label for="bast-evidence-keterlambatan" >BA Keterlambatan Delivery</label>

                              <input type="checkbox" id="bast-evidence-bapp" value="BAPP" name="bast-evidence[]" >
                              <label for="bast-evidence-bapp" >BAPP (BA Progress Pekerjaan)</label>                      
                            </div>          
                         </div>
                       </div>
                   </div>

                   <div class="form-group col-6">
                      <div class="row">
                        
                        <div class="col-12">
                            <div class="form-group"> 
                              <label>Term Of Payment</label>
                              <select class="form-control" id="bast-type" name="bast-type">
                                  <option value="OTC">One Time Charge (OTC)</option>
                                  <option value="TERMIN" <?= $type == 'TERMIN' ? 'selected' : ''?>>TERMIN</option>
                                  <option value="PROGRESS" <?= $type == 'PROGRESS' ? 'selected' : ''?> >PROGRESS</option>
                              </select>
                            </div>
                        </div>
                        
                        <div class="col-12 <?= $type == 'PROGRESS' ? 'hidden' : ''?> container-bast-type-info" id="container-bast-termin">
                            <div class="form-group"> 
                              <label>Termin</label>
                              <input type="number" min="1" name="bast-termin" id="bast-termin" class="form-control bast-type-info" placeholder="Bast Termin"  value="<?= $type_info ?>" />
                            </div>
                        </div>
  
                        <div class="col-12 <?= $type == 'TERMIN' ? 'hidden' : ''?> container-bast-type-info" id="container-bast-progress">
                            <div class="form-group"> 
                              <label>Progress (%)</label>
                              <input type="number" min="1" name="bast-progress" id="bast-progress" class="form-control bast-type-info" value="<?= $type_info ?>" placeholder="Bast Project Progress" />
                            </div>
                        </div>

                        <div class="col-12">
                            <label>Approver</label>
                              <div id="signer">
                                
                              </div>
                              <div class="w-100 mt-1">
                                <button class="pull-right btn btn-md btn-addon btn-prime btn-primary" id="btn-add-signer" type="button">
                                  <i class="fa fa-plus"></i>
                                  <span>Add</span>
                                </button>
                              </div>
                        </div>
                      </div>
                    </div> 

                    <div class="col-2 text-right pr-3">
                      <strong class="pr-2" style="font-size: 1.2em;margin-top: 5px;display: inline-block;">BAUT ?</<strong></strong>
                      <label class="switch" style="margin-top:5px !important;">
                        <input type="checkbox" name="baut" id="baut" class="primary">
                        <span class="slider round" id="slider-baut"></span>
                      </label>
                    </div>


                </div>
              </div>             
            </div>

          </div>

          <div class="row text-center mt-4">
               <input type="hidden" id="bast-note" name="bast-note">
              <button class="btn btn-success btn-md btn-prime btn-addon" id="btn-submit-bast" type="button" style="margin: auto;">
                <span class="pr-2">Submit</span>
                <i class="fa fa-save"></i>
              </button>
          </div>

        </form>
      </div>
  

    </div>
  </div>
</div>


<script type="text/javascript">    
var total_signer = 0;
 var Page = function () {
          const addSigner = () => {
              total_signer += 1;
              let index = total_signer;
              
              let container_row = document.createElement("div");
              $(container_row).addClass("row container-signer"+index);
                
              // field user
              let field_signer  = document.createElement("select");
                $(field_signer).addClass("form-control select-user");
                $(field_signer).attr("name","signer[]");
                $(field_signer).attr("id","signer-"+ index);
                $(field_signer).attr("required",true);
              let container_signer = document.createElement("div");
                $(container_signer).addClass("col-11 mt-1 container-signer"+index);
                $(container_signer).append(field_signer);

              // btn delete
              let icon_delete = document.createElement("i");
                $(icon_delete).addClass("fa fa-trash");
              let btn_delete  = document.createElement("button");
                $(btn_delete).addClass("btn btn-sm btn-xm btn-danger mt-1 btn-delete-signer container-signer"+index);
                $(btn_delete).data('id', index);
                $(btn_delete).attr('type','button');
                $(btn_delete).css('maxHeight','35px');
                $(btn_delete).append(icon_delete);

              $(container_row).append(container_signer);
              $(container_row).append(btn_delete);;
              console.log(container_row);
              $("#signer").append(container_row);

              $("#signer-"+index).select2({
                            placeholder: "Select Approver",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_user",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.ID, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                });
              generatePage();
            };

      const deleteSigner = (index) => {
        if(total_signer > 1){
          $('.container-signer'+index).remove();
          --total_signer;  
        }else{
           bootbox.alert("at least have 1 signer");
        }
        
      }

      const setTopInfo = () =>{
        let type = $("#bast-type").val();
        $(".container-bast-type-info").addClass('hidden');
        $(".bast-type-info").attr('required',false);
        $(".bast-type-info").val('');

        if(type == 'TERMIN'){
          $("#container-bast-termin").removeClass('hidden');
          $("#container-bast-termin").attr('required',true);
        }else if(type == 'PROGRESS'){
          $("#container-bast-progress").removeClass('hidden');
          $("#container-bast-progress").attr('required',true);
        }else{
          $("#container-bast-termin").addClass('hidden');
          $("#container-bast-termin").attr('required',true);
          $("#container-bast-progress").addClass('hidden');
          $("#container-bast-progress").attr('required',true);
          $("#bast-termin").val('1');
        }


      }

      return {
          init: function() { 
            addSigner();
            setTopInfo();
            $(document).on('change','#bast-type', (e) =>{setTopInfo();});
            $(document).on('click','#btn-add-signer', (e) =>{addSigner();});
            $(document).on('click','.btn-delete-signer', (e) =>{
                deleteSigner($(e.currentTarget).data("id"))
              });
            $(document).on('click','#btn-submit-bast', (e) =>{
               e.preventDefault();
               let total_evidence = 0;
              $("[name='bast-evidence[]']").map((index, value) =>{
                if($('#'+$(value).attr('id')).is(':checked')){
                  total_evidence++;
                }
              });

                if(total_evidence == 0){
                  bootbox.alert("at least have 1 evidence");
                }

                if($('#form-bast').valid() && total_evidence != 0){
                          bootbox.prompt({
                                title: "Confirm Data",
                                placeholder: "Write some note?",
                                inputType: 'textarea',
                                buttons: {
                                    confirm: {
                                        label: '<i class="fa fa-check"></i> Proses',
                                        className: 'btn btn-md btn-addon btn-success'
                                    },
                                    cancel: {
                                        label: '<i class="fa fa-times"></i> Batal',
                                        className: 'btn btn-md btn-addon btn-danger col-sm-offset-4'
                                    }
                                },
                                callback: function(result) {
                                    if(result!=null){
                                      showLoading();
                                      $('#bast-note').val(result);
                                      $('.rupiah').map((index, value) =>{
                                        $('#'+$(value).attr('id')).val($(value).unmask());
                                      });
                                      $('#form-bast').submit();
                                    }
                                    else{
                                        bootbox.hideAll();
                                    }
                                }
                            }); 
                              /**/
                  }
            });
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>