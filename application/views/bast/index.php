<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-4">
						<h4 class="card-title mb-0"><strong>BAST</strong></h4>
						<div class="small text-muted"><?= $user['division'] ?></div>
					</div>
          <div class="col-sm-8">
            <div class="row">
             <div class="col-md-12">
               <div class="float-right" style="margin-bottom: 10px;">
                    <a class="btn btn-success btn-md btn-addon btn-prime" id="btn-add-bast">
                      <i class="fa fa-plus"></i>
                      <span class="pr-2">submit</span>
                    </a>
                </div>
             </div>
            </div>
          </div>		
					<div class="col-sm-12" id="table_content">
						<table id="dataBast" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
							<thead>
								<tr>
									<th style="width: 50%">PROJECT</th>
									<th style="width: 20%">STATUS</th> 
									<th style="width: 15%">TOP</th>
									<th style="width: 15%">VALUE <small>(Rp)</small> </th>
								</tr>
							</thead>
							 <tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
      <div class="card-footer">
                <a class="btn btn-primary btn-xs btn-addon btn-prime" href="<?= base_url() ?>download-list-bast?>">
                  <i class="fa fa-download"></i>
                  <span class="pr-2">DOWNLOAD</span>
                </a>
      </div>
		</div>
	</div>
</div>

 <!-- Modal Customer-->
 <div class="modal  fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-bast">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Select No <strong>P8</strong> and <strong>TOP</strong> </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-12">
                    <form id="form-bast">
                        <div class="form-group">
                          <label>SPK (P8)</label>
                          <select name="spk" id="spk" class="form-control" required>
                          </select>
                        </div>

                        <div class="form-group hidden" id="container_top">
                          <label>Term Of Payment</label>
                            <select name="top" id="top" class="form-control" disabled>
                            <option value="">Select TOP </option>
                            </select>
                        </div>
                        <div class="w-100 text-center">
                          <button type="button" id="btn-submit-bast"  class="btn btn-sm btn-success" >Submit P8</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>


<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataBast').DataTable({
                  initComplete: function(settings, json) {
                                $('.rupiah').priceFormat({
                                    prefix: '',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    centsLimit: 0
                                });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'asc'],
                    ajax: { 
                        'url'  :base_url+'datatable/bast', 
                        'type' :'POST',
                        'data' : {
                                  status  : $('#status').val(),
                                  pm      : $('#pm').val(),
                                  customer: $('#customer').val(),
                                  regional: $('#regional').val(),
                                  type    : $('#type').val(),
                                  mitra   : $('#mitra').val(),
                                  segmen  : $('#segmen').val()
                                  }   
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){   
                                      let partner    = obj.PARTNER_NAME;   
                                      let spk        = obj.SPK; 
                                      let partnerspk =  "<strong class='text-primary mt-2'>"+partner+"</strong><br><span>"+spk+"</span>";
                                    return "<strong>"+obj.PROJECT_NAME+"</strong><br>"+partnerspk; 
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){
                                  let approver = obj.APPROVER;
                                  let status   = obj.STATUS;
                                  if(status !== 'RECEIVED' && status !== 'APPROVED' && status !== 'DONE' && status !== 'TAKE OUT'){
                                    status = 'Check by <strong>'+ approver+"</strong>";
                                  }
                                  if(obj.REVISION == '1'){
                                    status = 'Revision by <strong>'+ approver+"</strong>"; 
                                  }
                                  return status;
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){   
                                    let termin    = obj.TERMIN;
                                    let progress  = obj.PROGRESS;
                                    let cat       = '';
                                  if(termin == '' || termin == null || termin == undefined){
                                      cat = 'PROGRESS '+progress+'%'; 
                                  }else{
                                      cat   = 'TERMIN - '+termin;
                                  }
                                    
                                  return cat;
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){   
                                    return "<strong class='rupiah pull-right'>"+obj.VALUE+"</strong>"
                            }            
                                    
                        },  


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'bast/view/'+aData['ID']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            tableInit();
            $(document).on('click','.row_links',function(e){
            	e.stopImmediatePropagation();
            	console.log($(this).data('link'));
            	window.location = $(this).data('link');
            });

            $(document).on('click','#btn-add-bast', function(){
              $('#modal-bast').modal('show');
            });

            $(document).on("click","#btn-submit-bast",(e) => {
                e.preventDefault();
                if($('#form-bast').valid()){
                  showLoading();
                  let spk = $('#spk').val();
                  let top = $('#top').val();
                  spk = spk.replace(/\//g,'^');
                  top = top.replace(/\//g,'^');
                  console.log(spk);
                  window.location.href = base_url+'bast/add/'+encodeURI(spk)+'/'+encodeURI(top) ;
                }

             });

            $(document).on("change","#spk",(e) => {
                e.preventDefault();
                
                if($('#spk').val() !== null && $('#spk').val() !== null ){
                  console.log('spk = '+$('#spk').val());
                   $('#container_top').removeClass('hidden');
                   $('#top').attr('disabled',false);
                   $("#top").select2({
                              placeholder: "Select Term Of Payment",
                              width: 'resolve',
                              allowClear : true,
                              ajax: {
                                  type: 'POST',
                                  url:base_url+"master/get_spk_top_partner",
                                  dataType: "json",
                                  data: function (params) {
                                      return {
                                          q: params.term,
                                          spk: $('#spk').val(),
                                      };
                                  },
                                  processResults: function (data) {
                                      return {
                                          results: $.map(data, function(obj) {
                                              return { id: obj.ID, text: obj.VALUE +' - '+obj.NOTE};
                                          })
                                      };
                                  },
                                  
                              }
                  });
                }else{
                  console.log($('#spk').val());
                  $('#top').val('').trigger('change');
                  // $('#container_top').addClass('hidden');
                  $('#top').Attr('disabled',true);
                }
             });

            $("#spk").select2({
                            placeholder: "Select SPK (P8)",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_spk",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.SPK, text: obj.SPK +'-'+obj.PARTNER_NAME};
                                        })
                                    };
                                },
                                
                            }
                });

           }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>