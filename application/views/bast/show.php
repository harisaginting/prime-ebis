<style type="text/css">
/*Checkboxes styles*/
input[type="checkbox"] { display: none; }

input[type="checkbox"] + label {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 10px;
  font: 14px/20px;
  color: #000;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

input[type="checkbox"] + label:last-child { margin-bottom: 0; }

input[type="checkbox"] + label:before {
  content: ''; 
  display: block;
  width: 20px;
  height: 20px;
  border: 2px solid #f0f3f5;
  position: absolute;
  left: 0;
  top: 0;
  opacity: .6;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

input[type="checkbox"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}

.card-project  .project-info{
  border-bottom: 1px solid #888 !important;
}

.card-project  .form-control, .card-project  .project-info {
  background: #f0f3f5;
  border-color: #f0f3f5;
  color: #000 !important;
}

.card-project label {
  font-size: 0.7em !important;
}

.list-group-item:hover{
  color: #1b4a9c !important;
}
</style>

<link href="<?= base_url(); ?>assets/css/project.css" rel="stylesheet">
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <span class="judul"><strong><?= $bast['BAUT'] == '0' ? 'BAST' : 'BAUT' ?></strong> <?= $bast['SPK'] ?></span>
        <a class="btn btn-primary btn-sm pull-right" href="<?= base_url().'export-bast/'.$bast["ID"] ?>" target="_blank"><i class="fa fa-file-pdf-o"></i><span class="ml-2">Export</span></a>
      </div>
      <div class="card-body"> 
          <div class="row">
            
            <?php if(!empty($bast['FILENAME'])) :  ?>
            <div class="col-md-12">
              <div class="card card-project" style="border-radius: 10px;">
                <div class="card-body row" style="background:#f0f3f5;border-radius: 10px;">
                  <iframe style="width: 100%;min-height: 500px;" src="<?= base_url()?>assets/document/<?= $project['ID_PROJECT'] ?>/<?= $bast['FILENAME'] ?>"></iframe>
                </div>
              </div>
            </div>
          <?php endif; ?>


            <div class="col-md-12">
              <div class="card card-project" style="border-radius: 10px;">
                <div class="card-body row" style="background:#f0f3f5;border-radius: 10px;">
                  <div class="form-group col-12"> 
                    <label>Name</label>
                    <div class="w-100 project-info">
                      <strong><?= $project['NAME'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-4"> 
                    <label>Value <small class="text-primary"><strong>exclude PPN</strong></small></label>
                     <div class="w-95 project-info">
                        <strong class="rupiah"><?= $project['VALUE'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-4"> 
                    <label>Customer</label>
                    <div class="w-95 project-info">
                        <strong><?= $project['CUSTOMER_NAME'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-4"> 
                    <label>Kontrak Bersama</label>
                      <div class="w-95 project-info">
                        <strong><?= $project['KB'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-3">
                    <label>Division</label>
                    <div class="w-95 project-info">
                      <strong><?= $project['DIVISION'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-3"> 
                    <label>Segmen</label>
                    <div class="w-95 project-info">
                        <strong><?= $project['SEGMEN_NAME'] ?></strong>
                      </div>
                  </div>
                  
                    <div class="form-group col-3">
                      <label>Category</label>
                      <div class="w-95 project-info">
                        <strong><?= $project['CATEGORY_NAME'] ?></strong>
                      </div>
                    </div>

                    <?php if ($project['PM_NAME']) :  ?>
                      <div class="form-group col-3">
                        <label>Project Manager</label>
                        <div class="w-95 project-info">
                          <strong><?= $project['PM_NAME'] ?></strong>
                        </div>
                      </div>
                    <?php endif; ?>

                    <div class="form-group col-3">
                      <label>Regional</label>
                      <div class="w-95 project-info">
                        <strong><?= $project['REGIONAL_NAME'] ?></strong>
                      </div>
                    </div>

                    
                    
                  
                  <?php if (!empty($project['DESCRIPTION'])) : ?>
                    <div class="form-group col-12"> 
                      <label>Description</label>
                        <div class="w-100 ml-0 mr-0 project-info">
                            <strong><?= $project['DESCRIPTION'] ?></strong>
                          </div>
                    </div>
                  <?php endif; ?>
                  
                  <?php if (!empty($project['document'])) : ?>
                    <div class="w-100">
                      <label>Document</label>
                      <div class="w-100">
                          <ul class="list-group scroll-thin" style="max-height: 160px !important;overflow-y: scroll;">
                            <?php foreach ($project['document'] as $key => $value) : ?>
                                <a target="_blank" href="<?= base_url().'assets/document/'.$project['ID_PROJECT'].'/'.$value['NAME'] ?>" ><li class="list-group-item" style="padding: 0.2rem 1.25rem;"><?= $value['NAME'] ?></li></a>
                            <?php endforeach; ?>
                          </ul>
                      </div>
                    </div>
                  <?php endif; ?>

              </div>
            </div>
            </div>


            <div class="col-md-12">
              <div class="card card-project" style="border-radius: 10px;">
                <div class="card-body row" style="background:#f0f3f5;border-radius: 10px;">
                  <div class="form-group col-12"> 
                    <label>No. <?= $bast['BAUT'] == '0' ? 'BAST' : 'BAUT' ?></label>
                    <div class="w-95 project-info">
                        <strong><?= $bast['NO_BAST'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-6"> 
                    <label>Customer</label>
                    <div class="w-95 project-info">
                        <strong class="text-primary"><?= $bast['CUSTOMER_NAME'] ?></strong>
                      </div>
                  </div>
                  
                  <div class="form-group col-6"> 
                    <label>Partner</label>
                    <div class="w-95 project-info">
                        <strong class="text-primary"><?= $bast['PARTNER_NAME'] ?></strong>
                      </div>
                  </div>            
      

                  <div class="form-group col-6"> 
                    <label>BAST Date</label>
                    <div class="w-95 project-info">
                        <strong><?= $bast['BAST_DATE2'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-6"> 
                    <label>BAST Value</label>
                    <div class="w-95 project-info">
                        <strong class="rupiah text-danger"><?= $bast['VALUE'] ?></strong>
                      </div>
                  </div>


                  <div class="form-group col-4"> 
                    <label>SPK</label>
                    <div class="w-95 project-info">
                        <strong><?= $bast['SPK'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-2 pr-4"> 
                    <label>SPK Date</label>
                      <div class="w-95 project-info">
                        <strong><?= $bast['SPK_DATE2'] ?></strong>
                      </div>
                  </div>

                  <div class="form-group col-4">
                    <label>KL</label>
                    <div class="w-95 project-info">
                      <strong><?= $bast['KL'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-2 pr-4"> 
                    <label>KL Date</label>
                    <div class="w-95 project-info">
                        <strong><?= $bast['KL_DATE2'] ?></strong>
                      </div>
                  </div>
                  
                    <div class="form-group col-6">
                      <label>PIC</label>
                      <div class="w-95 project-info">
                        <strong><?= $bast['PIC'] ?> (<?= $bast['PIC_EMAIL'] ?>)</strong>
                      </div>
                    </div>

                    <div class="form-group col-6"> 
                      <label>Term Of Payment</label>
                        <div class="w-95 project-info">
                            <strong><?= $bast['TYPE'] ?>  <?= $bast['TYPE'] == 'TERMIN' ? 'ke-'.$bast['TERMIN'] : $bast['PROGRESS'] ?> </strong>
                          </div>
                    </div>

                    <div class="form-group col-6"> 
                      <label>Evidence</label>
                        <ul class="list-group">
                          <?php foreach ($evidence as $key => $value) : ?>
                            <li class="list-group-item" style="background: #f0f3f5; border:0px;"><i class="fa fa-check"></i><span class="ml-2"><?= $value['VALUE'] ?></span></li>
                          <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="form-group col-6"> 
                      <div class="row">
                        <div class="form-group col-12"> 
                            <label>Status</label>
                              <div class="w-95 project-info">
                                  <strong class="text-primary"><?= $bast['STATUS_NAME'] ?> </strong>
                                </div>
                          </div>

                          <div class="col-12">
                              <label>Approval</label>
                                <ul class="list-group">
                                  <li class="list-group-item <?= $bast['STATUS'] =='RECEIVED' ? 'active' : '' ?>"><span class="ml-2">RECEIVED</span></li>
                                  <?php foreach ($approver as $key => $value) : ?>
                                    <li class="list-group-item <?= $bast['STATUS'] == $value['STEP'] ? 'active' : '' ?> "><span class="ml-2">Check by <?= $value['NAME'] ?></span></li>
                                  <?php endforeach; ?>
                                  <li class="list-group-item <?= $bast['STATUS'] =='APPROVED' ? 'active' : '' ?>"><span class="ml-2">APPROVED</span></li>
                                  <li class="list-group-item <?= $bast['STATUS'] =='DONE' ? 'active' : '' ?>"><span class="ml-2">DONE</span></li>
                                  <li class="list-group-item <?= $bast['STATUS'] =='TAKE_OUT' ? 'active' : '' ?>"><span class="ml-2">TAKE OUT</span></li>
                                </ul>
                          </div>

                      </div>
                    </div>

                    <div class="col-12 mt-5 pt-3" style="border-bottom: 2px solid black;">
                      <span class="h4 w-100">APPROVAL HISTORY</span>
                    </div>
                    <div class="col-12 mt-2">
                      <?php foreach ($history as $key => $value) : ?>
                        <div class="card-body rounded mt-1 <?= $value['REVISION'] == 1 ? 'bg-warning' : 'bg-white' ?> " style="border : 1px solid #ddd;">
                          
                          <h6><strong class="text-dark"><?= $value['REVISION'] == 1 ? 'REVISION' : '' ?></strong> <strong><?= !empty($value['APPROVAL']) ? 'CHECK BY '.$value['APPROVAL'] : $value['STATUS'] ?></strong></h6>
                          <div class="pl-2 w-100" style="font-size: 0.9em"><?= $value['COMMEND'] ?></div>
                        </div>
                      <?php endforeach; ?>
                    </div>

              </div>
            </div>
            </div>

          </div>

          <form id="form-update-bast" class="<?= $bast['STATUS'] == 'APPROVED'  ? '' : 'hidden' ?>">
            <div class="row">
                <input type="hidden" name="approval-status" id="approval-status" value="<?= $bast['STATUS'] ?>">
                <input type="hidden" name="id_project" id="id_project" value="<?= $project['ID_PROJECT'] ?>">
                <input type="hidden" id="bast-note" name="bast-note">
                <input type="hidden" id="bast-status" name="bast-status" value="<?= $bast['STATUS'] ?>">
                <div class="col-4 offset-4">
                    <div class="form-group">
                      <input type="file" name="bast-file" id="bast-file" class="form-control">
                    </div>
                </div>
            </div>
          </form>

          <?php if((empty($bast['CURRENT_APPROVER']) || $user['userid'] == $bast['CURRENT_APPROVER'])  && ($user['division'] == $bast['DIVISION'] || $user['division'] == 'EBIS')) : ?>
          <div class="row text-center mt-4">
               <div style="margin: auto;">
                <?php if($bast['REVISION'] == 0 ) : ?>   
                  <button class="mr-2 btn btn-warning btn-md btn-addon btn-prime" id="btn-revision" type="button"><i class="fa fa-ban"></i><span class="pr-2">Revision</span></button>
                  <button class="ml-2 btn btn-success btn-md btn-addon btn-prime" id="btn-approve" type="button"><i class="fa fa-check-circle-o"></i><span class="pr-2">Approve</span></button>
               <?php else :  ?>
                  <button class="ml-2 btn btn-success btn-md btn-addon btn-prime" id="btn-approve" type="button"><i class="fa fa-check-circle-o"></i><span class="pr-2">Approve</span></button>
               <?php endif;  ?>
               </div>
          </div>
        <?php endif; ?>

      </div>
  

    </div>
  </div>
</div>


<script type="text/javascript">    
var total_signer = 0;
 var Page = function () {


      return {
          init: function() { 
            $('#file-bast').fileinput({
                  overwriteInitial: true,
                  uploadAsync: true,
                  initialPreview  : false,
                  initialPreviewShowDelete : false,                   
                  showRemove:false,
                  showUpload:false,
                  showPreview : false   
                });
            $(document).on('click','#btn-approve', (e) =>{
                e.preventDefault();
                bootbox.prompt({
                                title: "Approve <?= $bast['BAUT'] == '0' ? 'BAST' : 'BAUT' ?>",
                                placeholder: "Write some note...",
                                inputType: 'textarea',
                                buttons: {
                                    confirm: {
                                        label: '<i class="fa fa-check"></i><span class="pr-2">Approve</span>',
                                        className: 'btn btn-md btn-addon btn-success'
                                    },
                                    cancel: {
                                        label: '<i class="fa fa-times"></i><span class="pr-2">Batal</span>',
                                        className: 'btn btn-md btn-addon btn-danger col-sm-offset-4'
                                    }
                                },
                                callback: function(result) {
                                    if(result!=null){
                                      showLoading();
                                      $('#bast-note').val(result);
                                      $('.rupiah').map((index, value) =>{
                                        $('#'+$(value).attr('id')).val($(value).unmask());
                                      });
 
                                      let form = $('#form-update-bast')[0];
                                      let formData = new FormData(form);


                                      $.ajax({
                                              url: base_url+'bast/approve_bast/<?= $bast['ID'] ?>',
                                              type:'POST',
                                              data:  formData,
                                              async : true,
                                              contentType:false,
                                              processData:false,
                                              dataType : "text",
                                              success:function(result){                                                      
                                                       if(result =='success'){
                                                        location.reload();
                                                      }else{
                                                        hideLoading();
                                                        bootbox.alert("Failed!", function(){});
                                                        }
                                                      return result;
                                              }

                                      });  
                                    }
                                    else{
                                        bootbox.hideAll();
                                    }
                                }
                            }); 
            });

            $(document).on('click','#btn-revision', (e) =>{
                e.preventDefault();
                bootbox.prompt({
                                title: "Revision BAST",
                                placeholder: "Write some note...",
                                inputType: 'textarea',
                                buttons: {
                                    confirm: {
                                        label: '<i class="fa fa-check"></i><span class="pr-2">Revision</span>',
                                        className: 'btn btn-md btn-addon btn-warning'
                                    },
                                    cancel: {
                                        label: '<i class="fa fa-times"></i><span class="pr-2">Batal</span>',
                                        className: 'btn btn-md btn-addon btn-danger col-sm-offset-4'
                                    }
                                },
                                callback: function(result) {
                                    if(result!=null){
                                      showLoading();
                                      $('#bast-note').val(result);
                                      $('.rupiah').map((index, value) =>{
                                        $('#'+$(value).attr('id')).val($(value).unmask());
                                      });
 
                                      let form = $('#form-update-bast')[0];
                                      let formData = new FormData(form);


                                      $.ajax({
                                              url: base_url+'bast/revision_bast/<?= $bast['ID'] ?>',
                                              type:'POST',
                                              data:  formData,
                                              async : true,
                                              contentType:false,
                                              processData:false,
                                              dataType : "text",
                                              success:function(result){                                                      
                                                       if(result =='success'){
                                                        location.reload();
                                                      }else{
                                                        hideLoading();
                                                        bootbox.alert("Failed!", function(){});
                                                        }
                                                      return result;
                                              }

                                      });  
                                    }
                                    else{
                                        bootbox.hideAll();
                                    }
                                }
                            }); 
            });
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>