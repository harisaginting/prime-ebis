<div id="chartProjectStatus">
</div> 

<script type="text/javascript">
   var colors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"],
      categories = <?= json_encode($progress) ?>,
      data = <?= json_encode($chartProgress) ?>,
      browserData = [],
      versionsData = [],
      i,
      j,
      dataLen = data.length,
      drillDataLen,
      brightness;

      colorA = ['#ffc107','#d81011','#0fc13e'];
      colorB = <?= json_encode($colorTProj); ?>;


  // Build the data arrays
      for (i = 0; i < dataLen; i += 1) {

          // add browser data 
          browserData.push({
              name: categories[i],
              y: data[i].Y,
              color: colorA[i]
          });

          // add version data
          drillDataLen = data[i].drilldown.data.length;
          for (j = 0; j < drillDataLen; j += 1) {
              brightness = 0.2 - (j / drillDataLen) / 5;
              versionsData.push({
                  name: data[i].drilldown.categories[j],
                  y: data[i].drilldown.data[j],
                  color: colorB[j],
              });
          }
      }

      // Create the chart
      Highcharts.chart('chartProjectStatus', {
          chart: {
              type: 'pie',
              backgroundColor : '#ffffff00',
          },
          exporting: { enabled: false },
          title: {
                  style: {
                   color: '#f42020',
                   textTransform: 'uppercase',
                   fontSize: '20px'
                },
                  text: ''
              },
          credits: {
                          text: '',
                          href: 'https://prime.telkom.co.id/sdv/projects',
                          style: {
                          color: '#a40000'},
                      },
          subtitle: {
              text: ''
          },
          yAxis: {
              title: {
                  text: 'Projects Status'
              }
          },
          series: [{
              name: 'Total Projects',
              data: browserData,
              size: '100%',
              innerSize: '0%',
              dataLabels: {
                  style: {
                      fontSize : '11px',
                      border : '0px',
                    },
                  formatter: function () {
                      // display only if larger than 1
                      return this.y > 0 ? '<b>' + this.point.name + ':</b> ' +
                          this.y : null;
                  },
                  color: '#fff',
                  border : '0px',
                  distance: -65 
              }
            },
            {
              name: 'Total Projects',
              data: versionsData,
              size: '100%',
              innerSize: '100%',
              dataLabels: {
                  style: {
                      fontSize : '10px',
                      border : '1px'
                    },
                  formatter: function () {
                      var a = '#d81011';
                      switch(this.point.name){
                        case 'APPLICATION' :
                            a = '#ff8201';
                          break;
                        case 'CONNECTIVITY' :
                            a = '#32f7ca';
                          break;
                        case 'CPE DEVICES' :
                            a = '#0b8fc1';
                          break;
                        case 'SMART BUILDING':
                            a = '#d60db4';
                          break;
                        default:
                          break;
                      }

                      return this.y > 0 ? "<b style='color:"+a+"'>" + this.point.name + " : <span style='font-size:14px;color:"+a+"'>"+this.y+'</b> ' : null;
                  },
                  color: '#000',
                  // distance: 70
              }
            }
          ],
          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 600
                  },
                  chartOptions: {
                      series: [{
                          id: 'versions',
                          dataLabels: {
                              enabled: false
                          }
                      }]
                  }
              }]
          }
      }); 
</script>