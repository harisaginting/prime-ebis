 <div id="chartProjectScale" class="h-75"></div>

<script type="text/javascript">
	var colors = Highcharts.getOptions().colors,
          categories = [
              "BIG",
              "MEGA",
              "ORDINARY",
              "REGULAR",
          ],
          data = <?= json_encode($chartProjectScale); ?>,
          projectScale = [],
          i,
          j,
          dataLen = data.length,
          drillDataLen,
          brightness;


      // Build the data arrays
      for (i = 0; i < dataLen; i += 1) {

          // add browser data 
          projectScale.push({
              name: categories[i],
              y: data[i].Y,
              color: data[i].color,
              x: data[i].V
          });
      }

      // Create the chart
      Highcharts.chart('chartProjectScale', {
          chart: {
              type: 'pie',
              backgroundColor: '#ffffff00',
              height:'550px',
          },
          exporting: { enabled: false },
          title: {text: '' },
          credits: { text: ''},
          subtitle: { text: ''},
          yAxis: {
              title: {
                  text: 'Projects Scale'
              }
          },
          plotOptions: {
              pie: {
                  shadow: false,
                  center: ['50%', '35%']
              }
          },
          series: [{
              name: 'Total Projects',
              data: projectScale,
              size: '80%',
              innerSize:'0%',
              dataLabels: {
                  style: {
                      fontSize : '11px',
                      border : '0px',
                      // textOutline : false
                    },
                  formatter: function () {
                      var a = '#6f6f6f';
                      return this.y > 1 ? "<span style='borderColor:#fff'>" + this.point.name + " : <span style='font-size:11px;'>"+this.y+"</> ": null;
                  },
                  border : '0px',
                  distance: -40
              }
          }],
      });
</script>

