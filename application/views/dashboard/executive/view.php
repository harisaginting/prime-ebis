<!-- CUSTOM STYLE -->
<style type="text/css">
	.btn-info{
		background-color: #f6e28a;
    	border-color: #f6e28a;
	}

	.btn-info.active{
		background-color: #f26e39 !important;
    	border-color: #f26e39 !important;
	}

	.card{
	-webkit-box-shadow: 2px 2px 5px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 2px 2px 5px 0px rgba(0,0,0,0.75);
	box-shadow: 2px 2px 5px 0px rgba(0,0,0,0.75);
	}
  
	.border-smoth{
		border-radius: 11px;
	}

	.border-smoth-top{
		border-radius: 11px 11px 6px 6px;
	}

	.border-smoth-bottom{
		border-radius: 6px 6px 11px 11px;
	}

	.h2	{
		font-family: Montserrat;
		font-weight: 800;
	}

	.title-1, .title-2,.title-3, .title-4{
		font-family: Montserrat;
		font-weight: 700;
		font-size: 1.1em;
		text-align: center;
	}
	.title-1{
	}
	.title-3{
		font-size: 0.9em;
	}
	.title-4{
		font-size: 0.7em;
		margin-bottom: 0px; 
		color: #aaa !important;
	}

	.square {
	  height: 1.5em;
	  width: 1.5em;
	  border-radius: 5px;
	}

	.bg-mega{
		background-color: #010440;
	}

	.bg-big{
		background-color: #44aff2;
	}

	.bg-ordinary{
		background-color: #7eddf2;
	}
	.bg-regular{
		background-color: #f2e205;
	}
	.legend-chart{
		font-size: 0.7em;
		display: flex;
	}

	.text-warning{
	  color : #ff7400 !important;
	}
	.text-success{
	  color : #13a33e !important;
	}

	.text-success{
	  color : #13a33e !important;
	}
</style>
<!-- END CUSTOM STYLE -->



<div class="row mb-2">
	<div class="col-md-6 col-sm-12">
		<span class="h2">DASHBOARD EXECUTIVE SUMMARY</span>
	</div>
	<div class="col-md-4 offset-md-2 col-sm-12">
		<div class="w-100" style="display: flex;">
			<select class="form-control mr-1 project-global-filter" id="project-priority">
				<option value="all">All Priority</option>
				<option value="1">Strategic</option>
				<option value="0">Non Strategic</option>
			</select>
			<select class="form-control ml-1 project-global-filter" id="project-year">
				<option value="">All Year</option>
				<?php $year = date('Y');?>
				<?php for ($i = $year; $i >= $year - 5; $i--) { ?>
					<option value="<?= $i ?>"><?= $i ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
</div>


<div class="row">
	
	<div class="col-md-6 col-sm-12 ">
		<div class="mb-1 card border-smoth-top" style="background-color: #2f49ca;background-image: url(./assets/img/login_background.jpeg);background-size: cover;background-repeat: no-repeat;background-position: center;">
			<div class="card-body bg-white border-smoth-top" style="background-color: #ffffffee !important;">
				<div class="row">
					<div class="offset-4 col-4 card-title title-2 mb-1">
						Projects Total
					</div>
					<div class="col-4 card-title title-2 mb-1">
						Projects Value
					</div>
				</div>

				<div class="row text-success pr-3">
					<div class="col-4">
						<strong class="title-1">Active</strong>
					</div>
					<div class="title-2 col-4 text-right pr-5 miliar" id="t_active"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
					<div class="title-2 col-4 text-right pr-5 miliar" id="v_active"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
				</div>
				<div class="row text-warning pr-3">
					<div class="col-4">
						<strong class="title-1">Close</strong>
					</div>
					<div class="title-2 col-4 text-right pr-5 miliar" id="t_close"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
					<div class="title-2 col-4 text-right pr-5 miliar" id="v_close"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
				</div>
				<div class="row text-dark pr-3">
					<div class="col-4">
						<strong class="title-1">Total</strong>
					</div>
					<div class="title-2 col-4 text-right pr-5 miliar" id="t_all"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
					<div class="title-2 col-4 text-right pr-5 miliar" id="v_all"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
				</div>

			</div>
		</div>


		<div class="card mb-1 border-smoth-bottom" style="background-color: #2f49ca;background-image: url(./assets/img/login_background.jpeg);background-size: cover;background-repeat: no-repeat;background-position: center;">
			<div class="card-body pl-1 pr-1 bg-white border-smoth-bottom" style="background-color: #ffffffee !important;">
				<div class="row pl-2 pr-2">
					<div class="col-12 card-title title-3">
						<span class="pull-left">Status Total Project</span>
						<div class="pull-right">
							<button class="btn btn-sm btn-info active btn-project-status-qty"  id="btn-project-status-qty-qty" style="width: 70px;" data-id="qty">Total</button>
							<button class="btn btn-sm btn-info btn-project-status-qty" id="btn-project-status-qty-value" style="width: 70px;" data-id='value'>Value</button>
						</div>
						<div class="pull-right mr-2">
							<select class="form-control" id="btn-project-status-qty-division" style="max-height: 29px;">
								<option value="all">All Division</option>
								<option value="DBS">DBS</option>
								<option value="DES">DES</option>
								<option value="DGS">DGS</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row pl-3 pr-3">
					<div class="col-12">
						<div class="row pl-0" id="container-project-status-qty"></div>
					</div>
				</div>

			</div>
		</div>

		<div class="card border-smoth-bottom" style="background-color: #2f49ca;background-image: url(./assets/img/login_background.jpeg);background-size: cover;background-repeat: no-repeat;background-position: center;">
			<div class="card-body pt-2 bg-white border-smoth-bottom" style="background-color: #ffffffee !important;">
				<div class="row">
					<div class="col-12 mb-1 card-title title-3">
						<span class="pull-left">Active Project Per Division</span>
					</div>
				</div>
				<div class="row">
					<div class="offset-4 col-4 card-title title-2  mb-1">
						Projects Total
					</div>
					<div class="col-4 card-title title-2  mb-1">
						Projects Value
					</div>
				</div>

				<div class="row text-success pr-3">
					<div class="col-4">
						<strong class="title-1">DES</strong>
					</div>
					<div class="title-2 col-4 text-right pr-5 rupiah" id="t_des"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
					<div class="title-2 col-4 text-right pr-5 rupiah" id="v_des"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
				</div>
				<div class="row text-warning pr-3">
					<div class="col-4">
						<strong class="title-1">DBS</strong>
					</div>
					<div class="title-2 col-4 text-right pr-5 rupiah" id="t_dbs"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
					<div class="title-2 col-4 text-right pr-5 rupiah" id="v_dbs"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
				</div>
				<div class="row text-primary pr-3">
					<div class="col-4">
						<strong class="title-1">DGS</strong>
					</div>
					<div class="title-2 col-4 text-right pr-5 rupiah" id="t_dgs"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
					<div class="title-2 col-4 text-right pr-5 rupiah" id="v_dgs"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></div>
				</div>
			</div>
		</div>

	</div>



	<div class="col-md-6 col-sm-12 ">
		<div class="mb-1 card border-smoth-top" style="background-color: #2f49ca;background-image: url(./assets/img/mtelkom.jpg);background-size: cover;background-repeat: no-repeat;background-position: right;">
			<div class="card-body bg-white border-smoth-top" style="background-color: #fffffff1 !important;">
				<div class="row">
					<div class="col-12 card-title title-1">
					<span class="pull-left">Active Project Scale</span>
					<div class="pull-right">
						<button class="btn btn-sm btn-info active btn-project-scale" id="btn-project-scale-qty" style="width: 70px;" data-id="qty">Total</button>
						<button class="btn btn-sm btn-info btn-project-scale" id="btn-project-scale-value" style="width: 70px;" data-id='value'>Value</button>
					</div>
					<div class="pull-right mr-2">
						<select class="form-control" id="btn-project-scale-division" style="max-height: 29px;">
							<option value="all">All Division</option>
								<option value="DBS">DBS</option>
								<option value="DES">DES</option>
								<option value="DGS">DGS</option>
						</select>
					</div>
				</div>
				</div>

				<div class="row">
					<div id="chart-project-scale" class="w-100"></div>
				</div>
				<div class="row">
					<div class="col-md-3 col-6 flex legend-chart">
						<div class="square bg-mega"></div>
						<div><strong class="ml-2">Mega (>50 M)</strong></div>
					</div>
					<div class="col-md-3 col-6 flex legend-chart">
						<div class="square bg-big"></div>
						<div><strong class="ml-2">Big (20 – 50 M)</strong></div>
					</div>
					<div class="col-md-3 col-6 flex legend-chart">
						<div class="square bg-ordinary"></div>
						<div><strong class="ml-2">Ordinary (1-20 M)</strong></div>
					</div>
					<div class="col-md-3 col-6 flex legend-chart">
						<div class="square bg-regular"></div>
						<div><strong class="ml-2">Regular (<1 M)</strong></div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col-12">
		<div class="card border-smoth">
			<div class="card-body bg-white border-smoth">
				<div class="row">
					<div class="form-group col-2">
					    <label class="label-field title-4">DIVISION</label>
					    <select class="form-control project-filter" id="project-division">
					      	<option value="all">All DIVISION</option>
							<option value="DBS">DBS</option>
							<option value="DES">DES</option>
							<option value="DGS">DGS</option>
					    </select>
					  </div>

					  <div class="form-group col-2">
					    <label class="label-field title-4">SEGMEN</label>
					    <select class="form-control project-filter" id="project-segmen">
					      <option value="">All SEGMEN</option>
					    	<?php foreach ($list_segmen as $key => $value): ?>
						        <option value="<?= $value['CODE'] ?>"><?= $value['NAME'] ?></option>
						    <?php endforeach ?>
					    </select>
					  </div>

					  <div class="form-group col-2">
					    <label class="label-field title-4">SCALE</label>
					    <select class="form-control project-filter" id="project-scale">
					      <option value="">All SCALE</option>
					      	  <option value="BIG" <?= $scale == 'BIG' ? 'selected' : '' ?>>BIG</option>
						      <option value="MEGA" <?= $scale == 'MEGA' ? 'selected' : '' ?> >MEGA</option>
						      <option value="ORDINARY" <?= $scale == 'ORDINARY' ? 'selected' : '' ?>>ORDINARY</option>
						      <option value="REGULAR" <?= $scale == 'REGULAR' ? 'selected' : '' ?>>REGULAR</option>
					    </select>
					  </div>

					  <div class="form-group col-2">
					    <label class="label-field title-4">STATUS</label>
					    <select class="form-control project-filter"  id="project-progress">
					      	  <option value="">All STATUS</option>
					      	  <option value="LEAD" <?= $progress == 'LEAD' ? 'selected' : '' ?>>LEAD</option>
						      <option value="LAG" <?= $progress == 'LAG' ? 'selected' : '' ?>>LAG</option>
						      <option value="DELAY" <?= $progress == 'DELAY' ? 'selected' : '' ?>>DELAY</option>
					    </select>
					  </div>

					  <div class="form-group col-2">
					    <label class="label-field title-4">CUSTOMER</label>
					    <select class="form-control project-filter"  id="project-customer">
					      <option value="">All CUSTOMER</option>
					      <?php foreach ($list_customer as $key => $value): ?>
					        <option value="<?= $value['CODE'] ?>"><?= $value['NAME'] ?></option>
					      <?php endforeach ?>
					    </select>
					  </div>

					  <div class="form-group col-2">
					    <label class="label-field title-4">PARTNER</label>
					    <select class="form-control project-filter"  id="project-partner">
					      <option value="">All PARTNER</option>
					      <?php foreach ($list_partner as $key => $value): ?>
					        <option <?= $partner == $value['NAME'] ? 'selected' : '' ?> value="<?= $value['NAME'] ?>"><?= $value['NAME'] ?></option>
					      <?php endforeach ?>
					    </select>
					  </div>

					</div>

					<table id="list-project" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
						<thead>
							<tr>
								<th style="width: 30%;vertical-align: top;">PROJECT NAME</th>
								<th style="width: 10%;text-align: center;vertical-align: top;">VALUE<small>(IDR)</small> </th>
								<th style="width: 10%;vertical-align: top;text-align: center;">PERIOD</th>
								<th style="width: 15%;vertical-align: top;">GROSS PROFIT MARGIN</th>
								<th style="width: 20%;vertical-align: top;">PARTNER</th>
								<th style="width: 15%;vertical-align: top;">PROGRESS</th>
							</tr>
						</thead>
						 <tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
var Dashboard = function () {
 	
 	var tableInit = function(){                     
	    var table = $('#list-project').DataTable({
	              initComplete: function(settings, json) {
	                    $('.rupiah').priceFormat({
	                        prefix: '',
	                        centsSeparator: ',',
	                        thousandsSeparator: '.',
	                        centsLimit: 0
	                    });

	                    $('.miliar').priceFormat({
	                        suffix: ' M',
	                        prefix: '',
	                        centsSeparator: ',',
	                        thousandsSeparator: '.',
	                        centsLimit: 0
	                    });
	                    $(function () {
	                      $('[data-toggle="tooltip"]').tooltip()
	                    });
	                },
	                processing: true,
	                serverSide: true,
	                order :[0,'desc'],
	                ajax: { 
	                    'url'  :base_url+'datatable/dashboard-executive/project', 
	                    'type' :'POST',
	                    'data' : {
	                              division  : $('#project-division').val(),
	                              scale     : $('#project-scale').val(),
	                              progress  : $('#project-progress').val(),
	                              partner   : $('#project-partner').val(),
	                              customer  : $('#project-customer').val(),
	                              segmen  	: $('#project-segmen').val(),
	                              year_start : $('#project-year').val(),
	                              priority	: $('#project-priority').val(),
	                              }   
	                    },
	                aoColumns: [
	                    { 
                            'mRender': function(data, type, obj){
                                    let w         = "";
                                    let note      = "";
                                    let pm        = ""; 
                                    let am        = "";
                                    let segmen 	  = "";
                                    let tr 	 	  = "";
                                    let cc 	 	  = "";
                                    
                        
                                    if((obj.STATUS=='DELAY'||obj.STATUS=='LAG')){
                                      if(obj.REASON_OF_DELAY == ""||obj.REASON_OF_DELAY == null){
                                        w = "&nbsp;&nbsp;&nbsp;<br><span style='font-size:24px;' class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='right' data-original-title='Please fill reason of delay (Symptom)' aria-describedby='tooltip549771'></span>";
                                      }else{
                                        w = "<span class='text-primary'><br>SYMPTOM <span class='text-danger'>"+obj.REASON_OF_DELAY+"</span></span>";
                                      }
                                      
                                    }  

                                    if (obj.PM_NAME !== null) {
                                      pm = "[PROJECT &nbsp;MANAGER <strong class='text-dark' >"+obj.PM_NAME+"</strong>]<br>";
                                    }
                                    
                                    if(obj.AM_NAME !== null){
                                      am = "[ACCOUNT MANAGER <strong class='text-dark' >"+obj.AM_NAME+"</strong>]<br>";
                                    } 

                                    if(obj.CUSTOMER_NAME !== null){
                                      cc = "[CUSTOMER <strong class='text-dark' >"+obj.CUSTOMER_NAME+"</strong>]<br>";
                                    } 

                                    if(obj.SEGMEN !== null){
                                      segmen = "[SEGMEN <strong class='text-dark' >"+obj.SEGMEN+"</strong>]<br>";
                                    } 

                                    if(obj.V_REGIONAL !== null){
                                      tr = "[TR <strong class='text-dark' >"+obj.V_REGIONAL+"</strong>]<br>";
                                    } 


                                    let id = "<label class='badge badge-info' style='font-size:12px;'><strong class='text-white'>"+obj.DIVISION+" </strong>"+obj.SCALE+"</label>";
                                    return "<div style='padding: 0px 5px;'>"+id+"<div style='margin-top:-8px;'><span class='project-text'>"+obj.NAME+"</span></div>"+"<div class='text-primary' style='font-size:12px;' ><div class='mt-1' style='font-style:italic;font-size:0.85em'>"+tr+cc+segmen+am+pm+"</div>";   
                            }            
                                    
                        },  
	                    { 
	                        'mRender': function(data, type, obj){   
	                                 return "<div class='w-100'><strong class='pull-right rupiah'>"+obj.VALUE+"</strong></div>" 
	                        }            
	                                
	                    }, 
	                    { 
	                        'mRender': function(data, type, obj){   
	                        		var period = obj.M_DURATION + " Month";
	                        		if (obj.M_DURATION < 1) {
	                        			 period = "<1 Month";
	                        		}
	                                return  "<div class='text-center w-100'>"+period+"</div>"; 
	                        }            
	                                
	                    }, 
	                    { 
	                        'mRender': function(data, type, obj){   
	                        	   let kl_value 	= obj.PARTNER_VALUE;
	                        	   let t_kl 		= 0;
	                        	   let gpm 			= 100;
	                        	   let p_value 		=  parseInt(obj.VALUE);

	                        	   if (kl_value != null && kl_value != '') {
	                        	   		let arr_kl_value = obj.PARTNER_VALUE.split("@");
		                        	   for (var i = 0; i <= arr_kl_value.length; i++) {
		                        	   		if (arr_kl_value[i]) {
		                        	   			let p_kl = parseInt(arr_kl_value[i]);
		                        	   			t_kl = t_kl + p_kl;
		                        	   		}
		                        	   }
	                        	   }
	                        	   gpm = (((p_value - (p_value * 0.03) - t_kl )/p_value)*100).toFixed(2);;
	                        	   return gpm + '%';
	                        	   // return "N/A";
	                        }            
	                                
	                    }, 
	                    { 
	                        'mRender': function(data, type, obj){   
	                               let partner 	= []; 
	                        	   let kl 		= [];

	                        	   if (obj.PARTNER_NAME != null && obj.PARTNER_NAME != "") {
	                        	   	partner =  obj.PARTNER_NAME.split("#");
	                        	   }

	                        	   if (obj.PARTNER_KL != null && obj.PARTNER_KL != "") {
	                        	   	kl = obj.PARTNER_KL.split("@");
	                        	   }
	                        	  
	                        	   console.log('partner',partner);
	                        	   let result = "";

	                        	   for (var i = 0; i <= partner.length; i++) {
	                        	   		if (partner[i]) {
	                        	   			result = result  + "<strong>"+partner[i]+"</strong>";
	                        	   			if (kl[i]) {
	                        	   				result = result + "<br><span style='font-size:0.8em;' class='pl-1'>"+kl[i]+"</span><br><br>";
	                        	   			}else{
	                        	   				result = result + "<br><br>";
	                        	   			}
	                        	   		}
	                        	   }

	                               return "<div class='w-100'>"+result+"</div>";
	                        }            
	                                
	                    }, 
	                    { 
	                        'mRender': function(data, type, obj){   
	                        		let w 	= "";
	                        		if((obj.PROGRESS=='DELAY'||obj.PROGRESS=='LAG')){
                                      if(obj.REASON_OF_DELAY == ""||obj.REASON_OF_DELAY == null){
                                        w = "";
                                      }else{
                                        w = "<span class='text-primary'><br>SYMPTOM <span class='text-danger'>"+obj.REASON_OF_DELAY+"</span></span>";
                                      }
                                      
                                    }  


                                    let ach   = 0;
                                    if(obj.TOTAL_ACHIEVEMENT != '' && obj.TOTAL_ACHIEVEMENT != null){
                                      ach = obj.TOTAL_ACHIEVEMENT;
                                    }
                                    let d_ach ="<div class='clearfix mt-2'>"
                                            +"<div class='float-left'>"
                                            +"<strong>"+ach+"%</strong>"
                                            +"</div>"
                                            +"<div class='float-right'>"
                                            +"<small class='text-muted'>ACHIEVEMENT</small>"
                                            +"</div>"
                                            +"</div>"
                                            +"<div class='progress progress-xs'>"
                                            +"<div class='progress-bar bg-success' role='progressbar' "
                                            +"style='width: "+ach+"%' aria-valuenow='"+ach+"' aria-valuemin='0' "
                                            +"aria-valuemax='100'></div>"
                                            +"</div>"
                                            ; 


                                    let start = "-";
                                    if (obj.START_DATE != null && obj.START_DATE != "") {
                                    	start = obj.START_DATE;
                                    }
                                    let end = "-"
                                    if (obj.END_DATE != null && obj.END_DATE != "") {
                                    	end = obj.END_DATE;
                                    }
	                                let period = "<div class='w-100' style='font-size:0.8em;font-weight:700;text-align:center;border-bottom:1px solid black;margin-bottom:5px;'><span class='pull-left'>"+start+"</span>-<span class='pull-right'>"+end+"</span></div>";

	                                let status = "<span class='btn btn-warning btn-sm mb-2' style='font-size:0.6em !important;'>LAG</span>";

	                                if (obj.PROGRESS == 'LEAD') {
	                                	status = "<span class='btn btn-success btn-sm mb-2' style='font-size:0.6em !important;'>LEAD</span>";
	                                }

	                                if (obj.PROGRESS == 'DELAY') {
	                                	status = "<span class='btn btn-danger btn-sm mb-2 pull-right' style='font-size:0.6em !important;'>DELAY</span>";
	                                }


	                                let b_status = "<div class='w-100 clearfix'>"+status+"</div>";
	                                return  b_status + period+d_ach + w; 
	                        }            
	                                
	                    }, 


	                   ],  
	                   fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	                      var a = null;
	                      if(aData['WEIGHT']==0){
	                        $(nRow).addClass('disabled')  
	                      }else{
	                        $(nRow).addClass( aData['INDICATOR'] );  
	                      }  
	                      $(nRow).addClass('row_links');
	                      $(nRow).data('link',base_url+'project/show/'+aData['ID_PROJECT']); 
	                      return nRow;
	                      }    
	            });  
	};  


	const chartStatusProject = (type = null) =>{
		let div 		= $("#btn-project-status-qty-division").val();
    	let url 		= base_url+'dashboard/status-project/'+div;
    	let priority 	=  $('#project-priority').val();
		let year_start 	=  $('#project-year').val();
	    $.ajax({
	            url: url + "?priority=" +priority+"&year_start="+year_start ,
	            type:'GET',
	            async : true,
	            dataType : "json",
	            success:function(result){
	                let mode = '';
	                let total = result.total_project;
	                let seriesdata = [{
	                          name: result.STATUS[0],
	                          data: [result.TOTAL[0]]
	                      }, {
	                          name: result.STATUS[1],
	                          data: [result.TOTAL[1]]
	                      }, {
	                          name: result.STATUS[2],
	                          data: [result.TOTAL[2]]
	                      }];
	                if(type == 'value'){
	                    seriesdata = [{
	                          name: result.STATUS[0],
	                          data: [result.VALUE[0]]
	                      }, {
	                          name: result.STATUS[1],
	                          data: [result.VALUE[1]]
	                      }, {
	                          name: result.STATUS[2],
	                          data: [result.VALUE[2]]
	                      }];
	                    mode = 'M';
	                    total = result.total_project_value;
	                }

	                let options = {
	                              chart: {
	                                type: 'bar',
	                                height:'90px',
									backgroundColor: null
	                              },
	                              exporting: { enabled: false },
	                              title: {
	                                  text: null
	                              },
	                              colors: ['#010440','#7eddf2','#44aff2','#44eeaa'],
	                              credits: {text:''},
	                              xAxis: {
	                                  labels:{
	                                    enabled: false
	                                  },
	                              },
	                              yAxis: {
	                                  max:total-1 ,
	                                  title: {
	                                      text: ''
	                                  },
	                                  labels:{
	                                    enabled: false
	                                  }
	                              },
	                              legend: {
	                                  enabled: false
	                              },
	                              plotOptions: {
	                                 series: {
	                                      stacking: 'normal',
	                                      cursor:'pointer',
	                                      events: {
	                                            click: function (event) {
	                                                window.location.href = base_url+'project/data/' + this.name.toUpperCase()+"?priority="+priority;
	                                            }
	                                        }
	                                    },
	                                        bar: {
	                                            grouping: true,                  
	                                            groupPadding:0.1,
	                                            pointWidth:100,
	                                            pointPadding: 0,
	                                            dataLabels: {
	                                                enabled: true,
	                                                inside:true,
	                                                useHTML: true,
	                                                align: 'center',
	                                                color: 'white',
	                                                style: {
	                                                    fontWeight: 'bold'
	                                                },                      
	                                                verticalAlign: 'middle',
	                                                formatter: function () {
	                                                    if (this.series.name) 
	                                                      return '<span style="color:white">' + this.series.name + ' ' + this.y +mode+'</span>';
	                                                    else return '';
	                                                }
	                                            }
	                                        }
	                                    },          
	                              series: seriesdata
	                          };
	                        $("#container-project-status-qty").empty();
	                        Highcharts.chart('container-project-status-qty', options);
	            }
	      });
  	}

 

  	const chartScaleProject = (mode = 'qty') => {
  		let div 		= $("#btn-project-scale-division").val();
    	let url 		= base_url+'dashboard/chart-project-scale/'+div; 
    	let priority 	= $('#project-priority').val();
		let year_start 	= $('#project-year').val(); 
	    $.ajax({
	            url: url+"?priority=" +priority+"&year_start="+year_start,
	            type:'GET',
	            async : true,
	            dataType : "json",
	            success:function(result){
	                  let v_reg = 0; 
	                  let v_ord = 0; 
	                  let v_meg = 0;
	                  let v_big = 0;

	                  if (mode == 'value') {
	                  		 if (result.REGULAR.value && result.REGULAR.value != 0) {
			                    v_reg = parseInt(result.REGULAR.value);
			                  }
			                  if (result.ORDINARY.value && result.ORDINARY.value != 0) {
			                    v_ord = parseInt(result.ORDINARY.value);
			                  }
			                  if (result.BIG.value && result.BIG.value != 0) {
			                    v_big = parseInt(result.BIG.value);
			                  }

			                  if (result.MEGA.value && result.MEGA.value != 0) {
			                    v_meg = parseInt(result.MEGA.value);
			                  }
	                  }

	                  if (mode == 'qty') {
	                  		if (result.REGULAR.qty && result.REGULAR.qty != 0) {
			                    v_reg = parseInt(result.REGULAR.qty);
			                  }
			                  if (result.ORDINARY.qty && result.ORDINARY.qty != 0) {
			                    v_ord = parseInt(result.ORDINARY.qty);
			                  }
			                  if (result.BIG.qty && result.BIG.qty != 0) {
			                    v_big = parseInt(result.BIG.qty);
			                  }

			                  if (result.MEGA.qty && result.MEGA.qty != 0) {
			                    v_meg = parseInt(result.MEGA.qty);
			                  }
	                  }

	                  console.log("mode",mode);
	                  let options = {
	                    chart: {
	                        plotBackgroundColor: null,
	                        backgroundColor: null,
	                        plotBorderWidth: null,
	                        plotShadow: false,
	                        type: 'pie',
	                        height:'371px',
	                    },
	                    width: '100%',
	                    title: {text:''},
	                    credits: {text:''},
	                    // tooltip: {
	                    //     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	                    // },
	                    plotOptions: {
	                        pie: {
	                            allowPointSelect: true,
	                            cursor: 'pointer',
	                            dataLabels: {
	                                enabled: true
	                            }
	                        }
	                    },
	                    exporting: { enabled: false },
	                    series: [{
	                        name: 'Scale',
	                        colorByPoint: true,
	                        dataLabels: {
	                                  style: {
	                                      fontSize : '11px',
	                                      border : '0px',
	                                      // textOutline : false
	                                    },
	                                  formatter: function () {
	                                      var a 	= '#6f6f6f';
	                                      var fmt 	=  '';
	                                      if (mode == 'value') {
	                                      	fmt = ' M';
	                                      }
	                                      return "<span style='borderColor:#fff'>" + this.point.name + " : <span style='font-size:11px;'>"+this.y+fmt+"</> ";
	                                  },
	                                  border : '0px',
	                                  distance: -50
	                              },
	                        point:{
	                              events:{
	                                  click: function (event) {
	                                      window.location = base_url+'project/data?scale=' + this.name.toUpperCase()+"&priority="+priority;
	                                  }
	                              }
	                        }, 
	                        data: [
	                        {
	                            name: 'Regular',
	                            y: v_reg ,
	                            color: '#f2e205'
	                        }, {
	                            name: 'Ordinary',
	                            y: v_ord ,
	                            color: '#7eddf2'
	                        }, {
	                            name: 'Big',
	                            y: v_big ,
	                            color: '#44aff2'
	                        }, {
	                            name: 'Mega',
	                            y: v_meg ,
	                            color: '#010440'
	                        }
	                        ]
	                    }],
	                  };
	                  Highcharts.chart('chart-project-scale', options);
	            }
	    });
  	}


	const dashboardInfo = () => {
		let priority 	= $('#project-priority').val();
		let year_start 	= $('#project-year').val(); 
	    $.ajax({
	                  url: base_url+"dashboard-executive-info?priority=" +priority+"&year_start="+year_start,
	                  type:'GET',
	                  async : true,
	                  dataType : "json",
	                  success:function(result){
	                  	  console.log(result);
	                      $("#t_active").html(result.t_active);
	                      $("#v_active").html(result.v_active+' M');

	                      $("#t_close").html(result.t_close);
	                      $("#v_close").html(result.v_close+' M');
	                  
	                      $("#t_all").html(result.t_all);
	                      $("#v_all").html(result.v_all+' M');

	                      $("#t_des").html(result.t_des);
	                      $("#v_des").html(result.v_des+' M');

	                      $("#t_dbs").html(result.t_dbs);
	                      $("#v_dbs").html(result.v_dbs+' M');

	                      $("#t_dgs").html(result.t_dgs);
	                      $("#v_dgs").html(result.v_dgs+' M');
	                      $('.miliar').priceFormat({
		                        suffix: ' M',
		                        prefix: '',
		                        centsSeparator: ',',
		                        thousandsSeparator: '.',
		                        centsLimit: 0
		                    });
	                  }
	         }); 
	}

    return {
        init: function() {
        	chartScaleProject();
           	chartStatusProject();
           	tableInit();
           	dashboardInfo();
            

           	$(document).on('change','.project-global-filter', function (e) {
              e.stopImmediatePropagation();
              	 	$("#btn-project-scale-value").removeClass("active");
	              	$("#btn-project-scale-qty").addClass("active");
	              	chartScaleProject('qty');
	              	$("#btn-project-status-qty-value").removeClass("active");
              		$("#btn-project-status-qty-qty").addClass("active");
              		chartStatusProject('qty');
	           		
	           		$('#list-project').dataTable().fnDestroy();
	           		tableInit();
	           		dashboardInfo();
             });


            $(document).on('change','.project-filter', function (e) {
              e.stopImmediatePropagation();
              $('#list-project').dataTable().fnDestroy();
              tableInit();
             });
           // dashboardInfo();

           $(document).on("click",".btn-project-status-qty", (e)=>{
              let type = $(e.currentTarget).data('id');
              $(".btn-project-status-qty").removeClass("active");
              $(e.currentTarget).addClass("active");
              chartStatusProject(type);
           });

           $(document).on('change','#btn-project-status-qty-division', function (e) {
              $("#btn-project-status-qty-value").removeClass("active");
              $("#btn-project-status-qty-qty").addClass("active");
              chartStatusProject('qty');
            });

           $(document).on("click",".btn-project-scale", (e)=>{
              let type = $(e.currentTarget).data('id');
              $(".btn-project-scale").removeClass("active");
              $(e.currentTarget).addClass("active");
              chartScaleProject(type);
           });               

           $(document).on('change','#btn-project-scale-division', function (e) {
              $("#btn-project-scale-value").removeClass("active");
              $("#btn-project-scale-qty").addClass("active");
              chartScaleProject('qty');
            });        

           $(document).on('click','.row_links',function(e){
              e.stopImmediatePropagation();
              window.location = $(this).data('link');
            });

           $(document).on('click','#info-total-project, #info-total-project-value',function(e){
              e.stopImmediatePropagation();
              window.location = base_url+"project/data/active"
            });
      }
  }

}();

jQuery(document).ready(function() {
      Dashboard.init();
  });  
</script>