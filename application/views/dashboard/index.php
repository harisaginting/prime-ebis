<label>Project Management <strong><?= $this->session->userdata("division"); ?></strong></label>
<div class="row">
  <div class="col-2">
      <div class="card card-pm-summary">
        <div class="card-body">
            <div class="w-90 mb-1 mt-0 mb-0 text-white" style="font-size: 3.5rem;margin-top: -10px !important;"><strong><?= $total_pm ?></strong></div>
            <div class="w-90 h6 mt-0 text-white" style="margin-top: -15px !important;"><strong>Projects Manager</strong></div>
        </div>
      </div>
      <div class="card card-project-summary">
        <div class="card-body">
            <div class="w-90 mb-1 mt-0 mb-0 text-white" style="font-size: 3.5rem;margin-top: -10px !important;"><strong><?= $total_project_active['TOTAL'] ?></strong></div>
            <div class="w-90 h5 mt-0 text-white" style="margin-top: -15px !important;"><strong style="font-size: 14px;">Projects Active</strong></div>
            <?php if(!empty($total_project_active['VALUE'])) : ?>
            <div class="w-90 h6 text-white">
              <span style="font-size: 0.7em">Worth</span> <span style="font-size: 0.9em" class="rupiah"><?= $total_project_active['VALUE'] ?></span>
            </div>
            <?php endif; ?>
        </div>
      </div>
      <div class="card card-project-closed-summary">
        <div class="card-body pt-0 pb-1">
            <div class="w-90 mb-1 mt-0 mb-0 text-white" ><strong style="font-size: 2.5rem;margin-top: -10px !important;"><?= $total_project_closed['TOTAL'] ?></strong></div>
            <div style="margin-top: -20px;" class="text-white pl-2">
              <small style="font-weight: 800;letter-spacing: 0.8px;">Projects Closed</small>
            </div>
        </div>
      </div>
      <div class="card card-project-candidate-summary">
        <div class="card-body pt-0 pb-1">
            <div class="w-90 mb-1 mt-0 mb-0 text-white" ><strong style="font-size: 2.5rem;margin-top: -10px !important;"><?= $total_project_candidate['TOTAL'] ?></strong> </div>
            <div style="margin-top: -20px;" class="text-white pl-2">
              <small style="font-weight: 800;letter-spacing: 0.8px;">Projects Candidate</small>
            </div>
        </div>
      </div>
  </div>
  <div class="col-10">
      <div class="row">
        <div class="col-4 p-0" id="project_scale">
            <div class="gooey">
              <span class="dot"></span>
              <div class="dots">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
        </div>
        <div class="col-8 p-0" id="project_progress">
            <div class="gooey">
              <span class="dot"></span>
              <div class="dots">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
        </div>
      </div>
  </div>

  <div class="col-12">
        <div class="gooey">
          <span class="dot"></span>
          <div class="dots">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
  </div>

  <div class="col-6">
        <div class="gooey">
          <span class="dot"></span>
          <div class="dots">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
  </div>

</div>


<script type="text/javascript">
var Dashboard = function () {      
    return {
        init: function() {
            $.ajax({
                  url: base_url+'dashboard/chart_project_scale',
                  type:'POST',
                  async : true,
                  dataType : "html",
                  success:function(result){
                    $('#project_scale').empty().append(result);
              }
          }).done(
              $.ajax({
                    url: base_url+'dashboard/chart_project_progress',
                    type:'POST',
                    async : true,
                    dataType : "html",
                    success:function(result){
                      $('#project_progress').empty().append(result);
                  }
              }));
      }
  }

}();

jQuery(document).ready(function() {
      Dashboard.init();
  });  
</script>




<style type="text/css">
  .card-project-summary{
    border-radius: 10px;
    height: 150px;
    background-image: url(./assets/img/project.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-color: #006eff;
  }

  .card-pm-summary{
    border-radius: 10px;
    height: 150px;
    background-image: url(./assets/img/project_manager.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-color: #006eff;
  }

  .card-project-candidate-summary{
    border-radius: 10px;
    height: 70px;
    background-image: url(./assets/img/candidate.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-color: #006eff;
  }

  .card-project-closed-summary{
    border-radius: 10px;
    height: 70px;
    background-image: url(./assets/img/check.png);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-color: #006eff;
  }

  .card-pm-summary > .card-body , .card-project-summary > .card-body, .card-project-closed-summary > .card-body, .card-project-candidate-summary > .card-body {
      background: #006effed;
      border-radius: 10px;
  }

  /*.card-project-closed-summary > .card-body, .card-project-candidate-summary > .card-body{
      background: #ff9800d9;
      border-radius: 10px;
  }*/
</style>