<style type="text/css">
	.btn-info{
		background-color: #f6e28a;
    	border-color: #f6e28a;
	}

	.btn-info.active{
		background-color: #f26e39 !important;
    	border-color: #f26e39 !important;
	}

	.card{
	-webkit-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
	box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
	}
  
  .text-kecil{
    font-size: 0.8em !important;
  }

  .text-skecil{
    font-size: 0.6em !important;
  }

	#project-partner.table-bordered.dataTable tbody td,
  #project-segmen.table-bordered.dataTable tbody td{
		padding: 2px 12px 2px 12px;
	} 

  #project-segmen.table-bordered.dataTable tbody td{
    padding: 8px 12px 8px 12px;
  } 

  thead{
    background: #fff !important;
    color: #000 !important;
  }

  #project-pm, #project-pm td {
    border: 0px !important; 
  } 
  
  #project-pm td{
    padding-top: 0px;
    padding-bottom: 0px;
  }

  #project-pm td .card {
    padding-bottom: 0px;
    padding-top: 0px;
    margin-top: 0px !important;
    box-shadow: 2px 2px 2px 0px rgba(187, 184, 184, 0.75);
  }

  #project-pm thead{
    display: none;
  }
  
  #info-total-project, #info-total-project-value{
    cursor: default;
  }

  .row_links{
    cursor: pointer;
  }

  .top-type{
    color:#555;
    font-weight: 700;
    font-size: 1.2em;
  }

  .top-count{
    color:#44aff2;
    font-weight: 700;
    font-size: 1.2em;
  }

  .top-value{
    color: #ffc107;
    font-weight: 700;
    font-size: 1.2em;
  }

  .container-symptom{
    height: 150px;
    border : 1px solid #ddd;
    border-radius: 10px;
    padding: 10px;
  }

  .total-symptom{
    font-size: 1.1em;
    font-weight: 800;
    font-family: Montserrat;  
  }

  .total-symptom{
    font-size: 1em;
    font-weight: 700;  
  }

  #chart-location {
    height: 500px; 
}

.text-warning{
  color : #ff7400 !important;
}
#project-pm_previous{
  display: none;
}
</style>

<div class="row mt-4 pl-3 pr-3">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				    <div class="row pl-2">
              <div class="col-12 mt-0 text-center h1 pt-1 pb-0 pl-0 pr-0">
                <div class="w-100 text-skecil text-left pl-2" style="margin-bottom: -11px;color: #bebebe;">
                  <strong class="pl-1 text-success fw-800">TOTAL PROJECT ACTIVE</strong>
                </div>
                <strong id="info-total-project" style="color:#010440;font-size: 2em;"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></strong>
              </div>
              <div class="col-12 mt-0 text-center h1 pt-2 pb-0 pl-0 pr-0">
                <div class="w-100 text-skecil text-left pl-2" style="margin-bottom: -11px;color: #bebebe;">
                  <strong class="pl-1 text-navy fw-800">TOTAL PROJECT VALUE (IDR)</strong>
                </div>
                <strong id="info-total-project-value" style="font-size: 2em;" class="text-warning mt-0"><img src="<?= base_url(); ?>/assets/img/simple_loader.gif" height="25"></strong>
              </div>
          </div>
			</div>
      <div class="col-md-8">
          <div class="row">
            <div class="col-12 pl-0 pr-0" style="margin-bottom: -5px;">
              <div style="margin-top: 7px;float: left;"><strong class="text-muted fw-700 h6">STATUS</strong></div>
              <div class="pull-right pr-3">
                <button class="btn btn-sm btn-info active btn-project-status-qty" style="width: 70px;" data-id="qty">Total</button>
                <button class="btn btn-sm btn-info btn-project-status-qty" style="width: 70px;" data-id='value'>Value</button>
              </div>
            </div>
            <div class="col-12">
              <div class="row" id="container-project-status-qty">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 pl-0 pr-0" style="margin-bottom: -5px;">
             <div style="margin-top: 7px;float: left;"><strong class="text-muted fw-700 h6">PROGRESS</strong></div>              
             <div class="pull-right pr-3">
                <button class="btn btn-sm btn-info active btn-project-active-status-qty" data-id="qty" style="width: 70px;">Total</button>
                <button class="btn btn-sm btn-info btn-project-active-status-qty" data-id="value" style="width: 70px;">Value</button>
              </div>
            </div>
            <div class="col-12">
              <div class="row" id="container-project-active-status-qty">
                 
              </div>
            </div>
          </div>
      </div>

		</div>
	</div>
</div>


  <div class="row mt-3 pl-3 pr-1">
    <div class="col-md-4 col-sm-12">
          <div class="w-100"><strong class="fw-700 h6 text-muted">SCALE</strong></div>
          <div id="chart-project-scale" class="w-100"></div>
    </div>
    <div class="col-md-8 col-sm-12">
      <div class="card bg-white w-100 p-2">
        <div class="w-100"><strong class="fw-700 h6 text-muted">PARTNERS</strong></div>
        <table class="table table-responsive-sm table-bordered" id="project-partner">
          <thead>
            <tr>
              <th style="width: 60%;" class="border-top-0 border-right-0 border-bottom-0"><span class="pl-2">NAME</span></th>
              <th style="width: 20%;text-align: center;" class="border-top-0 border-right-0 border-bottom-0">TOTAL</th>
              <th style="width: 20%;text-align: center;" class="border-top-0 border-bottom-0">VALUE(M)</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

<div class="row">
  <div class="col-md-4 col-sm-12 pr-2">
		<div class="card bg-white p-2" style="height: 570px;">
			<div class="w-100" style="min-height: 45px;"><strong class="h3 fw-700 text-muted">PROJECT MANAGER</strong></div>
			<table class="table table-responsive-sm table-bordered" id="project-pm">
				<thead>
					<tr>
						<th style="width: 80%;" class="border-top-0 border-bottom-0 border-right-0"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
  
  <div class="col-md-4 col-sm-12 pr-2 pl-2">
    <div class="card bg-white p-2" style="height: 570px;">
      <div class="w-100" style="min-height: 45px;"><strong class="h3 fw-700 text-muted">PORTOFOLIO</strong></div>
      <table style="height: 460px;overflow-y: scroll;display: block;" class="table table-responsive-sm scroll-thin">
        <tbody class="w-100" style="display: inline-table;">
           <?php foreach ($type_project_value as $key => $value) : ?>
             <tr>
               <td><?= $value['CATEGORY_PROJECT'] ?></td>
               <td style="min-width: 100px;" class="text-right"><?= $value['VALUE_PROJECT'] < 1 && $value['VALUE_PROJECT'] > 0 ? '0'.$value['VALUE_PROJECT'] : $value['VALUE_PROJECT'] ?> M</td>
             </tr>
           <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="col-md-4 col-sm-12 pl-2">
    <div class="card bg-white p-2 pb-3" style="height: 570px;">
      <div class="w-100">
        <div style="min-height: 55px;float: left;"><strong class="h3 fw-800 text-muted">SEGMEN</strong></div>
        <div class="pull-right pr-1">
          <button id="btn-project-segmen-total" class="btn btn-sm btn-info active" style="width: 70px;">Jumlah</button>
          <button id="btn-project-segmen-value" class="btn btn-sm btn-info" style="width: 70px;">Nilai</button>
        </div>
          <table class="table table-responsive-sm table-bordered" id="project-segmen">
            <thead>
              <tr>
                <th style="width: 70%;" class="border-top-0 border-bottom-0 border-right-0 pl-2">SEGMEN</th>
                <th style="width: 30%;" class="border-top-0 border-bottom-0 text-center" id="label-project-segmen">TOTAL</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-md-4 col-sm-12 pr-2">
    <div class="card bg-white p-2 mb-1" style="height: 300px;">
      <div class="w-100"><strong class="text-muted h3 fw-700">TARGET</strong></div>
      <div class="progress-group mt-3">
        <div class="progress-group-header align-items-end">
        <div class="row w-100">
          <div class="col-6">
             <strong style="font-size: 1.2em;"><?= date('F',strtotime('first day of -1 month')); ?></strong>
          </div>
          <div class="col-3" id="t_project_lm">0</div>
          <div class="col-3" id="v_project_lm">0M</div>
        </div>
        <div class="text-muted small" id="tp_project_lm">(56%)</div>
        </div>
          <div class="progress-group-bars">
            <div class="progress progress-md">
              <div class="progress-bar" role="progressbar" style="width: 0%" id="p_project_lm" aria-valuemax="100"></div>
            </div>
          </div>
        </div>

        <div class="progress-group mt-3">
        <div class="progress-group-header align-items-end">
        <div class="row w-100">
          <div class="col-6">
             <strong style="font-size: 1.2em;"><?= date('F') ?></strong>
          </div>
          <div class="col-3" id="t_project_m">0</div>
          <div class="col-3" id="v_project_m">0M</div>
        </div>
        <div class="text-muted small" id="tp_project_m">(56%)</div>
        </div>
          <div class="progress-group-bars">
            <div class="progress progress-md">
              <div class="progress-bar" role="progressbar" style="width: 0%" id="p_project_m"></div>
            </div>
          </div>
        </div>

        <div class="progress-group mt-3">
        <div class="progress-group-header align-items-end">
        <div class="row w-100">
          <div class="col-6">
             <strong style="font-size: 1.2em;"><?= date('F',strtotime('first day of +1 month')); ?></strong>
          </div>
          <div class="col-3" id="t_project_nm">0</div>
          <div class="col-3" id="v_project_nm">0M</div>
        </div>
        <div class="text-muted small" id="tp_project_nm">(56%)</div>
        </div>
          <div class="progress-group-bars">
            <div class="progress progress-md">
              <div class="progress-bar" role="progressbar" style="width: 0%" id="p_project_nm"></div>
            </div>
          </div>
        </div>

    </div>

    <div class="card bg-white p-2 mb-1" style="height: 150px;">
      <div class="w-100"><strong class="text-muted h5 fw-700">TERM Of PAYMENT (CUSTOMER)<small> <?= date('F') ?></small></strong></div>
      
      <div class="w-100" style="border-bottom: 1px solid black">
        <div class="row mt-3">
            <div class="col-5 top-type">OTC</div>
            <div class="col-4 top-count text-right" id="top-customer-otc">XX</div>
            <div class="col-3 top-value text-right" id="top-customer-otc-value">XXM</div>
        </div>
      </div>

      <div class="w-100">
        <div class="row">
            <div class="col-5 top-type">TERMIN</div>
            <div class="col-4 top-count text-right" id="top-customer-termin">XX</div>
            <div class="col-3 top-value text-right" id="top-customer-termin-value">XXM</div>
        </div>
      </div>

    </div>

    <div class="card bg-white p-2" style="height: 150px;">
      <div class="w-100"><strong class="text-muted h5 fw-700">TERM OF PAYMENT (PARTNER)<small> <?= date('F') ?></small></strong></div>
      
      <div class="w-100" style="border-bottom: 1px solid black">
        <div class="row mt-3">
            <div class="col-5 top-type">OTC</div>
            <div class="col-4 top-count text-right" id="top-partner-otc">XX</div>
            <div class="col-3 top-value text-right" id="top-partner-otc-value">XXM</div>
        </div>
      </div>

      <div class="w-100">
        <div class="row">
            <div class="col-5 top-type">TERMIN</div>
            <div class="col-4 top-count text-right" id="top-partner-termin">XX</div>
            <div class="col-3 top-value text-right" id="top-partner-termin-value">XXM</div>
        </div>
      </div>

    </div>
  </div>
  
  <div class="col-md-4 col-sm-12 pr-2 pl-2">
    <div class="card bg-white p-2" style="height: 605px;">
      <div class="w-100"><strong class="text-muted h3 fw-700">TOP 3 SYMPTOM</strong></div>
       
       <div class="container-symptom mt-3">
        <div class="w-100"><strong class="h6 fw-700">TELKOM</strong></div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-telkom-name-1">1.</div>
            <div class="col-2 total-symptom"  id="symptom-telkom-total-1">-</div>
        </div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-telkom-name-2">2.</div>
            <div class="col-2 total-symptom"  id="symptom-telkom-total-2">-</div>
        </div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-telkom-name-3">3.</div>
            <div class="col-2 total-symptom"  id="symptom-telkom-total-3">-</div>
        </div>
      </div>

      <div class="container-symptom mt-3">
        <div class="w-100"><strong class="h6 fw-700">MITRA</strong></div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-mitra-name-1">1.</div>
            <div class="col-2 total-symptom"  id="symptom-mitra-total-1">-</div>
        </div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-mitra-name-2">2.</div>
            <div class="col-2 total-symptom"  id="symptom-mitra-total-2">-</div>
        </div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-mitra-name-3">3.</div>
            <div class="col-2 total-symptom"  id="symptom-mitra-total-3">-</div>
        </div>
      </div>

      <div class="container-symptom mt-3">
        <div class="w-100"><strong class="h6 fw-700">CUSTOMER</strong></div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-customer-name-1">1. </div>
            <div class="col-2 total-symptom"  id="symptom-customer-total-1">-</div>
        </div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-customer-name-2">2. </div>
            <div class="col-2 total-symptom"  id="symptom-customer-total-2">-</div>
        </div>
        <div class="list-symptom row mt-2">
            <div class="col-10 name-symptom" id="symptom-customer-name-3">3. </div>
            <div class="col-2 total-symptom"  id="symptom-customer-total-3">-</div>
        </div>
      </div>

    </div>
  </div>

  <div class="col-md-4 col-sm-12 pl-2">
    <div class="card bg-white p-0">
      <div class="col-12 p-0" style="margin-bottom: -30px;z-index: 900;">
        <div class="row">
          <div class="col-5 offset-1 text-center pt-2">
            <strong class="text-muted h3 fw-700">BAST </strong>
          </div>
          <div class="col-5 pt-2 text-center">
            <strong class="text-muted h3 fw-700">BAUT </strong>
          </div>
        </div>
      </div>
      <div class="col-12 pl-1 pr-1" id="chart-bast">
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-12">
    <div class="card bg-white p-0 pt-1">
        <div class="w-100 mb-3"><strong class="h3 text-muted fw-700 pl-2 pt-2">ACHIEVEMENT</strong></div>
        <div id="chart-achievement"></div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card bg-white p-0 pt-1">
        <div class="w-100 mb-3"><strong class="h3 text-muted fw-700 pl-2 pt-2">LOCATION</strong></div>
        <div class="row">
            <div class="col-md-9 col-sm-12 pr-0" id="chart-location"></div>
            <div class="col-md-3 col-sm-12 pl-0">
              <table class="table">
                <thead>
                  <th>Regional</th>
                  <th class="text-right">Qty</th>
                  <th class="text-right">Value</th>
                </thead>
                <tbody>
                  <?php foreach ($regional["row"] as $key => $value): ?>
                    <tr>
                      <td><?= $value["REGIONAL_NAME"] ?></td>
                      <td class="text-right" style="color:#5dc8ff;font-weight: 800;"><?= $value["QUANTITY"] ?></td>
                      <td class="text-right" style="color:#f26e39;font-weight: 800;"><?= $value["TOTAL_VALUE"] < 1 && $value["TOTAL_VALUE"] > 0 ? "0".$value["TOTAL_VALUE"] : $value["TOTAL_VALUE"] ?>M</td>
                    </tr>  
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
Highcharts.chart('chart-bast', {
    chart: {
        type: 'column',
        height: '600px',
    },
    width: '100%',
    title: {text:''},
    credits: {text:''},
    exporting: { enabled: false },
    xAxis: {
        categories: ['BAST', 'BAUT']
    },
    yAxis: {
        visible : false,
        min: 0,
        title: {
            text: '0'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'gray'
            }
        }
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                style: {fontWeight: 'bolder'},
                formatter: function() {return this.series.userOptions.name + ': ' + this.y},
                inside: true,
            }
        }
    },
    series: [{
        name: 'Received',
        data: <?= json_encode($bast["RECEIVED"]) ?>,
        color: '#cccccc'
    }, {
        name: 'Approval',
        data: <?= json_encode($bast["CHECK"]) ?>,
        color: '#0ca5c7'
      
    }, {
        name: 'Revision',
        data: <?= json_encode($bast["REVISION"]) ?>,
        color : "#f2e205"
    },
    {
        name: 'Approved',
        data: <?= json_encode($bast["APPROVED"]) ?>,
        color: '#0cc723'
    },
    {
        name: 'Done',
        data: <?= json_encode($bast["DONE"]) ?>,
        color: '#3a0cc7'
    },
    {
        name: 'Take Out',
        data: <?= json_encode($bast["TAKE_OUT"]) ?>,
        color: '#0c46c7'
    }
    ]
});


var Dashboard = function () {
  const chartLocation = () =>{
    // Prepare demo data
    // Data is joined to map using value of 'hc-key' property by default.
    // See API docs for 'joinBy' for more info on linking data and map.
    var data = [
        ['id-3700', 0],
        ['id-ac', 1],
        ['id-jt', 2],
        ['id-be', 3],
        ['id-bt', 4],
        ['id-kb', 5],
        ['id-bb', 6],
        ['id-ba', 7],
        ['id-ji', 8],
        ['id-ks', 9],
        ['id-nt', 10],
        ['id-se', 11],
        ['id-kr', 12],
        ['id-ib', 13],
        ['id-su', 14],
        ['id-ri', 15],
        ['id-sw', 16],
        ['id-ku', 17],
        ['id-la', 18],
        ['id-sb', 19],
        ['id-ma', 20],
        ['id-nb', 21],
        ['id-sg', 22],
        ['id-st', 23],
        ['id-pa', 24],
        ['id-jr', 25],
        ['id-ki', 26],
        ['id-1024', 27],
        ['id-jk', 28],
        ['id-go', 29],
        ['id-yo', 30],
        ['id-sl', 31],
        ['id-sr', 32],
        ['id-ja', 33],
        ['id-kt', 34]
    ];

    var dataValue = <?= json_encode($regional["det"]) ?>;
    console.log(dataValue);
    // Create the chart
    Highcharts.mapChart('chart-location', {
                  chart: {
                      map: 'countries/id/id-all',
                  },

                  title: {
                      text: ''
                  },
                  exporting: { enabled: false },
                  legend: {
                        enabled: true,
                        verticalAlign: 'top',
                        labelFormatter: function () {
                            return "Regional "+this.name;
                        }
                    },
                  plotOptions: {
                      map: {
                          allAreas: false,
                          joinBy: ['hc-a2','code'],
                          dataLabels: {
                              enabled: true,
                              color: '#000',
                              style: {
                                  fontWeight: 'bold',
                                  fontFamily: 'sans-serif'
                              },
                              format: null,
                              formatter: function () {
                                var loc = ["Jambi","Banten","Jawa Barat","Yogyakarta","Nusa Tenggara Barat","Kalimantan Tengah","Irian Jaya Barat"];
                                // console.log(this.series);
                                  if(loc.includes(this.point.name)){
                                    // console.log(loc.indexOf(this.point.name));
                                    return "regional "+this.series.name;  
                                  }
                                  
                              }
                          }
                      },
                      series : {
                          events:{
                              click: function (event) {
                                // console.log(this.name);
                                  window.location = base_url+'project/data?regional=' + this.name;
                              }
                          }
                      }
                  },
                  tooltip: {
                      style: {
                          fontFamily: 'sans-serif'
                      },
                      formatter: function () {
                         return this.key +" ,Regional "+this.series.name+"( <strong>"+dataValue[parseInt(this.series.name)-1].qty+"</strong> Project, Value :<strong>"+dataValue[parseInt(this.series.name)-1].value+" )</strong>";
                      }
                  },
                  colors: ['#4dbd74','#c30f0f','#ffe000','#0072ff','#0ca5c7','#4ad000','#f18500'],
                  series: [
                  {
                      name: '1',
                      data: ['AC','SU','SB','LA','JA','KR','BB','SL','KR','RI'].map(function (code) {
                          return { code: code }
                      }),
                  },
                  {
                      name: '2',
                      data: ['JK','BT'].map(function (code) {
                          return { code: code }
                      })
                  },
                  {
                      name: '3',
                      data: ['JR'].map(function (code) {
                          return { code: code }
                      })
                  },
                  {
                      name: '4',
                      data: ['JT','YO'].map(function (code) {
                          return { code: code }
                      })
                  },
                  {
                      name: '5',
                      data: ['JI','NT','NB'].map(function (code) {
                          return { code: code }
                      })
                  },
                  {
                      name: '6',
                      data: ['KB','KS','KT','KU','KI'].map(function (code) {
                          return { code: code }
                      })
                  },
                  {
                      name: '7',
                      data: ['SW','SE','SR','ST','SG','GO','PA','IB'].map(function (code) {
                          return { code: code }
                      })
                  }

                  ]
              });


    }

	var tableProjectPartner = function(){                     
        var table = $('#project-partner').DataTable({
                  initComplete: function(settings, json) {
                        $('.rupiah').priceFormat({
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.',
                            centsLimit: 0
                        });
                        $(function () {
                          $('[data-toggle="tooltip"]').tooltip()
                        });
                    },
                    dom: 'rtip',
                    processing: true,
                    serverSide: true,
                    order :[2,'desc'],
                    ajax: { 
                        'url'  :base_url+'datatable/dashboard_project_partner', 
                        'type' :'POST',
                        'data' : {
                                  status  : $('#status').val(),
                                  pm      : $('#pm').val(),
                                  customer: $('#customer').val(),
                                  regional: $('#regional').val(),
                                  type    : $('#type').val(),
                                  mitra   : $('#mitra').val(),
                                  segmen  : $('#segmen').val()
                                  }   
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='w-100'><strong>"+obj.PARTNER_NAME+"</strong></div>" 
                            }            
                                    
                        },
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='w-100 text-center'><strong>"+obj.PROJECT_COUNT+"</strong></div>" 
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='w-100'><strong class='pull-right'>Rp. "+obj.PROJECT_VALUE+" M</strong></div>" 
                            }            
                                    
                        }, 
                       ],
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'project/data?partner='+aData['ID_PARTNER']); 
                          return nRow;
                          }

                });  
    };
  var tableProjectPm = function(){                     
      var tablePm = $('#project-pm').DataTable({
                initComplete: function(settings, json) {
                      $('.rupiah').priceFormat({
                          prefix: '',
                          centsSeparator: ',',
                          thousandsSeparator: '.',
                          centsLimit: 0
                      });
                      $('.miliar').priceFormat({
                          suffix: ' M',
                          prefix: '',
                          centsSeparator: ',',
                          thousandsSeparator: '.',
                          centsLimit: 2
                      });
                      $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                      });
                  },
                  dom: 'rtip',
                  processing: true,
                  serverSide: true,
                  pageLength: 6,
                  order :[0,'DESC'],
                  ajax: { 
                      'url'  :base_url+'datatable/dashboard_project_pm', 
                      'type' :'POST',
                      'data' : {
                                status  : $('#status').val(),
                                pm      : $('#pm').val(),
                                customer: $('#customer').val(),
                                regional: $('#regional').val(),
                                type    : $('#type').val(),
                                mitra   : $('#mitra').val(),
                                segmen  : $('#segmen').val()
                                }   
                      },
                  aoColumns: [
                      { 
                          'mRender': function(data, type, obj){   
                                   avatar = obj.AVATAR;
                                   value  = obj.PROJECT_VALUE;
                                   if(!parseInt(value)){
                                    value = "0 "+value;
                                   }
                                   value = value.replace(".", ",");

                                   if(avatar == null || avatar == ''){
                                    avatar = 'default.png';
                                   }
                                   return "<div class='card mb-1'><div class='w-100 card-body p-2 bg-white'><img class='border circle pull-left' width='15%;' style='border: 3px solid #ddd !important;' src='"+base_url+"/assets/avatar/"+avatar+"'  /><div class='pl-2 pt-1' style='display:inline-block;width:80%;'>"
                                    +"<span style='font-size:0.9em;font-weight:700;'>"+obj.PM_NAME+"</span>"
                                    +"<div class='w-100'><span><strong style='font-size:1em;'>"+obj.PROJECT_COUNT+"</strong> Project <strong class='text-success'>Active</strong></span>"
                                    +"<span class='pull-right'><strong style='font-size:1.2em;' class='miliar'>"+value +"<strong></span></div>"
                                    +"</div></div></div>";
                          }            
                                  
                      }

                     ]    
              });  
  };      

  var tableProjectSegmen = function(typex = "total"){       
      $('#label-project-segmen').html( typex.toUpperCase() );      
      $('#project-segmen').DataTable().destroy();
      $('#project-segmen').DataTable({
                initComplete: function(settings, json) {
                      $('.rupiah').priceFormat({
                          prefix: '',
                          centsSeparator: ',',
                          thousandsSeparator: '.',
                          centsLimit: 0
                      });
                      $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                      });
                  },
                  dom: 'rtip',
                  processing: true,
                  serverSide: true,
                  order :[1,'desc'],
                  ajax: { 
                      'url'  :base_url+'datatable/dashboard_project_segmen', 
                      'type' :'POST',
                      'data' : {
                                status  : $('#status').val(),
                                pm      : $('#pm').val(),
                                customer: $('#customer').val(),
                                regional: $('#regional').val(),
                                type    : $('#type').val(),
                                mitra   : $('#mitra').val(),
                                segmen  : $('#segmen').val()
                                }   
                      },
                  aoColumns: [
                      { 
                          'mRender': function(data, type, obj){   
                                   return "<div class='w-100'><strong>"+obj.SEGMEN_NAME+"</strong></div>" 
                          }            
                                  
                      },
                      { 
                          'mRender': function(data, type, obj){   
                                   let value = obj.PROJECT_COUNT;
                                   if(typex == "value"){
                                       let mil = obj.PROJECT_VALUE;
                                       if(mil < 1 && mil > 0){
                                          mil = "0"+mil;
                                       }
                                       value = mil+"M";
                                   }  
                                   return "<div class='w-100 text-right pr-2'><strong>"+ value +"</strong></div>" 
                          }            
                                  
                      }, 


                     ]    
              });  
  };


  const chartStatusProject = (type = null) =>{
    let url = base_url+'dashboard/status_project?priority=all';
    $.ajax({
            url: url,
            type:'POST',
            async : true,
            dataType : "json",
            success:function(result){
                let mode = '';
                let total = result.total_project - ((result.total_project * 10)/100);
                let seriesdata = [{
                          name: result.STATUS[0],
                          data: [result.TOTAL[0]]
                      }, {
                          name: result.STATUS[1],
                          data: [result.TOTAL[1]]
                      }, {
                          name: result.STATUS[2],
                          data: [result.TOTAL[2]]
                      }];
                if(type == 'value'){
                    seriesdata = [{
                          name: result.STATUS[0],
                          data: [result.VALUE[0]]
                      }, {
                          name: result.STATUS[1],
                          data: [result.VALUE[1]]
                      }, {
                          name: result.STATUS[2],
                          data: [result.VALUE[2]]
                      }];
                    mode = 'M';
                    total = result.total_project_value - ((result.total_project_value * 10)/100);
                }

                let options = {
                              chart: {
                                  type: 'bar',
                                  height:'90px',
                                  pointWidth : '50px',
                                  backgroundColor: null
                              },
                              exporting: { enabled: false },
                              title: {
                                  text: null
                              },
                              colors: ['#010440','#7eddf2','#44aff2'],
                              credits: {text:''},
                              xAxis: {
                                  labels:{
                                    enabled: false
                                  },
                              },
                              yAxis: {
                                  max:total,
                                  title: {
                                      text: ''
                                  },
                                  labels:{
                                    enabled: false
                                  }
                              },
                              legend: {
                                  enabled: false
                              },
                              plotOptions: {
                                 series: {
                                      stacking: 'normal',
                                      cursor:'pointer',
                                      events: {
                                            click: function (event) {
                                                window.location.href = base_url+'project/data/' + this.name.toUpperCase();
                                            }
                                        }
                                    },
                                        bar: {
                                            grouping: true,                  
                                            groupPadding:0.1,
                                            pointWidth:70,
                                            pointPadding: 0,
                                            dataLabels: {
                                                enabled: true,
                                                inside:true,
                                                useHTML: true,
                                                align: 'center',
                                                color: 'white',
                                                style: {
                                                    fontWeight: 'bold'
                                                },                      
                                                verticalAlign: 'middle',
                                                formatter: function () {
                                                    if (this.series.name) 
                                                      return '<span style="color:white">' + this.series.name + ' ' + this.y +mode+'</span>';
                                                    else return '';
                                                }
                                            }
                                        }
                                    },          
                              series: seriesdata
                          };
                        $("#container-project-status-qty").empty();
                        Highcharts.chart('container-project-status-qty', options);
            }
      });
  }

  const chartProgressProject = (type = null) =>{
    let url = base_url+'dashboard/progress_project?priority=all';
    // let seriesdata = [];
    $.ajax({
            url: url,
            type:'POST',
            async : true,
            dataType : "json",
            success:function(result){
                let mode  = '';
                let total = result.total_project - ((result.total_project * 10)/100);
                let seriesdata = [];
                if(type == 'value'){
                    mode  = 'M';
                    total = result.total_project_value - ((result.total_project_value * 10)/100);
                    result.VALUE.forEach((value,index) => {
                        seriesdata.push({name : result.PROGRESS[index], data: [value]});
                    });
                }else{
                    mode = '';
                    result.TOTAL.forEach((value,index) => {
                        seriesdata.push({name : result.PROGRESS[index], data: [value]});
                    });
                }
                color = [];
                result.PROGRESS.forEach((value,index) => {
                    switch(value) {
                          case "LEAD":
                            color.push("#2FBAA0");
                            break;
                          case "LAG":
                            color.push("#f6e28a");
                            break;
                          case "DELAY":
                            color.push("#EF4730");
                            break;
                          default:
                        }
                })


                let options = {
                              chart: {
                                  type: 'bar',
                                  height:'90px',
                                  pointWidth : '50px',
                                  backgroundColor: null
                              },
                              exporting: { enabled: false },
                              title: {
                                  text: null
                              },
                              colors: color,
                              credits: {text:''},
                              xAxis: {
                                  labels:{
                                    enabled: false
                                  },
                              },
                              yAxis: {
                                  title: {
                                      text: ''
                                  },
                                  labels:{
                                    enabled: false
                                  },
                                  max : total
                              },
                              legend: {
                                  enabled: false
                              },
                              plotOptions: {
                                 series: {
                                      stacking: 'normal',
                                      cursor:'pointer',
                                      events: {
                                            click: function (event) {
                                                window.location.href = base_url+'project/data?progress=' + this.name;
                                            }
                                        }
                                  },
                                        bar: {
                                            grouping: true,                  
                                            groupPadding:0.1,
                                            pointWidth:70,
                                            pointPadding: 0,
                                            dataLabels: {
                                                enabled: true,
                                                inside:true,
                                                useHTML: true,
                                                align: 'center',
                                                color: 'white',
                                                style: {
                                                    fontWeight: 'bold'
                                                },                      
                                                verticalAlign: 'middle',
                                                formatter: function () {
                                                    if (this.series.name) 
                                                      return '<span style="color:#000">' + this.series.name + ' ' + this.y +mode+'</span>';
                                                    else return '';
                                                }
                                            }
                                        }
                                    },          
                              series: seriesdata
                          };
                        $("#container-project-active-status-qty").empty();
                        Highcharts.chart('container-project-active-status-qty', options);
            }
      });
  }

  const chartAchievementProject = (paramsDataLead, paramsDataLag, paramsDataDelay) => {
      Highcharts.chart('chart-achievement', {
            chart: {
                type: 'scatter',
                zoomType: 'xy'
            },
            exporting : false,
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            credits: {
                text: ''
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: 'Value(Billions)'
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true
            },
            yAxis: {
                title: {
                    text: 'Progress (%)'
                },
                min : 0,
                max : 100,
            },
            legend: false,
            plotOptions: {
                scatter: {
                    marker: {
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        }
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        pointFormat: '{point.x} project, {point.y} achievement'
                    }
                }
            },
            series: [
            {
                name: 'LEAD',
                color: '#2FBAA0',
                data: paramsDataLead
            },
            {
                name: 'LAG',
                color: '#f2e205',
                data: paramsDataLag
            },
            {
                name: 'DELAY',
                color: '#EF4730',
                data: paramsDataDelay
            }
            ]
        });
  }

  const chartScaleProject = () => {
    let url = base_url+'dashboard/chart_project_scale_simple?priority=all';
    
    $.ajax({
            url: url,
            type:'GET',
            async : true,
            dataType : "json",
            success:function(result){
                  let v_reg = 0; 
                  let v_ord = 0; 
                  let v_meg = 0;
                  let v_big = 1;
                  if (result.REGULAR && result.REGULAR != 0) {
                    v_reg = parseInt(result.REGULAR);
                  }
                  if (result.ORDINARY && result.ORDINARY != 0) {
                    v_ord = parseInt(result.ORDINARY);
                  }
                  if (result.BIG && result.BIG != 0) {
                    v_big = parseInt(result.BIG);
                  }

                  if (result.MEGA && result.MEGA != 0) {
                    v_meg = parseInt(result.MEGA);
                  }

                  let options = {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                        height:'380px',
                    },
                    width: '100%',
                    title: {text:''},
                    credits: {text:''},
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    exporting: { enabled: false },
                    series: [{
                        name: 'Scale',
                        colorByPoint: true,
                        dataLabels: {
                                  style: {
                                      fontSize : '11px',
                                      border : '0px',
                                      // textOutline : false
                                    },
                                  formatter: function () {
                                      var a = '#6f6f6f';
                                      return this.y > 1 ? "<span style='borderColor:#fff'>" + this.point.name + " : <span style='font-size:11px;'>"+this.y+"</> ": null;
                                  },
                                  border : '0px',
                                  distance: -50
                              },
                        point:{
                              events:{
                                  click: function (event) {
                                      window.location = base_url+'project/data?scale=' + this.name.toUpperCase();;
                                  }
                              }
                        }, 
                        data: [
                        {
                            name: 'Regular',
                            y: v_reg ,
                            color: '#f2e205'
                        }, {
                            name: 'Ordinary',
                            y: v_ord ,
                            color: '#7eddf2'
                        }, {
                            name: 'Big',
                            y: v_big ,
                            color: '#44aff2'
                        }, {
                            name: 'Mega',
                            y: v_meg ,
                            color: '#010440'
                        }
                        ]
                    }],
                  };
                  Highcharts.chart('chart-project-scale', options);
            }
    });
  }

  const dashboardInfo = () => {
    $.ajax({
                  url: base_url+'dashboard-info',
                  type:'POST',
                  async : true,
                  dataType : "json",
                  success:function(result){
                      $("#info-total-project").html(result.total_project);

                      if (result.total_value_project >= 1000) {
                        let val = result.total_value_project / 1000; 
                        $("#info-total-project-value").html(val.toFixed(2)+' T');
                      }else{
                        $("#info-total-project-value").html(result.total_value_project+' M');
                      }
                      
                      $("#t_project_lm").text(result.lastMonth.total);
                      $("#v_project_lm").text(result.lastMonth.value+'M');
                      $("#tp_project_lm").text(result.lastMonth.ach+'%');
                      $("#p_project_lm").css("width",result.lastMonth.ach+'%');

                      if(result.lastMonth.ach < 50){
                        $("#p_project_lm").addClass('bg-danger');
                      }else if(result.lastMonth.ach < 70){
                        $("#p_project_lm").addClass('bg-warning');
                      }else{
                        $("#p_project_lm").addClass('bg-success');
                      }

                      $("#t_project_m").text(result.currentMonth.total);
                      $("#v_project_m").text(result.currentMonth.value+'M');
                      $("#tp_project_m").text(result.currentMonth.ach+'%');
                      $("#p_project_m").css("width",result.currentMonth.ach+'%');

                      if(result.currentMonth.ach < 50){
                        $("#p_project_m").addClass('bg-danger');
                      }else if(result.currentMonth.ach < 70){
                        $("#p_project_m").addClass('bg-warning');
                      }else{
                        $("#p_project_m").addClass('bg-success');
                      }

                      $("#t_project_nm").text(result.nextMonth.total);
                      $("#v_project_nm").text(result.nextMonth.value+'M');
                      $("#tp_project_nm").text(result.nextMonth.ach+'%');
                      $("#p_project_nm").css("width",result.nextMonth.ach+'%');

                      if(result.nextMonth.ach < 50){
                        $("#p_project_nm").addClass('bg-danger');
                      }else if(result.nextMonth.ach < 70){
                        $("#p_project_nm").addClass('bg-warning');
                      }else{
                        $("#p_project_nm").addClass('bg-success');
                      }

                      $("#top-customer-otc").html(result.top_customer.otc.TOTALS)
                      $("#top-customer-otc-value").html(result.top_customer.otc.VALUE+"M")
                      $("#top-customer-termin").html(result.top_customer.termin.TOTALS)
                      $("#top-customer-termin-value").html(result.top_customer.termin.VALUE+"M")

                      $("#top-partner-otc").html(result.top_partner.otc.TOTALS)
                      $("#top-partner-otc-value").html(result.top_partner.otc.VALUE+"M")
                      $("#top-partner-termin").html(result.top_partner.termin.TOTALS)
                      $("#top-partner-termin-value").html(result.top_partner.termin.VALUE+"M")

                      chartAchievementProject(result.group_achievement_lead, result.group_achievement_lag, result.group_achievement_delay);


                      for (var i = 1; i <= result.top3symptom.CUSTOMER.length; i++) {
                        $("#symptom-customer-name-"+i).text(i +". "+result.top3symptom.CUSTOMER[i-1].VALUE)
                        $("#symptom-customer-total-"+i).text(result.top3symptom.CUSTOMER[i-1].TOTAL)
                      }

                      for (var i = 1; i <= result.top3symptom.TELKOM.length; i++) {
                        $("#symptom-telkom-name-"+i).text(i +". "+result.top3symptom.TELKOM[i-1].VALUE)
                        $("#symptom-telkom-total-"+i).text(result.top3symptom.TELKOM[i-1].TOTAL)
                      }

                      for (var i = 1; i <= result.top3symptom.MITRA.length; i++) {
                        $("#symptom-mitra-name-"+i).text(i +". "+result.top3symptom.MITRA[i-1].VALUE)
                        $("#symptom-mitra-total-"+i).text(result.top3symptom.MITRA[i-1].TOTAL)
                      }
                  }
         }); 
  }

    return {
        init: function() {
           tableProjectPartner();
           tableProjectPm();
           tableProjectSegmen();
           chartStatusProject();
           chartProgressProject();
           chartLocation();
           chartScaleProject();
           dashboardInfo();
           $('.miliar').priceFormat({
                suffix: ' M',
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.',
                centsLimit: 2
            });

           $(document).on("click","#btn-project-segmen-total", (e)=>{
              tableProjectSegmen("total");
           });

           $(document).on("click","#btn-project-segmen-value", (e)=>{
              tableProjectSegmen("value");
           });           

           $(document).on("click",".btn-project-status-qty", (e)=>{
              let type = $(e.currentTarget).data('id');
              $(".btn-project-status-qty").removeClass("active");
              $(e.currentTarget).addClass("active");
              chartStatusProject(type);
           });

           $(document).on("click",".btn-project-active-status-qty", (e)=>{
              let type = $(e.currentTarget).data('id');
              $(".btn-project-active-status-qty").removeClass("active");
              $(e.currentTarget).addClass("active");
              chartProgressProject(type);
           });                       

           $(document).on('click','.row_links',function(e){
              e.stopImmediatePropagation();
              console.log($(this).data('link'));
              window.location = $(this).data('link');
            });

           $(document).on('click','#info-total-project, #info-total-project-value',function(e){
              e.stopImmediatePropagation();
              window.location = base_url+"project/data/active"
            });
      }
  }

}();

jQuery(document).ready(function() {
      Dashboard.init();
  });  
</script>