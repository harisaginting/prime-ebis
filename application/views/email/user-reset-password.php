<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Notifikasi Prime</title>
<style type="text/css">
  *, *:before, *:after {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body {
  font-family: arial, sans-serif;
  color: #384047;
  font-size: 12px !important;
  max-width: 100%;
  min-width: 100%;
  margin: auto;
}

table {
  max-width: 100%;
  min-width: 100%;
  margin: auto;
}

caption {
  /*font-size: 0.8em;*/
  /*font-weight: bold;*/
  padding: 10px 0;
  background-color: #00f;
}

.headcaption {
  /*font-size: 1em;*/
  font-weight: bold;
  padding: 10px 0;
  background-color: #00f;
}

thead th {
  /*font-weight: bold;*/
  background: #d32f2f;
  color: #FFF;
}

tr {
  background: #f4f7f8;
  border-bottom: 1px solid #FFF;
  margin-bottom: 5px;
}

tr:nth-child(even) {
  background: #e8eeef;
}

th, td {
  text-align: left;
  padding: 10px;
  /*font-size: 14px;*/
  font-weight: 300;
}

td {
  text-align: left;
  padding: 10px;
  font-size: 10px;
  font-weight: 1;
}


tfoot tr {
  background: none;
}

tfoot td {
  padding: 10px 2px;
  /*font-size: 1em;*/
  font-style: italic;
  color: #f00;
}

.btn {
    display: inline-block;
    font-weight: 400;
    color: #23282c;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: 0.375rem 0.75rem;
    font-size: 0.875rem;
    line-height: 1.5;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    color: #fff;
    background-color: #4dbd74;
    border-color: #4dbd74;
    border-radius: 7px;
}

</style>
</head>
  <body style="background-color: #eee;max-width: 97%;min-width: 97%;">
      <header>
        <div style="max-width: 97%;min-width: 97%; padding: 20px;margin:10px 10px 0px 10px;background-color: #fff;">
            <!-- <a href="telkom.co.id"><img height="47" src="<?= base_url(); ?>assets/img/thumb-telkom.png" alt="TELKOM INDONESIA"></a> -->
            <a href="telkom.co.id"><img height="47" src="https://drive.google.com/uc?export=view&id=1HwhmuSYNgf77ZOE04gFAYFG3Ct1aSvcW" alt="TELKOM INDONESIA"></a>
         </div>     
      </header>
      <div class="headcaption" style="max-width: 97%;min-width: 97%; padding: 10px;margin:0px 10px 0px 10px;background-color: #aaa; color:000;">
        Hai <strong><?= $user['NAME']?>, Proses reset password untuk USER ID  <strong><?= $user['ID']?></strong> telah kami terima,
        untuk melakukan reset password silahkan klik tombol dibawah: 
      </div> 

      <div style="max-width: 97%;min-width: 97%; padding: 5px;margin:0px 10px 0px 10px;background-color: #aaa; color:#000;">
          <a class="btn" target="_blank" style="margin:auto;display: block;width: 255px;" href="<?= base_url() ?>user-reset-password/<?= $user['CHANGE_PASSWORD_TOKEN'] ?>">RESET PASSWORD</a>
      </div>

      
  </body>
</html>
