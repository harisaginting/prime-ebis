 <div class="row">
	<div class="col-md-8 offset-md-2">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-5">
						<h4 class="card-title mb-0">About</h4>
						<div class="small text-muted">Prime</div>
					</div>

					<div class="col-sm-7" style="margin-bottom: 15px;">
						<div class="float-right">
						</div>
					</div>
				
					<div class="col-sm-12">
						<table 	id="dataRole" 
								class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">

							</thead>
							<tbody>
								<tr>
									<td style="width: 30%;">Name</td>
									<td style="width: 70%;">Prime (Project Management & Monitoring for Enterprise)</td>
								</tr>

								<tr>
									<td colspan="2"><strong>Web Server</strong></td>
								</tr>
								<tr>
									<td>Operational System</td>
									<td>RedHat Enterprise Server 6.5</td>
								</tr>

								<tr>
									<td>BIOS</td>
									<td>VMware, Inc.</td>
								</tr>

								<tr>
									<td>Proccessor</td>
									<td>Intel(R) Xeon(R) CPU E7- 4870  @ 2.40GHz</td>
								</tr>

								<tr>
									<td>Memory</td>
									<td>4GB (3.924.688 kB)</td>
								</tr>

								<tr>
									<td>Disk</td>
									<td>107.4 GB, [107.374.182.400 bytes]</td>
								</tr>

								<tr>
									<td>IP Address</td>
									<td>10.60.160.111</td>
								</tr>

								<tr>
									<td>PHP Version</td>
									<td>5.6.36</td>
								</tr>

								<tr>
									<td colspan="2"><strong>Database Server</strong></td>
								</tr>

								<tr>
									<td>RDBMS</td>
									<td>Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - 64bit </td>
								</tr>

								<tr>
									<td>IP Address</td>
									<td>10.62.13.150:1525</td>
								</tr>

								<tr>
									<td colspan="2">
										<strong>Application</strong>
									</td>
								</tr>

								<tr>
									<td colspan="2" style="padding: 0px 12px;">
										<strong>Back End</strong></small>
									</td>
								</tr>

								<tr>
									<td>Language</td>
									<td>PHP (5.6.36)</td>
								</tr>

								<tr>
									<td>Framework</td>
									<td>Codeigniter V 3.1.9</td>
								</tr>

								<tr>
									<td>Library</td>
									<td><a href="https://github.com/PHPOffice">Phpoffice</a>  <br><a href="https://github.com/chriskacerguis/codeigniter-restserver">RestfullAPI</a> </td>
								</tr>




							</tbody>
						</table>

					</div>
				</div>


			</div>


		</div>
	</div>
</div>