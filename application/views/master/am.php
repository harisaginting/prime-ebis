<div class="row">
  <!-- Partners -->
  <div class="col-md-10 offset-md-1 col-sm-12">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-md-8 col-sm-11">
            <h4 class="Judul">Account Managers</h4>
          </div>

          <div class="col-md-4 col-sm-1" style="margin-bottom: 15px;">
            <div class="pull-right">
                <button class="btn btn-success btn-md btn-addon btn-prime" id="btn-add-am" type="button" style="margin-bottom: 4px">
                  <i class="fa fa-plus"></i>
                  <span class="pr-2"> New </span>
                </button>
            </div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="tableAm" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 60%">NAME</th>
                  <th style="width: 35%">SEGMEN</th>
                  <th style="width: 5%"></th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>
  
    </div>
  </div>
</div>

<!-- Modal AM-->
 <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-am">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Add <small>Account Manager</small></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
              <div class="modal-body relative">
                <div class="row">
                  <div class="col-12">
                      <form id="form-add-am">
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="NAME" id="name" class="form-control" required />
                          </div>

                          <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="EMAIL" id="email" class="form-control" required />
                          </div>

                          <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="PHONE" id="phone" class="form-control" required />
                          </div>

                          <div class="form-group">
                            <label>Segmen</label>
                            <select name="SEGMEN" id="select_segmen" class="form-control"></select>
                          </div>

                          <div class="w-100 text-center">
                            <button type="button" id="btn-save-add-am"  class="btn btn-sm btn-success" >Save</button>
                          </div>
                      </form>
                  </div>
                </div>
              </div>
          </div>
      </div>
</div>

<!-- Modal Edit AM-->
 <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-edit-am">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Edit <small>Account Manager</small></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
              <div class="modal-body relative">
                <div class="row">
                  <div class="col-12">
                      <form id="form-edit-am">
                          <div class="form-group">
                            <input type="hidden" id="edit-id" name="ID">
                            <label>Name</label>
                            <input type="text" name="NAME" id="edit-name" class="form-control" required />
                          </div>

                          <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="EMAIL" id="edit-email" class="form-control" readOnly required />
                          </div>

                          <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="PHONE" id="edit-phone" class="form-control" required />
                          </div>

                          <div class="form-group">
                            <label>Segmen</label>
                            <select name="SEGMEN" id="edit-select_segmen" class="form-control" required ></select>
                          </div>

                          <div class="w-100 text-center">
                            <button type="button" id="btn-save-edit-am"  class="btn btn-sm btn-success" >Update</button>
                          </div>
                      </form>
                  </div>
                </div>
              </div>
          </div>
      </div>
</div>


<script type="text/javascript">    
  var Page = function () {

      const generateTableAm = () => {    

          const tableAm = $('#tableAm').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/am', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  {
                                    mRender : function(data, type, obj){   
                                              let phone = '';
                                              if(obj.PHONE !== null && obj.phone !== 'null'){
                                                phone = obj.PHONE;
                                              }
                                              return "<div class='w-100' style='font-size:14px;font-weight:900;'>"+obj.NAME+"</div><div style='font-size:12px;'>"+obj.EMAIL+"</div><div style='font-size:12px;'>"+phone+"</div>";   
                                      }            
                                  },
                                  { mData: 'SEGMEN_NAME'},
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0''><button class='btn btn-sm btn-warning col-12 btn-edit-am w-90 mt-1'data-id='"+obj.EMAIL+"'><i class='fa fa-edit'></i></button><button class='col-12 btn btn-sm btn-danger w-90 mt-1 btn-delete-am ' data-id='"+obj.EMAIL+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }         
                                  },
                                 ]            
                  });  
      }; 

      return {
          init: function() { 
            generateTableAm();

            $(document).on('click','#btn-add-am', function(){
              $('#modal-add-am').modal('show');
            });

            $(document).on('click','.btn-edit-am', function(e){
              let email_am = $(e.currentTarget).data("id");
              $.ajax({
                      url: base_url+'master/get_am',
                      type:'POST',
                      data:  {email : email_am} ,
                      dataType : "json",
                      success:function(result){
                          $('#edit-name').val(result.NAME);
                          $('#edit-email').val(result.EMAIL);
                          $('#edit-phone').val(result.PHONE);
                          $('#edit-id').val(result.ID);
                          $("#edit-select_segmen").select2({
                                          placeholder: "Select Segmen",
                                          width: 'resolve',
                                          allowClear : true,
                                          ajax: {
                                              type: 'POST',
                                              url:base_url+"master/get_segmen",
                                              dataType: "json",
                                              data: function (params) {
                                                  return {
                                                      q: params.term,
                                                      page: params.page,
                                                  };
                                              },
                                              processResults: function (data) {
                                                  return {
                                                      results: $.map(data, function(obj) {
                                                          return { id: obj.CODE, text: obj.NAME};
                                                      })
                                                  };
                                              },
                                              
                                          }
                          });
                          if(result.SEGMEN !== null && result.SEGMEN !== 'null'){
                              let newOption = new Option(result.SEGMEN_NAME, result.SEGMEN, false, false);
                              $('#edit-select_segmen').append(newOption).trigger('change');
                          }
                          
                          $('#modal-edit-am').modal('show');             
                      }

              }); 
              
            });

             $(document).on('click','#btn-save-edit-am', function(){
                if($("#form-edit-am").valid()){
                  let data = $('#form-edit-am')[0];
                  var formData = new FormData(data);
                  showLoading();
                  $.ajax({
                              url: base_url+'master/am/edit',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                      hideLoading();
                                       if(result =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }
                                      window.location.reload();
                                      return result;
                              }

                      });  
                }
            });

            $(document).on('click','#btn-save-add-am', function(){
                if($("#form-add-am").valid()){
                  let data = $('#form-add-am')[0];
                  var formData = new FormData(data);
                  showLoading();
                  $.ajax({
                              url: base_url+'master/am/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                      window.location.reload();
                              }

                      });  
                }
            });

            $("#select_segmen").select2({
                            placeholder: "Select Segmen",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_segmen",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
            });

            $(document).on("click",".btn-delete-am",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/am/delete',
                                        type:'POST',
                                        async : true,
                                        data : {EMAIL : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });               
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>