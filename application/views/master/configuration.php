<div class="row">
  <!-- EMAIL CONFIGURATION -->
  <div class="col-md-10 offset-md-1 col-sm-12">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
          <div class="col-12">
            <h4 class="Judul">EMAIL</h4>
          </div>        
          <div class="col-12 ">
            <form id="form-email-configuration">
              <?php foreach ($EMAIL as $key => $value) : ?>
              <div class="form-group" id="form-email-configuration">
                      <label><?= strtoupper($value['VALUE']) ?> <span class="text-danger">*</span> </label>
                      <input type="<?= $value['VALUE'] == 'smtp_port' || $value['VALUE'] == 'smtp_timeout' ? 'number' : 'text'  ?>" name="<?= $value['VALUE']; ?>" id="<?= $value['VALUE']; ?>" class="form-control" value="<?= $value['REMARKS']; ?>" required />
              </div>
              <?php endforeach; ?>
            </form>
          </div>

          <div class="col-sm-12 text-center">
              <button class="btn btn-md btn-success" id="btn-save-email" type="button">SAVE</button>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">    
  var Page = function () {

      return {
          init: function() { 

             $(document).on('click','#btn-save-email', function(){
                if($("#form-email-configuration").valid()){
                  let data = $('#form-email-configuration')[0];
                  var formData = new FormData(data);
                  showLoadingMini();
                  $.ajax({
                              url: base_url+'configuration-email',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                  hideLoadingMini();
                                   if(result =='success'){
                                    bootbox.alert("Success!", function(){ 
                                    window.location.reload();
                                    });
                                  }
                                  window.location.reload();
                                  return result;
                              }

                      });  
                }
            });               
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>