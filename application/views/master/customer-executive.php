<div class="row">
	
  <!-- Customers -->
  <div class="col-12">
		<div class="card">
			<div class="card-body" style="min-height: 600px;">
				<div class="row">
					<div class="col-sm-5">
						<h4 class="Judul">List Customers VVIP</h4>
					</div>

					<div class="col-sm-7" style="margin-bottom: 15px;">
						<div class="pull-right">
                <button id="btn-add-customer" class="btn btn-brand btn-success btn-sm" type="button" style="margin-bottom: 4px">
                    <i class="fa fa-plus"></i>
                    <span> Add Customer VVIP</span>
                  </button>
						</div>
					</div>
				
					<div class="col-sm-12 ">
						<table 	id="tableCustomer" 
								class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
							<thead>
								<tr>
									<th style="width: 15%">NIPNAS</th>
									<th style="width: 45%">NAME</th>
                  <th style="width: 20%">PERIOD</th>
                  <th style="width: 20%">DESCRIPTION</th>
                  <th style="width: 10%"></th>
								</tr>
							</thead>
							 <tbody>
							</tbody>
						</table>

					</div>
				</div>


			</div>
		</div>
	</div>
  
<!-- Modal Customer-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-customer">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Add Customer VVIP</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
            <div class="modal-body relative">
              <div class="row">
                <div class="col-12">
                    <form id="form-add-customer">
                        <div class="form-group">
                          <label>CUSTOMER</label>
                          <select class="form-control customer" name="customer" id="customer" required>
                          </select>
                        </div>

                        <div class="form-group">
                          <label>START PERIOD</label>
                          <input class="form-control date-picker" type="text" name="start" id="start" value="" readonly required>
                        </div>

                        <div class="form-group">
                          <label>END PERIOD</label>
                          <input class="form-control date-picker" type="text" name="end" id="end" value="" readonly required>
                        </div>

                        <div class="form-group">
                          <label>DESCRIPTION</label>
                          <textarea class="form-control" rows="5" name="description" id="description"></textarea>
                        </div>

                        <div class="w-100 text-center">
                          <button type="button" id="btn-save-add-customer"  class="btn btn-sm btn-success" >Save</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

</div>


<script type="text/javascript">    
  var Page = function () {
      const generateTableCustomer = () => {    

          const tableCustomer = $('#tableCustomer').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/master/customer-executive', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  { mData: 'CODE'},
                                  { mData: 'NAME'},
                                  {
                                    mRender : function(data, type, obj){  
                                            let start = "-";
                                            let end = "-"; 
                                            if (obj.START_DATE != null && obj.START_DATE != "") {
                                              start = obj.START_DATE2;
                                            }
                                            if (obj.END_DATE != null && obj.END_DATE != "") {
                                              end = obj.END_DATE2;
                                            }
                                            let period = "<div class='w-100' style='font-size:0.8em;font-weight:700;text-align:center;border-bottom:1px solid black;margin-bottom:5px;'><span class='pull-left'>"+start+"</span>-<span class='pull-right'>"+end+"</span></div>";
                                            return period;
                                      }            
                                  },
                                  { mData: 'DESCRIPTION'},
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0' style='min-width:70px;'><button class='btn btn-sm btn-disabled col-6'><i class='fa fa-edit'></i></button><button class=' btn btn-sm btn-danger col-6 btn-delete-customer'  data-id='"+obj.ID+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }            
                                  },
                                 ]            
                  });  
      }; 

      return {
          init: function() { 
            generateTableCustomer();
            $(document).on('click','#btn-add-customer', function(){
              $('#modal-add-customer').modal('show');
            });

            $(document).on('click','#btn-save-add-customer', function(){
                if($("#form-add-customer").valid()){
                  let data = $('#form-add-customer')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/customer-executive/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                       if(result.trim() =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }else{
                                        bootbox.alert(result, function(){});
                                        }
                                      return result;
                              }

                      });  
                }
            });

            $(".customer").select2({
                            placeholder: "Select Customer",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_customer",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME+" - "+obj.CODE};
                                        })
                                    };
                                },
                                
                            }
                });


                
            $(document).on("click",".btn-delete-customer",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  console.log(id);
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/customer-executive/delete',
                                        type:'POST',
                                        async : true,
                                        data : {ID : id},
                                        dataType : "text",
                                        success:function(result){
                                          console.log('result',result);
                                         if(result.trim() == 'success'){
                                            bootbox.alert("Success!", function(){ 
                                            window.location.reload();
                                            });
                                          }else{
                                            bootbox.alert("Failed! "+ result, function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>