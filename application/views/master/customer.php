<div class="row">
	
  <!-- Customers -->
  <div class="col-sm-12 col-md-10 offset-md-1">
		<div class="card">
			<div class="card-body" style="min-height: 600px;">
				<div class="row">
					<div class="col-sm-5">
						<h4 class="Judul">Customers</h4>
					</div>

					<div class="col-sm-7" style="margin-bottom: 15px;">
						<div class="pull-right">
                <button id="btn-add-customer" class="btn btn-md btn-addon btn-prime btn-success" type="button" style="margin-bottom: 4px">
                    <i class="fa fa-plus"></i>
                    <span class="pr-2"> New</span>
                  </button>
						</div>
					</div>
				
					<div class="col-sm-12 ">
						<table 	id="tableCustomer" 
								class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
							<thead>
								<tr>
									<th style="width: 20%">NIPNAS</th>
									<th style="width: 75%">NAME</th>
                  <th style="width: 5%"></th>
								</tr>
							</thead>
							 <tbody>
							</tbody>
						</table>

					</div>
				</div>


			</div>
		</div>
	</div>
  


  <!-- Modal Customer-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-customer">
    <div class="modal-dialog modal-primary  modal-md">
      <div class="modal-content">
        <div class="modal-header">
                <h4 class="modal-title" >Add Customer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
          </div>
          <div class="modal-body relative">
            <div class="row">
              <div class="col-12">
                  <form id="form-add-customer">
                      <div class="form-group">
                        <label>NIPNAS</label>
                        <input type="text" name="CODE" id="code" class="form-control" required />
                      </div>

                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="NAME" id="name" class="form-control" required />
                      </div>

                      <div class="w-100 text-center">
                        <button type="button" id="btn-save-add-customer"  class="btn btn-sm btn-success" >Save</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

 <!-- Modal Edit Customer-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-edit-customer">
    <div class="modal-dialog modal-primary  modal-md">
      <div class="modal-content">
        <div class="modal-header">
                <h4 class="modal-title" >Edit <small>Customer</small></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
          </div>
          <div class="modal-body relative">
            <div class="row">
              <div class="col-12">
                  <form id="form-edit-customer">
                      <div class="form-group">
                        <label>NIPNAS</label>
                        <input type="hidden" name="ID" id="edit-id" class="form-control"/>
                        <input type="text" name="CODE" id="edit-code" class="form-control" required  readonly />
                      </div>

                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="NAME" id="edit-name" class="form-control" required />
                      </div>

                      <div class="w-100 text-center">
                        <button type="button" id="btn-save-edit-customer"  class="btn btn-sm btn-success" >Update</button>
                      </div>
                  </form>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>


<script type="text/javascript">    
  var Page = function () {
      const generateTableCustomer = () => {    

          const tableCustomer = $('#tableCustomer').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/customer', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  { mData: 'CODE'},
                                  { mData: 'NAME'},
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0' ><button class='btn btn-sm btn-warning col-12 btn-edit-customer' data-id='"+obj.CODE+"'><i class='fa fa-edit'></i></button><button class='btn btn-sm btn-danger col-12 btn-delete-customer mt-1'  data-id='"+obj.CODE+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }            
                                  },
                                 ]            
                  });  
      }; 

  
      return {
          init: function() { 
            generateTableCustomer();
            $(document).on('click','#btn-add-customer', function(){
              $('#modal-add-customer').modal('show');
            });


            $(document).on('click','.btn-edit-customer', function(e){
              let code = $(e.currentTarget).data("id");
              $.ajax({
                      url: base_url+'master/get_customer/'+code,
                      type:'POST',
                      dataType : "json",
                      success:function(result){
                          $('#edit-name').val(result.NAME);
                          $('#edit-code').val(result.CODE);
                          $('#edit-id').val(result.ID);                          
                          $('#modal-edit-customer').modal('show');             
                      }

              }); 
            });

            $(document).on('click','#btn-save-add-customer', function(){
                if($("#form-add-customer").valid()){
                  // let data = $("#form-add-customer").serialize();
                  let data = $('#form-add-customer')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/customer/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                       if(result =='success'){
                                        window.location.reload();
                                      }
                                      return result;
                              }

                      });  
                }
            });

            $(document).on('click','#btn-save-edit-customer', function(){
                if($("#form-edit-customer").valid()){
                  let data = $('#form-edit-customer')[0];
                  var formData = new FormData(data);
                  showLoading();
                  $.ajax({
                              url: base_url+'master/customer/edit',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                      hideLoading();
                                       if(result =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }
                                      window.location.reload();
                                      return result;
                              }

                      });  
                }
            });
            $(document).on("click",".btn-delete-customer",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  console.log(id);
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/customer/delete',
                                        type:'POST',
                                        async : true,
                                        data : {CODE : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>