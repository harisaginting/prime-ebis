<div class="row">
  
  <!-- Customers -->
  <div class="col-sm-12 col-md-12">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-sm-12">
            <h4 class="card-title mb-0">LOG</h4>
            <div class="small text-muted">Prime</div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="table-pm" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 7%">TIME</th>
                  <th style="width: 25%">USER</th>
                  <th style="width: 10%">MODULE</th>
                  <th style="width: 55%">ACTION</th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>

    </div>
  </div>


<script type="text/javascript">    
  var Page = function () {
      const generateTableLog = () => {    

          const tablePm = $('#table-pm').DataTable({
                      processing: true,
                      serverSide: true,
                      order : [[ 0, "desc" ]],
                      ajax: { 
                          'url'  :base_url+'datatable/log', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  {
                                    mRender : function(data, type, obj){   
                                            let d =  "<div class='w-100'><strong style='font-size:12px;'>"+obj.DATE_CREATED2+"</strong></div>";   
                                            return d;  
                                      }
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            let name =  "<div class='w-100'><strong style='font-size:12px;'><span class='text-muted'>"+obj.DIVISION+"</span> "+obj.NAME+"</strong></div><div class='pl-1'>"+obj.ID_USER+"</div><div class='pl-1'>"+obj.CLIENT_IP+"<div>";   
                                            return name;  
                                      }
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            return "<strong style='font-size:12px;'>"+obj.MODULE+"</strong>";     
                                      }       
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            let desc = obj.DESCRIPTION;
                                            try {
                                                let desc_ = JSON.parse(obj.DESCRIPTION);
                                                desc      = JSON.stringify(desc_, null,"\n");
                                            }
                                            catch(err) {
                                                console.log("not object");
                                            }

                                            return "<strong style='font-size:12px;'>"+obj.ACTION+"</strong><p style='font-size:11px;' class='text-muted'>"+desc+"</p>";   
                                      }       
                                  },
                                 ]            
                  });  
      }; 



      return {
          init: function() { 
            generateTableLog();              
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>