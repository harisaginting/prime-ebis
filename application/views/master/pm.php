<div class="row">
  
  <!-- Customers -->
  <div class="col-sm-12 col-md-10 offset-md-1">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-sm-5">
            <h4 class="Judul">Project Managers</h4>
          </div>

          <div class="col-sm-7" style="margin-bottom: 15px;">
            <div class="pull-right">
                 <button id="btn-add-pm" class="btn btn-success btn-md btn-addon btn-prime" type="button" style="margin-bottom: 4px">
                  <i class="fa fa-plus"></i>
                  <span class="pr-2"> New </span>
                </button>
            </div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="table-pm" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 95%">NAME</th>
                  <th style="width: 5%"></th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>

    </div>
  </div>

<!-- Modal PM-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-pm">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" >Add <small>Project Manager</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-add-pm">
                    <div class="form-group">
                      <label>Name <span class="text-danger">*</span> </label>
                      <input type="text" name="NAME" id="name" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Email <span class="text-danger">*</span></label>
                      <input type="email" name="EMAIL" id="email" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Phone <span class="text-danger">*</span></label>
                      <input type="text" name="PHONE" id="phone" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Division <span class="text-danger">*</span></label>
                      <select name="DIVISION" id="division" class="form-control" required>
                        <?php if($user['division'] == 'EBIS') :?>
                          <option value="DBS">DBS</option>
                          <option value="DES">DES</option>
                          <option value="DGS">DGS</option>
                        <?php else :  ?>
                          <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
                        <?php endif;  ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Regional <span class="text-danger">*</span></label>
                      <select name="REGIONAL" id="regional" class="form-control" required>
                        <?php foreach ($regional as $key => $value): ?>
                          <option value="<?= $value['ID'] ?>"><?= $value['VALUE'] ?></option> 
                        <?php endforeach ?>
                      </select>
                    </div>

                    <div class="w-100 text-center">
                      <button type="button" id="btn-save-add-pm"  class="btn btn-sm btn-success" >Save</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<!-- Modal Edit PM-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-edit-pm">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" >Edit <small>Project Manager</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-edit-pm">
                    <div class="form-group">
                      <label>Name <span class="text-danger">*</span> </label>
                      <input type="hidden" name="ID" id="edit-id" required />
                      <input type="text" name="NAME" id="edit-name" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Email <span class="text-danger">*</span></label>
                      <input type="email" name="EMAIL" id="edit-email" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Phone <span class="text-danger">*</span></label>
                      <input type="text" name="PHONE" id="edit-phone" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Division <span class="text-danger">*</span></label>
                      <select name="DIVISION" id="edit-division" class="form-control" required>
                        <?php if($user['division'] == 'EBIS') :?>
                          <option value="DBS">DBS</option>
                          <option value="DES">DES</option>
                          <option value="DGS">DGS</option>
                        <?php else :  ?>
                          <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
                        <?php endif;  ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Regional <span class="text-danger">*</span></label>
                      <select name="REGIONAL" id="edit-regional" class="form-control" required>
                        <?php foreach ($regional as $key => $value): ?>
                          <option value="<?= $value['ID'] ?>"><?= $value['VALUE'] ?></option> 
                        <?php endforeach ?>
                      </select>
                    </div>

                    <div class="w-100 text-center">
                      <button type="button" id="btn-save-edit-pm"  class="btn btn-sm btn-success" >Update</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<script type="text/javascript">    
  var Page = function () {
      const generateTablePm = () => {    

          const tablePm = $('#table-pm').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/pm', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  {
                                    mRender : function(data, type, obj){   
                                            let name =  "<div class='w-100'><strong style='font-size:12px;'>"+obj.NAME+"</strong></div><div class='pl-1'>"+obj.EMAIL+"</div><div class='pl-1'>"+obj.DIVISION+"<div>";   
                                            return name;  
                                      }
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0'><button class='col-12 btn btn-sm btn-warning w-90 btn-edit-pm' data-id='"+obj.ID+"'><i class='fa fa-edit'></i></button><button class='col-12 btn btn-sm btn-danger w-90 mt-1 btn-delete-pm' data-id='"+obj.ID+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }       
                                  },
                                 ]            
                  });  
      }; 



      return {
          init: function() { 
            generateTablePm();
            $(document).on('click','#btn-add-pm', function(){
              $('#modal-add-pm').modal('show');
            });

            $(document).on('click','.btn-edit-pm', function(e){
              let id = $(e.currentTarget).data("id");
              $.ajax({
                      url: base_url+'master/get_pm',
                      data : {id : id},
                      type:'POST',
                      dataType : "json",
                      success:function(result){
                          console.log(result);
                          $('#edit-name').val(result.NAME);
                          $('#edit-id').val(result.ID);                            
                          $('#edit-email').val(result.EMAIL);                            
                          $('#edit-phone').val(result.PHONE);                            
                          $('#edit-division').val(result.DIVISION);                            
                          $('#edit-regional').val(result.REGIONAL);                            
                          $('#modal-edit-pm').modal('show');             
                      }

              }); 
            });

            $(document).on('click','#btn-save-edit-pm', function(){
                if($("#form-edit-pm").valid()){
                  let data = $('#form-edit-pm')[0];
                  var formData = new FormData(data);
                  showLoading();
                  $.ajax({
                              url: base_url+'master/pm/edit',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                      window.location.reload();
                                      return result;
                              }

                      });  
                }
            });

            $(document).on('click','#btn-save-add-pm', function(){
                if($("#form-add-pm").valid()){
                  let data = $('#form-add-pm')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/pm/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                window.location.reload();
                              }

                      });  
                }
            });

            $(document).on("click",".btn-delete-pm",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/pm/delete',
                                        type:'POST',
                                        async : true,
                                        data : {CODE : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
                
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>