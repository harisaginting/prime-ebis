<div class="row">
	
  <!-- PROJECT -->
  <div class="col-8">
		<div class="card">
			<div class="card-body" style="min-height: 600px;">
				<div class="row">
					<div class="col-sm-12">
						<h4 class="Judul">PROJECT</h4>
					</div>
				
					<div class="col-sm-12 ">
						<table 	id="tableCustomer" 
								class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
							<thead>
								<tr>
									<th style="width: 20%">NIPNAS</th>
									<th style="width: 70%">NAME</th>
                  <th style="width: 10%"></th>
								</tr>
							</thead>
							 <tbody>
							</tbody>
						</table>

					</div>
				</div>


			</div>
		</div>
	</div>
  
  <!-- Partners -->
  <div class="col-12">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-sm-5">
            <h4 class="Judul">List Partners</h4>
          </div>

          <div class="col-sm-7" style="margin-bottom: 15px;">
            <div class="pull-right">
              <button class="btn btn-brand btn-success btn-sm" id="btn-add-partner" type="button" style="margin-bottom: 4px">
                <i class="fa fa-plus"></i>
                <span> Add Partner</span>
              </button>
            </div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="tablePartner" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 10%">CODE</th>
                  <th style="width: 70%">NAME</th>
                  <th style="width: 10%">CFU</th>
                  <th style="width: 10%"></th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>
    </div>
  </div>



  <!-- Modal Customer-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-customer">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Add Customer</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
            <div class="modal-body relative">
              <div class="row">
                <div class="col-12">
                    <form id="form-add-customer">
                        <div class="form-group">
                          <label>NIPNAS</label>
                          <input type="text" name="CODE" id="code" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" name="NAME" id="name" class="form-control" required />
                        </div>

                        <div class="w-100 text-center">
                          <button type="button" id="btn-save-add-customer"  class="btn-sm btn-success" >Save</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>


<!-- Modal Partner-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-partner">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Add Partner</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
            <div class="modal-body relative">
              <div class="row">
                <div class="col-12">
                    <form id="form-add-partner">
                        <div class="form-group">
                          <label>CODE</label>
                          <input type="text" name="CODE" id="code" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" name="NAME" id="name" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Telkom Group?</label>
                          <select name="CFU" id="CFU" class="form-control" required />
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                          </select>
                        </div>

                        <div class="w-100 text-center">
                          <button type="button" id="btn-save-add-partner"  class="btnbtn -sm btn-success" >Save</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

</div>


<script type="text/javascript">    
  var Page = function () {
      const generateTableCustomer = () => {    

          const tableCustomer = $('#tableCustomer').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/customer', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  { mData: 'CODE'},
                                  { mData: 'NAME'},
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0' style='min-width:70px;'><button class='btn btn-sm btn-disabled col-6'><i class='fa fa-edit'></i></button><button class=' btn btn-sm btn-danger col-6 btn-delete-customer'  data-id='"+obj.CODE+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }            
                                  },
                                 ]            
                  });  
      }; 

      const generateTablePartner = () => {    

          const tableCustomer = $('#tablePartner').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/partner', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  { mData: 'CODE'},
                                  { mData: 'NAME'},
                                  {
                                    mRender : function(data, type, obj){   
                                             if(obj.CFU === 1 || obj.CFU === '1'){  
                                              return "<div class='w-100 text-center'><i class='fa fa-check'></i></div>";
                                              }else{
                                                return "<div class='w-100 text-center'><i class='fa fa-cross'></i></div>";
                                              }   
                                      }            
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0' style='min-width:70px;'><button class='btn btn-sm btn-disabled col-6'><i class='fa fa-edit'></i></button><button class='btn btn-sm btn-danger col-6 mt-1 btn-delete-partner' data-id='"+obj.CODE+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }           
                                  },
                                 ]            
                  });  
      }; 
      return {
          init: function() { 
            generateTableCustomer();
            generateTablePartner();
            $(document).on('click','#btn-add-customer', function(){
              $('#modal-add-customer').modal('show');
            });

            $(document).on('click','#btn-add-partner', function(){
              $('#modal-add-partner').modal('show');
            });

            $(document).on('click','#btn-save-add-customer', function(){
                if($("#form-add-customer").valid()){
                  // let data = $("#form-add-customer").serialize();
                  let data = $('#form-add-customer')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/customer/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                       if(result =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }else{
                                        bootbox.alert($result, function(){});
                                        }
                                      return result;
                              }

                      });  
                }
            });


            $(document).on('click','#btn-save-add-partner', function(){
                if($("#form-add-partner").valid()){
                  let data = $('#form-add-partner')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/partner/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                       if(result =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }else{
                                        bootbox.alert($result, function(){});
                                        }
                                      return result;
                              }

                      });  
                }
            });
                
            $(document).on("click",".btn-delete-customer",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  console.log(id);
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/customer/delete',
                                        type:'POST',
                                        async : true,
                                        data : {CODE : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });

            $(document).on("click",".btn-delete-partner",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/partner/delete',
                                        type:'POST',
                                        async : true,
                                        data : {CODE : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>