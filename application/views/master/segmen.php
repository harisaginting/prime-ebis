<div class="row">
  
  <!-- Customers -->
  <div class="col-sm-12 col-md-10 offset-md-1">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-sm-5">
            <h4 class="Judul">Segmen</h4>
          </div>

          <div class="col-sm-7" style="margin-bottom: 15px;">
            <div class="pull-right">
                 <button id="btn-add-segmen" class="btn btn-success btn-md btn-addon btn-prime" type="button" style="margin-bottom: 4px">
                  <i class="fa fa-plus"></i>
                  <span class="pr-2"> New </span>
                </button>
            </div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="tableSegmen" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 95%">NAME</th>
                  <th style="width: 5%"></th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>

    </div>
  </div>

<!-- Modal Segmen-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-segmen">
  <div class="modal-dialog modal-primary  modal-md">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" >Add <small>Segmen</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-add-segmen">
                    <div class="form-group">
                      <label>CODE</label>
                      <input type="text" name="CODE" id="code" class="form-control" required />
                    </div>

                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" name="NAME" id="name" class="form-control" required />
                    </div>

                    <div class="w-100 text-center">
                      <button type="button" id="btn-save-add-segmen"  class="btn btn-sm btn-success" >Save</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<!-- Modal Edit Segmen-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-edit-segmen">
  <div class="modal-dialog modal-primary  modal-md">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" >Edit <small>Segmen</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-edit-segmen">
                    <div class="form-group">
                      <label>CODE</label>
                      <input type="hidden" name="ID" id="edit-id" />
                      <input type="text" name="CODE" id="edit-code" class="form-control" required readonly />
                    </div>

                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" name="NAME" id="edit-name" class="form-control" required />
                    </div>

                    <div class="w-100 text-center">
                      <button type="button" id="btn-save-edit-segmen"  class="btn btn-sm btn-success" >Update</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">    
  var Page = function () {
      const generateTableSegmen = () => {    

          const tableSegmen = $('#tableSegmen').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/segmen', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  {
                                    mRender : function(data, type, obj){   
                                            let name =  "<div class='w-100'><strong style='font-size:12px;'>"+obj.CODE+"</strong></div><div>"+obj.NAME+"</div>";   
                                            return name;  
                                      }
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0'><button class='col-12 btn btn-sm btn-warning w-90 btn-edit-segmen' data-id='"+obj.CODE+"'><i class='fa fa-edit'></i></button><button class='col-12 btn btn-sm btn-danger w-90 mt-1 btn-delete-segmen' data-id='"+obj.CODE+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }       
                                  },
                                 ]            
                  });  
      }; 



      return {
          init: function() { 
            generateTableSegmen();
            $(document).on('click','#btn-add-segmen', function(){
              $('#modal-add-segmen').modal('show');
            });

            $(document).on('click','.btn-edit-segmen', function(e){
              let code = $(e.currentTarget).data("id");
              $.ajax({
                      url: base_url+'master/get_segmen/'+code,
                      type:'POST',
                      dataType : "json",
                      success:function(result){
                          $('#edit-name').val(result.NAME);
                          $('#edit-code').val(result.CODE);
                          $('#edit-id').val(result.ID);                            
                          $('#modal-edit-segmen').modal('show');             
                      }

              }); 
            });

            $(document).on('click','#btn-save-edit-segmen', function(){
                if($("#form-edit-segmen").valid()){
                  let data = $('#form-edit-segmen')[0];
                  var formData = new FormData(data);
                  showLoading();
                  $.ajax({
                              url: base_url+'master/segmen/edit',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                      window.location.reload();
                                      return result;
                              }

                      });  
                }
            });

            $(document).on('click','#btn-save-add-segmen', function(){
                if($("#form-add-segmen").valid()){
                  let data = $('#form-add-segmen')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/segmen/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                window.location.reload();
                              }

                      });  
                }
            });

            $(document).on("click",".btn-delete-segmen",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/segmen/delete',
                                        type:'POST',
                                        async : true,
                                        data : {CODE : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
                
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>