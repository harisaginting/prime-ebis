<div class="row">
  
  <!-- Customers -->
  <div class="col-12">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-sm-5">
            <h4 class="Judul">List Segmen</h4>
          </div>

          <div class="col-sm-7" style="margin-bottom: 15px;">
            <div class="pull-right">
                 <button id="btn-add-segmen" class="btn btn-brand btn-success btn-sm" type="button" style="margin-bottom: 4px">
                  <i class="fa fa-plus"></i>
                  <span> Add Segmen</span>
                </button>
            </div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="tableSegmen" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 90%">NAME</th>
                  <th style="width: 10%"></th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>

    </div>
  </div>
  
  <!-- Partners -->
  <div class="col-12">
    <div class="card">
      <div class="card-body" style="min-height: 600px;">
        <div class="row">
          <div class="col-md-8 col-sm-11">
            <h4 class="Judul">List Account Manager</h4>
          </div>

          <div class="col-md-4 col-sm-1" style="margin-bottom: 15px;">
            <div class="pull-right">
                <button class="btn btn-brand btn-success btn-sm" id="btn-add-am" type="button" style="margin-bottom: 4px">
                  <i class="fa fa-plus"></i>
                  <span> Add AM</span>
                </button>
            </div>
          </div>
        
          <div class="col-sm-12 ">
            <table  id="tableAm" 
                class="table table-master table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 50%">NAME</th>
                  <th style="width: 40%">SEGMEN</th>
                  <th style="width: 10%"></th>
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>

          </div>
        </div>


      </div>
  
    </div>
  </div>



  <!-- Modal Customer-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-segmen">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Add Segmen</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
            <div class="modal-body relative">
              <div class="row">
                <div class="col-12">
                    <form id="form-add-segmen">
                        <div class="form-group">
                          <label>CODE</label>
                          <input type="text" name="CODE" id="code" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" name="NAME" id="name" class="form-control" required />
                        </div>

                        <div class="w-100 text-center">
                          <button type="button" id="btn-save-add-segmen"  class="btn-sm btn-success" >Save</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>


<!-- Modal AM-->
 <div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-add-am">
      <div class="modal-dialog modal-primary  modal-md">
        <div class="modal-content">
          <div class="modal-header">
                  <h4 class="modal-title" >Add Account Manager</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
            </div>
            <div class="modal-body relative">
              <div class="row">
                <div class="col-12">
                    <form id="form-add-am">
                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" name="NAME" id="name" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" name="EMAIL" id="email" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Phone</label>
                          <input type="text" name="PHONE" id="phone" class="form-control" required />
                        </div>

                        <div class="form-group">
                          <label>Segmen</label>
                          <select name="SEGMEN" id="select_segmen" class="form-control" required /></select>
                        </div>

                        <div class="w-100 text-center">
                          <button type="button" id="btn-save-add-am"  class="btn-sm btn-success" >Save</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

</div>


<script type="text/javascript">    
  var Page = function () {
      const generateTableSegmen = () => {    

          const tableSegmen = $('#tableSegmen').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/segmen', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  {
                                    mRender : function(data, type, obj){   
                                            let name =  "<div class='w-100'><strong style='font-size:12px;'>"+obj.CODE+"</strong></div><div>"+obj.NAME+"</div>";   
                                            return name;  
                                      }
                                  },
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0' style='min-width:70px;'><button class='col-6 btn btn-sm btn-disabled w-90'><i class='fa fa-edit'></i></button><button class='col-6 btn btn-sm btn-danger w-90 mt-1 btn-delete-segmen' data-id='"+obj.CODE+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }       
                                  },
                                 ]            
                  });  
      }; 

      const generateTableAm = () => {    

          const tableAm = $('#tableAm').DataTable({
                      processing: true,
                      serverSide: true,
                      ajax: { 
                          'url'  :base_url+'datatable/am', 
                          'type' :'POST',
                      },
                      aoColumns: [
                                  {
                                    mRender : function(data, type, obj){   
                                              return "<div class='w-100' style='font-size:14px;font-weight:900;'>"+obj.NAME+"</div><div style='font-size:12px;'>"+obj.EMAIL+"</div><div style='font-size:12px;'>"+obj.PHONE+"</div>";   
                                      }            
                                  },
                                  { mData: 'SEGMEN'},
                                  {
                                    mRender : function(data, type, obj){   
                                            let button =  "<div class='row mr-0 ml-0' style='min-width:70px;'><button class='btn btn-sm btn-disabled col-6'><i class='fa fa-edit'></i></button><button class='col-6 btn btn-sm btn-danger w-90 mt-1 btn-delete-am' data-id='"+obj.EMAIL+"' data-name='"+obj.NAME+"'><i class='fa fa-trash'></i></button></div>";   
                                            return button;  
                                      }         
                                  },
                                 ]            
                  });  
      }; 

      return {
          init: function() { 
            generateTableSegmen();
            generateTableAm();
            $(document).on('click','#btn-add-segmen', function(){
              $('#modal-add-segmen').modal('show');
            });

            $(document).on('click','#btn-add-am', function(){
              $('#modal-add-am').modal('show');
            });

            $(document).on('click','#btn-save-add-segmen', function(){
                if($("#form-add-segmen").valid()){
                  let data = $('#form-add-segmen')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/segmen/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                       if(result =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }else{
                                        bootbox.alert($result, function(){});
                                        }
                                      return result;
                              }

                      });  
                }
            });


            $(document).on('click','#btn-save-add-am', function(){
                if($("#form-add-am").valid()){
                  let data = $('#form-add-am')[0];
                  var formData = new FormData(data);
                  $.ajax({
                              url: base_url+'master/am/add',
                              type:'POST',
                              data:  formData ,
                              async : true,
                              dataType : "text",
                              contentType:false,
                              processData:false,
                              success:function(result){
                                       if(result =='success'){
                                        bootbox.alert("Success!", function(){ 
                                        window.location.reload();
                                        });
                                      }else{
                                        bootbox.alert($result, function(){});
                                        }
                                      return result;
                              }

                      });  
                }
            });

            $("#select_segmen").select2({
                            placeholder: "Select Account Manager",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_segmen",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
            });

            $(document).on("click",".btn-delete-am",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/am/delete',
                                        type:'POST',
                                        async : true,
                                        data : {EMAIL : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });


            $(document).on("click",".btn-delete-segmen",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id    = $(e.currentTarget).data('id');
                  let name  = $(e.currentTarget).data('name');
                  bootbox.confirm({
                        message: "Delete "+name+"?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'master/segmen/delete',
                                        type:'POST',
                                        async : true,
                                        data : {CODE : id},
                                        dataType : "text",
                                        success:function(result){
                                         if(result=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
                
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>