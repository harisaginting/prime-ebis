<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
            <h4 class="card-title mb-0">Project <small><strong>BAST</strong></small></h4>
            <div class="small text-muted"><?= $user['division'] ?></div>
          </div>
          <div class="col-sm-12" id="table_content">
            <table id="dataBast" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th  colspan="2" class="pt-1 pb-1" style="border-bottom: 0px;">PROJECT</th> 
                  <th  colspan="4" class="pt-1 pb-1" style="border-bottom: 0px;">BAST</th> 
                </tr>
                <tr>
                  <th style="width: 45 %">NAME</th>
                  <th style="width: 10%;text-align: center;">VALUE<small>(IDR)</small> </th> 
                  <th style="width: 30%">NO.</th> 
                  <th style="width: 15%;text-align: center; ">VALUE<small>(IDR)</small></th> 
                  <th style="width: 10%">PROGRESS</th> 
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>

<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataBast').DataTable({
                  initComplete: function(settings, json) {
                                $('.rupiah').priceFormat({
                                    prefix: '',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    centsLimit: 0
                                });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'asc'],
                    ajax: { 
                        'url'  :base_url+'datatable/monitoring/bast-project', 
                        'type' :'POST',
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){   
                                    let customer  = "<span class='w-100 mt-1' style='font-size:0.95em;'>"+ obj.CUSTOMER_NAME+"</span>";
                                    let partner   = "<strong class='w-100 text-primary' style='font-size:0.95em;'>"+obj.PARTNER_NAME+"</strong>";
                                    let p8        = "<strong class='w-100 text-primary pl-2' style='font-size:0.95em;'>"+obj.P8+"</strong>";
                                    let period    = "<strong class='w-100' style='font-size:0.95em;'>"+obj.START_DATE2 
                                                    + "  -  " + obj.END_DATE2+"</strong>";
                                    return "<strong class='text-warning'>"+obj.NAME+"</strong><br>"+partner+"<br>"+p8+"<br>"+customer+"<br>"+period; 
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){
                                  let value = "<div class='w-100 text-right'><span class='rupiah'>"+obj.VALUE+"</span></div>"
                                  return value;
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){
                                let date    = "<span style='font-size:0.8em;font-family:Roboto'>"+obj.BAST_DATE2+"</span>";

                                let no_bast  = "";
                                if(obj.NO_BAST !== null && obj.NO_BAST !== ""){
                                  no_bast = no_bast = "<span style='font-weight:700;font-size:0.7em;font-family:Roboto'>"+obj.NO_BAST+"</span><br>"+date;  
                                  return no_bast;
                                }else{
                                  return "BAST ON PROGRESS"
                                }
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){
                                  let value = "<div class='w-100 text-right'><span class='rupiah'>"+obj.BAST_VALUE+"</span></div>"
                                  return value;
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){
                                  let approver = obj.APPROVER;
                                  let status   = obj.BAST_STATUS;
                                  if(status !== 'RECEIVED' && status !== 'APPROVED' && status !== 'DONE' && status !== 'TAKE OUT'){
                                    status = 'Check by <strong>'+ approver+"</strong>";
                                  }

                                  if(obj.REVISION == '1'){
                                    status = 'Revision by <strong>'+ approver+"</strong>"; 
                                  }
                                  return status;
                            }            
                                    
                        },


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'bast/view/'+aData['ID']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            tableInit();
          }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>