<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
            <h4 class="card-title mb-0">Project <small><strong>issue & action plan</strong></small></h4>
            <div class="small text-muted"><?= $user['division'] ?></div>
          </div>
          <div class="col-sm-12" id="table_content">
            <table id="dataBast" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
              <thead>
                <tr>
                  <th style="width: 40%">PROJECT</th>
                  <th style="width: 30%">ISSUE</th> 
                  <th style="width: 30%">ACTION PLAN</th> 
                </tr>
              </thead>
               <tbody>
              </tbody>
            </table>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>

<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataBast').DataTable({
                  initComplete: function(settings, json) {
                                $('.rupiah').priceFormat({
                                    prefix: '',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    centsLimit: 0
                                });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'asc'],
                    ajax: { 
                        'url'  :base_url+'datatable/monitoring/issue-actionplan', 
                        'type' :'POST',
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){   
                                    let customer  =  "<div class='w-100 mt-2' style='font-size:0.95em;'>"+ obj.CUSTOMER_NAME+"</div>";
                                    let period    =  "<strong class='w-100' style='font-size:0.95em;'>"+obj.START_DATE2 
                                                    + "  -  " + obj.END_DATE2+"</strong>";
                                    return "<strong>"+obj.NAME+"</strong><br>"+customer+period; 
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){
                                  let priority = "<div class='w-100 text-bold pl-2'><small style='font-size:0.85em;font-weight:700;'>"
                                                + obj.ISSUE_DATE + " </small><strong class='text-danger'>"+obj.IMPACT+" Risk </strong><div>"; 
                                  let issue    = "<div class='w-100' style='font-weight:700;'>"+obj.ISSUE_NAME+"<div>"; 
                                  return issue+priority;
                            }            
                                    
                        },  
                        { 
                            'mRender': function(data, type, obj){
                                  let pic_dept  = obj.PIC2;
                                  if(pic_dept== 'null' || pic_dept == null || pic_dept == ''){
                                      pic_dept = obj.PIC;
                                  }

                                  let action  = "<div class='w-100'><strong>"+obj.ACTION_NAME+"</strong></div>";   
                                  let pic     = "<div class='row mt-2'><div class='col-3'>PIC</div><div class='col-9'><strong>"
                                               +pic_dept+" - "+obj.PIC_NAME+"</strong></div></div>";
                                  let period  = "<div class='row'><div class='col-3'>Periode</div><div class='col-9'><strong>"
                                               +obj.ACTION_START+" - "+obj.ACTION_END+"</strong></div></div>";   
                                  return action+pic+period;
                            }            
                                    
                        },  


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'bast/view/'+aData['ID']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            tableInit();
          }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>