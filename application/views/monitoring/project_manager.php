<style type="text/css">
	.list-group-item:hover {
    	background: #6b6868 !important;
    	color: #129841;
	}

	.list-group-item{
		cursor: pointer;
	}

	.container-pm.active{
		background: #129841 !important;
		color: #fff;
	}
</style>

<div class="row"> 
	<!-- PROJECT MANAGER -->
	<div class="col-4">
		<div class="card">
			<div class="card-body" style="max-height: 620px; overflow-y: scroll;overflow-x: hidden;">
				<div class="w-100">
					<span class="fw-800 mb-2">Project Manager</span>
					<ul class="list-group">
						<?php foreach ($pm as $key => $value) : ?>
					  		<li class="list-group-item container-pm" data-id="<?= $value['ID'] ?>">
					  			<div class="row"> 
					  				<div class="col-4">
					  				 <?php if(!empty($value['AVATAR'])) :  ?>
					  					<img class="img-avatar" src="<?= base_url(); ?>assets/avatar/<?= $value['AVATAR'] ?>" alt="<?= $value['NAME'] ?>">
					  				 <?php else :  ?>
					  				 	<img class="img-avatar" src="<?= base_url(); ?>assets/img/avatars/default.png" alt="<?= $value['NAME'] ?>">
					  				 <?php endif;  ?>
					  				</div>
					  				<div class="col-8">
					  					<div><h6 class="mb-1" style="font-weight: 700;font-family: Montserrat;"><?= $value['NAME'] ?></h6></div>
					  					<div><span class="text-dark"><?= $value['EMAIL'] ?></span></div>
					  					<div><span class="text-dark"><?= $value['PHONE'] ?></span></div>
					  					<div><span class="text-dark"><?= $value['REGIONAL_NAME'] ?></span></div>
					  				</div>
					  			</div>
					  		</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>


	<!-- PROJECT ACTIVE -->
	<div class="col-8">
		<div class="card">
			<div class="card-body" id="container-project">
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">    
  var Page = function () {
          	$(document).on('click','.container-pm',function(e){
            // $('.container-pm').css('backgroundColor','#eee');
            // $('.container-pm').css('color','#000');
            // $(this).css('backgroundColor','#129841');
            // $(this).css('color','#fff');
            	$('#container-project').empty();
                if($(this).hasClass('active')){
                	$('.container-pm').removeClass('active');
                	$("#container-project").load( base_url+'monitoring/get_project_pm', { id : null });
                }else{
                	$('.container-pm').removeClass('active');
                	$(this).addClass('active');
                	var id = $(this).data('id');
                	$("#container-project").load( base_url+'monitoring/get_project_pm', { id : id });
                }
           	});
      return {
          init: function() { 
            $("#container-project").load( base_url+'monitoring/get_project_pm', { id : null }, function() {});
           }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>