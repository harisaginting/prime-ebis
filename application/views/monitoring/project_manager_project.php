<div class="row" >

  <?php foreach ($project as $key => $value): ?>
    <div class="col-md-12" >
      <div style="border-bottom-left-radius: 0px;border-bottom-right-radius: 0px;" class="card text-white <?= ($value['PROGRESS'] == 'LEAD' ? 'bg-success' : ($value['PROGRESS'] == 'LAG' ? 'bg-warning' : ($value['PROGRESS'] == 'DELAY'? 'bg-danger' : 'hidden')))  ?> nav-link-hgn">
        <div class="card-body pb-0">
        <a href="<?= base_url();?>projects/view/<?= $value['ID_PROJECT'] ?>"><span class="mb-0" style="font-size: 16px;font-weight: 700;color: #000;"><?= $value['NAME']; ?></span></a><br>
        <span class="text-dark">PLAN <?= $value['TOTAL_PLAN']; ?>% | ACHIEVMENT <?= $value['TOTAL_ACHIEVEMENT']; ?>%</span><br>
        <span class="text-dark">START <?= $value['START_DATE2']; ?></span><br>
        <span class="text-dark">END <?= $value['END_DATE2']; ?></span>
        </div>
        <div id="curve<?=$key; ?>" style="height: 150px;">
        </div>
    </div>
  </div>

  <script type="text/javascript">
    Highcharts.chart('curve<?= $key; ?>', {
        
            chart: {
                height: 150,
                backgroundColor : '<?= ($value['PROGRESS'] == 'LEAD' ? '#4dbd74' : ($value['PROGRESS'] == 'LAG' ? '#ffc107' : ($value['PROGRESS'] == 'DELAY'? '#f86c6b' : 'hidden')))  ?>'
            },
            credits: {
              enabled: false
            },
                title: false,
                xAxis: {
                    categories: ['WEEK',<?php echo "'".implode("','", $value['kurva']['WEEK'])."'"?>]
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    max: 100,
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#000'
                    }]
                },
                tooltip: {
                    // valueSuffix: '%'
                    formatter: function () {
                        var tooltipsArr = ['0',<?php echo "'".implode("','", $value['kurva']['DAYS'])."'"?>];
                        return tooltipsArr[this.point.index] +'<br>'+ this.series.name +' : '+ Highcharts.numberFormat(this.point.y, 2) +'%';
                    }
                },
                exporting: { enabled: false },
                legend : false,
                series: [{
                    name: 'Plan',
                    color: '#dedede',
                    data: [0,<?php echo implode(",", $value['kurva']['PLAN'])?>]
                }, {
                    name: 'Realization',
                    color: '#0f0',
                    data: [0,<?php echo implode(",", $value['kurva']['ACHIEVEMENT'])?>]
                }]
    });

  </script>

  <?php endforeach; ?>