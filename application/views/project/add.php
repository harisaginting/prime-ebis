<!-- SUMEMRNOTE -->
<link href="<?= base_url(); ?>assets/plugin/summernote/summernote-bs4.css" rel="stylesheet">
<script src="<?= base_url(); ?>assets/plugin/summernote/summernote-bs4.js"></script>
<!-- END SUMMERNOTE -->

<link href="<?= base_url(); ?>assets/css/project.css?v=<?= date('s') ?>" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<span class="judul">Add Project</span>
			</div>
			<div class="card-body">
				<form id="form-project" method="post" action="<?= base_url() ?>project-add">	
					<div class="row">
            <div class="col-md-12">
              
              <div class="form-group"> 
                <label>Name <span class="text-danger">*</span></label>
                <textarea class="form-control" id="name" name="name" style="height: 4em" required></textarea>
              </div>

              <div class="form-group"> 
                <label>Description</label>
                <div class="col-sm-12">
                    <div id="summernote"></div>
                </div>
                <input type="hidden" name="description" id="description">
              </div>

              <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="form-group"> 
                      <label>ID LOP</label>
                      <input class="form-control" type="text" name="id_global" id="id_global" value="">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>NO. ORDER NCX</label>
                        <input class="form-control" type="text" name="no_ncx" id="no_ncx" value="">
                      </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>NO. ORDER PROJECT</label>
                        <input class="form-control" type="text" name="no_project" id="no_project" value="">
                      </div>
                  </div>

                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>NO. SID</label>
                        <input class="form-control" type="text" name="sid" id="sid" value="">
                      </div>
                </div>
              </div>
            </div>

						<div class="col-md-6">
							<div class="form-group"> 
								<label>Customer <span class="text-danger">*</span></label>
								<select name="customer" id="customer" class="form-control" required>
								</select>
							</div>
							
							<div class="row">
                <div class="col-md-12">
                  <div class="form-group"> 
                    <label>Account Manager <span class="text-danger">*</span></label>
                    <select class="form-control" name="am" id="am" required>
                      
                    </select>
                  </div>
                </div>

								<div class="col-md-12">		
									<div class="form-group"> 
										<label>Segmen <span class="text-danger">*</span></label>
                    <input type="hidden" name="segmen" id="segmen" required readonly>
                    <input type="text" name="segmen-name" id="segmen-name" class="form-control" required readonly >
									</div>
								</div>
							</div>

              <div class="form-group"> 
                <label>No. Kontrak Bersama (KB) <span class="text-danger">*</span></label>
                <input type="text" name="kb" id="kb" placeholder="nomor kontrak bersama" class="form-control" required>
              </div>

              <div class="form-group">
                <label>Group Type</label>
                <select class="form-control"  id="group_type" name="group_type">
                  <option value="NON CONNECTIVITY">NON CONNECTIVITY</option>
                  <option value="CONNECTIVITY">CONNECTIVITY</option>
                  <option value="CONNECTIVITY + NON CONNECTIVITY">CONNECTIVITY + NON CONNECTIVITY</option>
                </select>
              </div>

						</div>

						<div class="col-md-6">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Start date <span class="text-danger">*</span></label>
                    <input placeholder="dd/mm/yyyy" class="form-control date-picker" type="text" name="start" id="start" readonly required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label>Kickoff date <span class="text-danger">*</span></label>
                      <input class="form-control date-picker" type="text" name="kickoff" id="kickoff" readonly placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>End date <span class="text-danger">*</span></label>
                    <input placeholder="dd/mm/yyyy" class="form-control date-picker" type="text" name="end" id="end" readonly required>
                  </div>
                </div>
              </div>
              <div class="form-group"> 
                <label>Value <small>exclude PPN</small><span class="text-danger">*</span></label>
                <input type="text" name="value" id="value" class="form-control rupiah" required />
              </div>


            
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label>Category <span class="text-danger">*</span></label>
										<select class="form-control" id="category" name="category" required>
											<?php foreach ($type_project as $key => $value): ?>
												<option value="<?= $value['ID'] ?>"><?= $value['VALUE'] ?></option>	
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Regional <span class="text-danger">*</span></label>
										<select class="form-control" id="regional" name="regional" required>
											<?php foreach ($regional as $key => $value): ?>
												<option value="<?= $value['ID'] ?>"><?= $value['VALUE'] ?></option>	
											<?php endforeach ?>
										</select>
									</div>
								</div>

                <div class="col-md-3">
                  <div class="form-group">
                      <label>Division <span class="text-danger">*</span></label>
                      <select required class="form-control" name="division" id="division" required>
                       <?php if($user['division'] == 'EBIS') :?>
                        <option value="DBS">DBS</option>
                        <option value="DES">DES</option>
                        <option value="DGS">DGS</option>
                      <?php else :  ?>
                        <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
                      <?php endif;  ?>
                      </select>
                    </div>
                </div>

							</div>

              <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                      <label>Project Admin</label>
                      <select class="form-control" name="admin" id="admin">
                      </select>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label>Monitoring Request Date</label>
                      <input placeholder="dd/mm/yyyy" class="form-control date-picker" type="text" name="monitoring" id="monitoring" readonly>
                  </div>
                </div>
              </div>
              
              <div class="w-100 pl-1 clearfix">
                  <label>Is Project Strategic ?</label>
                  <div class="w-100" style="display: flex;">
                    <strong class="text-danger mt-2">NO</strong>
                    <label class="switch switch-strategic ml-2 mr-2" style="margin-top:5px !important;">
                      <input type="checkbox" name="strategic" id="strategic" class="primary">
                      <span class="slider round" id="slider-strategic"></span>
                    </label>
                    <strong class="text-success mt-2">YES</strong>
                  </div>
              </div>

						</div>	
					</div>
					
          <div class="row mt-2">
            <div class="col-12">
              <div class="form-group"  style="margin-bottom: 1px !important">
                <div class="row">
                  <div class="col-12 mb-2">
                    <label  style="margin-bottom: 0px !important">Term Of Payment (Customer)</label>
                    <button id="btnAddTopCustomer" type="button" class="btn btn-info" style="padding:0px;width: 25px;height: 25px;border-radius: 50%"><i class="fa fa-plus"></i></button>
                  </div>
                  <div class="col-12 ml-3" id="top-customer" >
                </div>        
                </div>
              </div>
            </div>
          </div>

					<div class="row text-center mt-4">
              <button class="btn btn-success" id="btn-save-project" type="button" style="margin: auto;">save project</button>
          </div>

				</form>
			</div>
	

		</div>
	</div>
</div>


<script type="text/javascript">    
 var total_top_customer = 0;
 var Page = function () {
  		const wywsigInit = () => {                     
          $('#summernote').summernote({
              height : 150,
              maxHeight : 1200,
              toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
              ]
          });
      };

      const addTopCustomer = () => {
  			total_top_customer += 1;
  			let index = total_top_customer;
  			
        let container_row = document.createElement("div");
        $(container_row).addClass("row container-top-customer"+index);

  			//field date 
  			let field_date 	= document.createElement("input");
  				$(field_date).addClass("form-control date-picker");
  				$(field_date).attr("id","top-customer-date-"+ index);
  				$(field_date).attr("readonly",true);
  				$(field_date).attr("type","text");
  				$(field_date).attr("name","top-customer-date[]");
  				$(field_date).attr("id","top-customer-date-"+index);
  				$(field_date).attr("placeholder","due date");
          $(field_date).attr("required","required");
  			let container_date = document.createElement("div");
  				$(container_date).addClass("col-2 mt-1 container-top-customer"+index);
  				$(container_date).append(field_date);


        // field progress
        let field_progress   = document.createElement("input");
          $(field_progress).addClass("form-control");
          $(field_progress).attr("name","top-customer-progress[]");
          $(field_progress).attr("type","number");
          $(field_progress).attr("min","0");
          $(field_progress).attr("max","100");
          $(field_progress).attr("id","top-customer-progress-"+ index);
          $(field_progress).attr("placeholder","Progress (%)");
        let container_progress = document.createElement("div");
          $(container_progress).addClass("col-2 mt-1 container-top-customer"+index);
          $(container_progress).append(field_progress);

  			// field value
  			let field_value 	= document.createElement("input");
  				$(field_value).addClass("form-control rupiah");
  				$(field_value).attr("name","top-customer-value[]");
  				$(field_value).attr("type","text");
  				$(field_value).attr("id","top-customer-value-"+ index);
  				$(field_value).attr("placeholder","value");
          $(field_value).attr("required","required");
  			let container_value = document.createElement("div");
  				$(container_value).addClass("col-3 mt-1 container-top-customer"+index);
  				$(container_value).append(field_value);
  				
  			// field note
  			let field_note 	= document.createElement("input");
  				$(field_note).addClass("form-control");
  				$(field_note).attr("name","top-customer-note[]");
  				$(field_note).attr("type","text");
  				$(field_note).attr("id","top-customer-note-"+ index);
  				$(field_note).attr("placeholder","note");
          $(field_note).attr("required","required");
  			let container_note = document.createElement("div");
  				$(container_note).addClass("col-4 mt-1 container-top-customer"+index);
  				$(container_note).append(field_note);

  			// btn delete
  			let icon_delete = document.createElement("i");
  				$(icon_delete).addClass("fa fa-trash");
  			let btn_delete 	= document.createElement("button");
  				$(btn_delete).addClass("btn btn-danger mt-1 btn-delete-top-customer container-top-customer"+index);
  				$(btn_delete).data('id', index);
          $(btn_delete).attr('type','button');
          $(btn_delete).css('maxHeight','35px');
  				$(btn_delete).append(icon_delete);

        $(container_row).append(btn_delete);
  			$(container_row).append(container_date);
        $(container_row).append(container_progress);
  			$(container_row).append(container_value);
  			$(container_row).append(container_note);

        $("#top-customer").append(container_row);
        // console.log($('#buttonDelete1').data());
  			generatePage();
  		};

      const deleteTopCustomer = (index) => {
        if(total_top_customer > 1){
          $('.container-top-customer'+index).remove();
          --total_top_customer;  
        }else{
           bootbox.alert("Minimal Term Of Payment KB adalah 1 (OTC)");
        }
        
      }

      return {
          init: function() { 
          	 wywsigInit();
             addTopCustomer();

             $(document).on("click","#btn-save-project",(e) => {
                e.preventDefault();
                if($('#form-project').valid()){
                  let desc = $('#summernote').summernote('code');
                  $('#description').val(desc);
                  $('.rupiah').map((index, value) =>{
                    $('#'+$(value).attr('id')).val($(value).unmask());
                  });
                  showLoading();
                  $('#form-project').submit();
                }

             });

             $(document).on('click','#btnAddTopCustomer', (e) =>{addTopCustomer();});
             $(document).on('click','.btn-delete-top-customer', (e) =>{deleteTopCustomer($(e.currentTarget).data("id"))});

              // $("#segmen").select2({
              //               placeholder: "Select Segmen",
              //               width: 'resolve',
              //               allowClear : true,
              //               ajax: {
              //                   type: 'POST',
              //                   url:base_url+"master/get_segmen",
              //                   dataType: "json",
              //                   data: function (params) {
              //                       return {
              //                           q: params.term,
              //                           page: params.page,
              //                       };
              //                   },
              //                   processResults: function (data) {
              //                       return {
              //                           results: $.map(data, function(obj) {
              //                               return { id: obj.CODE, text: obj.NAME};
              //                           })
              //                       };
              //                   },
                                
              //               }
              // 	});

               $(document).on("change","#am",(e) => {
                e.preventDefault();
                $.ajax({
                        url: base_url+'data-account-manager-segmen',
                        type:'POST',
                        data:  {email : $('#am').val()},
                        async : true,
                        dataType : "json",
                        success:function(result){
                                console.log(result);
                                if(result.status =='success'){
                                  $("#segmen").val(result.SEGMEN);
                                  $("#segmen-name").val(result.SEGMEN_NAME);
                                }else{
                                  alert(result.status);
                                  }
                                return result;
                        }

                });  
             });

              $("#admin").select2({
                            placeholder: "Select Project Admin",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get-admin",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.EMAIL, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                }); 


              $("#customer").select2({
                            placeholder: "Select Customer",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_customer",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                });

              $("#am").select2({
                            placeholder: "Select Account Manager",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_am",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.EMAIL, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
              	}); 


              $("#admin").select2({
                            placeholder: "Select Project Admin",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get-admin",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.EMAIL, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                }); 

              $('#form-project').validate({
                rules: {
                      name        : "required",
                      customer    : "required",
                      segmen      : "required",
                      value       : "required",
                      start       : "required",
                      end         : "required",
                      category    : "required",
                      regional    : "required",
                      description : "required",
                    },
                    errorPlacement: function(error, element) {
                      let label = element.parent();
                      $(label).append(error);
                    }
                });             
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>