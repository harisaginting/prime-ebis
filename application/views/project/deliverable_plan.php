<style type="text/css">
  .card-project  .project-info{
    border-bottom: 1px solid #888 !important;
  }

  .card-project  .form-control, .card-project  .project-info {
    background: #f0f3f5;
    border-color: #f0f3f5;
    color: #000 !important;
  }
</style>
<div class="row">
	<div class="col-6">
		<div class="card-body card-project">
          <div class="row">
            <div class="col-12 mb-2"><span class="h6 judul">Deliverable Information</span></div>
            <div class="col-md-12">
              <div class="card card-project" style="border-radius: 10px;">
                <div class="card-body row" style="background:#f0f3f5;border-radius: 10px;">
                  
                  <div class="form-group col-12"> 
                    <label>Name</label>
                    <input type="hidden" name="id_project" id="id_project">
                    <div class="w-95 project-info">
                      <strong><?= $deliverable['NAME'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-12"> 
                    <label>Weight</label>
                    <input type="hidden" name="id_project" id="id_project">
                    <div class="w-95 project-info">
                      <strong><?= $deliverable['WEIGHT'] ?>%</strong>
                    </div>
                  </div>

                  <div class="form-group col-6"> 
                    <label>Start</label>
                    <input type="hidden" name="id_project" id="id_project">
                    <div class="w-95 project-info">
                      <strong><?= $deliverable['START_DATE2'] ?></strong>
                    </div>
                  </div>

                  <div class="form-group col-6"> 
                    <label>End</label>
                    <input type="hidden" name="id_project" id="id_project">
                    <div class="w-95 project-info">
                      <strong><?= $deliverable['END_DATE2'] ?></strong>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
      </div>
	</div>

  <div class="col-6">
    <div class="card-body">
          <div class="row">
            <div class="col-12 mb-2"><span class="h6 judul">Week Plan</span></div>
            <div class="col-12">
              <div class="card" style="border-radius: 10px;">
                <div class="card-body" style="background:#f0f3f5;border-radius: 10px;">
                
                <form id="form-week">
                <?php $sum=0; foreach ($chart as $key => $value) : ?>
                  <?php   $sum += floatval($value['PLAN']); ?>
                  <div class="row">
                      <div class="form-group col-7"> 
                        <label>Week <?= $value['WEEK'] ?></label>
                        <input type="number" name="week-<?= $value['WEEK'] ?>" id="week-<?= $value['WEEK'] ?>" class="form-control value-week-deliverable" value="<?= $value['PLAN'] ?>" max="<?= $deliverable['WEIGHT'] ?>" required>
                      </div>

                      <div class="col-5">
                        <div>
                          <span class="pull-left" id="sum-week-<?= $value['WEEK'] ?>" style="margin-top: 30px !important;"><?= $sum; ?> %</span>
                        </div>
                      </div>
                  </div>
                <?php endforeach; ?> 
                <div class="row">
                    <div class="col-12 text-center">
                      <strong class="text-danger hidden" id="alert-max">Total Plan must be <?= $deliverable['WEIGHT']; ?>%</strong><br>
                      <button class="btn btn-info btn-sm btn-prime" id="btn-save-plan-deliverable-week" type="button">Update Plan Deliverable</button>
                    </div>
                </div>

                </form>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
</div>


<script type="text/javascript">    
 var Page = function () {
  		

      return {
          init: function() { 
    	         $(document).on("keyup",".value-week-deliverable", (e) => {
                  let total_plan = 0;
                   $('.value-week-deliverable').map((index, value) =>{
                        let currentValue = $('#'+$(value).attr('id')).val();
                        total_plan = parseFloat(total_plan) + parseFloat(currentValue) ;
                        $('#sum-'+$(value).attr('id')).html(total_plan);
                    });
               });


               $(document).on("click","#btn-save-plan-deliverable-week", (e) => {
                   if($('#form-week').valid()){
                        let form        = $('#form-week')[0];
                        let formData    = new FormData(form);                 
                        let total_plan  = 0;

                         $('.value-week-deliverable').map((index, value) =>{
                              let currentValue = $('#'+$(value).attr('id')).val();
                              total_plan       = parseFloat(total_plan) + parseFloat(currentValue) ;
                              $('#sum-'+$(value).attr('id')).html(total_plan);
                          });


                        if(total_plan == <?= $deliverable['WEIGHT'] ?>){
                            $('#alert-max').addClass('hidden');
                            showLoading();
                            $.ajax({
                                  url: base_url+'project/update_week_deliverable/<?= $deliverable['ID'] ?>/<?= $deliverable['ID_PROJECT'] ?>',
                                  type:'POST',
                                  data:  formData,
                                  async : true,
                                  dataType : "text",
                                  contentType:false,
                                  processData:false,
                                  success:function(result){
                                         $("#modal-progress").modal('hide');
                                         hideLoading();
                                           if(result.trim()=='success'){
                                            location.reload();;
                                          }else{
                                            bootbox.alert("Failed!", function(){});
                                            }
                                          return result;
                                  }

                          }); 
                        }else{
                          $('#alert-max').removeClass('hidden');
                        }
                  
                     }
                  });


            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>