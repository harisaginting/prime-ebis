<!-- SUMEMRNOTE -->
<link href="<?= base_url(); ?>assets/plugin/summernote/summernote-bs4.css" rel="stylesheet">
<script src="<?= base_url(); ?>assets/plugin/summernote/summernote-bs4.js"></script>
<!-- END SUMMERNOTE -->

<link href="<?= base_url(); ?>assets/css/project.css?v=<?= date('s') ?>" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<span class="judul">
          <strong>Edit Project</strong>
          <strong class="text-muted pull-right"><?= $project['STATUS'] ?></strong>
        </span>
			</div>
			<div class="card-body">
				<form id="form-project" method="post" action="<?= base_url() ?>project-edit">	
					<div class="row">
            <div class="col-md-12 col-sm-12">
            
              <div class="form-group"> 
                <label>Name <span class="text-danger">*</span></label>
                <input type="hidden" name="id_project" id="id_project" value="<?= $project['ID_PROJECT'] ?>">
                <textarea class="form-control" id="name" name="name" style="height: 4em"><?= $project['NAME'] ?></textarea>
              </div>

              <div class="form-group"> 
                <label>Description</label>
                <div class="col-sm-12">
                    <div id="summernote"></div>
                </div>
                <div style="visibility: hidden;height: 1px !important;">
                  <input type="hidden" name="description" id="description" value="<?= $project['DESCRIPTION'] ?>">
                </div>
              </div>

              <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="form-group"> 
                      <label>ID LOP</label>
                      <input class="form-control" type="text" name="id_global" id="id_global" value="<?= $project['ID_PROJECT_GLOBAL'] ?>">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>NO ORDER NCX</label>
                        <input class="form-control" type="text" name="no_ncx" id="no_ncx" value="<?= $project['NO_NCX'] ?>">
                      </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>NO ORDER PROJECT</label>
                        <input class="form-control" type="text" name="no_project" id="no_project" value="<?= $project['NO_PROJECT'] ?>">
                      </div>
                  </div>

                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>SID</label>
                        <input class="form-control" type="text" name="sid" id="sid" value="<?= $project['SID'] ?>">
                      </div>
                </div>
              </div>
            
            </div>


						<div class="col-md-6 col-sm-12">

              

              

							<div class="form-group"> 
								<label>Customer <span class="text-danger">*</span></label>
								<select name="customer" id="customer" class="form-control">
                  <option value="<?= $project['CUSTOMER']?>" selected><?= $project['CUSTOMER_NAME'] ?></option>
								</select>
							</div>
							
							<div class="row">
                <div class="col-md-12">
                  <div class="form-group"> 
                    <label>Account Manager <span class="text-danger">*</span></label>
                    <select class="form-control" name="am" id="am" required>
                      <option value="<?= $project['AM'] ?>" selected><?= $project['AM_NAME'] ?></option>
                    </select>
                  </div>
                </div>

								<div class="col-md-12">		
									<div class="form-group"> 
										<label>Segmen <span class="text-danger">*</span></label>
                    <input type="hidden" name="segmen" id="segmen" required readonly value="<?= $project['SEGMEN'] ?>">
                    <input type="text" name="segmen-name" id="segmen-name" class="form-control" required readonly value="<?= $project['SEGMEN_NAME'] ?>">
									</div>
								</div>
								
							</div>


              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Start date <span class="text-danger">*</span></label>
                    <input class="form-control date-picker" type="text" name="start" id="start" value="<?= $project['START_DATE2'] ?>" readonly required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Kickoff date <span class="text-danger">*</span></label>
                    <input value="<?= $project['DATE_KICK_OFF2'] ?>" class="form-control date-picker" type="text" name="kickoff" id="kickoff" readonly placeholder="dd/mm/yyyy">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>End date <span class="text-danger">*</span></label>
                    <input value="<?= $project['END_DATE2'] ?>" class="form-control date-picker" type="text" name="end" id="end" readonly required>
                  </div>
                </div>
              </div>

              <div class="form-group"> 
                <label>No. Kontrak Bersama (KB) <span class="text-danger">*</span></label>
                <input type="text" name="kb" id="kb" placeholder="nomor kontrak bersama" class="form-control"  value="<?= $project['KB'] ?>" required>
              </div>

              <div class="form-group">
                <label>Group Type</label>
                <select class="form-control"  id="group_type" name="group_type">
                  <option value="NON CONNECTIVITY" <?= $project['GROUP_TYPE'] == "NON CONNECTIVITY" ? "selected" : "" ?> >NON CONNECTIVITY</option>
                  <option value="CONNECTIVITY" <?= $project['GROUP_TYPE'] == "CONNECTIVITY" ? "selected" : "" ?> >CONNECTIVITY</option>
                  <option value="CONNECTIVITY + NON CONNECTIVITY" <?= $project['GROUP_TYPE'] == "CONNECTIVITY + NON CONNECTIVITY" ? "selected" : "" ?>>CONNECTIVITY + NON CONNECTIVITY</option>
                </select>
              </div>

						</div>

						<div class="col-md-6">
              <div class="form-group"> 
                <label>Value <span class="text-danger">*</span><small>exclude PPN</small></label>
                <input type="text" name="value" id="value" class="form-control rupiah" value="<?= $project['VALUE'] ?>" required />
              </div>

              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Category <span class="text-danger">*</span></label>
                    <select class="form-control" id="category" name="category" required>
                      <?php foreach ($type_project as $key => $value): ?>
                        <option value="<?= $value['ID'] ?>" <?= $project['CATEGORY'] == $value['ID'] ? 'selected' : '' ?> ><?= $value['VALUE'] ?></option>  
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Regional <span class="text-danger">*</span></label>
                    <select class="form-control" id="regional" name="regional" required>
                      <?php foreach ($regional as $key => $value): ?>
                        <option value="<?= $value['ID'] ?>" <?= $project['REGIONAL'] == $value['ID'] ? 'selected' : '' ?> ><?= $value['VALUE'] ?></option>  
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                      <label>Division <span class="text-danger">*</span></label>
                      <select required class="form-control" name="division" id="division" required>
                       <?php if($user['division'] == 'EBIS') :?>
                        <option value="DBS" <?= $project['DIVISION'] == 'DBS' ? 'selected' :'' ?> >DBS</option>
                        <option value="DES" <?= $project['DIVISION'] == 'DES' ? 'selected' :'' ?>>DES</option>
                        <option value="DGS" <?= $project['DIVISION'] == 'DGS' ? 'selected' :'' ?>>DGS</option>
                      <?php else :  ?>
                        <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
                      <?php endif;  ?>
                      </select>
                    </div>
                </div>

              </div>

              <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                      <label>Project Manager</label>
                      <select name="pm" id="pm" class="form-control">
                        <option value="<?= $project['PM']?>" selected><?= $project['PM_NAME'] ?></option>
                      </select>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label>Appointment Date</label>
                      <input class="form-control date-picker" type="text" name="appointment" id="appointment" readonly value="<?= $project['DATE_PM_APPOINTMENT2'] ?>" placeholder="dd/mm/yyyy">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                      <label>Project Admin</label>
                      <select class="form-control" name="admin" id="admin">
                        <option value="<?= $project['PM_ADMIN']?>" selected><?= $project['PM_ADMIN_NAME'] ?></option>
                      </select>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label>Monitoring Request Date</label>
                      <input class="form-control date-picker" type="text" name="monitoring" id="monitoring" readonly value="<?= $project['DATE_MONITORING_REQUEST2'] ?>" placeholder="dd/mm/yyyy">
                  </div>
                </div>
              </div>


              <div class="w-100 pl-1 clearfix">
                  <label>Is Project Strategic ?</label>
                  <div class="w-100" style="display: flex;">
                    <strong class="text-danger mt-2">NO</strong>
                    <label class="switch switch-strategic ml-2 mr-2" style="margin-top:5px !important;">
                      <input type="checkbox" name="strategic" id="strategic" class="primary" <?= $project["IS_STRATEGIC"] == 1 ?  "checked" : '' ?>>
                      <span class="slider round" id="slider-strategic"></span>
                    </label>
                    <strong class="text-success mt-2">YES</strong>
                  </div>
              </div>

              <div class="w-100">
                <label>Document</label>
                <div class="w-100">
                    <ul class="list-group scroll-thin" style="max-height: 160px !important;overflow-y: scroll;">
                      <?php foreach ($project['document'] as $key => $value) : ?>
                          <a target="_blank" href="<?= base_url().'assets/document/'.$project['ID_PROJECT'].'/'.$value['NAME'] ?>" ><li class="list-group-item" style="padding: 0.2rem 1.25rem;"><?= $value['NAME'] ?></li></a>
                      <?php endforeach; ?>
                    </ul>
                </div>
                <div class="w-100 mt-1">
                  <button class="btn btn-sm btn-success pull-right" id="btn-add-document" type="button"> add document</button>
                </div>
              </div>

						</div>	
					</div>
					
          <div class="row mt-2">
            <div class="col-12">
              <div class="form-group"  style="margin-bottom: 1px !important">
                <div class="row">
                  <div class="col-12 mb-2">
                    <label  style="margin-bottom: 0px !important">Term Of Payment (Customer)</label>
                    <button id="btnAddTopCustomer" type="button" class="btn btn-info" style="padding:0px;width: 25px;height: 25px;border-radius: 50%"><i class="fa fa-plus"></i></button>
                  </div>
                  <div class="col-12 ml-3" id="top-customer" >
                </div>        
                </div>
              </div>
            </div>
          </div>

				</form>
			</div>

		</div>
	</div>
</div>


<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <span class="judul">Partners</span>
        <button class="btn btn-primary btn-md btn-addon btn-prime pull-right" id="btn-add-partner"><i class="fa fa-plus"></i><span class="pr-1">Partner</span></button>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
                <th style="width: 30%;">Partner</th>
                <th style="width: 20%;">Value</th>
                <th style="width: 40%;">Term Of Payment</th>
                <th style="width: 10%;"></th>
              </thead>
              <tbody>
                <?php foreach ($partner as $key => $value) : ?>
                  <tr>
                    <td>
                      <div class="row">
                        <div class="col-12">
                          <strong class="w-100 text-primary"><?= $value['PARTNER_NAME'] ?></strong>
                        </div>
                        <div class="col-3">SPK (P8) </div><div class="col-9">:<strong><?= $value['SPK'] ?></strong></div>
                        <div class="col-3">KL       </div><div class="col-9">:<strong><?= $value['KL'] ?></strong> </div>
                        <div class="col-12">
                          <input type="file" name="spk<?= $key ?>"  id="spk<?= $key ?>"  style="display: none;">
                        </div>

                      </div>
                    </td>
                    <td class="rupiah"><?= $value['VALUE'] ?></td>
                    <td>
                      <table class="table w-100">
                          <thead>
                            <th>Due Date</th>
                            <th>Value</th>
                            <th>Note</th>
                          </thead>
                          <tbody>
                            <?php foreach ($value['TOP'] as $key1 => $top) : ?>
                              <tr>
                                <td><?= $top['DUE_DATE2'] ?></td>
                                <td class="rupiah"><?= $top['VALUE'] ?></td>
                                <td><?= $top['NOTE'] ?></td>
                              </tr>
                            <?php endforeach; ?>
                          </tbody>
                      </table>
                    </td>
                    <td>
                      <div class="btn btn-sm btn-warning w-90 btn-edit-partner" 
                          data-spk="<?= $value['SPK']?>" 
                          data-kl="<?= $value['KL']?>" 
                          data-kl_date="<?= $value['KL_DATE2']?>" 
                          data-partner="<?= $value['ID_PARTNER']?>" 
                          data-partner_name="<?= $value['PARTNER_NAME']?>" 
                          data-spk_date="<?= $value['SPK_DATE2']?>" 
                          data-doc_spk="<?= $value['DOC_SPK']?>" 
                          data-value="<?= $value['VALUE']?>" 
                          data-top='<?= json_encode($value['TOP'])?>' 
                      >
                          Edit
                      </div>
                      <div class="btn btn-sm btn-danger w-90 mt-1 btn-delete-partner"
                        data-spk="<?= $value['SPK']?>" 
                        >Delete</div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <span class="judul"><strong>User Access</strong></span>
      </div>
      <div class="card-body">
        <table class="table w-100 table-striped mt-2">
            <thead>
              <th style="width: 40%;">USER</th>
              <th style="width: 30%;">ROLE</th>
              <th style="text-align: center;"></th>
            </thead>
            <tbody>
              <?php foreach ($user_access as $key => $value) : ?>
                <tr>
                  <td>
                    <strong><?= $value['NAME'] ?></strong>
                    <br>
                    <small class="text-muted"><?= $value['EMAIL'] ?></small>
                  </td>
                  <td>
                    <?= $value['ROLE_NAME'] ?>
                    <br>
                    <small class="text-muted"><?= $value['DIVISION'] ?></small>
                  </td>
                  <td style="text-align: center;">
                    <?php if ($value["ROLE"] != "1" && $value["ROLE"] != "0" && $user["email"] != $value["EMAIL"] ) : ?>
                        <button i class="btn btn-xs btn-prime btn-danger btn-delete-access" data-email="<?= $value['EMAIL'] ?>">
                          <span class="fa fa-trash"></span>
                        </button>
                    <?php else : ?>
                        <button i class="btn btn-xs btn-prime btn-disable">
                          <span class="fa fa-trash"></span>
                        </button>
                    <?php endif?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
        </table>
        <dir class="row">
          <div class="col-md-12">
              <button type="button" class="btn btn-success btn-md btn-addon btn-prime pull-right" id="btn-add-project-access">
              <i class="fa fa-save"></i>
              <span class="pr-2">
                <strong>ADD NEW ACCESS</strong>
              </button>
              <div class="form-group pull-right mr-2" style="min-width: 333px;"> 
                <select class="form-control" name="user-access" id="user-access">
                  <option></option>
                </select>
              </div>
          </div>
        </dir>
      </div>
    </div>
  </div>
</div>



<div class="row text-center mt-4 mb-5">

  <div class="col-md-2 col-sm-12"> 
      <a class="btn btn-disable btn-md btn-addon btn-prime w-100" href="<?= base_url()."p/".$project['ID_PROJECT'] ?>">
        <i class="fa fa-arrow-circle-left"></i>
        <span class="pr-2">
          <strong>PREVIEW</strong>
        </span>
      </a>
  </div>

  <div class="col-md-2 offset-md-4 col-sm-12"> 
      <?php if (!empty($p_access["D"]) && $access["PROJECT"]["D"])  :  ?>
      <button type="button" class="btn btn-danger btn-md btn-addon btn-prime w-100" id="btn-delete-project">
        <i class="fa fa-trash"></i>
        <span class="pr-2">
          <strong>DELETE</strong>
        </button>
        <?php else: ?>
          <button type="button" class="btn btn-disable btn-md btn-addon btn-prime w-100">
          <i class="fa fa-trash"></i>
          <span class="pr-2">
            <strong>DELETE</strong>
          </button>
        <?php endif; ?>
  </div>

  <div class="col-md-2 col-sm-12"> 
      <?php if($project['STATUS'] =='CANDIDATE') : ?>
            <button class="btn btn-primary btn-md btn-addon btn-prime w-100" type="button" id="active-project">
              <i class="fa fa-rocket"></i>
                <span class="pr-2">
                <strong>ACTIVATE</strong>
                </span>
            </button>
      <?php endif; ?>

      <?php if($project['STATUS'] =='ACTIVE') : ?>
          <button class="btn btn-primary btn-md btn-addon btn-prime w-100" type="button" id="close-project">
            <i class="fa fa-file-archive-o"></i>
              <span class="pr-2">
                <strong>CLOSE</strong>
              </span>
          </button>
      <?php endif; ?>
  </div>

  <div class="col-md-2 col-sm-12"> 
      <button class="btn btn-success btn-md btn-addon btn-prime w-100" id="btn-save-project">
        <i class="fa fa-floppy-o"></i>
        <span class="pr-2">
          <strong>UPDATE</strong>
        </span>
      </button>
  </div>
</div>


 <!-- Modal Partner-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-partner">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-partner" >Add <small>Partner</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-partner">
                    <div class="form-group">
                      <label>Partner</label>
                      <select class="form-control" id="partner" name="partner"></select>
                    </div>

                    <div class="form-group">
                      <label>No. KL</label>
                      <input type="text" name="kl" id="kl" class="form-control"/>
                    </div>

                    <div class="form-group">
                      <label>KL date</label>
                      <input class="form-control date-picker" type="text" name="kl_date" id="kl_date" readonly>
                    </div>

                    <div class="form-group">
                      <label>No. SPK</label>
                      <input type="text" name="spk" id="spk" class="form-control" required />
                    </div>
                    
                    <div class="form-group">
                      <label>SPK date</label>
                      <input class="form-control date-picker" type="text" name="spk_date" id="spk_date" readonly>
                    </div>

                    <div class="form-group">
                      <label>File SPK</label>
                      <input id="file_spk" name="file_spk" type="file" accept="pdf" class="form-control" >
                    </div>

                    <div class="form-group">
                      <label>Value</label>
                      <input type="text" name="value_partner" id="value_partner" class="form-control rupiah" required />
                    </div>
                    

                    <div class="form-group"  style="margin-bottom: 1px !important">
                        <div class="row">
                          <div class="col-12 mb-2">
                            <label  style="margin-bottom: 0px !important">Term Of Payment (Partner)</label>
                            <button id="btnAddTopPartner" type="button" class="btn btn-info" style="padding:0px;width: 25px;height: 25px;border-radius: 50%"><i class="fa fa-plus"></i></button>
                          </div>
                          <div class="col-12 ml-3" id="top-partner" >
                        </div>        
                        </div>
                      </div>

                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-save-partner" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Save</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

 <!-- Modal Document-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-document">
  <div class="modal-dialog modal-primary  modal-md">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-document" >Add <small>Document</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-document">
                    <div class="form-group">
                      <label>File Name</label>
                      <input type="text" name="document-name" id="document-name" class="form-control"/>
                    </div>

                    <div class="w-100">
                      <input id="document" name="document" type="file" accept="pdf" class="form-control fileinput" >
                    </div>
                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-save-document" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Add</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal Close-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-close">
  <div class="modal-dialog modal-primary  modal-md">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-close" >Close <small>Project</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-close">
                    <div class="form-group">
                      <label>Date Close</label>
                      <input type="text" name="close-date" id="close-date" class="form-control date-picker" readonly />
                    </div>

                    <div class="form-group">
                      <label>Lesson Learn</label>
                      <textarea class="form-control" name="close-lesson" id="close-lesson" rows="4"></textarea>
                    </div>

                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-set-close" class="btn-md btn-success btn-addon btn-prime btn" ><i class="fa fa-check"></i><span class="pl-1 pr-2">close project</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<script type="text/javascript">    
 var total_top_customer = 0;
 var total_top_partner = 0;
 var Page = function () {
      const wywsigInit = () => { 
          showLoadingMini();                    
          $('#summernote').summernote({
              height : 150,
              maxHeight : 1200,
              toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
              ]
          });
          $.ajax({
                url: base_url+'project-description/<?= $project['ID_PROJECT'] ?>',
                type:'GET',
                dataType : "text",
          }).done(function(result) {
            hideLoadingMini();
             $("#summernote").summernote("code",result);
          }).fail(function( jqXHR, textStatus ) {
            hideLoadingMini();
          });
      };

  		const addTopCustomer = (date = null, value = null, note = null, progress =null ) => {
        console.log(progress);
  			total_top_customer += 1;
  			let index = total_top_customer;
  			
        let container_row = document.createElement("div");
        $(container_row).addClass("row container-top-customer"+index);

  			//field date 
  			let field_date 	= document.createElement("input");
  				$(field_date).addClass("form-control date-picker");
  				$(field_date).attr("id","top-customer-date-"+ index);
  				$(field_date).attr("readonly",true);
  				$(field_date).attr("type","text");
  				$(field_date).attr("name","top-customer-date[]");
  				$(field_date).attr("id","top-customer-date-"+index);
  				$(field_date).attr("placeholder","due date");
          $(field_date).attr("required","required");
  			let container_date = document.createElement("div");
  				$(container_date).addClass("col-2 mt-1 container-top-customer"+index);
  				$(container_date).append(field_date);

        // field progress
        let field_progress   = document.createElement("input");
          $(field_progress).addClass("form-control");
          $(field_progress).attr("name","top-customer-progress[]");
          $(field_progress).attr("type","number");
          $(field_progress).attr("min","0");
          $(field_progress).attr("max","100");
          $(field_progress).attr("id","top-customer-progress-"+ index);
          $(field_progress).attr("placeholder","Progress (%)");
        let container_progress = document.createElement("div");
          $(container_progress).addClass("col-2 mt-1 container-top-customer"+index);
          $(container_progress).append(field_progress);

  			// field value
  			let field_value 	= document.createElement("input");
  				$(field_value).addClass("form-control rupiah");
  				$(field_value).attr("name","top-customer-value[]");
  				$(field_value).attr("type","text");
  				$(field_value).attr("id","top-customer-value-"+ index);
  				$(field_value).attr("placeholder","value");
          $(field_value).attr("required","required");
  			let container_value = document.createElement("div");
  				$(container_value).addClass("col-3 mt-1 container-top-customer"+index);
  				$(container_value).append(field_value);
  				
  			// field note
  			let field_note 	= document.createElement("input");
  				$(field_note).addClass("form-control");
  				$(field_note).attr("name","top-customer-note[]");
  				$(field_note).attr("type","text");
  				$(field_note).attr("id","top-customer-note-"+ index);
  				$(field_note).attr("placeholder","note");
          $(field_note).attr("required","required");
  			let container_note = document.createElement("div");
  				$(container_note).addClass("col-4 mt-1 container-top-customer"+index);
  				$(container_note).append(field_note);

  			// btn delete
  			let icon_delete = document.createElement("i");
  				$(icon_delete).addClass("fa fa-trash");
  			let btn_delete 	= document.createElement("button");
  				$(btn_delete).addClass("btn btn-danger mt-1 btn-delete-top-customer container-top-customer"+index);
  				$(btn_delete).data('id', index);
          $(btn_delete).attr('type','button');
          $(btn_delete).css('maxHeight','35px');
  				$(btn_delete).append(icon_delete);

        $(container_row).append(btn_delete);
  			$(container_row).append(container_date);
        $(container_row).append(container_progress);
  			$(container_row).append(container_value);
  			$(container_row).append(container_note);

        $("#top-customer").append(container_row);

        if(date !== null){
          $("#top-customer-date-"+index).val(date);
        }
        if(progress !== null){
          $("#top-customer-progress-"+index).val(progress);
        }
        if(value !== null){
          $("#top-customer-value-"+index).val(value);
        }
        if(note !== null){
          $("#top-customer-note-"+index).val(note);
        }


  			generatePage();
  		};

      const addAccess = (email) => {
          showLoadingMini();
          $.ajax({
                url: base_url+'project-add-access',
                type:'POST',
                data:  {id: '<?= $project['ID_PROJECT'] ?>',email :email} ,
                dataType : "text",
          }).done(function(result) {
            hideLoadingMini();
            $('#user-access').val('').trigger('change');
            if (result.trim() != "success") {
              alert("Request failed: " + result);
            }
            window.location.reload();
          }).fail(function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
          window.location.reload();
        });
        }

      const deleteTopCustomer = (index) => {
        if(total_top_customer > 1){
          $('.container-top-customer'+index).remove();
          --total_top_customer;  
        }else{
           bootbox.alert("Minimal Term Of Payment KB adalah 1 (OTC)");
        }
      }

      const deleteTopPartner = (index) => {
        if(total_top_partner > 1){
          $('.container-top-partner'+index).remove();
          --total_top_partner;  
        }else{
           bootbox.alert("Minimal Term Of Payment KL adalah 1 (OTC)");
        }
      }

      const addTopPartner = (date = null, value = null, note = null, progress= null) => {
          total_top_partner += 1;
          let index = total_top_partner;
          
          let container_row = document.createElement("div");
          $(container_row).addClass("row container-top-partner"+index);

          //field date 
          let field_date  = document.createElement("input");
            $(field_date).addClass("form-control date-picker");
            $(field_date).attr("id","top-partner-date-"+ index);
            $(field_date).attr("readonly",true);
            $(field_date).attr("type","text");
            $(field_date).attr("name","top-partner-date[]");
            $(field_date).attr("id","top-partner-date-"+index);
            $(field_date).attr("placeholder","due date");
            $(field_date).attr("required","required");
            $(field_date).val(date);
          let container_date = document.createElement("div");
            $(container_date).addClass("col-2 mt-1 container-top-partner"+index);
            $(container_date).append(field_date);


          // field progress
        let field_progress   = document.createElement("input");
          $(field_progress).addClass("form-control");
          $(field_progress).attr("name","top-partner-progress[]");
          $(field_progress).attr("type","number");
          $(field_progress).attr("min","0");
          $(field_progress).attr("max","100");
          $(field_progress).attr("id","top-partner-progress-"+ index);
          $(field_progress).attr("placeholder","Progress (%)");
        let container_progress = document.createElement("div");
          $(container_progress).addClass("col-2 mt-1 pr-1 pl-1 container-top-partner"+index);
          $(container_progress).append(field_progress);

          // field value
          let field_value   = document.createElement("input");
            $(field_value).addClass("form-control rupiah");
            $(field_value).attr("name","top-partner-value[]");
            $(field_value).attr("type","text");
            $(field_value).attr("id","top-partner-value-"+ index);
            $(field_value).attr("placeholder","value");
            $(field_value).attr("required","required");
            $(field_value).val(value);
          let container_value = document.createElement("div");
            $(container_value).addClass("col-3 mt-1 container-top-partner"+index);
            $(container_value).append(field_value);
            
          // field note
          let field_note  = document.createElement("input");
            $(field_note).addClass("form-control");
            $(field_note).attr("name","top-partner-note[]");
            $(field_note).attr("type","text");
            $(field_note).attr("id","top-partner-note-"+ index);
            $(field_note).attr("placeholder","note");
            $(field_note).attr("required","required");
            $(field_note).val(note);
          let container_note = document.createElement("div");
            $(container_note).addClass("col-4 mt-1 container-top-partner"+index);
            $(container_note).append(field_note);

          // btn delete
          let icon_delete = document.createElement("i");
            $(icon_delete).addClass("fa fa-trash");
          let btn_delete  = document.createElement("button");
            $(btn_delete).addClass("btn btn-danger mt-1 btn-delete-top-partner container-top-partner"+index);
            $(btn_delete).data('id', index);
            $(btn_delete).attr('type','button');
            $(btn_delete).css('maxHeight','35px');
            $(btn_delete).append(icon_delete);

          $(container_row).append(btn_delete);
          $(container_row).append(container_date);
          $(container_row).append(container_progress);
          $(container_row).append(container_value);
          $(container_row).append(container_note);

          $("#top-partner").append(container_row);

          if(date !== null){
            $("#top-partner-date-"+index).val(date);
          }
          if(value !== null){
            $("#top-partner-value-"+index).val(value);
          }
          if(note !== null){
            $("#top-partner-note-"+index).val(note);
          }

          if(progress !== null){
          $("#top-partner-progress-"+index).val(progress);
        }

          generatePage();
        };

      return {
          init: function() { 
             wywsigInit();
             $(document).on('click','#btnAddTopPartner', (e) =>{addTopPartner();});
             $(document).on('click','.btn-delete-top-partner', (e) =>{deleteTopPartner($(e.currentTarget).data("id"))});
             $(document).on("click","#btn-add-partner",()=>{
                $("#modal-title-partner").text("Add Partner");
                $('#form-partner .form-control').map((index, value) =>{
                  $(value).val('');
                });
                $('#partner').val('').trigger('change');
                total_top_partner = 0;$("#top-partner").empty();
                $('#file_spk').fileinput('destroy');
                $('#file_spk').fileinput({
                  overwriteInitial: true,
                  uploadAsync: true,
                  initialPreview  : false,
                  initialPreviewShowDelete : false,                   
                  showRemove:false,
                  showUpload:false,
                  showPreview : false   
                });
                $("#modal-partner").modal('show');
                addTopPartner();
             });


             $("#user-access").select2({
                        placeholder: "Select User",
                        width: 'resolve',
                        allowClear : true,
                        ajax: {
                            type: 'POST',
                            url:base_url+"master/get-user",
                            dataType: "json",
                            data: function (params) {
                                return {
                                    q: params.term,
                                    page: params.page,
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.EMAIL, text: obj.NAME};
                                    })
                                };
                            },
                            
                        }
              }); 

             $(document).on("click","#btn-add-project-access",()=>{
                addAccess($("#user-access").val())
             });

             $(document).on('click','#btn-add-document', (e) =>{
                $("#modal-document").modal('show');
             });

             $(document).on("click","#btn-delete-project",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();
                  bootbox.confirm({
                        message: "Delete This Project?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'project-delete',
                                        type:'POST',
                                        data:  {id :'<?= $project['ID_PROJECT'] ?>'} ,
                                        async : true,
                                        dataType : "json",
                                        success:function(hasil){
                                         console.log(hasil)
                                         if(hasil.data=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.href = base_url+'project-edit/<?= $project['ID_PROJECT']; ?>' ;
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return hasil;
                                        }

                                });
                          }
                        }
                    });
              });

             $(document).on("click",".btn-delete-access",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();
                  let email = $(e.currentTarget).data('email');
                  bootbox.confirm({
                        message: "Delete This user Access For This Project?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'project-delete-access',
                                        type:'POST',
                                        data:  {id :'<?= $project['ID_PROJECT'] ?>', email:email} ,
                                        async : true,
                                        dataType : "text"
                                }).done(function(result) {
                                    hideLoadingMini();
                                    if (result.trim() != "success") {
                                      alert("Request failed: " + result);
                                    }
                                    window.location.reload();
                                }).fail(function( jqXHR, textStatus ) {
                                  alert( "Request failed: " + textStatus );
                                  window.location.reload();
                                });
                          }
                        }
                    });
              });

              $(document).on("click",".btn-delete-partner",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let spk = $(e.currentTarget).data('spk');
                  console.log(spk);
                  bootbox.confirm({
                        message: "Delete This Partner?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                                $.ajax({
                                        url: base_url+'project/deletePartner',
                                        type:'POST',
                                        data:  {spk : spk} ,
                                        async : true,
                                        dataType : "json",
                                        success:function(result){
                                         if(result.data=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.href = base_url+'project-edit/<?= $project['ID_PROJECT']; ?>' ;
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });

             $(document).on("click",".btn-edit-partner",(e)=>{
              console.log($(e.currentTarget).data());
                $("#modal-title-partner").text("Edit Partner");
                $('#form-partner .form-control').map((index, value) =>{
                  $(value).val('');
                });

                let newOption = new Option($(e.currentTarget).data('partner_name'), $(e.currentTarget).data('partner'), false, false);

                $('#partner').append(newOption).trigger('change');
                $('#spk').val($(e.currentTarget).data('spk'));
                $('#spk_date').val($(e.currentTarget).data('spk_date'));
                $('#kl').val($(e.currentTarget).data('kl'));
                $('#kl_date').val($(e.currentTarget).data('kl_date'));
                $('#value_partner').val($(e.currentTarget).data('value'));
                $('#file_spk').fileinput('destroy');
                
                if($(e.currentTarget).data('doc_spk') !== '' && $(e.currentTarget).data('doc_spk') !== null ){

                      $('#file_spk').fileinput(
                          {
                          initialPreview: [
                              base_url+"assets/document/<?= $project['ID_PROJECT'] ?>/"+$(e.currentTarget).data('doc_spk'),
                          ],
                          initialPreviewAsData: true,
                          initialPreviewConfig: [
                              {type: "pdf", url: base_url+"assets/document/<?= $project['ID_PROJECT'] ?>/"+$(e.currentTarget).data('doc_spk'), downloadUrl: false}, 
                          ],
                          purifyHtml: true, 
                          autoReplace: true,
                          maxFileCount: 1,
                          overwriteInitial: true,
                          initialPreviewShowDelete : false,                   
                          showRemove:false,
                          showUpload:false,
                          }
                        );
                }else{
                      $('#file_spk').fileinput({
                        overwriteInitial: true,
                        uploadAsync: true,
                        initialPreview  : false,
                        initialPreviewShowDelete : false,                   
                        showRemove:false,
                        showUpload:false,
                        showPreview : false   
                      });
                }

                 let topPartner = $(e.currentTarget).data('top');
                 total_top_partner = 0;
                 $("#top-partner").empty();
                   topPartner.map((value,key)=>{
                    addTopPartner(value.DUE_DATE2, value.VALUE, value.NOTE);
                   });

                $("#modal-partner").modal('show');
             });


             $(document).on("click","#btn-save-project",(e) => {
                e.preventDefault();
                if($('#form-project').valid()){
                  let desc = $('#summernote').summernote('code');
                  $('#description').val(desc);
                  $('.rupiah').map((index, value) =>{
                    $('#'+$(value).attr('id')).val($(value).unmask());
                  });
                  $('#form-project').submit();
                }
             });

             $(document).on("click","#btn-save-document",(e) => {
                e.preventDefault();
                if($('#form-document').valid()){
                    let form = $('#form-document')[0];
                    let formData = new FormData(form);
                    showLoading();
                    $.ajax({
                            url: base_url+'project/saveDocument/<?= $project['ID_PROJECT'] ?>',
                            type:'POST',
                            data:  formData,
                            async : true,
                            dataType : "json",
                            contentType:false,
                            processData:false,
                            success:function(result){
                                    hideLoading();
                                     if(result.data=='success'){
                                      window.location.reload();
                                    }else{
                                      bootbox.alert("Failed!", function(){});
                                      }
                                    return result;
                            }

                    }); 
                }
             });

             $(document).on("click","#btn-save-partner",(e) => {
                e.preventDefault();
                if($('#form-partner').valid()){
                  $('.rupiah').map((index, value) =>{
                    $('#'+$(value).attr('id')).val($(value).unmask());
                  });

                  let form = $('#form-partner')[0];
                  let formData = new FormData(form);
                  showLoading();
                  $.ajax({
                          url: base_url+'project/savePartner/<?= $project['ID_PROJECT'] ?>',
                          type:'POST',
                          data:  formData,
                          async : true,
                          dataType : "json",
                          contentType:false,
                          processData:false,
                          success:function(result){
                                  $("#modal-partner").modal('hide');
                                  
                                   if(result.data=='success'){
                                    location.reload();
                                  }else{
                                    hideLoading();
                                    bootbox.alert("Failed!", function(){});
                                    }
                                  return result;
                          }

                  });  
                }

             });

             $(document).on('click','#btnAddTopCustomer', (e) =>{addTopCustomer();});
             $(document).on('click','.btn-delete-top-customer', (e) =>{deleteTopCustomer($(e.currentTarget).data("id"))});

              $(document).on("change","#am",(e) => {
                e.preventDefault();
                $.ajax({
                        url: base_url+'data-account-manager-segmen',
                        type:'POST',
                        data:  {email : $('#am').val()},
                        async : true,
                        dataType : "json",
                        success:function(result){
                                console.log(result);
                                if(result.status =='success'){
                                  $("#segmen").val(result.SEGMEN);
                                  $("#segmen-name").val(result.SEGMEN_NAME);
                                }else{
                                  alert(result.status);
                                  }
                                return result;
                        }

                });  
             });

              // $("#segmen").select2({
              //               placeholder: "Select Segmen",
              //               width: 'resolve',
              //               allowClear : true,
              //               ajax: {
              //                   type: 'POST',
              //                   url:base_url+"master/get_segmen",
              //                   dataType: "json",
              //                   data: function (params) {
              //                       return {
              //                           q: params.term,
              //                           page: params.page,
              //                       };
              //                   },
              //                   processResults: function (data) {
              //                       return {
              //                           results: $.map(data, function(obj) {
              //                               return { id: obj.CODE, text: obj.NAME};
              //                           })
              //                       };
              //                   },
                                
              //               }
              // 	});

              $("#customer").select2({
                            placeholder: "Select Customer",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_customer",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                });

              $("#pm").select2({
                            placeholder: "Select Project Manager",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_pm",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.ID, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
              	}); 

               $("#admin").select2({
                            placeholder: "Select Project Admin",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get-admin",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.EMAIL, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                }); 

              $("#am").select2({
                            placeholder: "Select Account Manager",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"data-account-manager",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.EMAIL, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                }); 

             $("#partner").select2({
                          placeholder: "Select Partner",
                          width: 'resolve',
                          allowClear : true,
                          ajax: {
                              type: 'POST',
                              url:base_url+"master/get_partner",
                              dataType: "json",
                              data: function (params) {
                                  return {
                                      q: params.term,
                                      page: params.page,
                                  };
                              },
                              processResults: function (data) {
                                  return {
                                      results: $.map(data, function(obj) {
                                          return { id: obj.CODE, text: obj.NAME};
                                      })
                                  };
                              },
                              
                          }
              }); 

              $('#form-project').validate({
                rules: {
                      name        : "required",
                      customer    : "required",
                      segmen      : "required",
                      value       : "required",
                      start       : "required",
                      end         : "required",
                      category    : "required",
                      regional    : "required",
                      description : "required",
                    },
                    errorPlacement: function(error, element) {
                      let label = element.parent();
                      $(label).append(error);
                    }
                });             
              
            
              $(document).on("click","#active-project",(e)=>{
                  if($('#form-project').valid()){
                    let text = "Activated This Project without PM ?"; 
                    if($('#pm').val() != null &&  $('#pm').val() != '' && $('#pm').val() != ' '){
                      text = "Activated This Project ?";
                    }
                    console.log($('#pm').val());
                     bootbox.confirm({
                          message: text,
                          buttons: {
                              confirm: {
                                  label: 'Yes',
                                  className: 'btn-success'
                              },
                              cancel: {
                                  label: 'No',
                                  className: 'btn-danger'
                              }
                          },
                          callback: function (result) {
                              if(result){
                                      showLoading();
                                      $.ajax({
                                              url: base_url+'project/activated_project/<?= $project['ID_PROJECT']; ?>',
                                              type:'POST',
                                              async : true,
                                              dataType : "json",
                                              success:function(result){
                                               hideLoading();
                                               if(result.data=='success'){
                                                bootbox.alert("Success!", function(){ 
                                                window.location.href = base_url+'project/show/<?= $project['ID_PROJECT']; ?>' ;
                                                });
                                              }else{
                                                bootbox.alert("Failed!", function(){});
                                                }
                                              return result;
                                              }

                                      });
                            }
                          }
                      });
                  }
              });             


              $(document).on('click','#close-project', (e) =>{
                  $("#modal-close").modal('show');
               });

              $(document).on("click","#btn-set-close",(e)=>{
                  if($('#form-close').valid()){
                    let form = $('#form-close')[0];
                    let formData = new FormData(form);
                    showLoading();
                    $.ajax({
                            url: base_url+'project/close_project/<?= $project['ID_PROJECT'] ?>',
                            type:'POST',
                            data:  formData,
                            async : true,
                            dataType : "text",
                            contentType:false,
                            processData:false,
                            success:function(result){
                                    if(result.trim() =='success'){
                                      window.location.reload();
                                    }else{
                                      bootbox.alert("Failed!", function(){});
                                    }
                                    return result;
                            }

                    }); 
                  }
              });  

              <?php
                if(!empty($top_customer)) :
                  foreach ($top_customer as $key => $value) : ?>
                 addTopCustomer('<?= $value['DUE_DATE2'] ?>', '<?= $value['VALUE'] ?>', '<?= $value['NOTE'] ?>','<?= $value['PROGRESS'] ?>')
               <?php 
                  endforeach;
                else : 
                ?>
                addTopCustomer();
              <?php endif; ?>

              <?php
                if(!empty($partner)) :
                  foreach ($partner as $key => $value) : ?>
                  <?php if(!empty($value['DOC_SPK'])) : ?>
                    $('#spk<?= $key ?>').fileinput(
                        {
                        initialPreview: [
                            base_url+"assets/document/<?= $project['ID_PROJECT'] ?>/<?= $value['DOC_SPK'] ?>",
                        ],
                        initialPreviewAsData: true,
                        initialPreviewConfig: [
                            {type: "pdf", url: base_url+"assets/document/<?= $project['ID_PROJECT'] ?>/<?= $value['DOC_SPK'] ?>", downloadUrl: false, width: '50px !important', caption : "<?= $value['SPK'] ?>", frameClass :"spk-file-preview"}, 
                        ],
                        purifyHtml: true, 
                        autoReplace: true,
                        maxFileCount: 1,
                        overwriteInitial: true,
                        initialPreviewShowDelete : false,                   
                        showRemove:false,
                        showUpload:false,
                        showBrowse:false,
                        showCaption:false
                        }
                      ); 
                  <?php else: ?>               
                  <?php endif; ?>               
              <?php endforeach; endif; ?>

           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>