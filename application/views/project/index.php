<link href="<?= base_url(); ?>assets/css/datatable.css" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-4">
						<h4 class="card-title mb-0">PROJECT <strong><?= $type == 'no_pm' ? 'NON PM' : str_replace('_', ' ', strtoupper($type)) ?></strong></h4>
						<div class="small text-muted"><?= $user['division'] ?></div>
					</div>

					<div class="col-sm-8" style="margin-bottom: 15px;">
						<div class="btn-group btn-group-toggle float-right">
							<a class="selectTable btn btn-outline-secondary <?= (strtolower($type)=='active') || $type == '' ? 'active' :''; ?>" data-id="active" href="<?= base_url(); ?>project-data/active">
							 Active
							</a>
							<a class="selectTable btn btn-outline-secondary <?= strtolower($type)=='no_pm' ? 'active' :''; ?>" data-id="no_pm"  href="<?= base_url(); ?>project-data/no_pm"> 
							 Non PM
							</a>
							<a class="selectTable btn btn-outline-secondary <?= strtolower($type)=='candidate' ? 'active' :''; ?>" data-id="candidate"  href="<?= base_url(); ?>project-data/candidate">
							 Candidate
							</a>
							<a class="selectTable btn btn-outline-secondary <?= strtolower($type)=='closed' ? 'active' :''; ?>" data-id="closed" href="<?= base_url(); ?>project-data/closed" >
							 Closed
							</a>
						</div>
					</div>
				
					<div class="col-sm-12" id="table_content">
						<?php
							if(empty($type)){
								$type = "Active";
							}
							switch ($type) {
								case 'closed':
									// echo "not avaliable";
									$this->load->view("project/table/closed");
									break;
								case 'no_pm':
									// echo "not avaliable";
									$this->load->view("project/table/no_pm");
									break;
								case 'candidate':
									// echo "not avaliable";
									$this->load->view("project/table/candidate");
									break;
								default:
									$this->load->view("project/table/active");
									break;
							}
						?>
					</div>
				</div>


			</div>
	

			<div class="card-footer">
				<a class="btn btn-primary btn-xs btn-addon btn-prime" href="<?= base_url() ?>download-list-project/<?= $type; ?>">
                  <i class="fa fa-download"></i>
                  <span class="pr-2">DOWNLOAD</span>
                </a>
			</div>

		</div>
	</div>
</div>