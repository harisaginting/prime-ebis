<style type="text/css">
  .label-field{
    font-size: 0.7em;
    color: #948c8c !important;
    margin: 0px !important;
  }
</style>
<div class="row mb-2">
  <div class="col-md-12">
      <button class="btn-disabled btn-md btn-addon btn-prime pull-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
      <i class="fa fa-filter"></i><strong>&nbsp;&nbsp;&nbsp;Filter&nbsp;&nbsp;&nbsp;</strong></button>
    </button>
  </div>
</div>

<div class="row collapse mb-3 pt-2" id="collapseExample" style="background-color: #00000029;border-radius: 5px;">
  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">DIVISION PROJECT</label>
    <select class="form-control project-filter" id="project-division">
      <?php if($user['division'] == 'EBIS') :?>
        <option></option>
        <option value="DBS">DBS</option>
        <option value="DES">DES</option>
        <option value="DGS">DGS</option>
      <?php else :  ?>
        <option value="<?= $user['division'] ?>" seleced><?= $user['division'] ?></option>
      <?php endif;  ?>
    </select>
  </div>


  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">REGIONAL PROJECT</label>
    <select class="form-control project-filter" id="project-regional">
      <option></option>
      <?php foreach ($list_regional as $key => $value) : ?>
        <?php if($value["VALUE"] != "Head Office") : ?>
        <option value="<?= $value["ID"] ?>" <?= "Regional ".$regional == $value["VALUE"] ? 'selected' : '' ?> style="text-transform: uppercase;"><?= $value["VALUE"] ?></option>
        <?php endif; ?>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">SCALE PROJECT</label>
    <select class="form-control project-filter" id="project-scale">
      <option></option>
      <option value="BIG">BIG</option>
      <option value="MEGA">MEGA</option>
      <option value="ORDINARY">ORDINARY</option>
      <option value="REGULAR">REGULAR</option>
    </select>
  </div>

  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">PROGRESS PROJECT</label>
    <select class="form-control project-filter"  id="project-progress">
      <option></option>
      <option value="LEAD">LEAD</option>
      <option value="LAG">LAG</option>
      <option value="DELAY">DELAY</option>
    </select>
  </div>

  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">PARTNER PROJECT</label>
    <select class="form-control project-filter"  id="project-partner">
    </select>
  </div>

  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">SEGMEN PROJECT</label>
    <select class="form-control project-filter"  id="project-segmen">
    </select>
  </div>
  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">CUSTOMER PROJECT</label>
    <select class="form-control project-filter"  id="project-customer">
    </select>
  </div> 

  <div class="form-group col-md-3 col-sm-6">
    <label class="label-field">PROJECT GROUP</label>
    <select class="form-control project-filter"  id="project-group">
      <option></option>
      <option value="NON CONNECTIVITY">NON CONNECTIVITY</option>
      <option value="CONNECTIVITY">CONNECTIVITY</option>
      <option value="CONNECTIVITY + NON CONNECTIVITY">CONNECTIVITY + NON CONNECTIVITY</option>
    </select>
  </div> 

</div>

<table id="dataActive" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
	<thead>
		<tr>
			<th style="width: 60%">NAME</th>
			<th style="width: 15%;text-align: center;">VALUE<small>(IDR)</small> </th>
			<th style="width: 25%">PROGRESS</th>
		</tr>
	</thead>
	 <tbody>
	</tbody>
</table>

<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataActive').DataTable({
                  initComplete: function(settings, json) {
                        $('.rupiah').priceFormat({
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.',
                            centsLimit: 0
                        });
                        $(function () {
                          $('[data-toggle="tooltip"]').tooltip()
                        });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'desc'],
                    ajax: { 
                        'url'  :base_url+'datatable/project_active', 
                        'type' :'POST',
                        'data' : {
                                  scale     : $('#project-scale').val(),
                                  progress  : $('#project-progress').val(),
                                  partner   : $('#project-partner').val(),
                                  regional  : $('#project-regional').val(),
                                  segmen    : $('#project-segmen').val(),
                                  customer  : $('#project-customer').val(),
                                  division  : $('#project-division').val(),
                                  group     : $('#project-group').val(),
                                  priority  : "<?= $priority ?>"
                                  }   
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){
                                    let w         = "";
                                    let note      = "";
                                    let pm        = ""; 
                                    let am        = "";
                                    let strategic = "";
                                    
                                    if (obj.IS_STRATEGIC == '1') {
                                      strategic = "<span class='badge badge-strategic'>project strategic</span>";
                                    }

                                    // if((obj.PROGRESS=='DELAY'||obj.PROGRESS=='LAG')){
                                    //   if(obj.REASON_OF_DELAY == ""||obj.REASON_OF_DELAY == null){
                                    //     w = "&nbsp;&nbsp;&nbsp;<br><span style='font-size:14px;' class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='right' data-original-title='Please fill reason of delay (Symptom)' aria-describedby='tooltip549771'></span>";
                                    //   }else{
                                    //     w = "<span class='text-primary'><br>SYMPTOM <span class='text-danger'>"+obj.REASON_OF_DELAY+"</span></span>";
                                    //   }
                                      
                                    // }   
                                    if((obj.CUSTOMER_NAME != null) && (obj.CUSTOMER_NAME != '') && (obj.CUSTOMER_NAME != 'null')){
                                      cs = "[CUSTOMER &nbsp;<strong class='text-dark' >"+obj.CUSTOMER_NAME+"</strong>]";
                                    };
                                    
                                    if((obj.REMARKS != null) && (obj.REMARKS != '') && (obj.REMARKS != 'null')){
                                      note = "<br><span class='text-muted'>"+obj.REMARKS+"</span>";
                                    };

                                    if (obj.PM_NAME !== null) {
                                      pm = "[PROJECT &nbsp;MANAGER <strong class='text-dark' >"+obj.PM_NAME+"</strong>]";
                                    }
                                    
                                    if(obj.AM_NAME !== null){
                                        am = "<br>[ACCOUNT MANAGER <strong class='text-dark' >"+obj.AM_NAME+"</strong>]";
                                    }


                                    let id = "<label class='badge badge-info' style='font-size:12px;'><strong class='text-white'>"+obj.DIVISION+" </strong>"+obj.SCALE+"</label>"+strategic;
                                    return "<div style='padding: 5px 10px;'>"+id+"<div style='margin-top:-8px;'><span class='project-text'>"+obj.NAME+"</span></div>"+"<div class='text-primary' style='font-size:12px;' >[SEGMEN <strong class='text-dark' >"+obj.SEGMEN+"</strong>] "
                                      + am
                                      +"<br>"+cs+"<br>"+pm+" "+note+w+"</div></div>";   
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='w-100'><strong class='pull-right rupiah'>"+obj.VALUE+"</strong></div>" 
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                    let plan  = 0;
                                    let ach   = 0;
                                    if(obj.TOTAL_PLAN != '' && obj.TOTAL_PLAN != null){
                                      plan = obj.TOTAL_PLAN;
                                    }
                                    if(obj.TOTAL_ACHIEVEMENT != '' && obj.TOTAL_ACHIEVEMENT != null){
                                      ach = obj.TOTAL_ACHIEVEMENT;
                                    }

                                    let status = "<span class='btn btn-warning btn-sm mb-2' style='font-size:0.6em !important;'>LAG</span>";

                                    if (obj.PROGRESS == 'LEAD') {
                                      status = "<span class='btn btn-success btn-sm mb-2' style='font-size:0.6em !important;'>LEAD</span>";
                                    }

                                    if (obj.PROGRESS == 'DELAY') {
                                      status = "<span class='btn btn-danger btn-sm mb-2 pull-right' style='font-size:0.6em !important;'>DELAY</span>";
                                    }


                                    let b_status = "<div class='w-100 clearfix'>"+status+"</div>";

                                    let period = "<div class='w-100' style='font-size:0.8em;font-weight:700;text-align:center;border-bottom:1px solid black;margin-bottom:5px;'><span class='pull-left'>"+obj.START_DATE+"</span>-<span class='pull-right'>"+obj.END_DATE+"</span></div>";
                                    return  b_status + period + "<div class='clearfix'>"
                                            +"<div class='float-left'>"
                                            +"<strong>"+plan+"%</strong>"
                                            +"</div>"
                                            +"<div class='float-right'>"
                                            +"<small class='text-muted'>PLAN THIS WEEK</small>"
                                            +"</div>"
                                            +"</div>"
                                            +"<div class='progress progress-xs'>"
                                            +"<div class='progress-bar bg-info' role='progressbar' "
                                            +"style='width: "+plan+"%' aria-valuenow='"+plan+"' aria-valuemin='0' "
                                            +"aria-valuemax='100'></div>"
                                            +"</div>"
                                            +"<div class='clearfix'>"
                                            +"<div class='float-left'>"
                                            +"<strong>"+ach+"%</strong>"
                                            +"</div>"
                                            +"<div class='float-right'>"
                                            +"<small class='text-muted'>ACHIEVEMENT</small>"
                                            +"</div>"
                                            +"</div>"
                                            +"<div class='progress progress-xs'>"
                                            +"<div class='progress-bar bg-success' role='progressbar' "
                                            +"style='width: "+ach+"%' aria-valuenow='"+ach+"' aria-valuemin='0' "
                                            +"aria-valuemax='100'></div>"
                                            +"</div>"
                                            ; 
                            }            
                                    
                        }, 


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          var a = null;
                          if(aData['WEIGHT']==0){
                            $(nRow).addClass('disabled')  
                          }else{
                            $(nRow).addClass( aData['INDICATOR'] );  
                          }  
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'p/'+aData['ID_PROJECT']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            $("#project-division").select2({
              placeholder: "All DIVISION",
              width: 'resolve',
              <?php if($user['division'] == 'EBIS') :?>
              allowClear : true
              <?php endif; ?>
            });

            $("#project-scale").select2({
              placeholder: "ALL SCALE",
              width: 'resolve',
              allowClear : true
            });

            <?php if (!empty($scale)): ?>
              $('#project-scale').val("<?= $scale ?>").trigger('change');
            <?php endif; ?>

            $("#project-regional").select2({
              placeholder: "ALL REGIONAL",
              width: 'resolve',
              allowClear : true
            });

            <?php if (!empty($regional)): ?>
              $('#project-regional').val("<?= $regional ?>").trigger('change');
            <?php endif; ?>

            $("#project-progress").select2({
              placeholder: "ALL PROGRESS",
              width: 'resolve',
              allowClear : true
            });

            <?php if (!empty($progress)): ?>
              $('#project-progress').val("<?= $progress ?>").trigger('change');
            <?php endif; ?>

            $("#project-group").select2({
              placeholder: "ALL GROUP",
              width: 'resolve',
              allowClear : true
            });

            $("#project-customer").select2({
                    placeholder: "ALL CUSTOMER",
                    width: 'resolve',
                    allowClear : true,
                    ajax: {
                        type: 'POST',
                        url:base_url+"master/get_customer",
                        dataType: "json",
                        data: function (params) {
                            return {
                                q: params.term,
                                page: params.page,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.CODE, text: obj.NAME};
                                })
                            };
                        },
                        
                    }
                });

            $("#project-segmen").select2({
                      placeholder: "ALL SEGMEN",
                      width: 'resolve',
                      allowClear : true,
                      ajax: {
                          type: 'POST',
                          url:base_url+"master/get_segmen",
                          dataType: "json",
                          data: function (params) {
                              return {
                                  q: params.term,
                                  page: params.page,
                              };
                          },
                          processResults: function (data) {
                              return {
                                  results: $.map(data, function(obj) {
                                      return { id: obj.CODE, text: obj.NAME};
                                  })
                              };
                          },
                          
                      }
              });

            $("#project-partner").select2({
                          placeholder: "ALL PARTNER",
                          width: 'resolve',
                          allowClear : true,
                          ajax: {
                              type: 'POST',
                              url:base_url+"master/get_partner",
                              dataType: "json",
                              data: function (params) {
                                  return {
                                      q: params.term,
                                      page: params.page,
                                  };
                              },
                              processResults: function (data) {
                                  return {
                                      results: $.map(data, function(obj) {
                                          return { id: obj.CODE, text: obj.NAME};
                                      })
                                  };
                              },
                              
                          }
              }); 

             <?php if (!empty($partner)): ?>
              var d_partner = new Option("FILTERED PARTNER", "<?= $partner ?>", false, false);
              $('#project-partner').append(d_partner).trigger('change');
            <?php endif; ?>
            
            tableInit();

            $(document).on('change','.project-filter', function (e) {
              e.stopImmediatePropagation();
              $('#dataActive').dataTable().fnDestroy();
              tableInit();
              });

            $(document).on('click','.row_links',function(e){
            	e.stopImmediatePropagation();
            	console.log($(this).data('link'));
            	window.location = $(this).data('link');
            });

           }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>