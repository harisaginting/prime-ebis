
<div class="row">
 <div class="col-md-12">
   <div class="float-right" style="margin-bottom: 10px;">
        <a class="btn btn-primary btn-md btn-addon btn-prime" href="<?= base_url(); ?>project-add">
          <i class="fa fa-plus"></i>
          <span>add project &nbsp;</span>
        </a>
    </div>
 </div>
</div>

<table id="dataActive" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
	<thead>
		<tr>
			<th style="width: 80%">NAME</th>
			<th style="width: 20%" class="text-center">VALUE</th>
		</tr>
	</thead>
	 <tbody>
	</tbody>
</table>

<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataActive').DataTable({
                  initComplete: function(settings, json) {
                                $('.rupiah').priceFormat({
                                    prefix: '',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    centsLimit: 0
                                });
                                $(function () {
                                  $('[data-toggle="tooltip"]').tooltip()
                                });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'desc'],
                    ajax: { 
                        'url'  :base_url+'datatable/project_candidate', 
                        'type' :'POST',
                        'data' : {
                                  status  : $('#status').val(),
                                  pm      : $('#pm').val(),
                                  customer: $('#customer').val(),
                                  regional: $('#regional').val(),
                                  type    : $('#type').val(),
                                  mitra   : $('#mitra').val(),
                                  segmen  : $('#segmen').val()
                                  }   
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){
                                    let segmen = "";
                                    if (obj.SEGMEN_NAME != "" && obj.SEGMEN_NAME != "null" && obj.SEGMEN_NAME != null) {
                                      segmen = " -"+obj.SEGMEN_NAME;
                                    }
                                    let project =  "<div style='width:65%'><div class='w-100'><small><strong>"+obj.DIVISION+segmen+"</strong></small></div><div class='w-100'><strong>"+obj.CUSTOMER_NAME+"</strong><small class='ml-2'>"+obj.KB+"</small></div><div class='w-100 pl-2'>"+obj.NAME+"</div></div>";

                                    let info = "<div style='width:35%;border-left:1px solid #ddd;' class='pl-1'><div class='row'><div class='col-2'>AM</div><div class='col-10'>: "+obj.AM_NAME+"</div></div><div class='row'><div class='col-2'>PM</div><div class='col-10'>: "+obj.PM_NAME+"</div></div></div>";

                            return "<div class='w-100 p-1' style='display:flex;'>"+project+info+"</div>"
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='rupiah' style='font-size:12px;width:100%;text-align:right;padding-right:5px;font-family:sans-serif;font-weight:900;'>"+obj.VALUE+"</div>";    
                            }            
                                    
                        },  


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          var a = null;
                          if(aData['WEIGHT']==0){
                            $(nRow).addClass('disabled')  
                          }else{
                            $(nRow).addClass( aData['INDICATOR'] );  
                          }  
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'p/'+aData['ID_PROJECT']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            tableInit();
            $(document).on('change','.Jselect2Active', function (e) {
              e.stopImmediatePropagation();
              $('#dataku').dataTable().fnDestroy();
              });

            $(document).on('click','.row_links',function(e){
            	e.stopImmediatePropagation();
            	console.log($(this).data('link'));
            	window.location = $(this).data('link');
            })

           }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>