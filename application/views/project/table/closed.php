
<table id="dataClosed" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
  <thead>
    <tr>
      <th style="width: 50%">NAME</th>
      <th style="width: 25%">VALUE</th>
      <th style="width: 20%">DATE CLOSED</th>
    </tr>
  </thead>
   <tbody>
  </tbody>
</table>

<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataClosed').DataTable({
                  initComplete: function(settings, json) {
                                $('.rupiah').priceFormat({
                                    prefix: '',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    centsLimit: 0
                                });
                                $(function () {
                                  $('[data-toggle="tooltip"]').tooltip()
                                });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'desc'],
                    ajax: { 
                        'url'  :base_url+'datatable/project_closed', 
                        'type' :'POST',
                        'data' : {
                                  status  : $('#status').val(),
                                  pm      : $('#pm').val(),
                                  customer: $('#customer').val(),
                                  regional: $('#regional').val(),
                                  type    : $('#type').val(),
                                  mitra   : $('#mitra').val(),
                                  segmen  : $('#segmen').val()
                                  }   
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){
                                    var division = "<div><small><strong>"+obj.DIVISION+"</strong></small></div>";                                    
                                    return "<div class='table-division'>"+division+"<span style='table-project-name'>"+obj.NAME+"</span>";   
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='rupiah' style='font-size:12px;width:100%;text-align:right;padding-right:5px;font-family:sans-serif;font-weight:900;'>"+obj.VALUE+"</div>";    
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                    return obj.DATE_CLOSED2; 
                            }            
                                    
                        },  


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          var a = null;
                          if(aData['WEIGHT']==0){
                            $(nRow).addClass('disabled')  
                          }else{
                            $(nRow).addClass( aData['INDICATOR'] );  
                          }  
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'p/'+aData['ID_PROJECT']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            tableInit();
            $(document).on('change','.Jselect2Active', function (e) {
              e.stopImmediatePropagation();
              $('#dataku').dataTable().fnDestroy();
              });

            $(document).on('click','.row_links',function(e){
              e.stopImmediatePropagation();
              console.log($(this).data('link'));
              window.location = $(this).data('link');
            })

           }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>