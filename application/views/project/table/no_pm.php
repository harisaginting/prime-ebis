<table id="dataNonpm" class="table table-responsive-sm table-bordered" style="width: 100%;margin-top: 10px;">
	<thead>
		<tr>
			<th style="width: 50%">NAME</th>
			<th style="width: 25%">VALUE</th>
			<th style="width: 20%">PROGRESS</th>
		</tr>
	</thead>
	 <tbody>
	</tbody>
</table>

<script type="text/javascript">    
  var Page = function () {

    var tableInit = function(){                     
        var table = $('#dataNonpm').DataTable({
                  initComplete: function(settings, json) {
                                $('.rupiah').priceFormat({
                                    prefix: '',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    centsLimit: 0
                                });
                                $(function () {
                                  $('[data-toggle="tooltip"]').tooltip()
                                });
                    },
                    processing: true,
                    serverSide: true,
                    order :[0,'desc'],
                    ajax: { 
                        'url'  :base_url+'datatable/project_nonpm', 
                        'type' :'POST',
                        'data' : {
                                  status  : $('#status').val(),
                                  pm      : $('#pm').val(),
                                  customer: $('#customer').val(),
                                  regional: $('#regional').val(),
                                  type    : $('#type').val(),
                                  mitra   : $('#mitra').val(),
                                  segmen  : $('#segmen').val()
                                  }   
                        },
                    aoColumns: [
                        { 
                            'mRender': function(data, type, obj){
                                    var id = "<div><small>ID: <strong>"+obj.DIVISION+"</strong>";



                                    var w     = "";
                                    var note  = "";
                                    if((obj.STATUS=='DELAY'||obj.STATUS=='LAG')){
                                      if(obj.REASON_OF_DELAY == ""||obj.REASON_OF_DELAY == null){
                                        w = "&nbsp;&nbsp;&nbsp;<br><span style='font-size:24px;' class='fa fa-exclamation-circle text-danger' data-toggle='tooltip' data-placement='right' data-original-title='Please fill reason of delay (Symptom)' aria-describedby='tooltip549771'></span>";
                                      }else{
                                        w = "<span class='text-primary'><br>SYMPTOM <span class='text-danger'>"+obj.REASON_OF_DELAY+"</span></span>";
                                      }
                                      
                                    }   

                                    if((obj.REMARKS != null) && (obj.REMARKS != '') && (obj.REMARKS != 'null')){
                                      note = "<br><span class='text-muted'>"+obj.REMARKS+"</span>";
                                    };
                                    

                                    return "<div style='padding: 5px 10px;'>"+id+"<br><span style='font-size:12px !important;font-family:sans-serif;font-weight:800;'>"+obj.NAME+"</span>"+"<div class='text-primary' style='font-size:12px;' >[SEGMEN <strong class='text-info' >"+obj.SEGMEN+"</strong>]</div></div>";   
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                     return "<div class='w-100'><strong class='pull-right rupiah'>"+obj.VALUE+"</strong></div>" 
                            }            
                                    
                        }, 
                        { 
                            'mRender': function(data, type, obj){   
                                    return "<div class='clearfix'>"
                                            +"<div class='float-left'>"
                                            +"<strong>"+obj.TOTAL_PLAN+"%</strong>"
                                            +"</div>"
                                            +"<div class='float-right'>"
                                            +"<small class='text-muted'>PLAN</small>"
                                            +"</div>"
                                            +"</div>"
                                            +"<div class='progress progress-xs'>"
                                            +"<div class='progress-bar bg-info' role='progressbar' "
                                            +"style='width: "+obj.TOTAL_PLAN+"%' aria-valuenow='"+obj.TOTAL_PLAN+"' aria-valuemin='0' "
                                            +"aria-valuemax='100'></div>"
                                            +"</div>"
                                            +"<div class='clearfix'>"
                                            +"<div class='float-left'>"
                                            +"<strong>"+obj.TOTAL_ACHIEVEMENT+"%</strong>"
                                            +"</div>"
                                            +"<div class='float-right'>"
                                            +"<small class='text-muted'>ACHIEVEMENT</small>"
                                            +"</div>"
                                            +"</div>"
                                            +"<div class='progress progress-xs'>"
                                            +"<div class='progress-bar bg-success' role='progressbar' "
                                            +"style='width: "+obj.TOTAL_ACHIEVEMENT+"%' aria-valuenow='"+obj.TOTAL_ACHIEVEMENT+"' aria-valuemin='0' "
                                            +"aria-valuemax='100'></div>"
                                            +"</div>"
                                            ; 
                            }            
                                    
                        },  


                       ],  
                       fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                          var a = null;
                          if(aData['WEIGHT']==0){
                            $(nRow).addClass('disabled')  
                          }else{
                            $(nRow).addClass( aData['INDICATOR'] );  
                          }  
                          $(nRow).addClass('row_links');
                          $(nRow).data('link',base_url+'p/'+aData['ID_PROJECT']); 
                          return nRow;
                          }    
                });  
    };    
      return {
          init: function() { 
            tableInit();
            $(document).on('change','.Jselect2Active', function (e) {
              e.stopImmediatePropagation();
              $('#dataku').dataTable().fnDestroy();
              });

            $(document).on('click','.row_links',function(e){
            	e.stopImmediatePropagation();
            	console.log($(this).data('link'));
            	window.location = $(this).data('link');
            })

           }
      };

  }();

  jQuery(document).ready(function() {
      Page.init();
  });       
           
</script>