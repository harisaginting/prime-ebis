<link href="<?= base_url(); ?>assets/css/project-detail.css?v=<?= date('s')?>" rel="stylesheet"></link>
<div class="container-fluid mt-2 mb-2">
	<div class="row container-view pb-5">
		<!-- TITLE -->
		<div class="col-md-12 text-center mt-4 pl-5 pr-5">
			<h6 class="mb-0 text-muted">TELKOM INDONESIA - <?= strtoupper($project['DIVISION']); ?></h6>
			<h3><?= strtoupper($project['NAME']); ?></h3>
		</div>

		<!-- ID -->
		<div class="col-md-10 offset-md-1 col-sm-12">
			<div class="row">
				<div class="col-md-3 col-sm-12"><small class="text-muted">ID LOP : <strong><?= !empty($project['ID_PROJECT_GLOBAL'])  ? $project['ID_PROJECT_GLOBAL'] : "-"?></strong> </small></div>
				<div class="col-md-3 col-sm-12"><small class="text-muted">SID : <strong><?= !empty($project['SID']) ? $project['SID'] : "-" ?></strong> </small></div>
				<div class="col-md-3 col-sm-12"><small class="text-muted">NO. NCX : <strong><?= !empty($project['NO_NCX']) ? $project['NO_NCX'] : '-' ?></strong> </small></div>
				<div class="col-md-3 col-sm-12"><small class="text-muted">NO. PROJECT : <strong><?= !empty($project['NO_PROJECT']) ? $project['NO_PROJECT'] : "-" ?></strong> </small></div>
			</div>
		</div>

		<!-- DESCRIPTION -->
		<div class="col-md-12 mt-3 mb-5">
			<small class="text-muted">DESCRIPTION :</small><br>
			<?= $project['DESCRIPTION']; ?>
		</div>

		<!-- DETAIL -->
		<div class="col-md-7">
			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">VALUE</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value rupiah"><?= $project['VALUE'];?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">CUSTOMER</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= $project['CUSTOMER_NAME'];?><small class="text-muted"><br>[<?= $project['CUSTOMER_CODE'];?>]</small> </div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">SEGMEN</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= $project['SEGMEN'];?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white pt-1">NOMOR KONTRAK BERLANGGANAN</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= !empty($project['KB']) ? $project['KB'] : "-";?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">CATEGORY</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail" >
					<div class="detail-value" style="margin-top: -3px;"><?= $project['CATEGORY_NAME']." <br> ".$project['GROUP_TYPE'];?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">START DATE</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= $project['START_DATE2'];?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">END DATE</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= $project['END_DATE2'];?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">KICK OFF DATE</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= !empty($project['DATE_KICK_OFF2']) ? $project['DATE_KICK_OFF2'] : "-";?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white">REGIONAL</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= $project['REGIONAL_NAME'];?></div>
				</div>
			</div>

			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white pt-1">MONITORING <br>REQUEST DATE</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value"><?= !empty($project['DATE_MONITORING_REQUEST2']) ? $project['DATE_MONITORING_REQUEST2'] : "-";?></div>
				</div>
			</div>
			<div class="row pl-3 pr-3">
				<div class="detail-param col-md-4 col-sm-12 border-detail">
					<div class="detail-param-value text-white pt-1">STATUS</div>
				</div>
				<div class="col-md-8 col-sm-12 bg-grey border-detail">
					<div class="detail-value" style="margin-top: -3px;"><?= $project['STATUS'] == "ACTIVE" ? $project['STATUS']."<br>".$project['PROGRESS']." PROGRESS" : 
					($project['STATUS'] == "CLOSED" ? $project['STATUS']."<br>".$project['DATE_CLOSED2'] : $project['STATUS']);?></div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
				<div class="container-person mt-1">
					<div class="row">
						<div class="col-sm-4 person-img-container">
							<div class="pl-2" style="width: 90%;">
								<img class="img-avatar person-avatar" style="width: 100%;height: 100%;" src="https://prime.telkom.co.id/sdv/<?= !empty($pm['AVATAR'])? $pm['AVATAR'] : '../user_picture/default-profile-picture.png' ; ?>" alt="<?= !empty($pm['NAME']) ? $pm['NAME'] : "PM"; ?>">
							</div>
						</div>
						<div class="col-sm-8 pt-3">
							<div class="w-100 text-white"><small>PROJECT MANAGER</small></div>
							<div class="w-100 person-name text-white"><?= !empty($pm['NAME']) ?  strtoupper($pm['NAME']) : "-" ?></div>
							<div class="w-100">
								<strong><?= !empty($pm['EMAIL']) ? '<span class="fa fa-envelope"></span> '.$pm['EMAIL'] : "-" ?></strong>
							</div>
						</div>
					</div>
				</div>

				<div class="container-person mt-2">
					<div class="row">
						<div class="col-sm-4 person-img-container">
							<div class="pl-2" style="width: 90%;">
								<img class="img-avatar person-avatar" style="width: 100%;height: 100%;" src="https://prime.telkom.co.id/sdv/<?= !empty($am['AVATAR'])? $am['AVATAR'] : '../user_picture/default-profile-picture.png' ; ?>" alt="<?= !empty($am['NAME']) ? $am['NAME'] : "AM"; ?>">
							</div>
						</div>
						<div class="col-sm-8 pt-3">
							<div class="w-100 text-white"><small>ACCOUNT MANAGER</small></div>
							<div class="w-100 person-name text-white"><?= !empty($am['NAME']) ?  strtoupper($am['NAME']) : "-" ?></div>
							<div class="w-100">
								<strong><?= !empty($am['EMAIL']) ? '<span class="fa fa-envelope"></span> '.$am['EMAIL'] : '-' ?></strong>
							</div>
						</div>
					</div>
				</div>

				<div class="container-person mt-2">
					<div class="row">
						<div class="col-sm-4 person-img-container">
							<div class="pl-2" style="width: 90%;">
								<img class="img-avatar person-avatar" style="width: 100%;height: 100%;" src="https://prime.telkom.co.id/sdv/<?= !empty($pm_admin['AVATAR'])? $pm_admin['AVATAR'] : '../user_picture/default-profile-picture.png' ; ?>" alt="<?= !empty($pm_admin['NAME']) ? $pm_admin['NAME'] : "PM ADMIN"; ?>">
							</div>
						</div>
						<div class="col-sm-8 pt-3">
							<div class="w-100 text-white"><small>PROJECT ADMIN</small></div>
							<div class="w-100 person-name text-white"><?= !empty($pm_admin['NAME']) ? $pm_admin['NAME'] :  "-" ?></div>
							<div class="w-100">
								<strong><?= !empty($pm_admin['NAME']) ? '<span class="fa fa-envelope"></span> '.$pm_admin['NAME'] : "" ?></strong>
							</div>
						</div>
					</div>
				</div>


				<div class="w-100 mt-3">
	                <strong class="h6 fw-700 text-black">DOCUMENT</strong>
	                <?php if(!empty($project['document'])) : ?>
	                <div class="w-100">
	                    <ul class="list-group scroll-thin" style="max-height: 160px !important;overflow-y: scroll;">
	                      <?php foreach ($project['document'] as $key => $value) : ?>
	                          <a target="_blank" href="<?= !empty($value['PATH']) ? base_url().$value['PATH']  : base_url().'/assets/document/'.$project['ID_PROJECT'].'/'.$value['NAME'] ?>" ><li class="list-group-item" style="padding: 0.2rem 1.25rem;"><i class="fa fa-file mr-2 text-success"></i> <?= $value['NAME'] ?></li></a>
	                      <?php endforeach; ?>
	                    </ul>
	                </div>
	                <?php else :?>
	                	 <br><small class="text-muted">No Data</small>
	                <?php endif;?>
	              </div>
				          	

		          	<div class="w-100 mt-2">
		                <div class="row">
		                	<div class="col-12 mb-0">
								<div style="float: left;margin-top: 7px;">
									<strong class="h6 fw-700 text-black">SYMPTOMS</strong>
								</div>
								<?php if($project['PROGRESS'] != 'LEAD'  &&  $project['STATUS'] == 'ACTIVE' && !empty($p_access["U"])) : ?> 
								<button class="btn btn-sm btn-info btn-prime pull-right circle btn-warning" type="button" id="btn-symptoms">
									<i class="fa fa-edit"></i>
								</button>
								<?php endif; ?>
			                </div>
			                <div class="col-12">
			                	<select multiple="true" class="form-control Jselect2" name="project-symptoms[]" id="project-symptoms">
								  <?php foreach ($list_symptom as $key => $value) : ?> 
								  	<option value="<?= $value['ID'] ?>"><?= $value['VALUE']." (".$value['REMARKS'].")" ?></option>
								  <?php endforeach; ?>


					              <option value="Delivery Barang/Jasa (Mitra)">Delivery Barang/Jasa (Mitra)</option>
					              <option value="Kesiapan Lokasi (Customer)">Kesiapan Lokasi (Customer)</option>
					              <option value="Perubahan Design/Spesifikasi (Customer)">Perubahan Design/Spesifikasi (Customer)</option>
					              <option value="Keterlambatan BAST (Customer)">Keterlambatan BAST (Customer)</option>
					              <option value="Keterlambatan SPK ke Mitra (Telkom)">Keterlambatan SPK ke Mitra (Telkom)</option>
					              <option value="Masalah Administrasi dan Pembayaran (Telkom)">Masalah Administrasi dan Pembayaran (Telkom)</option>
					              <option value="SoW Belum Sepakat (Telkom)">SoW Belum Sepakat (Telkom)</option>
					            </select>
			                </div>
		                </div>
		            </div>
				        	
		</div>
		<!-- END DETAIL -->
	
		<!-- TOP -->
		<div class="col-md-6 mt-4 col-sm-12">
			<div class="row">
				<div class="col-12">
					<strong class="h6 fw-700 text-black">TERM Of PAYMENT CUSTOMER</strong>
				</div>
				<div class="col-12">
					<?php if(!empty($top_customer)) : ?>
					<table class="table table-grey">
						<thead>
							<th style="width: 25%;">DUE DATE</th>
							<th style="width: 35%;">VALUE</th>
							<th style="width: 40%;">DESCRIPTION</th>
						</thead>
						<tbody>
							<?php foreach ($top_customer as $key => $value) : ?>
								<tr>
									<td><?= $value['DUE_DATE2']?></td>
									<td class="rupiah"><?= $value['VALUE']?></td>
									<td><?= $value['NOTE']?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<?php else: ?>
		                <small class="text-muted">No Data</small>
		            <?php endif; ?>
				</div>	
			</div>
		</div>

		<div class="col-md-6 mt-4 col-sm-12">
			<div class="row">
				<div class="col-12">
					<strong class="h6 fw-700 text-black">TERM Of PAYMENT MITRA</strong>
				</div>
				<div class="col-12">
					<?php if(!empty($partner)) : ?>
		                <?php foreach ($partner as $key => $value1) : ?>
		                      <div class="row">
		                        <div class="col-12" style="margin-bottom: -5px;"><strong class="text-primary"><?= $value1['PARTNER_NAME'] ?></strong></div>
		                        <div class="col-12" style="margin-bottom: -6px;">
		                        	<small>
		                        		SPK (P8) : 
			                        	<strong><?= $value1['SPK'] ?></strong> 
			                        	<?php if(!empty($value1['DOC_SPK'])) : ?> 
			                        		<a href="<?= base_url().'assets/document/'.$project['ID_PROJECT'].'/'.$value1['DOC_SPK'] ?>"  target="_blank" class="btn btn-sm btn-xm btn-success"><i class="fa fa-external-link"></i></a>
			                        	<?php endif; ?> 
		                        	</small>
		                        </div>
		                        <div class="col-12">
		                        	<small>
			                        	KL : <span class="rupiah"><?= $value1['VALUE'] ?></span>
		                        	</small>
		                        </div>
		                      </div>
		                      <table class="table table-grey col-12">
		                          <thead>
		                            <th>DUE DATE</th>
		                            <th>VALUE</th>
		                            <th>DESCRIPTION</th>
		                          </thead>
		                          <tbody>
		                            <?php foreach ($value1['TOP'] as $key1 => $top1) : ?>
		                              <tr>
		                                <td><?= $top1['DUE_DATE2'] ?></td>
		                                <td class="rupiah"><?= $top1['VALUE'] ?></td>
		                                <td><?= $top1['NOTE'] ?></td>
		                              </tr>
		                            <?php endforeach; ?>
		                          </tbody>
		                      </table>
		                <?php endforeach; ?>
		            <?php else: ?>
		                <small class="text-muted">No Data</small>
		            <?php endif; ?>
				</div>
			</div>
		</div>
		<!-- END -->

		<!-- CHART -->
		<?php if ($project["STATUS"] != "CANDIDATE") : ?>
		<div class="col-12 mt-3">
			<strong class="h6 fw-700 text-black">S - CURVE</strong>
		</div>
		<ul class="nav nav-tabs nav-chart">
			<li class="nav-item" style="">
				<a class="nav-link nav-link-chart active show" data-toggle="tab" href="#container-chart-total-progress" role="tab" aria-controls="deliverables" aria-selected="true">
				 COMULATIVE
				</a>
			</li>

			 <?php foreach ($partner as $key => $value) : ?>
				<li class="nav-item">
					<a class="nav-link nav-link-chart" data-toggle="tab" href="#container-chart-<?= $value['ID_PARTNER'] ?>" role="tab" aria-controls="deliverables" aria-selected="true">
					 <?= $value['PARTNER_NAME'] ?>
					</a>
				</li>
			<?php endforeach ?>
		</ul>
		<div class="col-12 tab-content" style="border:0px !important;"> 
			<div class="tab-pane w-100 active show pb-1 pt-1" id="container-chart-total-progress" role="tabpanel">
					<div class="w-100" id="chartProgress"></div>
					<div class="w-100 hidden" id="chartProgressMonth"></div>
			</div>
			<?php foreach ($partner as $key => $value) : ?>
				<div class="tab-pane w-100" id="container-chart-<?= $value['ID_PARTNER'] ?>" role="tabpanel">
						<div class="w-100" id="chartProgress<?= $value['ID_PARTNER'] ?>"></div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="col-md-12 pr-2">
			<div class="row pr-4">
				<div class="col-12">
					<button class="btn btn-sm btn-primary pull-right ml-1 mb-2" id="chart-scale-month">
						<strong>MONTH</strong>
					</button>
					<button class="btn btn-sm active btn-primary pull-right mb-2" id="chart-scale-week">
						<strong>WEEK</strong>
					</button>
				</div>
			</div>
		</div>
		<!-- END CHART -->


		<!-- DELIVERABLE -->
		<div class="col-sm-12 mt-2" style="padding-left: 0px;padding-right: 0px;">
						<!-- DELIVERABLES -->
						<ul class="nav nav-tabs nav-deliverable" role="tablist">
							<li class="nav-item" style="">
								<a class="nav-link nav-link-deliverable active show" data-toggle="tab" href="#deliverables" role="tab" aria-controls="deliverables" aria-selected="true">
								 DELIVERABLE
								</a>
							</li>
							<li class="nav-item" style="">
								<a class="nav-link nav-link-deliverable" data-toggle="tab" href="#issueAp" role="tab" aria-controls="issueAp" aria-selected="false">
								 ISSUE & ACTION PLAN
								</a>
							</li>
							<li class="nav-item" style="">
								<a class="nav-link nav-link-deliverable" data-toggle="tab" href="#bast" role="tab" aria-controls="bast" aria-selected="false">
								BAST
								</a>
							</li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active show" id="deliverables" role="tabpanel">
									<?php if($project['TOTAL_WEIGHT'] < 100 && !empty($p_access["U"])) : ?>
										<div class="row">
											<div class="col-sm-12" style="margin-top: 10px;margin-bottom: 10px;padding-right: 5px !important;">
												<button id="btn-deliverable" type="button" class="pull-right btn-sm btn btn-success btn-brand" style="margin-right: 10px;"> 
													<i class="fa fa fa-plus"></i> <span style="font-size: 12px;"><strong class="pr-2"	>deliverable</strong></span>
												</button>
											</div>
										</div>
							        <?php endif; ?>
							        <?php if (!empty($deliverable)) : ?>
									<table id="datakuProjectClosed" class="table table-responsive-sm">
						              <thead >
						                <tr>
						                  <th style="width: 2%;border-right: 1px solid #29363d;"></th>
						                  <th style="width: 24%;border-right: 1px solid #29363d;">Deliverable Name</th>
						                  <th style="width: 14%;border-right: 1px solid #29363d;">Period</th>
						                  <th style="width: 22%;border-right: 1px solid #29363d;">Description</th>
						                  <th style="width: 10%;border-right: 1px solid #29363d;">Weight</th>
						                  <th style="width: 25%;border-right: 1px solid #29363d;">Achievement</th>
						                  <th style="width: 5%;text-align: center;"></th>
						                </tr>
						              </thead> 
						              <tbody>
						              	<?php foreach ($deliverable as $key => $value) : ?>
										<tr>
											<td><small><?= $key+1 ?></small></td>
											<td><small><strong><?= $value['PIC_NAME']?></strong></small><br><?= $value['NAME']?></td>
											<td>
												<div class="w-100" style="margin-top: -5px;"><small>start</small></div>
												<div class="w-100"><strong><?= $value['START_DATE2']?></strong></div>
												<div class="w-100"><small>end</small></div>
												<div class="w-100"><strong><?= $value['END_DATE2']?></strong></div>
											</td>
											<td><?= $value['DESCRIPTION']?></td>
											<td><strong><?= number_format((float)$value['WEIGHT'], 2, '.', '') ?></strong>%</td>
											<td>

												<?php $total_achievement = 0;  ?>
												<?php if(!empty($achievement[$value['ID']])) :  ?>
												<?php foreach ($achievement[$value['ID']] as $key1 => $value1) : ?>
													<div class="row mt-1">
														<div class="col-8">
															<?php if (!empty($p_access["U"])) : ?>
															<button data-id="<?= $value1['ID']?>" data-id_deliverable="<?= $value['ID'] ?>" class="btn btn-sm btn-xm btn-danger mr-1 btn-delete-progress" data-achievement="<?= $value1['ACHIEVEMENT'] ?>" data-progress_date="<?= $value1['DATE_ACHIEVED3'] ?>"><i class="fa fa-trash"></i>
															</button>
															<?php endif; ?>
															<?= $value1['DATE_ACHIEVED2'] ?>
														</div>
														<div class="col-4"><?= $value1['ACHIEVEMENT'] ?>%</div>
													</div>

												<?php $total_achievement += $value1['ACHIEVEMENT']; endforeach;endif; ?>

												<div class="row">
													<div class="col-4 offset-8"><strong><?= $value['ACHIEVEMENT']?>%</strong></div>
												</div>
												<?php if($total_achievement < 100 && !empty($p_access["U"])) : ?>
												<div class="w-90 text-right mt-1 pr-4">
													<button class="btn btn-sm btn-success w-70 btn-progress-deliverable circle" data-start="<?= $value['START_DATE2']?>" data-id='<?= $value['ID']?>' data-name="<?= $value['NAME']?>" data-current_progress="<?= $value['ACHIEVEMENT']?>"><i class="fa fa-plus"></i></button>
												</div>
												<?php endif; ?>
											</td>
											<td>
												<?php if (!empty($p_access["U"])) : ?>
													<button class="btn btn-sm w-90 btn-warning btn-edit-deliverable" data-id="<?= $value['ID'] ?>"><i class="fa fa-edit"></i></button>
													<a href="<?= base_url() ?>project/edit_chart_deliverable/<?= $value['ID']?>" class="mt-1 btn btn-sm w-90 btn-info btn-edit-chart-deliverable" data-id="<?= $value['ID'] ?>"><i class="fa fa-line-chart"></i></a>
													<button class="mt-1 btn btn-sm w-90 btn-danger btn-delete-deliverable" data-id="<?= $value['ID'] ?>"><i class="fa fa-trash"></i></button>
												<?php else :  ?>
													<button class="btn btn-sm w-90 btn-disable"><i class="fa fa-edit"></i></button>
													<button class="mt-1 btn btn-sm w-90 btn-disable"><i class="fa fa-line-chart"></i></button>
													<button class="mt-1 btn btn-sm w-90 btn-disable"><i class="fa fa-trash"></i></button>
												<?php endif; ?>
											</td>
										</tr>
										<?php endforeach; ?>
						              </tbody>
						          	</table>
					              	<?php else : ?>
					              		<small class="text-muted">No Data</small>
					              	<?php endif; ?>
							</div>

							<div class="tab-pane" id="issueAp" role="tabpanel">
								<div class="row">
									<div class="col-sm-12" style="margin-top: 10px;margin-bottom: 10px;padding-right: 5px !important;">
										<?php if (!empty($p_access["U"]) ) : ?>
										<button id="btn-add-action" type="button" class="pull-right btn-sm btn btn-info btn-brand" style="margin-right: 10px;"> 
											<i class="fa fa fa-plus"></i> <span style="font-size: 12px;"><strong>action plan </strong></span>
										</button>
										<button id="btn-add-issue" type="button" class="pull-right btn-sm btn btn-danger btn-brand" style="margin-right: 10px;"> 
											<i class="fa fa fa-plus"></i> <span style="font-size: 12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;issue&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></span>
										</button>
										<?php else: ?>
											<button type="button" class="pull-right btn-sm btn btn-disable btn-brand" style="margin-right: 10px;"> 
												<i class="fa fa fa-plus"></i> <span style="font-size: 12px;"><strong>action plan </strong></span>
											</button>
											<button type="button" class="pull-right btn-sm btn btn-disable btn-brand" style="margin-right: 10px;"> 
												<i class="fa fa fa-plus"></i> <span style="font-size: 12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;issue&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></span>
											</button>
										<?php endif; ?>
										
									</div>
								</div>
								<div class="row">
									<?php if (!empty($issue)) :  ?>
										<?php foreach ($issue as $key => $value): ?>
										<div class="w-100 mb-1">
											<div class="col-md-12 mb-0">
												<div class="card mb-0 bg-danger">
													<div class="card-body pb-2 pt-2">
														<?php if (!empty($p_access["U"]) ) : ?>
														<button data-id="<?= $value['ID'] ?>" class="btn-delete-issue btn btn-xm btn-outline-secondary pull-right"><i class="fa fa-trash"></i></button>
														<button data-id="<?= $value['ID'] ?>" class="btn-edit-issue mr-1 btn btn-xm btn-outline-secondary pull-right"><i class="fa fa-edit"></i></button>
														<?php else : ?>
															<button class="btn btn-xm btn-disable pull-right"><i class="fa fa-trash"></i></button>
															<button class="mr-1 btn btn-xm btn-disable pull-right"><i class="fa fa-edit"></i></button>
														<?php endif; ?>
														<div><strong><?= $value['IMPACT'] ?></strong></div>
														<div class="pl-2">
															<div class="badge badge-info">
																<?= $value['DATE_CREATED'] ?>	
															</div>
															<div class="badge <?= $value['ISSUE_STATUS'] == 'CLOSED' ? 'badge-success' :'badge-warning' ?> mr-1">
																<?= $value['ISSUE_STATUS'] == 'CLOSED' ? $value['ISSUE_STATUS'].'-'.$value['DATE_CLOSED'] : $value['ISSUE_STATUS'] ?>
															</div>


																<?= $value['NAME'] ?>
															</div>
														<div class="pl-3"><small><?= $value['DESCRIPTION'] ?></small></div>
													</div>
												</div>
											</div>
											<?php foreach ($value['action'] as $key1 => $value1): ?>
												<div class="col-10 offset-2 mt-0">
													<div class="card mt-0 bg-primary mr-1 mb-1">
														<div class="card-body pb-1 pt-1">
															<?php if (!empty($p_access["U"]) ) : ?>
															<button data-id="<?= $value1['ID'] ?>" class="btn-delete-action btn btn-xm btn-outline-secondary pull-right"><i class="fa fa-trash"></i></button>
															<button data-id="<?= $value1['ID'] ?>" class="btn-edit-action mr-1 btn btn-xm btn-outline-secondary pull-right"><i class="fa fa-edit"></i></button>
															<?php else: ?>
																<button  class="btn btn-xm btn-disable pull-right"><i class="fa fa-trash"></i></button>
																<button class="mr-1 btn btn-xm btn-disable pull-right"><i class="fa fa-edit"></i></button>
															<?php endif; ?>
															<div>
																<div class="badge badge-info">
																	<?= $value1['START_DATE'].' <small>To</small> '.$value1['END_DATE'] ?>
																</div>
																<div class="badge <?= $value1['ACTION_STATUS'] == 'CLOSED' ? 'badge-success' :'badge-warning' ?> mr-1">
																<?= $value1['ACTION_STATUS'] == 'CLOSED' ? $value1['ACTION_STATUS'].'-'.$value1['DATE_CLOSED'] : $value1['ACTION_STATUS'] ?>
																</div>
															</div>														
															<div class="pl-2">
																<strong>
																	<?= !empty($value1['PIC_NAME2']) ? $value1['PIC_NAME2'].' - '.$value1['PIC_NAME'] : $value1['PIC'].' - '.$value1['PIC_NAME']  ?>
																</strong>
															</div>
															<div class="pl-3">
																<?= $value1['NAME'] ?>
															</div>
															
														</div>
													</div>
												</div>
											<?php endforeach ?>
										</div>
									<?php endforeach ?>
									<?php else : ?>
										<small class="text-muted">No Data</small>
									<?php endif; ?>
								</div>
							</div>

							<div class="tab-pane" id="bast" role="tabpanel">
								<?php if(!empty($partner)) : ?>
									<?php foreach ($partner as $key => $value) : ?>
										<strong class="text-primary">BAST <?=  $value['PARTNER_NAME']  ?></strong>
										<?php if(!empty($value['BAST'])) : ?>
											<ul class="list-group scroll-thin w-90">
												<?php foreach ($value['BAST'] as $key1 => $value1) : ?>
							                        <li class="list-group-item" style="padding: 0.2rem 1.25rem;">
							                        <div class="row">
							                        	<div class="col-4">
							                        		<?= $value1['SPK'] ?>
							                        		<div class="w-100 pl-2" style="font-weight: 600;">
							                        			<?= $value1['TYPE'] ?>
								                        		<?= $value1['TYPE'] == 'PROGERSS' ? $value1['PROGRESS'].'%' : '' ?>
								                        		<?= $value1['TYPE'] == 'TERMIN' ? $value1['TERMIN']  : '' ?>
							                        		</div>
							                        	</div>
							                        	<div class="col-2">
							                        		<?= $value1['BAST_DATE2'] ?>
							                        	</div>
							                        	<div class="col-2 text-right">
							                        		<span class="rupiah"><?= $value1['VALUE'] ?></span>
							                        	</div>
							                        	<div class="col-2">
							                        		<?= empty($value1['APPROVAL_NAME']) ? $value1['STATUS'] : 'Check by '.$value1['APPROVAL_NAME'] ?>
							                        	</div>
							                        </div>
							                        </li>
												<?php endforeach; ?>
						                    </ul>     
										<?php else : ?>
										<br><small class="text-muted">No Data</small><br><br>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>

						</div>
		</div>
		<?php endif; ?>
	</div>	
</div>
<?php if (!empty($p_access["U"]) ) : ?>
	<div class="w-100 mb-5" style="display: block;min-height: 25px;">
		<a class="btn btn-sm btn-warning" style="position: absolute;right: 12px;" href="<?= base_url().'project-edit/'.$project['ID_PROJECT']; ?>"><strong>Edit Project</strong></a>
	</div>
<?php else : ?>
	<div class="w-100 mb-5" style="display: block;min-height: 25px;">
		<button class="btn btn-sm btn-disable" style="position: absolute;right: 12px;"><strong>Edit Project</strong></button>
	</div>
<?php endif; ?>



<!-- MODAL -->
<!-- Modal Delivrable-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-deliverable">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-deliverable" >Add <small>Deliverable</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-deliverable">
					<div class="form-group">
                      <label>Name</label>
                      <input class="form-control" type="text" name="deliverable-name" id="deliverable-name" required>
                    </div>
	
                    <div class="form-group">
                      <label>PIC</label>
                      <select class="form-control" id="deliverable-pic" name="deliverable-pic" placeholder="Select PIC Deliverable"> 
                      	<?php $distictpartner = null; foreach ($partner as $key => $value) : ?>
							<?php if($distictpartner != $value['ID_PARTNER']) : ?>
                      		<option value="<?= $value["ID_PARTNER"]?>"><?= $value['PARTNER_NAME'] ?></option> 
                      		<?php $distictpartner = $value['ID_PARTNER']; endif; ?>
                      	<?php endforeach; ?>
                      	<option value="<?= $project['SEGMEN'] ?>"><?= $project['SEGMEN'] ?></option>
                      	<option value="<?= $project['DIVISION'] ?>"><?= $project['DIVISION'] ?></option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Description</label>
                      <textarea name="deliverable-description" id="deliverable-description" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                      <label>Start</label>
                      <input class="form-control min-date" type="text" name="deliverable-start" id="deliverable-start" readonly required>
                    </div>
                    
                    <div class="form-group">
                      <label>End</label>
                      <input class="form-control min-date" type="text" name="deliverable-end" id="deliverable-end" readonly required>
                    </div>

                    <div class="form-group">
                      <label>Weight</label>
                      <input type="number"  max="<?= 100 - $project['TOTAL_WEIGHT'] ?>" min="0" name="deliverable-weight" id="deliverable-weight" class="form-control" required />
                    </div>
						                    

                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-save-deliverable" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Save</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<!-- Modal Edit Delivrable-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-edit-deliverable">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-edit-deliverable" >Edit <small>Deliverable</small></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-edit-deliverable">
					<div class="form-group">
                      <label>Name</label>
                      <input type="hidden" name="edit-deliverable-id" id="edit-deliverable-id" required>
                      <input class="form-control" type="text" name="edit-deliverable-name" id="edit-deliverable-name" required>
                    </div>
	
                    <div class="form-group">
                      <label>PIC</label>
                      <select class="form-control" id="edit-deliverable-pic" name="edit-deliverable-pic" placeholder="Select PIC Deliverable">
                      	<?php $distictpartner = null; foreach ($partner as $key => $value) : ?>
							<?php if($distictpartner != $value['ID_PARTNER']) : ?>
                      		<option value="<?= $value["ID_PARTNER"]?>"><?= $value['PARTNER_NAME'] ?></option> 
                      		<?php $distictpartner = $value['ID_PARTNER']; endif; ?>
                      	<?php endforeach; ?>
                      	<option value="<?= $project['SEGMEN'] ?>"><?= $project['SEGMEN'] ?></option>
                      	<option value="<?= $project['DIVISION'] ?>"><?= $project['DIVISION'] ?></option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Description</label>
                      <textarea name="deliverable-description" id="edit-deliverable-description" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                      <label>Start</label>
                      <input class="form-control min-date" type="text" name="edit-deliverable-start" id="edit-deliverable-start" readonly required >
                    </div>
                    
                    <div class="form-group">
                      <label>End</label>
                      <input class="form-control min-date" type="text" name="edit-deliverable-end" id="edit-deliverable-end" readonly required >
                    </div>

                    <div class="form-group">
                      <label>Weight</label>
                      <input type="number"  max="100" min="0" id="edit-deliverable-weight" name="edit-deliverable-weight" id="deliverable-weight" class="form-control" required />
                    </div>
						                    

                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-save-edit-deliverable" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Update</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal Progress-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-progress">
  <div class="modal-dialog modal-primary  modal-md">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-progress" >Update Progress</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-progress">
					<div class="form-group">
                      <label id="progress-deliverable-name" name="progress-deliverable-name"></label>
                      <input type="hidden" name="progress-deliverable-id" id="progress-deliverable-id" required>
                      <input type="hidden" name="progress-project-id" id="progress-project-id" value="<?= $project['ID_PROJECT'] ?>" required>
                      <input type="hidden" name="progress-project-start" id="progress-project-start" value="<?= $project['START_DATE2'] ?>" required>
                    </div>
					
					<div class="form-group">
                      <label>Date</label>
                      <input class="form-control min-date-progress" type="text" name="progress-date" id="progress-date" readonly required>
                    </div>

                    <div class="form-group">
                      <label>Progress</label>
                      <input type="number" name="progress-value" id="progress-value" class="form-control" required />
                    </div>
						                    
                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-update-progress" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Update Progress</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal Issue-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-issue">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-issue" >Add Issue</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-issue">
                	<input type="hidden" name="issue-id" id='issue-id'>
					<div class="form-group">
                      <label>Name</label>
                      <input class="form-control" type="text" name="issue-name" id="issue-name" required>
                    </div>

                    <div class="form-group">
                      <label>Deliverable</label>
	                      <select class="form-control" id="issue-deliverable" name="issue-deliverable" placeholder="Select Deliverable"> 
	                      </select>
                    </div>
	
                    <div class="form-group">
                      <label>Impact</label>
                      <select class="form-control" id="issue-impact" name="issue-impact" placeholder="Impact"> 
                      	<option value="No Impact">No Impact</option>
                      	<option value="Potential">Potential</option>
                      	<option value="Significant">Significant</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Date</label>
                      <input class="form-control min-date" type="text" name="issue-date" id="issue-date" readonly required>
                    </div>

                    <div class="form-group">
                      <label>Description</label>
                      <textarea name="issue-description" id="issue-description" class="form-control"></textarea>
                    </div>
                    					                    

                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-save-issue" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Save</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal Action-->
<div class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-action">
  <div class="modal-dialog modal-primary  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
              <h4 class="modal-title" id="modal-title-action" >Add Action</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
        </div>
        <div class="modal-body relative">
          <div class="row">
            <div class="col-12">
                <form id="form-action">
					<input class="form-control" type="hidden" name="action-id" id="action-id" required>
					<div class="form-group">
                      <label>Name</label>
                      <input class="form-control" type="text" name="action-name" id="action-name" required>
                    </div>

                    <div class="form-group">
                      <label>Issue</label>
	                      <select class="form-control" id="action-issue" name="action-issue" placeholder="Select Issue"> 
	                      </select>
                    </div>

                    <div class="form-group">
                      <label>PIC</label>
	                      <select class="form-control" id="action-pic" name="action-pic" placeholder="Select PIC Action Plan"> 
	                      	<?php $distictpartner = null; foreach ($partner as $key => $value) : ?>
								<?php if($distictpartner != $value['ID_PARTNER']) : ?>
	                      		<option value="<?= $value["ID_PARTNER"]?>"><?= $value['PARTNER_NAME'] ?></option> 
	                      		<?php $distictpartner = $value['ID_PARTNER']; endif; ?>
	                      	<?php endforeach; ?>
	                      	<option value="<?= $project['SEGMEN'] ?>"><?= $project['SEGMEN'] ?></option>
	                      	<option value="<?= $project['DIVISION'] ?>"><?= $project['DIVISION'] ?></option>
	                      </select>
                    </div>

                    <div class="form-group">
                      <label>PIC Name</label>
                      <input class="form-control" type="text" name="action-pic-name" id="action-pic-name">
                    </div>

                    <div class="form-group">
                      <label>Description</label>
                      <textarea name="action-description" id="action-description" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                      <label>Start</label>
                      <input class="form-control min-date" type="text" name="action-start" id="action-start" readonly required>
                    </div>
                    
                    <div class="form-group">
                      <label>End</label>
                      <input class="form-control min-date" type="text" name="action-end" id="action-end" readonly required>
                    </div>						                    

                    <div class="w-100 text-center mt-2">
                      <button type="button" id="btn-save-action" class="btn-sm btn-success btn-brand btn" ><i class="fa fa-plus"></i><span class="pl-1 pr-2">Save</span></button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- END MODAL -->

<script>
 var Page = function () {
 	const chartProgress = () => {
    		var chartLine = Highcharts.chart('chartProgress', {
			        chart: {
					        height: 400,
					        borderRadius : '10px',
					        backgroundColor : '#e4e5e6'
							},
							credits: {
								enabled: false
							},
					        title: {
				                    text: '',
				                    x: -20, 
				                    style: {
				                     textTransform: 'uppercase',
				                     fontSize: '12px'
				                    },
				                },
				                subtitle: {
				                    text: "",
				                },
					        xAxis: {
					            categories: <?= !empty($chart_progress['WEEK']) ? json_encode($chart_progress['WEEK']) : '[]' ?>,
					        },
					        yAxis: {
					        	// max : 100,
					            title: {
					                text: ''
					            },
					            max: 100,
					            plotLines: [{
					                value: 0,
					                width: 1,
					                color: '#808080'
					            }]
					        },
					        tooltip: {
					            // valueSuffix: '%'
					            formatter: function () {
					                var tooltipsArr = <?= !empty($chart_progress['DAYS']) ? json_encode($chart_progress['DAYS']) : '[]' ?>;
					                return tooltipsArr[this.point.index] +'<br>'+ this.series.name +' : '+ Highcharts.numberFormat(this.point.y, 2) +'%';
					            }
					        },
					        exporting : false,
					        legend: {
					            layout: 'horizontal',
					            align: 'center',
					            verticalAlign: 'bottom',
					            borderWidth: 0
					        },
					        series: [
					        {
					            name: '',
					            color: '#fefefe00',
					            data: <?= !empty($chart_progress['TOP']) ? json_encode($chart_progress['TOP']) : '[]' ?>,
					        },
					        {
					            name: 'Plan',
					            color: '#016ead',
					            data: <?= !empty($chart_progress['PLAN']) ? json_encode($chart_progress['PLAN']) : '[]' ?>,
					        }, {
					            name: 'Achievement',
					            color: '#06bd3e',
					            data: <?= !empty($chart_progress['ACHIEVEMENT']) ? json_encode($chart_progress['ACHIEVEMENT']) : '[]' ?>,
					        }
					        ]
				    });


				    var render_width  = chartLine.chartWidth;
				    var render_height = render_width * chartLine.chartHeight / chartLine.chartWidth;

				    var svg = chartLine.getSVG({
				        exporting: {
				            sourceWidth: chartLine.chartWidth,
				            sourceHeight: chartLine.chartHeight
				        }
				    });
				    var canvas = document.createElement('canvas');
				    canvas.height = render_height;
				    canvas.width = render_width;
    	}
    const chartProgressMonth =() => {
    		var chartLine = Highcharts.chart('chartProgressMonth', {
			        chart: {
					        height: 400,
					        borderRadius : '10px',
					        backgroundColor : '#e4e5e6'
							},
							credits: {
								enabled: false
							},
					        title: {
				                    text: '',
				                    x: -20, 
				                    style: {
				                     textTransform: 'uppercase',
				                     fontSize: '12px'
				                    },
				                },
				                subtitle: {
				                    text: "",
				                },
					        xAxis: {
					            categories: <?= !empty($chart_month['MONTH']) ? json_encode($chart_month['MONTH']) : '[]' ?>,
					        },
					        yAxis: {
					        	// max : 100,
					            title: {
					                text: ''
					            },
					            max: 100,
					            plotLines: [{
					                value: 0,
					                width: 1,
					                color: '#808080'
					            }]
					        },
					        tooltip: {
					            // valueSuffix: '%'
					            formatter: function () {
					                var tooltipsArr = <?= !empty($chart_month['MONTH']) ? json_encode($chart_month['MONTH']) : '[]' ?>;
					                return tooltipsArr[this.point.index] +'<br>'+ this.series.name +' : '+ Highcharts.numberFormat(this.point.y, 2) +'%';
					            }
					        },
					        exporting : false,
					        legend: {
					            layout: 'horizontal',
					            align: 'center',
					            verticalAlign: 'bottom',
					            borderWidth: 0
					        },
					        series: [
					        {
					            name: 'Plan',
					            color: '#016ead',
					            data: <?= !empty($chart_month['PLAN']) ? json_encode($chart_month['PLAN']) : '[]' ?>,
					        }, {
					            name: 'Achievement',
					            color: '#06bd3e',
					            data: <?= !empty($chart_month['ACH']) ? json_encode($chart_month['ACH']) : '[]' ?>,
					        }]
				    });


				    var render_width  = chartLine.chartWidth;
				    var render_height = render_width * chartLine.chartHeight / chartLine.chartWidth;

				    var svg = chartLine.getSVG({
				        exporting: {
				            sourceWidth: chartLine.chartWidth,
				            sourceHeight: chartLine.chartHeight
				        }
				    });
				    var canvas = document.createElement('canvas');
				    canvas.height = render_height;
				    canvas.width = render_width;
    	}
    const generateChartPartner = function(id, week, plan, achievement){
    		var chartLine = Highcharts.chart(id, {
			        chart: {
					        height: 400,
					        borderRadius : '10px',
					        backgroundColor : '#e4e5e6'
							},
							credits: {
								enabled: false
							},
					        title: {
				                    text: '',
				                    x: -20, 
				                    style: {
				                     textTransform: 'uppercase',
				                     fontSize: '12px'
				                    },
				                },
				                subtitle: {
				                    text: "",
				                },
					        xAxis: {
					            categories: week,
					        },
					        yAxis: {
					        	// max : 100,
					            title: {
					                text: ''
					            },
					            max: 100,
					            plotLines: [{
					                value: 0,
					                width: 1,
					                color: '#808080'
					            }]
					        },
					        exporting : false,
					        legend: {
					            layout: 'horizontal',
					            align: 'center',
					            verticalAlign: 'bottom',
					            borderWidth: 0
					        },
					        series: [
					        {
					            name: 'Plan',
					            color: '#016ead',
					            data: plan,
					        },
					        {
					            name: 'Achievement',
					            color: '#06bd3e',
					            data: achievement,
					        }
					        ]
				    });


				    var render_width  = chartLine.chartWidth;
				    var render_height = render_width * chartLine.chartHeight / chartLine.chartWidth;

				    var svg = chartLine.getSVG({
				        exporting: {
				            sourceWidth: chartLine.chartWidth,
				            sourceHeight: chartLine.chartHeight
				        }
				    });
				    var canvas = document.createElement('canvas');
				    canvas.height = render_height;
				    canvas.width = render_width;
    	}


	return {
		init: function() { 
			<?php if ($project["STATUS"] != "CANDIDATE") : ?>
			chartProgress();
			chartProgressMonth();
			<?php endif; ?>

			$(document).on("click","#chart-scale-week", (e) => {
				$("#chart-scale-week").addClass("active");
				$("#chart-scale-month").removeClass("active");
				
				$("#chartProgress").removeClass("hidden");
				$("#chartProgressMonth").addClass("hidden");
			});

			$(document).on("click","#chart-scale-month", (e) => {
				$("#chart-scale-week").removeClass("active");
				$("#chart-scale-month").addClass("active");
				
				$("#chartProgress").addClass("hidden");
				$("#chartProgressMonth").removeClass("hidden");
			});

			<?php foreach ($chart_partner as $key => $value) : ?>
				generateChartPartner(<?= 'chartProgress'.$key  ?>,<?= json_encode($value['WEEK']) ?>,<?= json_encode($value['PLAN']) ?>, <?= json_encode($value['ACHIEVEMENT']) ?>);				
			<?php endforeach ?>
			let project_start_date 		= "<?= $project['START_DATE2'] ?>";
			let project_start_date_arr 	= project_start_date.split('/');
			$('.min-date').datepicker({
				format: "dd/mm/yyyy",
				startDate : new Date(parseInt(project_start_date_arr[2]), parseInt(project_start_date_arr[1] - 1),parseInt(project_start_date_arr[0])),
	            disableTouchKeyboard : false,
	            toggleActive: true,
	            forceParse: false,
	            autoclose: true
			});
			
			$(document).on("click","#btn-deliverable", (e) => {
                $('#form-deliverable .form-control').map((index, value) =>{
                  $(value).val('');
                });
                $("#deliverable-week").attr("max","<?= 100 - $project['TOTAL_WEIGHT'] ?>");
                $('#deliverable-pic').val('<?= $project['DIVISION'] ?>').trigger('change');
				$("#modal-deliverable").modal("show");
			});

			$(document).on("click","#btn-symptoms", (e) => {
                console.log($('#project-symptoms').val());
                let symptom = $('#project-symptoms').val();
                if(symptom[0] == undefined || symptom[0] == null || symptom[0] == ''){
                		bootbox.alert("cant update symptom with null value", function(){});
                }else{
			        showLoading();
			        $.ajax({
	                          url: base_url+'project/update_symptom/<?= $project['ID_PROJECT'] ?>',
	                          type:'POST',
	                          data:  {'symptom' : symptom},
	                          async : true,
	                          dataType : "text",
	                          success:function(result){
	                          		location.reload();
	                          }

	                  }); 
                }
			});


			$(document).on("click","#btn-add-issue", (e) => {
                $('#form-issue .form-control').map((index, value) =>{
                  $(value).val('');
                });
                $("#modal-title-issue").text("Add Issue");
				$("#modal-issue").modal("show");
			});

			$(document).on("click",".btn-edit-issue", (e) => {
                $('#form-issue .form-control').map((index, value) =>{
                  $(value).val('');
                });
                let id_issue = $(e.currentTarget).data('id');
                $("#issue-id").val(id_issue);
                $("#modal-title-issue").text("Edit Issue");
                $.ajax({
                      url: base_url+'master/get_issue/<?= $project['ID_PROJECT']?>',
                      data : {id : id_issue},
	                  type:'POST',
                      dataType : "json",
                      success:function(result){
                      	  
                      	  let option = new Option(result.DELIVERABLE_NAME,result.ID_DELIVERABLE);
						 option.selected = true;

                          $('#issue-name').val(result.NAME);
                          $("#issue-deliverable").append(option).trigger("change");
                          $('#issue-impact').val(result.IMPACT).trigger('change');    
                          $('#issue-date').val(result.DATE2);
                          $('#issue-description').val(result.NAME);
                          $("#modal-issue").modal("show");         
                      }

              });
			});

			$(document).on("click","#btn-add-action", (e) => {
                $('#form-action .form-control').map((index, value) =>{
                  $(value).val('');
                });
                $("#modal-title-action").text("Add Action");
				$("#modal-action").modal("show");
			});


			$(document).on("click",".btn-edit-action", (e) => {
                $('#form-action .form-control').map((index, value) =>{
                  $(value).val('');
                });
                let id = $(e.currentTarget).data('id');
                $("#action-id").val(id);
                $("#modal-title-action").text("Edit Action");
                $.ajax({
                      url: base_url+'master/get_action/<?= $project['ID_PROJECT']?>',
                      data : {id : id},
	                  type:'POST',
                      dataType : "json",
                      success:function(result){
                      	  
                      	  let option = new Option(result.ISSUE_NAME,result.ID_ISSUE);
						 option.selected = true;

                          $('#action-name ').val(result.NAME);
                          $("#action-issue").append(option).trigger("change");
                          $('#action-pic').val(result.PIC);
                          $('#action-pic-name').val(result.PIC_NAME);
                          $('#action-start').val(result.START_DATE2);
                          $('#action-end').val(result.END_DATE2);
                          $('#action-description').val(result.DESCRIPTION);
                          $("#modal-action").modal("show");         
                      }

              });
			});


			$(document).on("click",".btn-edit-deliverable", (e) => {
				let id_deliverable = $(e.currentTarget).data("id");
	              $.ajax({
	                      url: base_url+'master/get_deliverable/<?= $project['ID_PROJECT'] ?>',
	                      data : {id : id_deliverable},
	                      type:'POST',
	                      dataType : "json",
	                      success:function(result){
	                          console.log(result);
	                          let maxWeight = <?= 100 - $project['TOTAL_WEIGHT'] ?>+result.WEIGHT;
	                          if (maxWeight < 0) {maxWeight = 0;}
	                          console.log("maxWeight",maxWeight);
	                          $('#edit-deliverable-id').val(result.ID);
	                          $('#edit-deliverable-name').val(result.NAME);
	                          $('#edit-deliverable-description').val(result.DESCRIPTION);
	                          $('#edit-deliverable-start').val(result.START_DATE2);
	                          $('#edit-deliverable-end').val(result.END_DATE2);                         
	                          $('#edit-deliverable-weight').val(result.WEIGHT);
	                          $('#edit-deliverable-weight').attr("max",maxWeight);
	                          $('#edit-deliverable-pic').val(result.PIC).trigger('change');                         
	                          $('#modal-edit-deliverable').modal('show');             
	                      }

	              }); 
			});

			$(document).on("click","#btn-save-deliverable", (e) => {
			 if($('#form-deliverable').valid()){
                $('.rupiah').map((index, value) =>{
		            $('#'+$(value).attr('id')).val($(value).unmask());
		        });

		        let form = $('#form-deliverable')[0];
		        let formData = new FormData(form);
		        showLoading();
		        $.ajax({
                          url: base_url+'project/saveDeliverable/<?= $project['ID_PROJECT'] ?>',
                          type:'POST',
                          data:  formData,
                          async : true,
                          dataType : "text",
                          contentType:false,
                          processData:false,
                          success:function(result){
                          		   $("#modal-deliverable").modal('hide');
                                   if(result.trim()=='success'){
                                   	hideLoading();
                                    location.reload();
                                  }else{
                                    bootbox.alert("Failed!", function(){});
                                    }
                                  return result;
                          }

                  }); 
			
		     }
			});

			$(document).on("click","#btn-save-edit-deliverable", (e) => {
			 if($('#form-edit-deliverable').valid()){
                $('.rupiah').map((index, value) =>{
		            $('#'+$(value).attr('id')).val($(value).unmask());
		        });

		        let form = $('#form-edit-deliverable')[0];
		        let formData = new FormData(form);
		        showLoading();
		        $.ajax({
                          url: base_url+'project/updateDeliverable/<?= $project['ID_PROJECT'] ?>',
                          type:'POST',
                          data:  formData,
                          async : true,
                          dataType : "text",
                          contentType:false,
                          processData:false,
                          success:function(result){
                          		location.reload();
                          }

                  }); 
			
		     }
			});

			$(document).on("click","#btn-save-issue", (e) => {
			 if($('#form-issue').valid()){
		        let form = $('#form-issue')[0];
		        let formData = new FormData(form);
		        showLoading();
		        $.ajax({
                          url: base_url+'project/saveIssue/<?= $project['ID_PROJECT'] ?>',
                          type:'POST',
                          data:  formData,
                          async : true,
                          dataType : "text",
                          contentType:false,
                          processData:false,
                          success:function(result){
                          		   $("#modal-issue").modal('hide');
                          		   hideLoading();
                                   if(result.trim()=='success'){
                                    location.reload();
                                  }else{
                                    bootbox.alert("Failed!", function(){});
                                    }
                                  return result;
                          }

                  }); 
			
		     }
			});

			$(document).on("click","#btn-save-action", (e) => {
			 if($('#form-action').valid()){
		        let form = $('#form-action')[0];
		        let formData = new FormData(form);
		        showLoading();
		        $.ajax({
                          url: base_url+'project/saveAction/<?= $project['ID_PROJECT'] ?>',
                          type:'POST',
                          data:  formData,
                          async : true,
                          dataType : "text",
                          contentType:false,
                          processData:false,
                          success:function(result){
                          		   $("#modal-action").modal('hide');
                          		   hideLoading();
                                   if(result.trim()=='success'){
                                    location.reload();
                                  }else{
                                    bootbox.alert("Failed!", function(){});
                                    }
                                  return result;
                          }

                  }); 
		     }
			});



			$(document).on("click",".btn-delete-deliverable",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id = $(e.currentTarget).data('id');
                  bootbox.confirm({
                        message: "Delete This deliverable?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                            	showLoading();
                                $.ajax({
                                        url: base_url+'project/deleteDeliverable',
                                        type:'POST',
                                        data:  {id_deliverable : id, id_project : "<?= $project['ID_PROJECT'] ?>"} ,
                                        async : true,
                                        dataType : "text",
                                        success:function(result){
                                         if(result.trim()=='success'){
                                          hideLoading();
                                          location.reload();
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });


			$(document).on("click",".btn-progress-deliverable", (e) => {
                e.stopImmediatePropagation();
                e.preventDefault();
                $('#form-progress .form-control').map((index, value) =>{
                  $(value).val('');
                });

                let id 				= $(e.currentTarget).data('id');
                let name 			= $(e.currentTarget).data('name');
                let current_value 	= $(e.currentTarget).data('current_progress');
                let start_deliverable 	= $(e.currentTarget).data('start');
                let start_deliverable_arr = start_deliverable.split('/');

                $('.min-date-progress').datepicker({
					format: "dd/mm/yyyy",
					startDate : new Date(parseInt(start_deliverable_arr[2]), parseInt(start_deliverable_arr[1] - 1),parseInt(start_deliverable_arr[0])),
		            disableTouchKeyboard : false,
		            toggleActive: true,
		            forceParse: false,
		            autoclose: true
				});

                console.log(id);
                $("#progress-deliverable-name").text(name);
                $("#progress-deliverable-id").val(id);

                console.log(current_value);
                $("#progress-value").attr("min",0);
                $("#progress-value").attr("max",100 - current_value);
				$("#modal-progress").modal("show");
			});

			$(document).on("click","#btn-update-progress", (e) => {
			 if($('#form-progress').valid()){
		        let form = $('#form-progress')[0];
		        let formData = new FormData(form);
		        showLoading();
		        $.ajax({
                          url: base_url+'project/saveProgress',
                          type:'POST',
                          data:  formData,
                          async : true,
                          dataType : "text",
                          contentType:false,
                          processData:false,
                          success:function(result){
                          		   $("#modal-progress").modal('hide');
                          		   hideLoading();
                                   if(result.trim()=='success'){
                                    location.reload();
                                  }else{
                                    bootbox.alert("Failed!", function(){});
                                    }
                                  return result;
                          }

                  }); 
			
		     }
			});

			$(document).on("click",".btn-delete-progress",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id 				= $(e.currentTarget).data('id');
                  let id_deliverable 	= $(e.currentTarget).data('id_deliverable');
                  let achievement 		= $(e.currentTarget).data('achievement');
                  let progress_date 	= $(e.currentTarget).data('progress_date');
                  bootbox.confirm({
                        message: "Delete this achievement?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                            	showLoading();
                                $.ajax({
                                        url: base_url+'project/deleteProgress',
                                        type:'POST',
                                        data:  {achievement : achievement , id : id, id_deliverable : id_deliverable, project_id : "<?= $project['ID_PROJECT'] ?>", project_start : "<?= $project['START_DATE2'] ?>",progress_date : progress_date} ,
                                        async : true,
                                        dataType : "text",
                                        success:function(result){
                                         console.log(result);
                                         hideLoading();
                                         if(result.trim()=='success'){
                                          location.reload();
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });

			$(document).on("click",".btn-delete-issue",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id = $(e.currentTarget).data('id');
                  bootbox.confirm({
                        message: "Delete this issue and all action for this issue?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                            	showLoading();
                                $.ajax({
                                        url: base_url+'project/deleteIssue',
                                        type:'POST',
                                        data:  {id : id} ,
                                        async : true,
                                        dataType : "text",
                                        success:function(result){
                                         hideLoading();
                                         if(result.trim()=='success'){
                                          location.reload();
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });


			$(document).on("click",".btn-delete-action",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();

                  let id = $(e.currentTarget).data('id');
                  bootbox.confirm({
                        message: "Delete this action?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                            	showLoading();
                                $.ajax({
                                        url: base_url+'project/deleteAction',
                                        type:'POST',
                                        data:  {id : id} ,
                                        async : true,
                                        dataType : "text",
                                        success:function(result){
                                         hideLoading();
                                         if(result.trim()=='success'){
                                          location.reload();
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });



			// SELECT ZONE
			$("#issue-deliverable").select2({
                              placeholder: "Select Deliverable",
                              width: 'resolve',
                              allowClear : true,
                              ajax: {
                                  type: 'POST',
                                  url:base_url+"master/get_deliverable/<?= $project['ID_PROJECT']?>",
                                  dataType: "json",
                                  data: function (params) {
                                      return {
                                          q: params.term
                                      };
                                  },
                                  processResults: function (data) {
                                      return {
                                          results: $.map(data, function(obj) {
                                              return { id: obj.ID, text: obj.NAME};
                                          })
                                      };
                                  },
                                  
                              }
                  });

			$("#action-issue").select2({
                              placeholder: "Select Issue",
                              width: 'resolve',
                              allowClear : true,
                              ajax: {
                                  type: 'POST',
                                  url:base_url+"master/get_issue/<?= $project['ID_PROJECT']?>",
                                  dataType: "json",
                                  data: function (params) {
                                      return {
                                          q: params.term
                                      };
                                  },
                                  processResults: function (data) {
                                      return {
                                          results: $.map(data, function(obj) {
                                              return { id: obj.ID, text: obj.NAME};
                                          })
                                      };
                                  },
                                  
                              }
                  });
		}
	};

}();

jQuery(document).ready(function() {
      Page.init(event);
  }); 
</script>