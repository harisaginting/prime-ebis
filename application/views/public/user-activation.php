
<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="PROJECT APPLICATION MANAGEMENT">
  <title>PRIME | USER ACTIVATION</title>
  <!-- Icons-->
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/src/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:400,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/public.css?v=2">
  <!-- CoreUI and necessary plugins-->
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
  <!-- Jquery Validation -->
    <script src="<?= base_url(); ?>assets/plugin/jqueryvalidation.js"></script>
    <!-- JQUERY VALIDATION -->
    <script src="<?= base_url(); ?>assets/plugin/jquery-validation/dist/jquery.validate.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?= base_url(); ?>assets/plugin/jquery-validation/dist/additional-methods.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
      const base_url = "<?=base_url(); ?>";
    </script>
    <script src="<?= base_url(); ?>assets/main.js?v=<?= date('dmYH:i') ?>"></script>

</head>
<body class="app flex-row pt-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-10 offset-md-1 col-sm-12">
          <div class="card" style="border-radius: 20px;">
            <div class="card-body">
              <div class="row">
                <div class="col-12 text-center">
                    <div class="mb-3">
                      <div style="width: 100%;text-align: center;">
                        <span class="label-prime">PRIME</span>
                      </div>
                      <div style="width: 100%;text-align: center;margin-top: -25px;">
                        <span style="letter-spacing: 0.7em;font-size: 0.8em;font-weight: 800;">PROJECT MANAGEMENT</span>
                      </div>
                    </div>
                </div>
                <div class="col-12">
                  <?=$this->session->flashdata('notification')?> 
                </div>
                <div class="col-12">
                    <h5>Please set password for user ID <strong><?= $user['ID'] ?></strong></h5>
                    <form id="form-activate-user" action="<?= base_url(); ?>user/activate-proccess" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">

                          <div class="form-group">
                            <label>ID</label>
                            <input type="text" name="userid" id="userid" class="form-control" placeholder="username" required value="<?= $user['ID'] ?>" readOnly>
                            <input type="hidden" name="activation_code" id="activation_code" class="form-control" required value="<?= $user['ACTIVATION_CODE'] ?>" readOnly>
                          </div>

                          <div class="form-group">
                            <label>Name</label>
                            <input required type="text" name="name" id="name" class="form-control" placeholder="full name" value="<?= $user['NAME'] ?>" readOnly>
                          </div>

                          <div class="form-group">
                            <label>Email</label>
                            <input required value="<?= $user['EMAIL'] ?>" type="email" name="email" id="email" class="form-control" placeholder="user@domain" readOnly>
                          </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Password</label>
                              <input  type="password" minlength="8" name="password" id="password" class="password form-control" placeholder="create password" required>
                            </div>

                            <div class="form-group">
                              <label>Re-Type Password</label>
                              <input type="password" name="c_password" equalTo="#password" id="c_password" class="form-control" placeholder="confirm password" required>
                            </div>
                        </div>

                        <button type="button"  id="btn-save-user" class="col-md-6 offset-md-3 col-sm-12 btn btn-success btn-md btn-addon btn-prime">
                            <i class="fa fa-check"></i><span class="pl-0"> SET PASSWORD AND ACTIVATE </span>
                        </button>
                      </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

</body>
</html>

<script type="text/javascript">    
  var Page = function () {
  
      return {
          init: function() {  
              $(document).on("click","#btn-save-user",(e)=>{
                  if($('#form-activate-user').valid()){
                    $('#form-activate-user').submit();
                  }else{
                    alert('form value invalid');
                  }
              });  
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>