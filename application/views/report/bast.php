  <style type="text/css">
  	div{
  		font-family: 'Arial';
  	}
  	h4{
  		font-weight: bold;
  		font-size: 1em;
  	}

  	td{
  		font-size: 0.8em;
  		vertical-align: top;
  	}
  </style>

  <div style="height: 55px; border-bottom: 1px solid #000;">
  		<div style="width: 100%;text-align: center;">
		  	<h4>BERITA ACARA SERAH TERIMA (BAST)</h4>
		</div>
		<div style="width: 100%;margin-top: -20px;font-size: 0.9em;">
		  	<div style="margin-left: 43.5%;"><span>NO : <?= !empty($bast["NO_BAST"]) ? $bast["NO_BAST"] : "................................" ?></span></div>
		</div>
  </div>

  <div class="body-message" style="width: 100%">
  		<table style="width: 100%;padding-bottom: 0.2em;border-bottom: 1.5px solid #000;">
  			<tbody>
  				<tr>
  					<td width="30%">Pekerjaan <span style="float: right;">:</span></td>
  					<td width="70%"><?= $project['NAME'] ?></td>
  				</tr>
  				<tr>
  					<td width="30%">No. SPK / KL <span style="float: right;">:</span></td>
  					<td width="70%"><?= strtoupper($bast['SPK']) ?> <br>Tanggal <?= $bast['SPK_DATE_FIX'] ?></td>
  				</tr>
  				<tr>
  					<td width="30%">Pelaksana <span style="float: right;">:</span></td>
  					<td width="70%"><?= $bast['PARTNER_NAME_FIX']; ?></td>
  				</tr>
  			</tbody>
  		</table>
		
		<div style="width: 100%;font-size: 0.8em;margin-top: 0.2em;">
			Pada Hari ini 
			<strong><?= $bast["BAST_DATE_DAYNAME"] ?></strong>, 
			tanggal <strong><?= $bast["BAST_DATE_DAY"] ?></strong>, 
			bulan <strong><?= $bast["BAST_DATE_MONTH"] ?></strong>, 
			tahun <strong><?= $bast["BAST_DATE_YEAR"] ?></strong>, 
			(<?= $bast["BAST_DATE"] ?>), 
			yang bertanda tangan dibawah ini :  
		</div>
	
		<table style="width: 100%;margin-top: 0.5em;margin-left: -0.15em">
	  			<tbody>
	  				<tr>
	  					<td width="30.3%"><strong>Nama <span style="float: right;">:</span></strong></td>
	  					<td width="69.7%"> ............................................</td>
	  				</tr>
	  				<tr>
	  					<td width="30.3%"><strong>Jabatan <span style="float: right;">:</span></strong></td>
	  					<td width="69.7%"> ............................................</td>
	  				</tr>
	  				<tr>
	  					<td width="30.3%"><strong>Alamat<span style="float: right;">:</span></strong></td>
	  					<td width="69.7%"> ............................................</td>
	  				</tr>
	  			</tbody>
	  	</table>
	  	<div style="width: 100%;font-size: 0.8em;margin-top: 0em;">
			Dalam hal ini mewakili <strong><?= $bast["PARTNER_NAME_FIX"] ?></strong> selanjutnya disebut sebagai <strong>PIHAK PERTAMA</strong>.
		</div>

		<table style="width: 100%;margin-top: 0.5em;margin-left: -0.15em">
	  			<tbody>
	  				<tr>
	  					<td width="30.3%"><strong>Nama <span style="float: right;">:</span></strong></td>
	  					<td width="69.7%"> ............................................</td>
	  				</tr>
	  				<tr>
	  					<td width="30.3%"><strong>Jabatan <span style="float: right;">:</span></strong></td>
	  					<td width="69.7%"> ............................................</td>
	  				</tr>
	  				<tr>
	  					<td width="30.3%"><strong>Alamat<span style="float: right;">:</span></strong></td>
	  					<td width="69.7%"> ............................................</td>
	  				</tr>
	  			</tbody>
	  	</table>
	  	<div style="width: 100%;font-size: 0.8em;margin-top: 0em;">
			Dalam hal ini mewakili <strong>PT. TELEKOMUNIKASI INDONESIA, Tbk</strong> selanjutnya disebut sebagai <strong>PIHAK KEDUA</strong>.
		</div>
		<div style="width: 100%;font-size: 0.8em;margin-top: 0.8em;">
			Berdasarkan : 
			<ol>
			  <li>*Surat Penetapan Mitra atau Kontrak Layanan tentang Pengadaan Pekerjaan <?= $project['NAME'] ?></li>
			  <li>Tea</li>
			  <li>Milk</li>
			</ol>
		</div>


  </div>