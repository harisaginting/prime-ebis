<style type="text/css">
  .slider{
    background-color: #ff4848;
  }
  .slider-role{
    height: 20px;  
    margin:auto;
  }

  .slider-role:before {
    height: 15px !important;
    width: 15px !important;
    bottom: 2px !important;
  }

  .row-2{
    background-color: #c2ffcf !important;
  }
  .row-3{
    background-color: #ffefc2 !important;
  }
  .row-4{
    background-color: #e9fdff !important;
  }

  input:checked + .slider:before {
    -webkit-transform: translateX(36px);
    -ms-transform: translateX(36px);
    transform: translateX(36px);
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-5">
            <h4 class="card-title mb-0">ROLE ACCESS</h4>
            <div class="small text-muted">Prime</div>
          </div>
        
          <div class="col-sm-12">
            <table id="dataUser" class="table table-responsive-sm table-bordered table-striped" style="width: 100%;margin-top: 10px;">
                    <thead>
                      <tr>
                        <th style="min-width: 8% !important">ROLE</th>
                        <th style="min-width: 8% !important">MODULE</th>
                        <th style="text-align: center;">VIEW</th>
                        <th style="text-align: center;">ADD</th>
                        <th style="text-align: center;">UPDATE</th>
                        <th style="text-align: center;">DELETE</th>
                      </tr>
                    </thead>
                    <?php foreach ($role_access as $key => $value) : ?>
                      <tr class="row-<?= $value['ID_ROLE'] ?>">
                        <td><?= $value['NAME'] ?></td>
                        <td><?= $value['MODULE'] ?></td>
                        <td>
                          <div class="text-center w-100" style="display: flex;">
                            <label class="switch slider-role">
                              <input type="checkbox" name="slider-role" id="switch-<?= $value['ID'] ?>" data-sid="R" id="slider-<?= $value['ID'] ?>" class="primary slider-role-access" 
                              <?= $value["R"] == "1" ?  "checked" : '' ?>>
                              <span class="slider round slider-role"></span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="text-center w-100" style="display: flex;">
                            <label class="switch slider-role">
                              <input type="checkbox" name="slider-role" id="switch-<?= $value['ID'] ?>" data-sid="C" class="primary slider-role-access" 
                              <?= $value["C"] == "1" ?  "checked" : '' ?>>
                              <span class="slider round slider-role"></span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="text-center w-100" style="display: flex;">
                            <label class="switch slider-role">
                              <input type="checkbox" name="slider-role" id="switch-<?= $value['ID'] ?>" data-sid="U" class="primary slider-role-access" 
                              <?= $value["U"] == "1" ?  "checked" : '' ?>>
                              <span class="slider round slider-role"></span>
                            </label>
                          </div>
                        </td>
                        <td>
                          <div class="text-center w-100" style="display: flex;">
                            <label class="switch slider-role">
                              <input type="checkbox" name="slider-role" id="switch-<?= $value['ID'] ?>" data-sid="D" class="primary slider-role-access" 
                              <?= $value["D"] == "1" ?  "checked" : '' ?>>
                              <span class="slider round slider-role"></span>
                            </label>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <tbody>
                    </tbody>
                </table>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>


<script type="text/javascript">    
  var Page = function () {

    const updateRoleAccess = (id,val,sid) => {
      showLoadingMini();
      $.ajax({
            url: base_url+'settings/role-access',
            type:'POST',
            data:  {id :id, val : val, sid,sid} ,
            dataType : "text",
      }).done(function(result) {
        hideLoadingMini();
        if (result.trim() != "success") {
          alert("Request failed: " + result);
          window.location.reload();
        }
      }).fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      window.location.reload();
    });
    }
        
    return {
        init: function() { 
             $(document).on('change','.slider-role-access',function(e){
                let sid = $(e.currentTarget).data('sid');
                let id  = $(e.currentTarget).attr('id');
                let val = 0;
                if($("#"+id).is(':checked')){
                  val = 1;
                }
                updateRoleAccess(id,val,sid);
              });
         }
    };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
</script>