</main>
    </div>
  </body>


<script type="text/javascript">
    $('#pre-load-background').fadeOut();
    $('#pre-load-background-mini').fadeOut();
    const generatePage = () => {
        $(".Jselect2").select2();
        $('.date-picker').datepicker({
            format: "dd/mm/yyyy",
            disableTouchKeyboard : false,
            toggleActive: true,
            forceParse: false,
            autoclose: true
        });

        $(".date-picker").attr("autocomplete","off");
        $(".form-control").attr("autocomplete","off");

        $('.rupiah').priceFormat({
            prefix: 'Rp. ',
            centsSeparator: ',',
            thousandsSeparator: '.',
            centsLimit: 0
        });
        $('.fileinput').fileinput({
            showUpload:false
        });
    }
    generatePage();


    const showLoading = () => {
            $('#pre-load-background').fadeIn();
    }
    const hideLoading = () => {
            $('#pre-load-background').fadeOut();
    }

    const showLoadingMini = () => {
            $('#pre-load-background-mini').fadeIn();
    }
    const hideLoadingMini = () => {
            $('#pre-load-background-mini').fadeOut();
    }


    $('table').on( 'draw.dt', function () {
      $('.rupiah').priceFormat({
                            prefix: '',
                            centsSeparator: ',',
                            thousandsSeparator: '.',
                            centsLimit: 0
                        });
        });
</script>


</html>
