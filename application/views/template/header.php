<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>PRIME | <?= $title ?></title> 
    <!-- Icons-->
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/template/coreui/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="<?= base_url(); ?>assets/template/coreui/src/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/main.css?v=<?= date('dmYH') ?>" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/template/coreui/src/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900&display=swap" rel="stylesheet">

    <!-- Bootstrap 3 -->
    <link href="<?= base_url(); ?>assets/plugin/bootstrap3/css/bootstrap-hgn.css" rel="stylesheet" type="text/css">

    <!-- CoreUI and necessary plugins-->
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/pace-progress/pace.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="<?= base_url(); ?>assets/template/coreui/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script>

    <!-- Bootstrap File Input -->
    <link   href="<?=base_url(); ?>assets/plugin/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?= base_url(); ?>assets/plugin/bootstrap-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugin/bootstrap-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugin/bootstrap-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugin/bootstrap-fileinput/js/fileinput.min.js"></script>

    <!-- Selelct 2-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugin/select2/css/select2.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/plugin/select2/css/select2-bootstrap.css">
    <script src="<?= base_url(); ?>assets/plugin/select2/js/select2.full.min.js" type="text/javascript" charset="utf-8"></script>
    
    <!-- Bootbox -->
    <script src="<?= base_url(); ?>assets/plugin/bootbox/bootbox.min.js"></script>

    <!-- Jquery Validation -->
    <script src="<?= base_url(); ?>assets/plugin/jqueryvalidation.js"></script>

    <!-- Price Format -->
     <script src="<?= base_url(); ?>assets/plugin/jquery.priceformat.js"></script>

    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/r-2.2.2/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/r-2.2.2/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
    
    <!-- Highcharts -->
    <!-- <script src="https://code.highcharts.com/maps/highmaps.js"></script>
    <script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/maps/modules/data.js"></script>
    <script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script> -->

    <script src="<?= base_url(); ?>assets/plugin/highchart/lib/highmaps.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/lib/exporting.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/lib/data.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/lib/id-all.js"></script>

    <!-- <script src="<?= base_url(); ?>assets/plugin/highchart/code/highcharts.js?v=05042019abc"></script> -->
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/modules/data.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/modules/drilldown.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/modules/exporting.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/grouped-categories.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/plugin/highchart/code/highcharts-more.js"></script> -->
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/highcharts-3d.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/lib/canvg.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/lib/jspdf.js"></script>
    <script src="<?= base_url(); ?>assets/plugin/highchart/code/lib/rgbcolor.js"></script>

     <!-- moment.js -->
    <script src="<?= base_url(); ?>assets/plugin/moment/min/moment.min.js"  type="text/javascript"></script>
    <!-- datepicker -->
    <link href="<?= base_url(); ?>assets/plugin/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css">
    <script src="<?= base_url(); ?>assets/plugin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript" charset="utf-8"></script> 
  
    <!-- Awesomplete -->
    <link   href="<?= base_url(); ?>assets/plugin/awesomplete/awesomplete.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?= base_url(); ?>assets/plugin/awesomplete/awesomplete.min.js"></script>

   <!-- JQUERY VALIDATION -->
    <script src="<?= base_url(); ?>assets/plugin/jquery-validation/dist/jquery.validate.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?= base_url(); ?>assets/plugin/jquery-validation/dist/additional-methods.min.js" type="text/javascript" charset="utf-8"></script>

   <!-- Hands On Table -->
    <script src="<?= base_url(); ?>assets/plugin/handsontable/dist/handsontable.full.js"></script>
    <link   href="<?=base_url(); ?>assets/plugin/handsontable/dist/handsontable.full.min.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
      const base_url = "<?=base_url(); ?>";
    </script>
    <script src="<?= base_url(); ?>assets/main.js?v=<?= date('dmYH:i') ?>"></script>
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show"> 
    <header class="app-header navbar <?= $division ?>">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <span class="header-division"><?= $user['division'] ?></span>
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
     
      <ul class="nav navbar-nav ml-auto">
        <strong><?= $this->session->userdata('name') ?></strong>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <?php if(!empty($user['avatar'])) : ?>
                <img class="img-avatar" src="<?= base_url(); ?>assets/avatar/<?= $user['avatar'] ?>" alt="<?= $user['name'] ?>">
            <?php else : ?>
                <img class="img-avatar" src="<?= base_url(); ?>assets/img/avatars/default.png" alt="<?= $user['name'] ?>">
            <?php endif; ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong><?= $this->session->userdata('name') ?></strong>
            </div>
            <a class="dropdown-item" href="<?= base_url().'user/profile/'.$this->session->userdata('userid'); ?>">
              <i class="fa fa-user"></i> Profile</a>
            <a class="dropdown-item" href="<?= base_url(); ?>login/logout">
              <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">