<div class="sidebar <?= $division ?>">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item"> 
              <a class="nav-link <?= ($this->uri->segment(1)=="dashboard")||($this->uri->segment(1)=="") ? 'active' : ''; ?>" id="menu-dashboard" href="<?= base_url(); ?>" style=" <?= ($this->uri->segment(1)=="dashboard")||($this->uri->segment(1)=="") ? 'border-top-right-radius:15px' : ''; ?>"   ><i class="nav-icon icon-pie-chart"></i> <span class="nav-name">Dashboard </span> </a>
            </li>

            <?php if ($access["DASHBOARD_EXECUTIVE"]["R"]) : ?>
              <li class="nav-item"> 
                <a class="nav-link pb-1 <?= $this->uri->segment(1)=="dashboard-executive" ? 'active' : ''; ?>" id="menu-dashboard-executive" href="<?= base_url()."dashboard-executive"; ?>" style=" <?= $this->uri->segment(1)=="dashboard-executive" ? 'border-top-right-radius:15px' : ''; ?>"   ><i class="nav-icon icon-pie-chart"></i> 
                  <span class="nav-name">Dashboard <small><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXECUTIVE</small></span>
                </a>
              </li>
            <?php endif; ?>

            <?php if ($access["PROJECT"]["R"]) : ?>
             <li class="nav-item">
                 <a class="nav-link nav-link-hgn <?= $this->uri->segment(1)=="project" || $this->uri->segment(1)=="project-add" || $this->uri->segment(1)=="project-edit" || $this->uri->segment(1)=="p" || $this->uri->segment(1)=="project-data" ? 'active' : ''; ?>"  href="<?= base_url(); ?>project-data">
                  <i class="nav-icon icon-rocket"></i> Projects
                </a>
              </li>
            <?php endif; ?>

            <?php if ($access["BAST"]["R"]) : ?>
              <li class="nav-item">
                 <a class="nav-link nav-link-hgn <?= $this->uri->segment(1)=="bast" ? 'active' : ''; ?>"  href="<?= base_url(); ?>bast">
                  <i class="nav-icon icon-doc"></i> BAST
                </a>
              </li>
            <?php endif; ?>

            <?php if ($access["MONITORING"]["R"]) : ?>
              <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle <?= $this->uri->segment(1)=="monitoring" ? 'active' : ''; ?>" href="#">
                <i class="nav-icon icon-screen-desktop"></i> Monitoring</a>
                  <ul class="nav-dropdown-items">
                      <li class="nav-item">
                        <a class="nav-link" href="<?= base_url() ?>monitoring/pm">
                          <i class="nav-icon fa fa-circle-o"></i>PROJECT MANAGER</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="<?= base_url() ?>monitoring/issueap">
                          <i class="nav-icon fa fa-circle-o"></i>ISSUE-ACTION PLAN</a>
                      </li>
                      
                      <li class="nav-item">
                        <a class="nav-link" href="<?= base_url() ?>monitoring/bastproject">
                          <i class="nav-icon fa fa-circle-o"></i>BAST PROJECT</a>
                      </li>        
                </ul>
              </li>
            <?php endif; ?>


            <?php if ($access["UTILITY"]["R"]) : ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle <?= $this->uri->segment(1)=="utility" ? 'active' : ''; ?>" href="#">
              <i class="nav-icon icon-screen-desktop"></i> Utility</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>utility/uploader">
                        <i class="nav-icon fa fa-circle-o"></i>Upload Data</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
  
            <?php if ($access["ADMIN"]["R"]) : ?>
            <li class="nav-title">Admin Menu</li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url() ?>user">
                <i class="nav-icon icon-people"></i> Users</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="<?= base_url() ?>settings/role-access">
                <i class="nav-icon icon-lock"></i> Role Access</a>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon fa fa-tags"></i> Master</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>master/pm">
                        <i class="nav-icon icon-user"></i> Project Manager</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>master/am">
                        <i class="nav-icon fa fa-circle-o"></i>Account Manager</a>
                    </li>
                    
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>master/customer">
                        <i class="nav-icon fa fa-circle-o"></i>Customer</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>master/customer-executive/-">
                        <i class="nav-icon fa fa-circle-o"></i>Customer VVIP</a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>master/Partner">
                        <i class="nav-icon fa fa-circle-o"></i>Partner</a>
                    </li>
                    
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url() ?>master/segmen">
                        <i class="nav-icon fa fa-circle-o"></i>Segmen</a>
                    </li>                
              </ul>
            </li>           
            <li class="nav-item">
               <a class="nav-link nav-link-hgn <?= $this->uri->segment(1)=="log" ? 'active' : ''; ?>"  href="<?= base_url(); ?>log">
                <i class="nav-icon fa fa-history"></i> Log
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?= $this->uri->segment(1)=="configuration" ? 'active' : ''; ?>" href="<?= base_url() ?>master/configuration">
                <i class="nav-icon fa fa-wrench"></i>Config</a>
            </li>   
            <?php endif; ?>      
            
            
          </ul>

          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>


      <main class="main">
        <?=$this->session->flashdata('notification')?> 
        <div id="pre-load-background"></div>
        <div id="pre-load-background-mini"></div>