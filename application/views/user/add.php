<div class="row">
	<div class="col-8 offset-2">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 15px;">
						<h4 class="card-title mb-0">Add <small>User</small></h4>
					</div>
				
					<div class="col-sm-12">
              <form id="form_add_user" action="<?= base_url(); ?>user/add_proccess" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>User id</label>
                      <input type="text" name="userid" checkUserId="true" id="userid" class="form-control" placeholder="username" required>
                    </div>

                    <div class="form-group">
                      <label>Name</label>
                      <input required type="text" name="name" id="name" class="form-control" placeholder="full name">
                    </div>
                    
                    <div class="form-group">
                      <label>Role</label>
                      <select required id="role" name="role" class="form-control">
                        <?php foreach ($role as $key => $value) : ?>
                          <?php if ($value['ID'] != 0) : ?>
                            <option value="<?= $value['ID'] ?>"><?= $value['NAME'] ?></option>
                          <?php endif;  ?>
                        <?php endforeach; ?>
                      </select>
                    </div>  
                
                    <div class="form-group">
                      <label>Division</label>
                      <select required class="form-control" name="division" id="division">
                       <?php if($user['division'] == 'EBIS') :?>
                        <option value="EBIS">EBIS</option>
                        <option value="DBS">DBS</option>
                        <option value="DES">DES</option>
                        <option value="DGS">DGS</option>
                      <?php else :  ?>
                        <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
                      <?php endif;  ?>
                      </select>
                    </div>

                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Phone</label>
                      <input required type="number" name="phone" id="phone" class="form-control" placeholder="08xxx xxxx xxxx">
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input required checkUserEmail="true" type="email" name="email" id="email" class="form-control" placeholder="user@domain">
                    </div>
                    
                    <!-- <div class="form-group">
                      <label>Password</label>
                      <input  required type="password" minlength="8" name="password" id="password" class="form-control" placeholder="create password">
                    </div>

                    <div class="form-group">
                      <label>Re-Type Password</label>
                      <input type="password" name="c_password" equalTo="password" id="c_password" class="form-control" placeholder="confirm password" required>
                    </div> -->
                    
                    <div class="form-group">
                      <label>Regional</label>
                      <select id="regional" name="regional" class="form-control">
                        <?php if($user['regional_id'] == '1'):  ?>
                          <?php foreach ($regional as $key => $value) : ?>
                            <option value="<?= $value['ID'] ?>"><?= $value['VALUE'] ?></option>  
                          <?php endforeach ?>
                        <?php else : ?> 
                            <option value="<?= $user['regional_id'] ?>"><?= $user['regional'] ?></option>
                        <?php endif; ?> 
                      </select>
                    </div>

                  </div>

                    <button type="button"  id="btn-save-user" class="col-md-2 offset-md-5 btn btn-success btn-md btn-addon btn-prime">
                        <i class="fa fa-plus"></i><span class="pl-0"> save </span>
                    </button>
                </div>
              </form>

					</div>
				</div>


			</div>
	

		</div>
	</div>
</div>


<script type="text/javascript">    
  var Page = function () {
  
      return {
          init: function() {  
              $(document).on("click","#btn-save-user",(e)=>{
                  if($('#form_add_user').valid()){
                    showLoading();
                    $('#form_add_user').submit()
                  }
              });  
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>