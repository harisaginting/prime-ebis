<div class="row">
	<div class="col-8 offset-2">
		<div class="card border border-light">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 15px;">
						<h4 class="card-title mb-0">Change Password</h4>
					</div>
				
					<div class="col-sm-12">
              <form id="form-change-password" action="<?= base_url(); ?>user/change-password-process" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-12">

                    <div class="form-group">
                      <label>User id</label>
                      <input value="<?= $data['ID'] ?>" type="text" name="userid" id="userid" class="form-control" placeholder="username" required readonly>
                    </div>

                    <div class="form-group">
                      <label>Old Password</label>
                      <input required type="password" name="password" id="password" class="form-control" placeholder="old password" checkPassword="true">
                    </div>

                    <div class="form-group">
                      <label>Password</label>
                      <input  required type="password" name="new_password" id="new_password" class="form-control" placeholder="create new password">
                    </div>

                    <div class="form-group">
                      <label>Re-Type New Password</label>
                      <input type="password" name="c_new_password" equalTo="#new_password" id="c_new_password" class="form-control" placeholder="confirm new password" required>
                    </div>
                    

                  </div>
                  <button type="button" id="btn-save-user" class="col-md-2 offset-md-5 btn btn-success btn-md btn-addon btn-prime">
                      <i class="fa fa-floppy-o"></i><span> SAVE </span>
                  </button>
                </div>
              </form>

					</div>
				</div>


			</div>
	

		</div>
	</div>
</div>


<script type="text/javascript">    
  var Page = function () {
  
      return {
          init: function() {  
              $(document).on("click","#btn-save-user",(e)=>{
                  if($('#form-change-password').validate()){
                    $('#form-change-password').submit();
                  }else{
                    alert('form value invalid');
                  }
              });
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>