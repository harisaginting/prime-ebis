<div class="row">
	<div class="col-8 offset-2">
		<div class="card border border-light">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 15px;">
						<h4 class="card-title mb-0">Edit <small>User</small></h4>
					</div>
				
					<div class="col-sm-12">
              <form id="form_update_user" action="<?= base_url(); ?>user/update_proccess/<?= $data['ID'] ?>" method="post" enctype="multipart/form-data">
                <div class="row mb-2 pb-3 border border-light border-top-0 border-left-0  border-right-0 border-bottom-2">
                  <div class="col-12 text-center">
                    <?php if(empty($data['AVATAR'])) :  ?>
                      <img width="100" src="<?= base_url(); ?>assets/img/avatars/default.png" class="rounded-circle border border-dark border-2" />
                    <?php else : ?>
                      <img width="100" src="<?= base_url(); ?>assets/avatar/<?= $data['AVATAR'] ?>" class="rounded-circle border border-dark border-2" />
                    <?php endif; ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">

                    <div class="form-group">
                      <label>User id</label>
                      <input value="<?= $data['ID'] ?>" type="text" name="userid" id="userid" class="form-control" placeholder="username" required readonly>
                    </div>

                    <div class="form-group">
                      <label>Name</label>
                      <input required type="text" name="name" id="name" class="form-control" placeholder="full name" value="<?= $data['NAME'] ?>">
                    </div>
                     
                    <?php if($user['role_id'] == "1" || $user['role_id'] == "0") : ?>
                    <div class="form-group">
                      <label>Role</label>
                      <select required id="role" name="role" class="form-control">
                        <?php foreach ($role as $key => $value) : ?>
                          <?php if ($value['ID'] != 0) : ?>
                          <option value="<?= $value['ID'] ?>" <?= $data['ROLE'] == $value['ID'] ? 'selected' : '' ?> ><?= $value['NAME'] ?></option>
                          <?php endif;  ?>
                        <?php endforeach; ?>
                        <?php if ($data['ROLE'] == 0) : ?>
                            <option value="<?= $data['ROLE'] ?>" selected><?= $data['ROLE_NAME'] ?></option>
                        <?php  endif; ?>
                      </select>
                    </div>  
                    <?php endif; ?>
                
                    <div class="form-group">
                      <label>Division</label>
                      <select required class="form-control" name="division" id="division">
                       <?php if($data['DIVISION'] == 'EBIS') :?>
                        <option value="EBIS">EBIS</option>
                        <option value="DBS">DBS</option>
                        <option value="DES">DES</option>
                        <option value="DGS">DGS</option>
                      <?php else :  ?>
                        <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
                      <?php endif;  ?>
                      </select>
                    </div>

                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Phone</label>
                      <input required type="number" name="phone" id="phone" class="form-control" placeholder="08xxx xxxx xxxx" value="<?= $data['PHONE'] ?>">
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input required type="email" name="email" id="email" class="form-control" placeholder="user@domain" value="<?= $data['EMAIL'] ?>">
                    </div>
                    
                    <div class="form-group">
                      <label>Regional</label>
                      <select id="regional" name="regional" class="form-control">
                        <?php if($user['regional_id'] == '1'):  ?>
                          <?php foreach ($regional as $key => $value) : ?>
                            <option value="<?= $value['ID'] ?>"><?= $value['VALUE'] ?></option>  
                          <?php endforeach ?>
                        <?php else : ?> 
                            <option value="<?= $user['regional_id'] ?>"><?= $user['regional'] ?></option>
                        <?php endif; ?> 
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Avatar <small style="font-size: 0.7em">(1:1)</small></label>
                      <input id="avatar" name="avatar" type="file" class="form-control fileinput" accept="image/*" >
                    </div>

                  </div>

                  <button type="submit" class="col-md-2 offset-md-5 btn btn-success btn-md btn-addon btn-prime">
                      <i class="fa fa-floppy-o"></i><span> SAVE </span>
                  </button>
                </div>
              </form>

					</div>
				</div>


			</div>
	

		</div>
	</div>
</div>


<script type="text/javascript">    
  var Page = function () {
  
      return {
          init: function() {  

           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>