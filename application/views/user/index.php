<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-5">
						<h4 class="card-title mb-0">List Users</h4>
						<div class="small text-muted">Prime</div>
					</div>

					<div class="col-sm-7" style="margin-bottom: 15px;">
            <a class="btn btn-brand btn-success btn-sm pull-right" href="<?= base_url()?>user/add" style="margin-bottom: 4px">
              <i class="fa fa-plus"></i>
              <span> Add User</span>
            </a>
					</div>
				
					<div class="col-sm-12">
						<table id="dataUser" class="table table-responsive-sm table-bordered table-striped" style="width: 100%;margin-top: 10px;">
			              <thead>
			                <tr>
			                  <th style="min-width: 8% !important">ID</th>
                        <th style="min-width: 48% !important">NAME</th>
			                  <th style="min-width: 12% !important">ROLE</th>
			                  <th style="min-width: 10% !important">DIVISION</th>
                        <th style="min-width: 17% !important">CONTACT</th>
                        <th style="min-width: 17% !important">REGIONAL</th>
                        <th style="min-width: 5% !important"></th>
			                </tr>
			              </thead>
			              <tbody>
			              </tbody>
			          </table>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>


<script type="text/javascript">    
  var Page = function () {
    var tableInit = function(){    

        var table = $('#dataUser').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: { 
                        'url'  :base_url+'datatable/user', 
                        'type' :'POST',
                    },
                    aoColumns: [
                                { mData: 'ID'},
                                { mData: 'NAME'},
                                { mData: 'ROLE'},
                                { mData: 'DIVISION'},
                                {
                                  mRender : function(data, type, obj){   
                                            return obj.EMAIL +"<br>" +obj.PHONE;   
                                    }            
                                },
                                { mData: 'REGIONAL'},
                                 {
                                  mRender : function(data, type, obj){   
                                            return "<div><a href='<?= base_url() ?>user/profile/"+obj.ID+"' class='btn btn-info btn-sm mb-1'><i class='fa fa-eye'></i><span></span></a><a href='<?= base_url() ?>user/edit/"+obj.ID+"' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i><span></span></a></div><div><a class='mt-1 btn btn-danger btn-delete-user btn-sm' data-email='"+obj.EMAIL+"' data-id='"+obj.ID+"'><i class='fa fa-trash'></i></a></div>";   
                                    }            
                                },
                               ],
                               fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                               $(nRow).addClass('row_links');
		                           $(nRow).data('link',base_url+'user/profile/'+aData['ID']); 
		                           return nRow;
                                }            
                });  
    };    
      return {
          init: function() { 
            tableInit();    
              $(document).on("click",".btn-delete-user",(e)=>{
                  e.stopImmediatePropagation();
                  e.preventDefault();
                  let id    = $(e.currentTarget).data('id');
                  let email = $(e.currentTarget).data('email');
                  bootbox.confirm({
                        message: "Delete This User?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if(result){
                              showLoading();
                                $.ajax({
                                        url: base_url+'user/delete_user',
                                        type:'POST',
                                        data:  {id  : id, email : email} ,
                                        async : true,
                                        dataType : "json",
                                        success:function(result){
                                         hideLoading();
                                         if(result.data=='success'){
                                          bootbox.alert("Success!", function(){ 
                                          window.location.reload();
                                          });
                                        }else{
                                          bootbox.alert("Failed!", function(){});
                                          }
                                        return result;
                                        }

                                });
                      }
                        }
                    });
              });
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>