 <div class="row">
	<div class="col-md-3">
		<div class="card bg-white">
			<div class="card-body rounded">
				<?php if(empty($AVATAR)) :  ?>
					<img style="width:100%" src="<?= base_url(); ?>assets/img/avatars/default.png" class="rounded-circle border border-dark border-2" />		
				<?php else : ?>	
					<img style="width:100%" src="<?= base_url(); ?>assets/avatar/<?= $AVATAR ?>" class="rounded-circle border border-dark border-2" />
				<?php endif; ?>	
				
			</div>
		</div>
		<div class="card bg-primary">
			<div class="card-body rounded">
				<small>Division</small>
				<h3><strong><?= $DIVISION ?></strong></h3>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="card rounded border border-primary">
			<div class="card-header rounded-top bg-primary">
				<h4 class="mb-0 text-white pull-left"><?= $NAME; ?></h4>
				
				<a href="<?= base_url().'user/edit/'.$ID?>" class="btn btn-warning btn-sm pull-right ml-2 mr-2">EDIT PROFILE</a>
				<a href="<?= base_url().'user/change-password'?>" class="btn btn-info btn-sm pull-right">CHANGE PASSWORD</a>
			</div>
			<div class="card-body bg-white rounded-bottom pt-2">
				<div class="row">
					<div class="col-6">
						
						<div class="form-group"> 
		                    <label class="mb-0"><small>Role</small></label>
		                    <div class="w-100 project-info">
		                      <strong class="pl-2"><?= $ROLE_NAME ?></strong>
		                    </div>
		                </div>
		                <div class="form-group"> 
		                    <label class="mb-0"><small>Regional</small></label>
		                    <div class="w-100 project-info">
		                      <strong class="pl-2"><?= $REGIONAL_NAME ?></strong>
		                    </div>
		                </div>
						<div class="form-group"> 
		                    <label class="mb-0"><small>Phone</small></label>
		                    <div class="w-100 project-info">
		                      <strong class="pl-2"><?= $PHONE ?></strong>
		                    </div>
		                </div>
						<div class="form-group"> 
		                    <label class="mb-0"><small>Email</small></label>
		                    <div class="w-100 project-info">
		                      <strong class="pl-2"><?= $EMAIL ?></strong>
		                    </div>
		                </div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>	