<div class="row">
	<div class="col-12">
		<div class="card p-3" style="overflow-x: scroll;">
			<table class="table table-bordered" style="max-width: 100%;">
				<thead>
					<tr>
						<th>ID</th>
						<th>NAME</th>
						<th>CUSTOMER</th>
						<th>SEGMEN</th>
						<th>KB</th>
						<th>VALUE</th>
						<th>START</th>
						<th>END</th>
						<th>CATEGORY</th>
						<th>REGIONAL</th>
						<th>AM</th>
						<th>PM</th>
						<th>DESCRIPTION</th>
						<th>STATUS</th>
						<th>PROGRESS</th>
						<th>TOTAL WEIGHT</th>
						<th>TOTAL PLAN</th>
						<th>TOTAL ACHIEVEMENT</th>
						<th>DATE CLOSED</th>
						<th>SCALE</th>
						<th>LESSON LEARN</th>
						<th>CLOSED BY ID</th>
						<th>IS STRATEGIC</th>
						<th>SOURCE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data as $key => $value) : ?>
						<tr>
							<td><?= $value['ID_PROJECT'] ?></td>
							<td><?= $value['NAME'] ?></td>
							<td><?= $value['CUSTOMER'] ?></td>
							<td><?= $value['SEGMEN'] ?></td>
							<td><?= $value['KB'] ?></td>
							<td><?= $value['VALUE'] ?></td>
							<td><?= $value['START_DATE'] ?></td>
							<td><?= $value['END_DATE'] ?></td>
							<td><?= $value['CATEGORY'] ?></td>
							<td><?= $value['REGIONAL'] ?></td>
							<td><?= $value['AM'] ?></td>
							<td><?= $value['PM'] ?></td>
							<td><?= $value['DESCRIPTION'] ?></td>
							<td><?= $value['STATUS'] ?></td>
							<td><?= $value['PROGRESS'] ?></td>
							<td><?= $value['TOTAL_WEIGHT'] ?></td>
							<td><?= $value['TOTAL_PLAN'] ?></td>
							<td><?= $value['TOTAL_ACHIEVEMENT'] ?></td>
							<td><?= $value['DATE_CLOSED'] ?></td>
							<td><?= $value['SCALE'] ?></td>
							<td><?= $value['LESSON_LEARN'] ?></td>
							<td><?= $value['CLOSE_BY_ID'] ?></td>
							<td><?= $value['IS_STRATEGIC'] ?></td>
							<td><?= $value['SOURCE'] ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>