<link href="<?= base_url(); ?>assets/css/datatable.css" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-responsive-sm table-bordered" id="project-partner">
							<thead>
								<th style="width: 70%">NAME</th>
								<th style="width: 20%;">VALUE</th>
								<th></th>
							</thead>
							<tbody>
								<?php foreach ($data as $key => $value): ?>
									<tr class="project" data="<?= $value['ID_PROJECT'] ?>">
										<td>
											<?= $value['NAME'] ?>
										</td>
										<td class="rupiah">
											<?= $value['VALUE'] ?>
										</td>
										<td>
											<a class="btn btn-xs btn-success" href="<?= base_url(); ?>project/show/<?= $value['ID_PROJECT'] ?>" target="_blank">OPEN</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>