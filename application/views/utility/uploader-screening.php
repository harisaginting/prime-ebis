<link href="<?= base_url(); ?>assets/css/datatable.css" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<form id="form-project" method="post" action="<?= base_url() ?>utility/upload-project-process" enctype="multipart/form-data" style="min-width: 100% !important;">
					<div class="col-sm-12">
							<table class="table table-responsive-sm table-bordered">
								<thead>
									<tr>
										<th style="width: 35%;vertical-align: top;" class="border-top-0 border-right-0 border-bottom-0">
											<div class="pl-2">
												DIVISION - REGIONAL<br>
												CATEGORY <br>
												NAME
											</div>
										</th>
										<th style="width: 25%;text-align: left;vertical-align: top;" class="border-top-0 border-bottom-0">
											<div class="pl-2">
												VALUE<br>
												SEGMEN<br>
												CUSTOMER<br> NO. KB
											</div>
										</th>
										<th style="width: 20%;text-align: left;vertical-align: top;" class="border-top-0 border-bottom-0"><div class="pl-2">AM<br>PM</div></th>
										<th style="width: 20%;text-align: center;vertical-align: top;" class="border-top-0 border-bottom-0">PERIOD</th>
									</tr>
								</thead>
								<tbody>
										<?php if (!empty($import_v))  : ?>
					          			<?php foreach ($import_v as $key => $value) : ?>
					          					<tr>
					          						<td>
					          							<div class="row mb-1">
					          								<div class="col-6 mb-1">
					          									<select required class="form-control" name="project-division[]" id="project-division" required>
											                       <?php if($user['division'] == 'EBIS') :?>
											                        <option value="DBS" <?= $value['DIVISION'] == 'DBS' ? 'selected' :'' ?> >DBS</option>
											                        <option value="DES" <?= $value['DIVISION'] == 'DES' ? 'selected' :'' ?>>DES</option>
											                        <option value="DGS" <?= $value['DIVISION'] == 'DGS' ? 'selected' :'' ?>>DGS</option>
											                      <?php else :  ?>
											                        <option value="<?= $user['division'] ?>"><?= $user['division'] ?></option>
											                      <?php endif;  ?>
											                      </select>
					          								</div>
					          								<div class="col-6 mb-1">
					          									<select class="form-control" id="project-regional" name="project-regional[]" required>
																	<?php foreach ($regional as $key1 => $value1): ?>
																		<option value="<?= $value1['ID'] ?>" <?= $value['REGIONAL'] == $value1['ID'] ? 'selected' : '' ?> ><?= $value1['VALUE'] ?></option>	
																	<?php endforeach ?>
																</select>
					          								</div>
					          								<div class="col-12">
					          									<select class="form-control" id="project-category" name="project-category[]" required>
																	<?php foreach ($type_project as $key1 => $value1): ?>
																		<option value="<?= $value1['ID'] ?>" <?= $value['CATEGORY'] == $value1['ID'] ? 'selected' : '' ?> ><?= $value1['VALUE'] ?></option>	
																	<?php endforeach ?>
																</select>
					          								</div>
					          							</div>
					          							<textarea rows="6" id="project-name" name="project-name[]" class="form-control"><?=  $value["NAME"] ?>
					          							</textarea>
					          						</td>
					          						<td>
					          							<div class="form-group"> 
					          								<div class="form-group">
					          									<label class="mb-0"><small>VALUE</small> <span class="text-danger"></span></label>
					          									<input type="project-value" name="project-value[]" class="form-control rupiah" value="<?=  $value["VALUE"] ?>">
					          									</input>
					          								</div>
															<div class="form-group"> 
																<label class="mb-0"><small>SEGMEN</small> <span class="text-danger"></span></label>
																<select class="form-control project-segmen" name="project-segmen[]" id="project-segmen" required>
																	<option value="<?= $value['SEGMEN'] ?>"><?= $value['SEGMEN_NAME'] ?></option>
																</select>
															</div>
						          							<div class="form-group"> 
																<label class="mb-0"><small>CUSTOMER</small> <span class="text-danger"></span></label>
																<select class="form-control project-customer" name="project-customer[]" id="project-customer" required>
																	<option value="<?= $value['CUSTOMER'] ?>"><?= $value['CUSTOMER_NAME'] ?></option>
																</select>
															</div>
															<div class="form-group"> 
																<label class="mb-0"><small>NO. KB</small> <span class="text-danger"></span></label>
																<input type="project-kb" name="project-kb[]" class="form-control" value="<?=  $value["KB"] ?>">
					          							</input>
															</div>
					          						</td>
					          						<td>
					          							<div class="form-group"> 
															<label class="mb-0"><small>ACCOUNT MANAGER</small> <span class="text-danger"></span></label>
															<select class="form-control project-am" name="project-am[]" id="project-am" required>
																<option value="<?= $value['AM'] ?>"><?= $value['AM_NAME'] ?></option>
															</select>
														</div>
														<div class="form-group"> 
															<label class="mb-0"><small>PROJECT MANAGER</small> <span class="text-danger"></span></label>
															<select class="form-control project-pm" name="project-pm[]" id="project-pm" required>
																<option value="<?= $value['PM'] ?>"><?= $value['PM_NAME'] ?></option>
															</select>
														</div>
														<div class="form-group"> 
															<label class="mb-0"><small>PRIORITY</small> <span class="text-danger"></span></label>
															<select class="form-control project-priority" name="project-priority[]" id="project-segmen">
																<option value="0">NON STRATEGIC</option>
																<option value="1" <?= $value['IS_STRATEGIC'] == '1' ? 'selected' :'' ?>>STRATEGIC</option>
															</select>
														</div>
					          						</td>
					          						<td>
					          							<div class="form-group"> 
															<label class="mb-0"><small>START DATE</small> <span class="text-danger"></span></label>
															<input class="form-control date-picker" type="text" name="project-start[]" id="project-start" value="<?= $value['START_DATE'] ?>" readonly required>
														</div>
														<div class="form-group"> 
															<label class="mb-0"><small>END DATE</small> <span class="text-danger"></span></label>
															<input class="form-control date-picker" type="text" name="project-end[]" id="project-end" value="<?= $value['END_DATE'] ?>" readonly required>
														</div>
														<div class="form-group <?= empty($value['DATE_CLOSED']) ? 'hidden' : '' ?>"> 
															<label class="mb-0"><small>DATE CLOSED</small> <span class="text-danger"></span></label>
															<input class="form-control" type="text" name="project-closed[]" id="project-closed" value="<?= $value['DATE_CLOSED'] ?>" readonly>
														</div>
														<div class="form-group <?= empty($value['DATE_CLOSED']) ? 'hidden' : '' ?>"> 
															<label class="mb-0"><small>LESSON LEARN</small> <span class="text-danger"></span></label>
															<textarea rows="4" id="project-lesson" name="project-lesson[]" class="form-control"><?=  $value["LESSON_LEARN"] ?>
					          							</textarea>
														</div>
					          						</td>
					          					</tr>
					          			<?php endforeach; ?>
					          			<?php endif; ?>  
								</tbody>
							</table>        
							<div class="col-md-4 col-sm-12 offset-md-4 text-center">
								<button type="submit" class="btn btn-xs btn-primary w-100">SUBMIT</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">    
 var total_top_customer = 0;
 var total_top_partner = 0;
 var Page = function () {
      return {
          init: function() { 
              $(".project-segmen").select2({
                            placeholder: "Select Segmen",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_segmen",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
              	});

              $(".project-customer").select2({
                            placeholder: "Select Customer",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_customer",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.CODE, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                });

              $(".project-pm").select2({
                            placeholder: "Select Project Manager",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_pm",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.ID, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
              	}); 

              $(".project-am").select2({
                            placeholder: "Select Account Manager",
                            width: 'resolve',
                            allowClear : true,
                            ajax: {
                                type: 'POST',
                                url:base_url+"master/get_am",
                                dataType: "json",
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        page: params.page,
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results: $.map(data, function(obj) {
                                            return { id: obj.EMAIL, text: obj.NAME};
                                        })
                                    };
                                },
                                
                            }
                });      
            
           }
      };

  }();

  jQuery(document).ready(function() { 
      Page.init();
  });       
           
</script>