<link href="<?= base_url(); ?>assets/css/datatable.css" rel="stylesheet">
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-4 col-sm-12 offset-md-4">
						<form id="form-project" method="post" action="<?= base_url() ?>utility/upload-project" enctype="multipart/form-data">
		                    <div class="custom-file mt-3 mb-3">
							  <input type="file" class="custom-file-input" id="data-project" name="data-project" accept=".xlsx">
							  <label class="custom-file-label" for="data-project">Upload File Project</label>
							</div>
							<div class="text-center w-100">
								<button type="submit" class="btn btn-success btn-xs">submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<a class="btn btn-primary btn-xs btn-addon btn-prime" href="<?= base_url() ?>/assets/template/Template Upload Project.xlsx" target="_blank">
                  <i class="fa fa-download"></i>
                  <span class="pr-2">DOWNLOAD TEMPLATE</span>
                </a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(".custom-file-input").on("change", function() {
	  var fileName = $(this).val().split("\\").pop();
	  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
</script>